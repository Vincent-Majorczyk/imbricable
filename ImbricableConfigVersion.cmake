#####################
# ConfigVersion file
##
include(CMakePackageConfigHelpers)

write_basic_package_version_file(
	${CMAKE_CURRENT_BINARY_DIR}/ImbricableConfigVersion.cmake
	VERSION ${PROJECT_VERSION}
	COMPATIBILITY AnyNewerVersion
)

configure_package_config_file(
	${CMAKE_CURRENT_LIST_DIR}/CMake/ImbricableConfig.cmake.in
	${CMAKE_CURRENT_BINARY_DIR}/ImbricableConfig.cmake
	INSTALL_DESTINATION ${INSTALL_CONFIGDIR}
)

## Install all the helper files
install(
	FILES
	  ${CMAKE_CURRENT_BINARY_DIR}/ImbricableConfig.cmake
	  ${CMAKE_CURRENT_BINARY_DIR}/ImbricableConfigVersion.cmake
	DESTINATION ${INSTALL_CONFIGDIR}
)
