# Imbricable {#Imbricable}

## Licence {#Licence}

Copyright Vincent MAJORCZYK 2019

This framework is composed of computer libraries which provides tools for lazy developers. The purpose is to not take care of repetitive aspects of the application creation as producing of forms or registration system.

This framework is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/ or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty  and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.

In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers  and  experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.

The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.

## Introduction {#Introduction}

Imbricable, is a framework which provides tools for lazy developers. The purpose is to not take care of repetitive aspects of the application creation as producing of forms or registration system.

Even if a minimum of work is requiered, the framework aims to limitate development time and let developers working on more interesting stuffs.

The framework is divide in many librairies:
- [Imbricore](@ref Imbricore) which provides base classes and registration mechanism;
- *Imbrigui* which provides graphical user tools;
- *Imbrimath* which provides math tools;

## Getting started {#GettingStarted}
### Dependances
#### CMake
This project requires [CMake](https://en.wikipedia.org/wiki/Integrated_development_environment) to manage the build process of the framework.

*CMake* must be [downloaded and installed](https://cmake.org/download/).

#### Qt toolkit
This framework is based on the [Qt](https://en.wikipedia.org/wiki/Qt_(software)) toolkit.
An easy way to start the project is using the associated IDE, that means *QtCreator*.

*Qt* (and *QtCreator*) must be [installed](https://www.qt.io/download). The non-comercial licence allows to sell software at the condition Qt libraries are integrated as dll.

**notes about QtCreator and CMake:** CMake doesn't accept accentuate characters in buid directory. In french, Qt creates a folder with substring "Défaut" which causes problem during parsing. So be careful to names which are generated automaticaly by QtCreator.

### Documentation
[Doxygen](http://www.doxygen.nl/) is a tool which procuces a documentation of the code. The framework use the [Javadoc style](http://www.doxygen.nl/manual/docblocks.html#docexamples).

$$
e^3
$$

## Licencing
Each file should have an header with an explicit copyright. *QtCreator* may generate automaticaly the header of new files. The license template must be defined under **Tool > Options > C++ > License Template** and is available at *Licence/template_license_h.txt*. There are two other templates for *CMake* files and markdown files, respectively *Licence/template_license_cmake.txt* and *Licence/template_license_md.txt*.

## Bibliography
- [How to create cmake project with package](https://codingnest.com/basic-cmake-part-2/)
- [Another How to create cmake project](http://sirien.metz.supelec.fr/depot/SIR/TutorielCMake/index.html)
- [Another How to create cmake project](https://foonathan.net/blog/2016/03/03/cmake-install.html)

## Copyright
- Copyright: Licence CeCILL-C ([fr](http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html)),([en](http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html))
- Date: 2019
- Author: Vincent Majorczyk
