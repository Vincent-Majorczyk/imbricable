/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include <QDebug>

#include "Core/Registration/Xml/xml.h"
#include "version1.h"
#include "version2.h"

using namespace Imbricable;

namespace Imbrixamples {

/**
 * @brief explore inheritance and composition for complexe object and serialization with versionning
 *
 * # Inheritance concept 
 * - inheritance::Base: this class is the base class for inheritance case;
 * - inheritance::Derived1: this class is derived from inheritance::Base;
 * - inheritance::Derived2: this class is derived from inheritance::Base;
 * - inheritance::Derived1_Xml: allow to serialize an object inheritance::Derived1
 * - inheritance::Derived2_Xml: allow to serialize an object inheritance::Derived1
 *
 * When Base is modified in a futher version of the code, each derivated class must be concerned by a new version;
 * So, the old version of the code requires 5 classes, and the new version requires 10 classes (5 classes created).
 *
 * # Composition concept
 * - composition::Base: this class is the base class for composition case, this contains BaseData;
 * - composition::BaseData: data structure for the classe Base;
 * - composition::BaseData_Xml: allows to serialize an object composition::BaseData;
 * - composition::Derived1: this class is derived from inheritance::Base;
 * - composition::Derived1Data: this class contains specific data of composition::Derived2Data;
 * - composition::Derived1Data_Xml: allows to serialize an object composition::Derived1Data;
 * - composition::Derived1_Xml: allows to serialize an object composition::Derived1;
 * - composition::Derived2: this class is derived from inheritance::Base;
 * - composition::Derived2Data: this class contains specific data of composition::Derived2Data;
 * - composition::Derived2Data_Xml: allows to serialize an object composition::Derived2Data;
 * - composition::Derived2_Xml: allows to serialize an object composition::Derived2;
 *
 * When composition::BaseData is modified in a futher version of the code, only composition::BaseData and composition::BaseData_Xml are impacted;
 * So, the old version of the code requires 11 classes, and the new version requires 13 classes (2 classes created).
 *
 * # Conclusion:
 * - With the inheritance concept, the data designing is easy but the creating of a new version require more work;
 * - with the composition concept, the data designing is more complexe but the creating of a new version is speedest;
 */
namespace N015 {

/**
 * @brief result of the save
 */
struct Content
{
	/// @brief represents the xml which contains information about version classe;
	QString version;
	/// @brief represents the xml which contains information about model;
	QString model;
};

/**
 * @brief create and save a model to string with xml
 * @return
 */
Content save(S_Model model, S_XmlModelSerializerFactoryList factory)
{
	Content out;

	// serialize FooModel
	// - create document
	QDomDocument docW1("Version");
	// - serialize version
	XmlVersionManager v(factory);
	docW1.appendChild(v.serialize(&docW1));
	out.version = docW1.toString();

	// create serializer
	S_XmlModelSerializer serializer = factory->createSerializer("FooModel",nullptr);
	serializer->setName("Main");
	// - create document
	QDomDocument docW2("Model");
	serializer->setDocument(&docW2);
	// - serialize FooModel & display
	serializer->setModel(model);
	docW2.appendChild(serializer->serialize());
	out.model = docW2.toString();

	return out;
}

/**
 * @brief load and display a model from a string with xml
 * @param xmlContent
 */
S_Model load(Content xmlContent, S_XmlModelSerializerFactoryList factory)
{
	// "deserialize" Version
	// - load document
	QDomDocument docR1("Version");
	docR1.setContent(xmlContent.version);
	// - manage version
	XmlVersionManager v(factory);
	auto node = docR1.firstChild();
	v.deserialize(node);

	// create serializer
	S_XmlModelSerializer serializer = factory->createSerializer("FooModel",nullptr);
	serializer->initialize(factory);
	serializer->setName("Main");

	// "deserialize" Model
	// - load document
	QDomDocument docR2("Model");
	docR2.setContent(xmlContent.model);
	// - deserialize
	//node = node.nextSibling();
	serializer->setDocument(&docR2);
	node = docR2.firstChild();
	serializer->deserialize(node);

	return serializer->model();
}

/**
 * @brief serialize model with factory
 *
 * Steps:
 * - create a FooModel and its contents;
 * - create a factory which contains serializers;
 * - serialize the FooModel;
 * - deserialize to FooModel;
 *
 * Xml:
 * ```
 * ```
 *
 * Output:
 * ```
 * ```
 */

int main(int , char *[])
{
	// simulate version 1 of program: create, save and load a model
	// - create and save
	qInfo() << "Version 1: create, save, and load";
	Content outV1 = save(Version1::createModel(), Version1::createFactory());
	qInfo() << "> XML output (Version): " << outV1.version;
	qInfo() << "> XML output (Foo)    : " << outV1.model;

	// - load
	S_Model sm = load(outV1, Version1::createFactory());
	auto hello = std::dynamic_pointer_cast<Version1::FooModel>(sm);
	qInfo() << "> hello->toString() (deserialize):" << hello->toString() << "\n";

	// simulate version 2 of program: create, save and load a model
	// - create and save
	qInfo() << "Version 2: create, save, and load";
	Content outV2 = save(Version2::createModel(), Version2::createFactory());
	qInfo() << "> XML output (Version): " << outV2.version;
	qInfo() << "> XML output (Foo)    : " << outV2.model;

	// load
	sm = load(outV2, Version2::createFactory());
	auto hello2 = std::dynamic_pointer_cast<Version2::FooModel>(sm);
	qInfo() << "> hello->toString() (deserialize):" << hello2->toString() << "\n";

	// simulate version 2 of program:load a model from version 1
	qInfo() << "Version 1 to 2: load";
	sm = load(outV1, Version2::createFactory());
	auto hello3 = std::dynamic_pointer_cast<Version2::FooModel>(sm);
	qInfo() << "> hello->toString() (deserialize):" << hello3->toString();
	Content outV3 = save(sm, Version2::createFactory());
	qInfo() << "> XML output (Version): " << outV3.version;
	qInfo() << "> XML output (Foo)    : " << outV3.model;

	return 1;
}


}} // namespaces

int main(int argc, char **argv)
{
	return Imbrixamples::N015::main(argc, argv);
}

