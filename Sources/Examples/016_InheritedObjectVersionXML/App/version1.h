/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include <QDebug>

#include "Core/Registration/Xml/xml.h"

using namespace Imbricable;

namespace Imbrixamples::N015 {

/**
 * @brief contains the code of the version 1: classes (BarModel & FooModel), serializer and functions (createModel, createFactory)
 */
namespace Version1 {

/**
 * @brief current version of the BarModel (version 1).
 *
 * Here, the model contains one property `number` (qint8);
 *
 * @note this class use the alias `BarModel`: the alias aims to avoid deep modification in the code
 *
 * @see become Version2::BarModel_V1 in the next version of the code
 */
class BarModel_V1: public Model
{
public:
	/// @brief public property 'number'
	qint8 number = 0;

	/// @brief constructor
	BarModel_V1(qint8 value = 0)
	{ this->number = value; }

	/// @brief return content as string
	QString toString()
	{ return QString("BarV1 %1").arg(number); }
};

typedef BarModel_V1 BarModel;
typedef std::shared_ptr<BarModel_V1> S_BarModel;

/**
 * @brief class to serialize BarModel_V1
 *
 * @note this class use the alias `BarSerializer`: the alias aims to avoid deep modification in the code
 */
class BarSerializer_V1: public XmlTypedModelSerializer<BarModel_V1, BarSerializer_V1, 1>
{
protected:
	/// @brief build serializer
	void buildTree(std::shared_ptr<Model> m) override
	{
		auto hw = std::dynamic_pointer_cast<BarModel_V1>(m);
		this->template addNew<XmlProperty_Int8>(this, QString("Number8bits"), &hw->number);
	}

public:
	/// @brief constructor
	BarSerializer_V1(XmlSerializer* parent, QString name):
		XmlTypedModelSerializer<BarModel_V1,BarSerializer_V1,1>("BarModel",parent, name)
	{ }

	/// @brief constructor
	BarSerializer_V1(XmlSerializer* parent):
		XmlTypedModelSerializer<BarModel_V1,BarSerializer_V1,1>("BarModel", parent)
	{ }
};

typedef BarSerializer_V1 BarSerializer;
typedef std::shared_ptr<BarSerializer_V1> S_BarSerializer;

/**
 * @brief current version of the FooModel (version 1).
 *
 * Here, this class contains a S_BarModel_V1 (version 1)
 *
 * @note this class use the alias `FooModel`: the alias aims to avoid deep modification in the code
 */
class FooModel_V1: public Model
{
public:
	/// @brief public property for type 'generic1'
	S_BarModel bar;

	/// @brief return content as string
	QString toString()
	{
		return QString("Foo: <%1>").arg(this->bar->toString());
	}
};

typedef FooModel_V1 FooModel;
typedef std::shared_ptr<FooModel_V1> S_FooModel;

/**
 * @brief version 1 of the FooSerializer
 *
 * @note this class use the alias `FooSerializer`: the alias aims to avoid deep modification in the code
 */
class FooSerializer_V1: public XmlTypedModelSerializer<FooModel_V1, FooSerializer_V1, 1>
{
protected:
	/// @brief build serializer
	void buildTree(std::shared_ptr<Model> m) override
	{
		auto hw = std::dynamic_pointer_cast<FooModel>(m);
		auto b1 = addNew<BarSerializer>(this, "Bar");
		b1->setAccess(&hw->bar);
	}

public:
	/// @brief serializer constructor
	FooSerializer_V1(XmlSerializer* parent):XmlTypedModelSerializer("FooModel", parent) {}
};

typedef FooSerializer_V1 FooSerializer;
typedef std::shared_ptr<FooSerializer_V1> S_FooSerializer;

/**
 * @brief create a model
 * @return
 */
S_Model createModel()
{
	qint8 value = 14;
	auto bar = std::make_shared<BarModel>(value);
	auto foo = std::make_shared<FooModel>();
	foo->bar = bar;
	return foo;
}

/**
 * @brief create a factory
 * @return
 */
S_XmlModelSerializerFactoryList createFactory()
{
	S_XmlModelSerializerFactoryList factory = std::make_shared<XmlModelSerializerFactoryList>();
	factory->append<FooSerializer>();
	factory->append<BarSerializer>();
	return factory;
}



}} // namespace
