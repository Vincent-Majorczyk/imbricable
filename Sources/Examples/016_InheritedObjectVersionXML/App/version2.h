/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include <QDebug>

#include "Core/Registration/Xml/xml.h"

using namespace Imbricable;

namespace Imbrixamples::N015 {

/**
 * @brief contains the code of the version 2: classes (BarModel & FooModel), serializer and functions (createModel, createFactory, save and load)
 */
namespace Version2 {

/**
 * @brief previous version of the BarModel (version 1).
 *
 * Here, the model contains one property `number` (qint8);
 *
 * @see this class is not changed from the previous version of the code.
 *
 * @obsolete obsolete out of the loading of old files
 */
class BarModel_V1: public Model
{
public:
	/// @brief public property 'number'
	qint8 number = 0;

	/// @brief constructor
	BarModel_V1(qint8 value = 0)
	{ this->number = value; }

	/// @brief return content as string
	QString toString()
	{ return QString("BarV1 %1").arg(number); }
};

typedef std::shared_ptr<BarModel_V1> S_BarModel_V1;

/**
 * @brief current version of the BarModel (version 2).
 *
 * Here, the model contains one property `number` (qint16 instead quint8);
 *
 * @note this class use the alias `BarModel`: the alias aims to avoid deep modification in the code
 */
class BarModel_V2: public Model
{
public:
	/// @brief public property 'text'
	qint16 number = 0;

	/// @brief constructor
	BarModel_V2(qint16 value = 0)
	{ this->number = value; }

	/// @brief constructor to adapt previous version to current version
	BarModel_V2(S_BarModel_V1 oldversion)
	{ this->number = oldversion->number; }

	/// @brief return content as string
	QString toString()
	{ return QString("BarV2 %1").arg(number); }
};

typedef BarModel_V2 BarModel;
typedef std::shared_ptr<BarModel_V2> S_BarModel;

/**
 * @brief class to serialize BarModel_V1
 *
 * @note in this version of the code, the method 'upgrade' is added
 *
 * @obsolete obsolete out of the loading of old files
 */
class BarSerializer_V1: public XmlTypedModelSerializer<BarModel_V1, BarSerializer_V1, 1>
{
protected:
	/// @brief build serializer
	void buildTree(std::shared_ptr<Model> m) override
	{
		auto hw = std::dynamic_pointer_cast<BarModel_V1>(m);
		this->template addNew<XmlProperty_Int8>(this, QString("Number8bits"), &hw->number);
	}

public:
	/// @brief serializer constructor
	BarSerializer_V1(XmlSerializer* parent, QString name):
		XmlTypedModelSerializer<BarModel_V1,BarSerializer_V1,1>("BarModel",parent, name)
	{}

	/// @brief constructor
	BarSerializer_V1(XmlSerializer* parent):
		XmlTypedModelSerializer<BarModel_V1,BarSerializer_V1,1>("BarModel", parent)
	{}

	/// @brief upgrade to version 2
	S_Model upgrade() override
	{
		S_BarModel_V1 model0 = std::dynamic_pointer_cast<BarModel_V1>(this->model());
		Version2::S_BarModel model1 = std::make_shared<BarModel_V2>(model0);
		return model1;
	}
};

/**
 * @brief class to serialize BarModel_V2
 *
 * @note this class use the alias `BarSerializer`: the alias aims to avoid deep modification in the code
 */
class BarSerializer_V2: public XmlTypedModelSerializer<BarModel_V2, BarSerializer_V2, 2>
{
protected:
	/// @brief build serializer
	void buildTree(std::shared_ptr<Model> m) override
	{
		auto hw = std::dynamic_pointer_cast<BarModel_V2>(m);
		this->template addNew<XmlProperty_Int16>(this, QString("Number16bits"), &hw->number);
	}

public:
	/// @brief serializer constructor
	BarSerializer_V2(XmlSerializer* parent, QString name):
		XmlTypedModelSerializer<BarModel_V2,BarSerializer_V2,2>("BarModel",parent, name)
	{ }

	/// @brief serializer constructor
	BarSerializer_V2(XmlSerializer* parent):
		XmlTypedModelSerializer<BarModel_V2,BarSerializer_V2,2>("BarModel", parent)
	{ }


	S_XmlModelSerializer previous() override
	{
		return std::make_shared<BarSerializer_V1>(this->parent());
	}
};

typedef BarSerializer_V2 BarSerializer;
typedef std::shared_ptr<BarSerializer_V2> S_BarSerializer;

/**
 * @brief current version of the FooModel (version 1).
 *
 * Here, this class contains a S_BarModel_V2 (version 2)
 *
 * @note code is not changed compared to version 1, but S_BarModel is an alias to a BarModel_V2 instead of a BarModel_V1
 */
class FooModel_V1: public Model
{
public:
	/// @brief public property for type 'generic1'
	S_BarModel bar;

	/// @brief return content as string
	QString toString()
	{
		return QString("Foo: <%1>").arg(this->bar->toString());
	}
};

typedef FooModel_V1 FooModel;
typedef std::shared_ptr<FooModel_V1> S_FooModel;

/**
 * @brief class to serialize HelloWorldModel (version 2)
 *
 * @note code is not changed compared to version 1, but S_BarModel is an alias to a BarModel_V2 instead of a BarModel_V1
 */
class FooSerializer_V1: public XmlTypedModelSerializer<FooModel_V1, FooSerializer_V1, 1>
{
protected:
	/// @brief build serializer
	void buildTree(std::shared_ptr<Model> m) override
	{
		auto hw = std::dynamic_pointer_cast<FooModel>(m);
		auto b1 = addNew<BarSerializer>(this, "Bar");
		b1->setAccess(&hw->bar);
	}

public:
	/// @brief serializer constructor
	FooSerializer_V1(XmlSerializer* parent):XmlTypedModelSerializer("FooModel", parent) {}
};

typedef FooSerializer_V1 FooSerializer;
typedef std::shared_ptr<FooSerializer_V1> S_FooSerializer;

/**
 * @brief create a model
 * @return
 *
 * @note: int8 'value' is replaced by a int16
 */
S_Model createModel()
{
	qint16 value = 16;
	auto bar = std::make_shared<BarModel>(value);
	auto foo = std::make_shared<FooModel>();
	foo->bar = bar;
	return foo;
}

/**
 * @brief create a factory
 * @return
 */
S_XmlModelSerializerFactoryList createFactory()
{
	auto factory = std::make_shared<XmlModelSerializerFactoryList>();
	factory->append<FooSerializer>();
	factory->append<BarSerializer>();
	return factory;
}

}} // namespace


