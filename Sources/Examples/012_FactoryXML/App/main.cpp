/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include <QDebug>

#include "Core/Registration/Xml/xml.h"

using namespace Imbricable;

namespace Imbrixamples {

/**
 * @brief this example aims to test the serialization to xml with factory
 */
namespace N012 {

/**
 * @brief The model contains only data.
 *
 * Same as N001::HelloWorldModel
 *
 * Here, the model contains one property `text`;
 */
class FooModel: public Model
{
public:
	/// @brief public property 'text'
	QString text;
};

/**
 * @brief class to serialize HelloWorldModel
 */
class FooSerializer: public XmlModelSerializer
{
protected:
	void buildTree(std::shared_ptr<Model> m) override
	{
		auto hw = std::dynamic_pointer_cast<FooModel>(m);
		addNew<XmlProperty_QString>(this, "Text", &hw->text);
	}

public:
	/// @brief serializer constructor
	FooSerializer(XmlSerializer* parent):XmlModelSerializer(parent) {}

	/// @brief name of the model
	QString typeName() override { return  "FooModel"; }

	/// @brief version of the model
	uint version() override { return 1; }

	/// @brief create a new model
	S_Model createModel() override { return std::make_shared<FooModel>(); }

	/// @brief create a new serializer
	S_XmlModelSerializer createSerializer(XmlSerializer* parent) override { return std::make_shared<FooSerializer>(parent); }

	/// @brief evaluate if the model is compatible
	bool isType(S_Model model) override
	{
		Model &m = *(model.get());
		return typeid(m) == typeid(FooModel);
	}
};

/**
 * @brief The model contains only data.
 *
 * Here, the model contains one property `number`;
 */
class BarModel: public Model
{
public:
	/// @brief public property 'number'
	int number = 0;
};

/**
 * @brief class to serialize BarModel
 * @note XmlTypedModelSerializer<> is a compliant template to override typeName(), version(), createModel() and isType()
 */
class BarSerializer: public XmlTypedModelSerializer<BarModel,BarSerializer, 1>
{
protected:
	void buildTree(std::shared_ptr<Model> m) override
	{
		auto hw = std::dynamic_pointer_cast<BarModel>(m);
		addNew<XmlProperty_Int32>(this, "Number", &hw->number);
	}

public:
	/// @brief serializer constructor
	BarSerializer(XmlSerializer* parent):
		XmlTypedModelSerializer( "BarModel",parent) {}
};


/**
 * @brief serialize model with factory
 *
 * Steps:
 * - create a FooModel and a BarModel;
 * - create a factory which contains FooSerializer & BarSerializer;
 * - serialize the FooModel and the BarModel;
 * - deserialize to FooModel and BarModel;
 *
 * Output:
 * ```
 * > XML output (Foo):  "<!DOCTYPE FooOrBar1>\n<FooOrBar type=\"FooModel\" version=\"1\">\n <Text>I'm foo</Text>\n</FooOrBar>\n"
 * > XML output (Bar):  "<!DOCTYPE FooOrBar2>\n<FooOrBar type=\"BarModel\" version=\"1\">\n <Number>42</Number>\n</FooOrBar>\n"
 * > model.text (deserialization Foo): "I'm foo"
 * > model.text (deserialization Bar): 42
 * ```
 */
int main(int , char *[])
{
	// initialization
	// - create models
	auto foo = std::make_shared<FooModel>();
	foo->text = "I'm foo";
	auto bar = std::make_shared<BarModel>();
	bar->number = 42;

	// - createFactory
	auto factory = std::make_shared<XmlModelSerializerFactoryList>();
	factory->append<FooSerializer>();
	factory->append<BarSerializer>();

	// - create the serializer
	XmlFactorySerializer serializer(nullptr,"FooOrBar");
	serializer.initialize(factory);

	// serialize FooModel
	// - create document
	QDomDocument docW1("FooOrBar1");
	serializer.setDocument(&docW1);

	// - serialize FooModel & display
	serializer.setModel(foo);
	docW1.appendChild(serializer.serialize());
	QString output1 = docW1.toString();
	qInfo() << "> XML output (Foo): " << output1;

	// serialize BarModel
	// - create document
	QDomDocument docW2("FooOrBar2");
	serializer.setDocument(&docW2);

	// - serialize BarModel & display
	serializer.setModel(bar);
	docW2.appendChild(serializer.serialize());
	QString output2 = docW2.toString();
	qInfo() << "> XML output (Bar): " << output2;

	// "deserialize" FooModel
	// - load document
	QDomDocument docR1("HelloWorldDocument");
	docR1.setContent(output1);
	serializer.setDocument(&docR1);
	serializer.deserialize(docR1.firstChild());

	S_Model sm = serializer.model();
	auto hello2 = std::dynamic_pointer_cast<FooModel>(serializer.model());
	qInfo() << "> hello2->text (deserialize Foo):" << hello2->text;

	// "deserialize" BarModel
	// - load document
	QDomDocument docR2("HelloWorldDocument");
	docR2.setContent(output2);
	serializer.setDocument(&docR2);
	serializer.deserialize(docR2.firstChild());
	auto hello3 = std::dynamic_pointer_cast<BarModel>(serializer.model());
	qInfo() << "> hello3->number (deserialize Bar):" << hello3->number;
	return 1;
}

}} // namespaces

int main(int argc, char *argv[])
{
	return Imbrixamples::N012::main(argc,argv);
}

