#
# @copyright Licence CeCILL-C
# @date 2021
# @author Vincent Majorczyk
# @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
# @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#

file(
		GLOB_RECURSE
		source_files
		*.cpp *.h
)

# message("= CMAKE_SOURCE_DIR =${CMAKE_SOURCE_DIR}")

find_package(Qt5 COMPONENTS Core Xml Widgets REQUIRED)

set(EXECUTABLE_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/bin/${CMAKE_BUILD_TYPE})
include_directories(${CMAKE_SOURCE_DIR}
	"${CMAKE_SOURCE_DIR}/Sources")

# Tell CMake to create the helloworld executable
add_executable(example012 ${source_files})
# Use the Widgets module from Qt 5
target_link_libraries(example012 Imbricore Qt5::Core Qt5::Widgets Qt5::Xml)
