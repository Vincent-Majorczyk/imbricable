#
# @copyright Licence CeCILL-C
# @date 2021
# @author Vincent Majorczyk
# @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
# @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#

message("==> Configure Example 012")
add_subdirectory(App)
