#ifndef EXAMPLECONTROLER102_H
#define EXAMPLECONTROLER102_H

#include "examplemodel.h"
#include "Core/Actions/accessoraction.h"

namespace Imbrixamples::N102 {

class ExampleControler : public Imbricable::Controler
{
	using S_Model = Imbricable::S_Model;
	using S_Controler = Imbricable::S_Controler;

	template<typename C>
	using AccessorAction = Imbricable::AccessorAction<C>;

private:
	inline std::shared_ptr<ExampleModel> convert(Imbricable::S_Model m)
	{ return std::dynamic_pointer_cast<ExampleModel>(m); }

	double getLength(S_Model m) { return convert(m)->length; }
	void setLength(S_Model m, double value) { convert(m)->length = value; }

	double getArea(S_Model m) { return convert(m)->area; }
	void setArea(S_Model m, double value) { convert(m)->area = value; }

	double getVolume(S_Model m) { return convert(m)->volume; }
	void setVolume(S_Model m, double value) { convert(m)->volume = value; }

	double getAngle(S_Model m) { return convert(m)->angle; }
	void setAngle(S_Model m, double value) { convert(m)->angle = value; }

public:
	ExampleControler();

	AccessorAction<double> length {"length", this, &ExampleControler::getLength, &ExampleControler::setLength};
	AccessorAction<double> area {"area", this, &ExampleControler::getArea, &ExampleControler::setArea};
	AccessorAction<double> volume {"volume", this, &ExampleControler::getVolume, &ExampleControler::setVolume};
	AccessorAction<double> angle {"angle", this, &ExampleControler::getAngle, &ExampleControler::setAngle};
};

typedef std::shared_ptr<ExampleControler> S_ExampleControler ;

} // namespace

#endif // EXAMPLECONTROLER_H
