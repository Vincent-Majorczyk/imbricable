#ifndef EXAMPLETREE102_H
#define EXAMPLETREE102_H

#include <Gui/FieldContainers/fieldtreewidget.h>
#include "examplecontroler.h"

namespace Imbrixamples::N102 {

class ExampleTree : public Imbrigui::FieldTreeWidget
{
Q_OBJECT
	S_ExampleControler _controler = std::make_shared<ExampleControler>();


public:
	ExampleTree(Imbricable::Units::S_DimensionList dimensions, QWidget *parent = nullptr);

	Imbricable::S_Controler controler() override;

	bool isCompatible(Imbricable::S_Model model) override;
};

} // namespace

#endif // EXAMPLETREE_H
