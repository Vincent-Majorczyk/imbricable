/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef EXAMPLEMODEL102_H
#define EXAMPLEMODEL102_H

#include "Core/Object/model.h"

namespace Imbrixamples::N102 {

/**
 * @brief a simple example class
 */
class ExampleModel: public Imbricable::Model
{
public:
	double length = 0;				//!> Length property
	double area = 0;				//!> Area property
	double volume = 0;				//!> Volume property
	double angle = 0;				//!> Angle property

	/**
	 * @brief Main constructor
	 */
	ExampleModel();
};

} //namespace

#endif // EXAMPLEMODEL002_H
