#include "exampletree.h"
#include "Gui/Fields/fields.h"

using namespace Imbrixamples::N102;
using namespace Imbrigui;

ExampleTree::ExampleTree(Imbricable::Units::S_DimensionList dimensions, QWidget *parent):
	Imbrigui::FieldTreeWidget("N102ExampleTree", parent)
{
	this->setDimensions(dimensions);

	auto lengthgroup = this->addSubTitle("Length", "it concerns length");
	{
		Field* a, *b;
		this->addField(lengthgroup, "Length", a = new Field_DoubleUnit(&_controler->length, this->dimension("Length")));
		this->addField(lengthgroup, "Length2", b = new Field_DoubleUnit(&_controler->length, this->dimension("Length")));

		QObject::connect(a, &Field::modelChanged, b, &Field::updateField);
		QObject::connect(b, &Field::modelChanged, a, &Field::updateField);

		a->setMultiEditionEnabled(true);
		b->setMultiEditionEnabled(true);
	}

	auto areagroup = this->addSubTitle("Area", "it concerns area");
	{
		Field* a, *b;
		this->addField(areagroup, "Area", a = new Field_DoubleUnit(&_controler->area, this->dimension("Area")));
		this->addField(areagroup, "Area2", b = new Field_DoubleUnit(&_controler->area, this->dimension("Area")));

		QObject::connect(a, &Field::modelChanged, b, &Field::updateField);
		QObject::connect(b, &Field::modelChanged, a, &Field::updateField);

		a->setMultiEditionEnabled(true);
	}

	auto volumegroup = this->addSubTitle("Volume", "it concerns volume");
	{
		Field* a, *b;
		this->addField(volumegroup, "Volume", a = new Field_DoubleUnit(&_controler->volume, this->dimension("Volume")));
		this->addField(volumegroup, "Volume2", b = new Field_DoubleUnit(&_controler->volume, this->dimension("Volume")));

		QObject::connect(a, &Field::modelChanged, b, &Field::updateField);
		QObject::connect(b, &Field::modelChanged, a, &Field::updateField);
	}

	auto anglegroup = this->addSubTitle("Angle", "it concerns angle");
	{
		Field* a, *b;
		this->addField(anglegroup, "Angle", a = new Field_DoubleUnit(&_controler->angle, this->dimension("Angle")));
		this->addField(anglegroup, "Angle2", b = new Field_DoubleUnit(&_controler->angle, this->dimension("Angle")));

		QObject::connect(a, &Field::modelChanged, b, &Field::updateField);
		QObject::connect(b, &Field::modelChanged, a, &Field::updateField);
	}

}

Imbricable::S_Controler ExampleTree::controler()
{
	return _controler;
}

bool ExampleTree::isCompatible(Imbricable::S_Model model)
{
	return std::dynamic_pointer_cast<ExampleModel>(model) != nullptr;
}

