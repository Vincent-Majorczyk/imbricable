/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "Examples/102_FormWithUnits/Lib/exampletree.h"
#include "Examples/102_FormWithUnits/Lib/examplemodel.h"

#include <QApplication>
#include <QMainWindow>
#include <iostream>
#include <QDebug>
#include <QTableWidget>
#include <QJsonDocument>

using namespace Imbrixamples::N102;

namespace Imbrixamples {

/**
 * @brief this example aims to test the user interface about units properties and the saving of the GUI preferences.
 */
namespace N102 {

/**
 * @brief the main function of the example
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char *argv[])
{
	qDebug() << "begin";
	QApplication a(argc, argv);

	QCoreApplication::setOrganizationName("Imbricable");
	QCoreApplication::setApplicationName("Imbrixamples");

	// choice between text format (true) or binary format (false) to save preferences
	//bool textformat = true;
	//if(a.arguments().contains("--binary")) textformat = false;
	//if(a.arguments().contains("-b")) textformat = false;

	auto dimensions = std::make_shared<Imbricable::Units::DimensionList>();
	dimensions->initializeDefault();

	auto model = std::make_shared<ExampleModel>();

	auto *example = new ExampleTree(dimensions);
	example->controler()->setModel(model);
	example->updateFields();
	example->show();

	example->loadSettings("Imbrixample");

	qApp->setStyleSheet("QWidget[checkState=valid]{} QWidget[checkState=warning]{background-color: khaki;} QWidget[checkState=error]{background-color:lightcoral;}  QWidget[checkState=conflict]{background-color:paleturquoise;}");

	a.exec();

	example->saveSettings("Imbrixample");

	qDebug() << "  My length (m):" << model->length;
	qDebug() << "  My angle (rad):" << model->angle;

	qDebug() << "end";
	return 1;
}

}} // namespaces

int main(int argc, char *argv[])
{
	return Imbrixamples::N102::main(argc,argv);
}

