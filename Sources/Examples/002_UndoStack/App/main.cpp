/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include <memory>
#include <iostream>

#include <QDebug>
#include <QUndoStack>

#include "Core/Object/model.h"
#include "Core/Object/Interfaces/cnamedobject.h"
#include "Core/Actions/controler.h"
#include "Core/Actions/accessoraction.h"

using namespace Imbricable;

namespace Imbrixample {

/**
 * @brief this example shows the undostack integration in model-controler
 */
namespace N002 {

/**
 * @brief The model contains only data.
 *
 * Similar to N001::HelloWorldModel, but inherit from Imbricable::CNamedObject.
 *
 * Here Imbricable::CNamedObject gives a name when program display the content of the UndoStack
 */
class HelloWorldModel: public Model, public CNamedObject
{
public:
	/// @brief public property 'text'
	QString text;

	/// @brief model constructor
	/// @param text initialize the property 'text'
	HelloWorldModel(QString text):CNamedObject("HelloWorldObject"), text(text) {}

	/// @brief model destructor
	~HelloWorldModel() override {}
};

/**
 * @brief The controler contains accessors and methods to modify model data.
 *
 * Same as to N001::HelloWorldControler, but the method to check the text is removed
 */
class HelloWorldControler: public Controler
{
private:
	/// @brief convert model to HelloWorldModel
	inline std::shared_ptr<HelloWorldModel> convert(S_Model m)
	{ return std::dynamic_pointer_cast<HelloWorldModel>(m); }

	/// @brief 'get' accessor for HelloWorldModel::text property
	QString getText(S_Model m) { return convert(m)->text; }

	/// @brief 'set' accessor for HelloWorldModel::text property
	/// @param text new value for the property
	void setText(S_Model m, QString text) { convert(m)->text = text; }

public:
	/// @brief controler constructor
	HelloWorldControler():Controler("HelloWorld") {}

	/// @brief to be used as accessor to the HelloWorldModel::text
	AccessorAction<QString> text{ "text", this, &HelloWorldControler::getText, &HelloWorldControler::setText};
};

/**
 * @brief use undostack with simple example
 *
 * Steps:
 * - create a HelloWorldModel initialized with the text "hello world";
 * - create a HelloWorldControler associated to the model with a QUndoStack;
 * - do successive modifications of the value and display the content of the undostack;
 * - revert all modifications
 * - restore three modifications
 * - modify the value and display content of the undostack (item after current index are deleted)
 *
 * Output:
 * ```
 * > initial value "Hello world"
 * > value after successive modifications: "Saluton"
 * > foreach item in undostack:
 *  - undostack.text() "Modify HelloWorldObject::text 'Hello world'->'Salut'"
 *  - undostack.text() "Modify HelloWorldObject::text 'Salut'->'Hallo'"
 *  - undostack.text() "Modify HelloWorldObject::text 'Hallo'->'Cześć'"
 *  - undostack.text() "Modify HelloWorldObject::text 'Cześć'->'Hola'"
 *  - undostack.text() "Modify HelloWorldObject::text 'Hola'->'Ossu'"
 *  - undostack.text() "Modify HelloWorldObject::text 'Ossu'->'Saluton'"
 * > value after revert all successive modifications: "Hello world"
 * > value after restore 3 modifications: "Cześć"
 * > foreach item in undostack:
 *  - undostack.text() "Modify HelloWorldObject::text 'Hello world'->'Salut'"
 *  - undostack.text() "Modify HelloWorldObject::text 'Salut'->'Hallo'"
 *  - undostack.text() "Modify HelloWorldObject::text 'Hallo'->'Cześć'"
 *  - undostack.text() "Modify HelloWorldObject::text 'Cześć'->'Hǎo'"
 * ```
 */
void main()
{
	// create the HelloWorldModel with the text "hello world";
	auto hello = std::make_shared<HelloWorldModel>( "Hello world" );

	// create the Undostack
	QUndoStack undostack;

	// create the HelloWorldControler
	HelloWorldControler controler;
	controler.setModel(hello);
	controler.setUndoStack(&undostack);

	bool conflict;
	qInfo() << "> initial value" << controler.text.get(conflict);

	// do successive modifications
	controler.text.set("Salut");
	controler.text.set("Hallo");
	controler.text.set("Cześć");
	controler.text.set("Hola");
	controler.text.set("Ossu");
	controler.text.set("Saluton");

	qInfo() << "> value after successive modifications:" << controler.text.get(conflict);
	qInfo() << "> foreach item in undostack:";
	for(int i=0;i<undostack.count();i++)
		qInfo() << " - undostack.text()" << undostack.text(i);

	// revert all successive modification
	undostack.setIndex(0);
	qInfo() << "> value after revert all successive modifications:" << controler.text.get(conflict);

	undostack.setIndex(3);
	qInfo() << "> value after restore 3 modifications:" << controler.text.get(conflict);

	// do a new modifications
	controler.text.set("Hǎo");

	qInfo() << "> foreach item in undostack:";
	for(int i=0;i<undostack.count();i++)
		qInfo() << " - undostack.text()" << undostack.text(i);
}

}} // namespaces

int main(int, char *[])
{
	Imbrixample::N002::main();
}
