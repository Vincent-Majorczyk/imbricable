/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include <QDebug>

#include "Core/Registration/Xml/xml.h"

using namespace Imbricable;

namespace Imbrixamples {

/**
 * @brief this example aims to test the serialization to xml with factory
 */
namespace N013 {

/**
 * @brief Abstract model which contains only data.
 *
 * Here, the model contains one property `number` and a abstract methode to differenciate inherited classes;
 */
class BarModel: public Model
{
public:
	/// @brief public property 'text'
	qint64 number = 0;

	virtual QString toString() = 0;
};

/**
 * @brief simple inherited class with the overrided method 'toString'
 */
class Bar1Model: public BarModel
{
public:
	Bar1Model(qint64 value = 0){ this->number = value; }

	QString toString() override
	{
		return QString("Bar1 %1").arg(number);
	}
};

/**
 * @brief simple inherited class with the overrided method 'toString'
 */
class Bar2Model: public BarModel
{
public:
	Bar2Model(qint64 value = 0){ this->number = value; }

	QString toString() override
	{
		return QString("Bar2 %1").arg(number);
	}
};

/**
 * @brief class to serialize HelloWorldModel
 */
template<class TClass, int TBarNumber>
class BarSerializer: public XmlTypedModelSerializer<TClass,BarSerializer<TClass,TBarNumber>,1>
{
protected:
	void buildTree(std::shared_ptr<Model> m) override
	{
		auto hw = std::dynamic_pointer_cast<BarModel>(m);
		this->template addNew<XmlProperty_Int64>(this, QString("Number%1").arg(TBarNumber), &hw->number);
	}

public:
	/// @brief serializer constructor
	BarSerializer(XmlSerializer* parent):
		XmlTypedModelSerializer<TClass,BarSerializer<TClass,TBarNumber>,1>(QString("BarModel%1").arg(TBarNumber),parent) {}
};

typedef BarSerializer<Bar1Model,1> Bar1Serializer;
typedef BarSerializer<Bar2Model,2> Bar2Serializer;
typedef std::shared_ptr<Bar1Model> S_Bar1Model;
typedef std::shared_ptr<Bar2Model> S_Bar2Model;
typedef std::shared_ptr<BarModel> S_BarModel;

/**
 * @brief The model contains only data.
 *
 * Same as N001::HelloWorldModel
 *
 * Here, the model contains properties to subobjects;
 */
class FooModel: public Model
{
public:
	/// @brief public property for type 'Bar1Model'
	S_Bar1Model bar1 = nullptr;
	/// @brief public property for type 'Bar1Model'
	S_Bar2Model bar2 = nullptr;
	/// @brief public property for type generic model ('Bar1Model' or 'Bar2Model' or 'FooModel')
	S_Model generic1 = nullptr;
	/// @brief public property for type generic model ('Bar1Model' or 'Bar2Model' or 'FooModel')
	S_Model generic2 = nullptr;
	/// @brief public property for type 'Bar1Model' or 'Bar2Model'
	S_BarModel interface1 = nullptr;

	QString barToString(std::shared_ptr<BarModel> m)
	{
		return m? m->toString() : "null";
	}

	QString genToString(std::shared_ptr<Model> m)
	{
		if(!m) return "null";
		auto bar = std::dynamic_pointer_cast<BarModel>(m);
		if(bar) return barToString(bar);
		auto foo = std::dynamic_pointer_cast<FooModel>(m);
		if(foo) return foo->toString();
		return "invalid";
	}


	QString toString()
	{
		return QString("Foo: <%1> <%2> <%3> <%4> <%5>")
				.arg(barToString(this->bar1),
					 barToString(this->bar2),
					 genToString(this->generic1),
					 genToString(this->generic2),
					 barToString(this->interface1));
	}
};

/**
 * @brief class to serialize HelloWorldModel
 */
class FooSerializer: public XmlTypedModelSerializer<FooModel,FooSerializer,1>
{
protected:
	void buildTree(std::shared_ptr<Model> m) override
	{
		auto hw = std::dynamic_pointer_cast<FooModel>(m);
		// FooModel::bar1
		auto b1 = addNew<Bar1Serializer>(this);
		b1->setName("Bar1");
		b1->setAccess(&hw->bar1);
		// FooModel::bar2
		auto b2 = addNew<Bar2Serializer>(this);
		b2->setName("Bar2");
		b2->setAccess(&hw->bar2);
		// FooModel::generic1
		auto g1 = addNew<XmlFactorySerializer>(this, "Generic1");
		g1->setAccess(&hw->generic1);
		// FooModel::generic2
		auto g2 = addNew<XmlFactorySerializer>(this, "Generic2");
		g2->setAccess(&hw->generic2);
		// FooModel::interface1
		auto i1 = addNew<XmlFactorySerializer_Typed<BarModel>>(this, "Interface");
		i1->setAccess(&hw->interface1);
	}

public:
	/// @brief serializer constructor
	FooSerializer(XmlSerializer* parent):XmlTypedModelSerializer("FooModel", parent) {}
};

typedef std::shared_ptr<FooModel> S_FooModel;

/**
 * @brief serialize model with factory
 *
 * Steps:
 * - create a FooModel and its contents;
 * - create a factory which contains serializers;
 * - serialize the FooModel;
 * - deserialize to FooModel;
 *
 * Xml:
 * ```
 * <!DOCTYPE SubobjectExample>
 * <Main version=\"1\">
 *  <Bar1 version=\"1\">
 *   <Number1>1024</Number1>
 *  </Bar1>
 *  <Bar2 version=\"1\">
 *   <Number2>144</Number2>
 *  </Bar2>
 *  <Generic1 version=\"1\" type=\"BarModel1\">
 *   <Number1>42</Number1>
 *  </Generic1>
 *  <Generic2 version=\"1\" type=\"FooModel\">
 *   <Bar1 type=\"null\"/>
 *   <Bar2 version=\"1\">
 *    <Number2>56</Number2>
 *   </Bar2>
 *   <Generic1 type=\"null\"/>
 *   <Generic2 type=\"null\"/>
 *   <Interface version=\"1\" type=\"BarModel1\">
 *    <Number1>666</Number1>
 *   </Interface>
 *  </Generic2>
 *  <Interface version=\"1\" type=\"BarModel1\">
 *   <Number1>912</Number1>
 *  </Interface>
 * </Main>
 * ```
 *
 * Output:
 * ```
 * > XML output (Foo):  "<!DOCTYPE SubobjectExample>\n ..."
 * > hello->toString() (deserialize): "Foo: <Bar1 1024> <Bar2 144> <Bar1 42> <Foo: <null> <Bar2 56> <null> <null> <Bar1 666>> <Bar1 912>"
 * ```
 */
int main(int , char *[])
{
	// initialization
	// - create models
	auto foo = std::make_shared<FooModel>();
	auto b1 = std::make_shared<Bar1Model>(1024);
	auto b2 = std::make_shared<Bar2Model>(144);
	auto g1 = std::make_shared<Bar1Model>(42);
	auto g2 = std::make_shared<FooModel>();
	auto g2_b2 = std::make_shared<Bar2Model>(56);
	auto g2_i1 = std::make_shared<Bar1Model>(666);
	auto i1 = std::make_shared<Bar1Model>(912);


	foo->bar1 = b1; // Bar1 1024
	foo->bar2 = b2; // Bar2 144

	foo->generic1 = g1; // Bar1 42
	foo->generic2 = g2; // Foo
	g2->bar2 = g2_b2; // Bar2 56
	g2->interface1 = g2_i1; // Bar1 666

	foo->interface1 = i1; // Bar1 912

	// - createFactory
	auto factory = std::make_shared<XmlModelSerializerFactoryList>();
	factory->append<FooSerializer>();
	factory->append<Bar1Serializer>();
	factory->append<Bar2Serializer>();

	// - create the serializer
	auto serializer = std::make_shared<FooSerializer>(nullptr);
	serializer->initialize(factory);
	serializer->setName("Main");


	// serialize FooModel
	// - create document
	QDomDocument docW1("SubobjectExample");
	serializer->setDocument(&docW1);

	// - serialize FooModel & display
	serializer->setModel(foo);
	docW1.appendChild(serializer->serialize());
	QString output = docW1.toString();
	qInfo() << "> XML output (Foo): " << output;

	// "deserialize" FooModel
	// - load document
	QDomDocument docR1("SubobjectExample");
	docR1.setContent(output);

	serializer->setDocument(&docR1);
	serializer->deserialize(docR1.firstChild());

	S_Model sm = serializer->model();
	auto hello = std::dynamic_pointer_cast<FooModel>(serializer->model());
	qInfo() << "> hello->toString() (deserialize):" << hello->toString() << "\n";
	return 1;
}

}} // namespaces

int main(int argc, char *argv[])
{
	return Imbrixamples::N013::main(argc,argv);
}

