/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include <memory>
#include <iostream>

#include <QDebug>
#include <QUndoStack>

#include "Core/Object/model.h"
#include "Core/Object/Interfaces/cnamedobject.h"
#include "Core/Actions/controler.h"
#include "Core/Actions/accessoraction.h"

using namespace Imbricable;

/**
 * @brief examples
 */
namespace Imbrixample {

/**
 * @brief this example shows the model-controler patern
 */
namespace N001 {

/**
 * @brief The model contains only data.
 *
 * This contains public properties and constructor/destructor.
 * The methods aren't recommended because they must be declared in the controler.
 *
 * Here, the model contains one property `text`;
 */
class HelloWorldModel: public Model
{
public:
	/// @brief public property 'text'
	QString text;

	/// @brief model constructor
	/// @param text initialize the property 'text'
	HelloWorldModel(QString text): text(text) {}

	/// @brief model destructor
	~HelloWorldModel() override {}
};

/**
 * @brief The controler contains accessors and methods to modify model data.
 *
 * This contains private model, private methods, but public access to Imbricable::AccessorAction.
 *
 * Here, the controler contains `get` and `set` private accessors methods (for HelloWorldModel::text property),
 * `check` private method to verify if a String is considered compatible
 */
class HelloWorldControler: public Controler
{
private:
	/// @brief convert model to HelloWorldModel
	inline std::shared_ptr<HelloWorldModel> convert(S_Model m)
	{ return std::dynamic_pointer_cast<HelloWorldModel>(m); }

	/// @brief `get` assessor for HelloWorldModel::text property
	/// @param m model
	QString getText(S_Model m) { return convert(m)->text; }

	/// @brief `set` assessor for HelloWorldModel::text property
	/// @param m model
	/// @param text new value for the property
	void setText(S_Model m, QString text) { convert(m)->text = text; }

	/// @brief check if the candidate is valid to be setted to HelloWorldModel::text
	/// @param m model
	/// @param candidate
	CheckState checkText(S_Model m, QString candidate)
	{
		if(candidate.length() <= 11) return CheckState::Valid;
		else return CheckState::Error;

		Q_UNUSED(m)
	}

public:
	/// @brief controler constructor
	HelloWorldControler():Controler("HelloWorld") {}

	/// @brief to be used as accessor to the HelloWorldModel::text
	///
	/// this contains parameters to 'get', 'set' accessor and 'check' method
	AccessorAction<QString> text{ "text", this, &HelloWorldControler::getText, &HelloWorldControler::setText, &HelloWorldControler::checkText };
};

/**
 * @brief use the model-controler patern with simple example
 *
 * Steps:
 * - create a HelloWorldModel initialized with the text "hello world";
 * - create a HelloWorldControler associated to the model;
 * - verify if the candidate values are valid;
 * - modify the value of the model due to the controler;
 *
 * Output:
 * ```
 * > hello.text "hello world"
 * > controler.text.get() "hello world"
 * > candidate1 false
 * > candidate2 true
 * > hello.text "greetings"
 * > controler.text.get() "greetings"
 * ```
 */
void main1()
{
	// create the HelloWorldModel with the text "hello world";
	auto hello = std::make_shared<HelloWorldModel>( "hello world" );
	// create the HelloWorldControler
	HelloWorldControler controler;
	controler.setModel(hello);
	bool conflict;
	qInfo() << "> initial value";
	qInfo() << " - hello.text" << hello->text; // => "hello world"
	qInfo() << " - controler.text.get()" << controler.text.get(conflict); // => "hello world"

	// test candidates
	QString candidate1 = "goodbye world";
	QString candidate2 = "greetings";

	qInfo() << "> candidate1" << (controler.text.check(candidate1)==CheckState::Valid); // => false
	qInfo() << "> candidate2" << (controler.text.check(candidate2)==CheckState::Valid); // => true

	// set candidate
	controler.text.set(candidate2);
	qInfo() << "> final value";
	qInfo() << " - hello.text" << hello->text; // => "greetings"
	qInfo() << " - controler.text.get()" << controler.text.get(conflict); // => "greetings"
}

}} // namespaces

int main(int, char *[])
{
	Imbrixample::N001::main1();
}
