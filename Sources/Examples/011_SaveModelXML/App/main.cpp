/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include <QDebug>

#include "Core/Registration/Xml/xml.h"

using namespace Imbricable;

namespace Imbrixamples {

/**
 * @brief this example aims to test the serialization to xml
 */
namespace N011 {

/**
 * @brief The model contains only data.
 *
 * Same as N001::HelloWorldModel
 *
 * Here, the model contains one property `text`;
 */
class HelloWorldModel: public Model
{
public:
	/// @brief public property 'text'
	QString text;

	/// @brief model constructor
	/// @param text initialize the property 'text'
	HelloWorldModel(QString text): text(text) {}

	HelloWorldModel(): text("empty") {}
};

/**
 * @brief class to serialize HelloWorldModel
 */
class HelloWorldSerializer: public XmlModelSerializer
{
protected:
	void buildTree(std::shared_ptr<Model> m) override
	{
		auto hw = std::dynamic_pointer_cast<HelloWorldModel>(m);
		addNew<XmlProperty_QString>(this, "Text", &hw->text);
	}

public:
	/// @brief serializer constructor
	HelloWorldSerializer(XmlSerializer *parent = nullptr):XmlModelSerializer(parent, "HelloWorld") {}

	/// @brief name of the model
	/// @note not used here (factory usage)
	QString typeName() override { throw std::logic_error("not implemented: typeName"); }

	/// @brief version of the model
	/// @note serialization usage
	uint version() override { return 1; }

	/// @brief create a new model
	/// @note deserialization usage
	S_Model createModel() override { return std::make_shared<HelloWorldModel>(); }

	/// @brief evaluate if the model is compatible
	/// @note not used here (factory deserialization usage)
	bool isType(S_Model) override { throw std::logic_error("not implemented: isType"); }

	/// @brief create a new serializer
	/// @note not used here (factory usage)
	S_XmlModelSerializer createSerializer(XmlSerializer*) override { throw std::logic_error("not implemented: createSerializer"); }
};

/**
 * @brief serialize model with simple example
 *
 * Steps:
 * - create a HelloWorldModel initialized with the text "hello world";
 * - create a HelloWorldSerializer associated to the model;
 * - serialize the HelloWorldModel;
 * - deserialize the XML to HelloWorldModel;
 *
 * Output:
 * ```
 * > XML output:  "<!DOCTYPE HelloWorldDocument>\n<HelloWorld version=\"1\">\n <Text>hello world</Text>\n</HelloWorld>\n"
 * > model.text (after deserialization): "hello world"
 * ```
 */
int main(int, char *[])
{
	// "serialize" operation
	// - create model
	auto hello = std::make_shared<HelloWorldModel>( "hello world" );

	// - create document
	QDomDocument docW("HelloWorldDocument");

	// - create the HelloWorldSerializer
	HelloWorldSerializer serializerW;
	serializerW.setDocument(&docW);
	serializerW.setModel(hello);

	// - serialize & display
	docW.appendChild(serializerW.serialize());
	QString output = docW.toString();
	qInfo() << "> XML output: " << output;

	// "deserialize" operation
	// - load document
	QDomDocument docR("HelloWorldDocument");
	docR.setContent(output);
	qInfo() << "> XML input: " << docR.toString();

	// - create the HelloWorldSerializer
	HelloWorldSerializer serializerR;
	serializerR.setDocument(&docR);

	// - deserialize & display
	serializerR.deserialize(docR.firstChild());
	auto hello2 = std::dynamic_pointer_cast<HelloWorldModel>(serializerR.model());
	qInfo() << "> model.text (deserialization): " << hello2->text;
	return 1;
}

}} // namespaces

int main(int argc, char *argv[])
{
	return Imbrixamples::N011::main(argc,argv);
}

