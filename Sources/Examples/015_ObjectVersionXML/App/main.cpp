/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include <QDebug>

#include "Core/Registration/Xml/xml.h"
#include "version1.h"
#include "version2.h"

using namespace Imbricable;

namespace Imbrixamples {

/**
 * @brief this example aims to test the serialization to xml with version management.
 * This contains two namespaces Version1 and Version2 which correspond to different version of the same code.
 *
 * There are two model classes:
 * - FooModel: this class uses the same code for the two versions: the contained classe is different. Nevertheless, the class of the version 2 is not considered as a new version of the class
 *   - Version1::FooModel contains a Version1::BarModel
 *   - Version2::FooModel contains a Version2::BarModel
 * - BarModel: this class has a property 'number'
 *   - Version1::BarModel the number is coded in 8 bits;
 *   - Version2::BarModel the number is coded in 16 bits;
 */
namespace N015 {

/**
 * @brief result of the save
 */
struct Content
{
	/// @brief represents the xml which contains information about version classe;
	QString version;
	/// @brief represents the xml which contains information about model;
	QString model;
};

/**
 * @brief create and save a model to string with xml
 * @return
 */
Content save(S_Model model, S_XmlModelSerializerFactoryList factory)
{
	Content out;

	// serialize FooModel
	// - create document
	QDomDocument docW1("Version");
	// - serialize version
	XmlVersionManager v(factory);
	docW1.appendChild(v.serialize(&docW1));
	out.version = docW1.toString();

	// create serializer
	S_XmlModelSerializer serializer = factory->createSerializer("FooModel",nullptr);
	serializer->setName("Main");
	// - create document
	QDomDocument docW2("Model");
	serializer->setDocument(&docW2);
	// - serialize FooModel & display
	serializer->setModel(model);
	docW2.appendChild(serializer->serialize());
	out.model = docW2.toString();

	return out;
}

/**
 * @brief load and display a model from a string with xml
 * @param xmlContent
 */
S_Model load(Content xmlContent, S_XmlModelSerializerFactoryList factory)
{
	// "deserialize" Version
	// - load document
	QDomDocument docR1("Version");
	docR1.setContent(xmlContent.version);
	// - manage version
	XmlVersionManager v(factory);
	auto node = docR1.firstChild();
	v.deserialize(node);

	// create serializer
	S_XmlModelSerializer serializer = factory->createSerializer("FooModel",nullptr);
	serializer->initialize(factory);
	serializer->setName("Main");

	// "deserialize" Model
	// - load document
	QDomDocument docR2("Model");
	docR2.setContent(xmlContent.model);
	// - deserialize
	//node = node.nextSibling();
	serializer->setDocument(&docR2);
	node = docR2.firstChild();
	serializer->deserialize(node);

	return serializer->model();
}

/**
 * @brief serialize model with factory
 *
 * Steps:
 * - create a FooModel and its contents;
 * - create a factory which contains serializers;
 * - serialize the FooModel;
 * - deserialize to FooModel;
 *
 * Xml:
 * ```
 * ```
 *
 * Output:
 * ```
 * ```
 */

int main(int , char *[])
{
	// simulate version 1 of program: create, save and load a model
	// - create and save
	qInfo() << "Version 1: create, save, and load";
	Content outV1 = save(Version1::createModel(), Version1::createFactory());
	qInfo() << "> XML output (Version): " << outV1.version;
	qInfo() << "> XML output (Foo)    : " << outV1.model;

	// - load
	S_Model sm = load(outV1, Version1::createFactory());
	auto hello = std::dynamic_pointer_cast<Version1::FooModel>(sm);
	qInfo() << "> hello->toString() (deserialize):" << hello->toString() << "\n";

	// simulate version 2 of program: create, save and load a model
	// - create and save
	qInfo() << "Version 2: create, save, and load";
	Content outV2 = save(Version2::createModel(), Version2::createFactory());
	qInfo() << "> XML output (Version): " << outV2.version;
	qInfo() << "> XML output (Foo)    : " << outV2.model;

	// load
	sm = load(outV2, Version2::createFactory());
	auto hello2 = std::dynamic_pointer_cast<Version2::FooModel>(sm);
	qInfo() << "> hello->toString() (deserialize):" << hello2->toString() << "\n";

	// simulate version 2 of program:load a model from version 1
	qInfo() << "Version 1 to 2: load";
	sm = load(outV1, Version2::createFactory());
	auto hello3 = std::dynamic_pointer_cast<Version2::FooModel>(sm);
	qInfo() << "> hello->toString() (deserialize):" << hello3->toString();
	Content outV3 = save(sm, Version2::createFactory());
	qInfo() << "> XML output (Version): " << outV3.version;
	qInfo() << "> XML output (Foo)    : " << outV3.model;

	return 1;
}


}} // namespaces

int main(int argc, char **argv)
{
	return Imbrixamples::N015::main(argc, argv);
}

