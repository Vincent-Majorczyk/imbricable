/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include <QDebug>

#include "Core/Registration/Xml/xml.h"

using namespace Imbricable;

namespace Imbrixamples {

/**
 * @brief this example aims to test the serialization to xml with factory
 */
namespace N014 {

/**
 * @brief Abstract model which contains only data.
 *
 * Here, the model contains one property `number` and a abstract methode to differenciate inherited classes;
 */
class BarModel: public Model
{
public:
	/// @brief public property 'text'
	qint64 number = 0;

	virtual QString toString() = 0;
};

/**
 * @brief simple inherited class with the overrided method 'toString'
 */
class Bar1Model: public BarModel
{
public:
	Bar1Model(qint64 value = 0){ this->number = value; }

	QString toString() override
	{
		return QString("Bar1 %1").arg(number);
	}
};

/**
 * @brief simple inherited class with the overrided method 'toString'
 */
class Bar2Model: public BarModel
{
public:
	Bar2Model(qint64 value = 0){ this->number = value; }

	QString toString() override
	{
		return QString("Bar2 %1").arg(number);
	}
};

/**
 * @brief class to serialize HelloWorldModel
 */
template<class TClass, int TBarNumber>
class BarSerializer: public XmlTypedModelSerializer<TClass,BarSerializer<TClass,TBarNumber>,1>
{
protected:
	void buildTree(std::shared_ptr<Model> m) override
	{
		auto hw = std::dynamic_pointer_cast<BarModel>(m);
		this->template addNew<XmlProperty_Int64>(this, QString("Number%1").arg(TBarNumber), &hw->number);
	}

public:
	/// @brief serializer constructor
	BarSerializer(XmlSerializer* parent):
		XmlTypedModelSerializer<TClass,BarSerializer<TClass,TBarNumber>,1>(QString("BarModel%1").arg(TBarNumber),parent) {}
};

typedef BarSerializer<Bar1Model,1> Bar1Serializer;
typedef BarSerializer<Bar2Model,2> Bar2Serializer;
typedef std::shared_ptr<Bar1Model> S_Bar1Model;
typedef std::shared_ptr<Bar2Model> S_Bar2Model;
typedef std::shared_ptr<BarModel> S_BarModel;

/**
 * @brief The model contains only data.
 *
 * Same as N001::HelloWorldModel
 *
 * Here, the model contains lists of properties;
 */
class FooModel: public Model
{
public:
	/// @brief public property (list) for type 'Bar1Model'
	QList<S_Bar1Model> bar1;
	/// @brief public property (list) for type generic model ('Bar1Model' or 'Bar2Model' or 'FooModel')
	QList<S_Model> generic1;
	/// @brief public property (list) for type 'Bar1Model' or 'Bar2Model'
	QList<S_BarModel> interface1;

	QString barToString(S_BarModel m)
	{
		return m? m->toString() : "null";
	}

	QString genToString(S_Model m)
	{
		if(!m) return "null";
		auto bar = std::dynamic_pointer_cast<BarModel>(m);
		if(bar) return barToString(bar);
		auto foo = std::dynamic_pointer_cast<FooModel>(m);
		if(foo) return foo->toString();
		return "invalid";
	}

	template<class TList>
	QString listToString(QString name, TList& list)
	{
		QString slist = name + "[";
		for(auto& item : list)
		{
			slist += "<" + genToString(item) + "> ";
		}
		return  slist + "]";
	}


	QString toString()
	{
		return QString("Foo: <%1> <%2> <%3>")
				.arg(listToString("bar1", this->bar1),
					 listToString("gen1", this->generic1),
					 listToString("int1", this->interface1));
	}
};

/**
 * @brief class to serialize HelloWorldModel
 */
class FooSerializer: public XmlTypedModelSerializer<FooModel,FooSerializer,1>
{
protected:
	void buildTree(S_Model m) override
	{
		auto hw = std::dynamic_pointer_cast<FooModel>(m);
		// FooModel::bar1
		auto b1 = addNew<XmlModelSerializer_List<Bar1Serializer, QList, Bar1Model>>(this, "Bar1List");
		b1->setAccess(&hw->bar1);
		// FooModel::generic1
		auto g1 = addNew<XmlFactorySerializer_List<QList>>(this, "Generic1List");
		g1->setAccess(&hw->generic1);
		// FooModel::interface1
		auto i1 = addNew<XmlFactorySerializer_List<QList, BarModel>>(this, "Interface");
		i1->setAccess(&hw->interface1);
	}

public:
	/// @brief serializer constructor
	FooSerializer(XmlSerializer* parent):XmlTypedModelSerializer("FooModel", parent) {}
};

typedef std::shared_ptr<FooModel> S_FooModel;

/**
 * @brief serialize model with factory
 *
 * Steps:
 * - create a FooModel and its contents;
 * - create a factory which contains serializers;
 * - serialize the FooModel;
 * - deserialize to FooModel;
 *
 * Xml:
 * ```
 * <!DOCTYPE ListModelExample>
 * <Main version=\"1\">
 *  <Bar1List>
 *   <Item index=\"0\" version=\"1\">
 *    <Number1>1024</Number1>
 *   </Item>
 *   <Item index=\"1\" version=\"1\">
 *    <Number1>144</Number1>
 *   </Item>
 *  </Bar1List>
 *  <Generic1List>
 *   <Item index=\"0\" version=\"1\" type=\"BarModel2\">
 *    <Number2>42</Number2>
 *   </Item>
 *   <Item index=\"1\" version=\"1\" type=\"FooModel\">
 *    <Bar1List>
 *     <Item index=\"0\" version=\"1\">
 *      <Number1>56</Number1>
 *     </Item>
 *    </Bar1List>
 *    <Generic1List/>
 *    <Interface>
 *     <Item index=\"0\" version=\"1\" type=\"BarModel1\">
 *      <Number1>666</Number1>
 *     </Item>
 *    </Interface>
 *   </Item>
 *  </Generic1List>
 *  <Interface>
 *   <Item index=\"0\" version=\"1\" type=\"BarModel2\">
 *    <Number2>912</Number2>
 *   </Item>
 *   <Item index=\"1\" version=\"1\" type=\"BarModel1\">
 *    <Number1>1793</Number1>
 *   </Item>
 *  </Interface>
 * </Main>
 * ```
 *
 * Output:
 * ```
 * > XML output (Foo):  "<!DOCTYPE ListModelExample>\n ..."
 * > hello->toString() (deserialize): "Foo: <bar1[<Bar1 1024> <Bar1 144> ]> <gen1[<Bar2 42> <Foo: <bar1[<Bar1 56> ]> <gen1[]> <int1[<Bar1 666> ]>> ]> <int1[<Bar2 912> <Bar1 1793> ]>"
 * ```
 */
int main(int , char *[])
{
	// initialization
	// - create models
	auto foo = std::make_shared<FooModel>();
	auto b1 = std::make_shared<Bar1Model>(1024);
	auto b2 = std::make_shared<Bar1Model>(144);
	auto g1 = std::make_shared<Bar2Model>(42);
	auto g2 = std::make_shared<FooModel>();
	auto g2_b2 = std::make_shared<Bar1Model>(56);
	auto g2_i1 = std::make_shared<Bar1Model>(666);
	auto i1 = std::make_shared<Bar2Model>(912);
	auto i2 = std::make_shared<Bar1Model>(1793);

	foo->bar1.append(b1); // Bar1 1024
	foo->bar1.append(b2); // Bar1 144

	foo->generic1.append(g1); // Bar2 42
	foo->generic1.append(g2); // Foo
	g2->bar1.append(g2_b2); // Bar1 56
	g2->interface1.append(g2_i1); // Bar1 666

	foo->interface1.append(i1); // Bar2 912
	foo->interface1.append(i2); // Bar1 1793

	// - createFactory
	auto factory = std::make_shared<XmlModelSerializerFactoryList>();
	factory->append<FooSerializer>();
	factory->append<Bar1Serializer>();
	factory->append<Bar2Serializer>();

	// - create the serializer
	FooSerializer serializer(nullptr);
	serializer.setName("Main");
	serializer.initialize(factory);

	// serialize FooModel
	// - create document
	QDomDocument docW1("ListModelExample");
	serializer.setDocument(&docW1);

	// - serialize FooModel & display
	serializer.setModel(foo);
	docW1.appendChild(serializer.serialize());
	QString output = docW1.toString();
	qInfo() << "> XML output (Foo): " << output;

	// "deserialize" FooModel
	// - load document
	QDomDocument docR1("ListModelExample");
	docR1.setContent(output);

	serializer.setDocument(&docR1);
	serializer.deserialize(docR1.firstChild());

	S_Model sm = serializer.model();
	auto hello = std::dynamic_pointer_cast<FooModel>(serializer.model());
	qInfo() << "> hello->toString() (deserialize):" << hello->toString() << "\n";
	return 1;
}

}} // namespaces

int main(int argc, char *argv[])
{
	return Imbrixamples::N014::main(argc,argv);
}

