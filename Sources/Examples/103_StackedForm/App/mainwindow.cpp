#include "mainwindow.h"
#include <QDockWidget>

using namespace Imbrixamples::N103;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
	_viewer = new Imbrigui::StackedWidgetForModel("Stacked");
	this->setCentralWidget(_viewer);

	auto dock = new QDockWidget("model selection");

	_list = new QListWidget();
	_list->setSelectionMode(QAbstractItemView::ExtendedSelection);

	this->addDockWidget(Qt::DockWidgetArea::LeftDockWidgetArea, dock);
	dock->setWidget(_list);

	this->connect(_list,&QListWidget::itemSelectionChanged, this, &MainWindow::displaySelectedModel);
}

void MainWindow::addModel(Imbricable::S_Model m, QString name)
{
	_models.append(m);
	_list->addItem(name);
}

void MainWindow::displaySelectedModel()
{

	//auto l = _list->selectedItems();


	//auto si = _list->selectedItems();
	auto indexes = _list->selectionModel()->selectedIndexes();

	QList<Imbricable::S_Model> list;
	for(auto i : indexes)
	{
		int index = i.row();
		list.append(_models[index]);
	}

	_viewer->setModels(list);
}

