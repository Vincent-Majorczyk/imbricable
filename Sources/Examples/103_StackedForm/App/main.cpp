/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "Examples/101_FormWithUndoRedo/Lib/examplemodel.h"
#include "Examples/101_FormWithUndoRedo/Lib/examplecontroler.h"
#include "Examples/101_FormWithUndoRedo/Lib/exampleform.h"

#include "Examples/102_FormWithUnits/Lib/examplemodel.h"
#include "Examples/102_FormWithUnits/Lib/examplecontroler.h"
#include "Examples/102_FormWithUnits/Lib/exampletree.h"
#include "mainwindow.h"

#include <QApplication>
#include <QMainWindow>
#include <iostream>
#include <QDebug>
#include <QTableWidget>

using namespace Imbricable;
using namespace Imbrixamples;
using namespace Imbrigui;

namespace Imbrixamples {

/**
 * @brief this example aims to test the user interface about units properties and the saving of the GUI preferences.
 */
namespace N103 {

/**
 * @brief the main function of the example
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char *argv[])
{
	qDebug() << "begin";
	QApplication a(argc, argv);
	QCoreApplication::setOrganizationName("Imbricable");
	QCoreApplication::setApplicationName("Imbrixamples");

	// create data
	auto dummy = std::make_shared<Model>();
	std::shared_ptr<N101::ExampleModel> model_null;
	auto model1 = std::make_shared<N101::ExampleModel>();
	model1->myBool3 = true;
	auto model2 = std::make_shared<N101::ExampleModel>();
	model2->myBool3 = false;
	auto model3 = std::make_shared<N102::ExampleModel>();
	model3->length = 1024;
	auto model4 = std::make_shared<N102::ExampleModel>();
	model4->length = 42;
	auto model5 = std::make_shared<N102::ExampleModel>();
	model4->area = 666;

	// create main window
	auto *mainwindow = new MainWindow();


	auto dimensions = std::make_shared<Imbricable::Units::DimensionList>();
	dimensions->initializeDefault();

	//   set form of the StackedDataViewer
	mainwindow->viewer()->stackedWidget()->addWidget(new N101::ExampleForm());
	mainwindow->viewer()->stackedWidget()->addWidget(new N102::ExampleTree(dimensions));

	//   add data to user interface
	mainwindow->addModel(dummy, "Dummy");
	mainwindow->addModel(model_null, "Base null");
	mainwindow->addModel(model1, "Base n°1");
	mainwindow->addModel(model2, "Base n°2");
	mainwindow->addModel(model3, "Units n°1");
	mainwindow->addModel(model4, "Units n°2");
	mainwindow->addModel(model5, "Units n°3");

	mainwindow->show();

	qApp->setStyleSheet("QWidget[checkState=valid]{} QWidget[checkState=warning]{background-color:	khaki;} QWidget[checkState=error]{background-color:lightcoral;}  QWidget[checkState=conflict]{background-color:paleturquoise;}");
	mainwindow->viewer()->loadSettings("Imbrixample");
	a.exec();
	mainwindow->viewer()->saveSettings("Imbrixample");

	return 1;
}

}} // namespaces

int main(int argc, char *argv[])
{
	return Imbrixamples::N103::main(argc,argv);
}

