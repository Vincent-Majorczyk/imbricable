#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Gui/FieldContainers/stackedwidgetformodel.h"
#include <QListWidget>


namespace Imbrixamples::N103
{

class MainWindow : public QMainWindow
{
	Q_OBJECT

	Imbrigui::StackedWidgetForModel* _viewer;
	QListWidget* _list;
	QList<Imbricable::S_Model> _models;
public:
	explicit MainWindow(QWidget *parent = nullptr);

	inline Imbrigui::StackedWidgetForModel* viewer() { return _viewer; }
	void addModel(Imbricable::S_Model m, QString name);

signals:

public slots:
	void displaySelectedModel();
};

} // namespace

#endif // MAINWINDOW_H
