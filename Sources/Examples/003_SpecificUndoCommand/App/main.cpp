/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include <memory>
#include <iostream>

#include <QDebug>
#include <QUndoStack>

#include "Core/Object/model.h"
#include "Core/Object/Interfaces/cnamedobject.h"
#include "Core/Actions/controler.h"
#include "Core/Actions/accessoraction.h"

using namespace Imbricable;

namespace Imbrixample {

/**
 * @brief this example shows the undostack integration in model-controler with a specific UndoCommand
 */
namespace N003 {

/**
 * @brief The model contains only data.
 *
 * Similar to N002::HelloWorldModel, but have another property 'length'
 *
 * Here, when 'text' is modified, 'length' is modified, but it is possible to modify 'length' later.
 * That means the undostack must modify two values when undo.
 */
class HelloWorldModel: public Model, public CNamedObject
{
public:
	/// @brief public property 'text'
	QString text;
	/// @brief public property 'length'
	int length = 0;

	/// @brief model constructor
	/// @param text initialize the property 'text'
	HelloWorldModel(QString text):CNamedObject("HelloWorldObject"), text(text) {}

	/// @brief model destructor
	~HelloWorldModel() override {}
};

/**
 * @brief specific UndoCommand for the property 'text'
 *
 * - when property 'text' is modified, the property 'length' is modified;
 * - so undo command must revert text and length;
 * - override redo command is not requiered because the base redo command already modify 'text' and 'length'
 *
 */
class HelloWorldAccessorActionUndoCommand_Text: public AccessorActionUndoCommand<QString>
{
protected:
	/// @brief store 'length' command
	int _oldvalue2;

public:
	/**
	 * @brief constructor
	 * @note this constructor must be the same as Imbricable::AccessorActionUndoCommand<QString>
	 */
	HelloWorldAccessorActionUndoCommand_Text(S_Model model, AccessorAction<QString>* accessor, QString newValue, QUndoCommand *parent = nullptr):
		AccessorActionUndoCommand<QString>(model,accessor,newValue,parent)
	{
		// get the current 'length'
		_oldvalue2 = std::dynamic_pointer_cast<HelloWorldModel>(model)->length;
	}

	/**
	 * @brief override of the undo command.
	 *
	 * revert manually the two property 'text' and 'length'
	 *
	 * @note another solution is the use of `AccessorActionUndoCommand<QString>::undo();` instead of `std::dynamic_pointer_cast<HelloWorldModel>(this->model())->text = _oldvalue;`.
	 * Nevertheless, this first solution modify the property 'length' twice: with the 'undo()' which use accessor 'setText()' (depending of the new value for 'text') and with the restauration by _oldvalue2
	 */
	void undo() override
	{
		// undo the 'text'
		std::dynamic_pointer_cast<HelloWorldModel>(this->model())->text = _oldvalue;

		// undo the 'length'
		std::dynamic_pointer_cast<HelloWorldModel>(this->model())->length = _oldvalue2;
	}
};

/**
 * @brief The controler contains accessors and methods to modify model data.
 *
 * Similar to N002::HelloWorldControler, but the assessor to the property `length` is added
 */
class HelloWorldControler: public Controler
{
private:
	/// @brief convert model to HelloWorldModel
	inline std::shared_ptr<HelloWorldModel> convert(S_Model m)
	{ return std::dynamic_pointer_cast<HelloWorldModel>(m); }

	/// @brief 'get' accessor for HelloWorldModel::text property
	QString getText(S_Model m) { return convert(m)->text; }

	/// @brief 'set' accessor for HelloWorldModel::text property
	/// @param text new value for the property 'text' and the length of this string is set to the property 'length'
	void setText(S_Model m, QString text) { convert(m)->text = text; convert(m)->length = text.length(); }

	/// @brief 'get' accessor for HelloWorldModel::length property
	int getlength(S_Model m) { return convert(m)->length; }

	/// @brief 'set' accessor for HelloWorldModel::length property
	/// @param length new value for the property 'length'
	void setLength(S_Model m, int length) { convert(m)->length = length;}

public:
	/// @brief to be used as accessor to the HelloWorldModel::text
	/// @note a second template parameter is setted: HelloWorldAccessorActionUndoCommand_Text
	AccessorAction<QString> text{ "text", this, &HelloWorldControler::getText, &HelloWorldControler::setText};

	/// @brief controler constructor
	HelloWorldControler():Controler("HelloWorld")
	{
		text.changeUndoCommand<HelloWorldAccessorActionUndoCommand_Text>();
	}

	/// @brief to be used as accessor to the HelloWorldModel::length
	AccessorAction<int> length{ "length", this, &HelloWorldControler::getlength, &HelloWorldControler::setLength};
};

/**
 * @brief use undostack with simple example
 *
 * Steps:
 * - create a HelloWorldModel initialized with the text "hello world";
 * - create a HelloWorldControler associated to the model with a QUndoStack;
 * - do successive modifications of the value and display the content of the undostack;
 * - revert modifications to verify the behavior of a undo with the set of the text
 * - restore
 *
 * Output:
 * ```
 * > initial value "Hello world" 0
 * > value after successive modifications: "Salut" 87
 * > foreach item in undostack:
 *  - undostack.text() "Modify HelloWorldObject::length '0'->'24'"
 *  - undostack.text() "Modify HelloWorldObject::text 'Hello world'->'Salut'"
 *  - undostack.text() "Modify HelloWorldObject::length '5'->'87'"
 * > value at index 0: "Hello world" 0
 * > value at index 1: "Hello world" 24
 * > value at index 2: "Salut" 5
 * > value at index 3: "Salut" 87
 * ```
 */
void main()
{
	// create the HelloWorldModel with the text "hello world";
	auto hello = std::make_shared<HelloWorldModel>( "Hello world" );

	// create the Undostack
	QUndoStack undostack;

	// create the HelloWorldControler
	HelloWorldControler controler;
	controler.setModel(hello);
	controler.setUndoStack(&undostack);
	bool conflict;
	qInfo() << "> initial value" << controler.text.get(conflict) << controler.length.get(conflict);

	// do successive modifications
	controler.length.set(24);
	controler.text.set("Salut");
	controler.length.set(87);

	qInfo() << "> value after successive modifications:" << controler.text.get(conflict) << controler.length.get(conflict);
	qInfo() << "> foreach item in undostack:";
	for(int i=0;i<undostack.count();i++)
		qInfo() << " - undostack.text()" << undostack.text(i);

	// revert all successive modification
	undostack.setIndex(0);
	qInfo() << "> value at index 0:" << controler.text.get(conflict) << controler.length.get(conflict);

	undostack.setIndex(1);
	qInfo() << "> value at index 1:" << controler.text.get(conflict) << controler.length.get(conflict);

	undostack.setIndex(2);
	qInfo() << "> value at index 2:" << controler.text.get(conflict) << controler.length.get(conflict);

	undostack.setIndex(3);
	qInfo() << "> value at index 3:" << controler.text.get(conflict) << controler.length.get(conflict);
}

}} // namespaces

int main(int, char *[])
{
	Imbrixample::N003::main();
}
