#ifndef EXAMPLECONTROLER101_H
#define EXAMPLECONTROLER101_H

#include "examplemodel.h"
#include "specificundocommand.h"
#include "Core/Actions/accessoraction.h"

namespace Imbrixamples::N101 {

class ExampleControler : public Imbricable::Controler
{
	using S_Model = Imbricable::S_Model;
	using CheckState = Imbricable::CheckState;
	using RangedInt = Imbricable::RangedInt;

	template<typename C>
	using AccessorAction = Imbricable::AccessorAction<C>;

private:
	inline std::shared_ptr<ExampleModel> convert(S_Model m)
	{ return std::dynamic_pointer_cast<ExampleModel>(m); }

	/**
	 * @brief get the boolean 'MyBool'
	 * @return the value
	 */
	inline bool getMyBool1(S_Model m) { return convert(m)->myBool1; }

	/**
	 * @brief set the boolean 'MyBool'
	 * @param value
	 */
	inline void setMyBool1(S_Model m, bool value) { convert(m)->myBool1 = value; }

	/**
	 * @brief check if the boolean is valid (as 'MyBool')
	 * @param value
	 * @return
	 */
	inline CheckState checkMyBool1(S_Model m, bool value)
	{
		if(value ^ convert(m)->myBool3) return CheckState::Error;
		return CheckState::Valid;
		Q_UNUSED(m)
	}

	/**
	 * @brief get the boolean 'MyBool2'
	 * @return the value
	 */
	inline bool getMyBool2(S_Model m) { return convert(m)->myBool2; }

	/**
	 * @brief set the boolean 'MyBool2'
	 * @param value
	 */
	inline void setMyBool2(S_Model m, bool value) { convert(m)->myBool2 = value; }

	/**
	 * @brief check if the boolean is valid (as 'MyBool2')
	 * @param value
	 * @return
	 */
	inline CheckState checkMyBool2(S_Model m, bool value)
	{
		if(value ^ convert(m)->myBool3)return CheckState::Error;
		else return CheckState::Valid;
		Q_UNUSED(m)
	}

	/**
	 * @brief get the boolean 'MyBool3'
	 * @return the value
	 */
	inline bool getMyBool3(S_Model m) { return convert(m)->myBool3; }

	/**
	 * @brief set the boolean 'MyBool3'
	 * @param value
	 */
	inline void setMyBool3(S_Model m, bool value) { convert(m)->myBool3 = convert(m)->myBool2 = convert(m)->myBool1 = value; }

	/**
	 * @brief check if the boolean is valid (as 'MyBool')
	 * @return
	 */
	inline CheckState checkMyBool3(S_Model m, bool)
	{
		return CheckState::Valid;
	}

	/**
	 * @brief get the QString 'MyString'
	 * @return the value
	 */
	inline QString getMyString(S_Model m) { return convert(m)->myString; }

	/**
	 * @brief set the QString 'MyString'
	 * @param value
	 */
	inline void setMyString(S_Model m, QString value) { convert(m)->myString = value; }

	/**
	 * @brief check if the QString is valid (as 'MyString')
	 * @param value
	 * @return
	 */
	inline CheckState checkMyString(S_Model m, QString value)
	{
		if(value.count()<20) return CheckState::Valid;
		return CheckState::Error;
		Q_UNUSED(m)
	}

	/**
	 * @brief get the qint8 'MyChar'
	 * @return the value
	 */
	inline qint8 getMyChar(S_Model m) { return convert(m)->myChar; }

	/**
	 * @brief set the qint8 'MyChar'
	 * @param value
	 */
	inline void setMyChar(S_Model m, qint8 value) { convert(m)->myChar = value; }

	/**
	 * @brief get the quint8 'MyUChar'
	 * @return the value
	 */
	inline quint8 getMyUChar(S_Model m) { return convert(m)->myUChar; }

	/**
	 * @brief set the quint8 'MyUChar'
	 * @param value
	 */
	inline void setMyUChar(S_Model m, quint8 value) { convert(m)->myUChar = value; }

	/**
	 * @brief get the qint16 'MyShort'
	 * @return the value
	 */
	inline qint16 getMyShort(S_Model m) { return convert(m)->myShort; }

	/**
	 * @brief set the qint16 'MyShort'
	 * @param value
	 */
	inline void setMyShort(S_Model m, qint16 value) { convert(m)->myShort = value; }

	/**
	 * @brief get the quint16 'MyUShort'
	 * @return the value
	 */
	inline quint16 getMyUShort(S_Model m) { return convert(m)->myUShort; }

	/**
	 * @brief set the quint16 'MyUShort'
	 * @param value
	 */
	inline void setMyUShort(S_Model m, quint16 value) { convert(m)->myUShort = value; }

	/**
	 * @brief get the qint32 'MyInt'
	 * @return the value
	 */
	inline qint32 getMyInt(S_Model m) { return convert(m)->myInt; }

	/**
	 * @brief set the qint32 'MyInt'
	 * @param value
	 */
	inline void setMyInt(S_Model m, qint32 value) { convert(m)->myInt = value; }

	/**
	 * @brief check if the qint32 is valid (as 'MyInt')
	 * @param value
	 * @return
	 */
	inline CheckState checkMyInt(S_Model m, qint32 value)
	{
		if(value != 3) return CheckState::Valid;
		return CheckState::Error;
		Q_UNUSED(m)
	}

	/**
	 * @brief get the quint32 'MyUInt'
	 * @return the value
	 */
	inline quint32 getMyUInt(S_Model m) { return convert(m)->myUInt; }

	/**
	 * @brief set the quint32 'MyUInt'
	 * @param value
	 */
	inline void setMyUInt(S_Model m, quint32 value) { convert(m)->myUInt = value; }

	/**
	 * @brief get the qint64 'MyLongLong'
	 * @return the value
	 */
	inline qint64 getMyLongLong(S_Model m) { return convert(m)->myLongLong; }

	/**
	 * @brief set the qint64 'MyLongLong'
	 * @param value
	 */
	inline void setMyLongLong(S_Model m, qint64 value) { convert(m)->myLongLong = value; }

	/**
	 * @brief get the quint64 'MyULong'
	 * @return the value
	 */
	inline quint64 getMyULongLong(S_Model m) { return convert(m)->myULongLong; }

	/**
	 * @brief set the quint64 'MyULong'
	 * @param value
	 */
	inline void setMyULongLong(S_Model m, quint64 value) { convert(m)->myULongLong = value; }

	/**
	 * @brief get the float 'MyFloat'
	 * @return the value
	 */
	inline float getMyFloat(S_Model m) { return convert(m)->myFloat; }

	/**
	 * @brief set the float 'MyFloat'
	 * @param value
	 */
	inline void setMyFloat(S_Model m, float value) { convert(m)->myFloat = value; }

	/**
	 * @brief get the double 'MyDouble'
	 * @return the value
	 */
	inline double getMyDouble(S_Model m) { return convert(m)->myDouble; }

	/**
	 * @brief set the double 'MyDouble'
	 * @param value
	 */
	inline void setMyDouble(S_Model m, double value) { convert(m)->myDouble = value; }

	/**
	 * @brief get the qint32 'MaxRange'
	 * @return the value
	 */
	inline qint32 getMyMaxRange(S_Model m) { return convert(m)->myRange.maximum(); }

	/**
	 * @brief set the qint32 'MaxRange'
	 * @param value
	 */
	inline void setMyMaxRange(S_Model m, qint32 value) { convert(m)->myRange.setMaximum(value); }

	/**
	 * @brief check if the qint32 is valid (as 'MaxRange')
	 * @param value
	 * @return
	 */
	inline CheckState checkMyMaxRange(S_Model m, qint32 value)
	{
		if(convert(m)->myRange.verifyMaximum(value)) return CheckState::Valid;
		return CheckState::Error;
		Q_UNUSED(m)
	}

	/**
	 * @brief get the qint32 'MinRange'
	 * @return the value
	 */
	inline qint32 getMyMinRange(S_Model m) { return convert(m)->myRange.minimum(); }

	/**
	 * @brief set the qint32 'MinRange'
	 * @param value
	 */
	inline void setMyMinRange(S_Model m, qint32 value) { convert(m)->myRange.setMinimum(value); }

	/**
	 * @brief check if the qint32 is valid (as 'MinRange')
	 * @param value
	 * @return
	 */
	inline CheckState checkMyMinRange(S_Model m, qint32 value)
	{
		if(convert(m)->myRange.verifyMinimum(value)) return CheckState::Valid;
		return CheckState::Error;
		Q_UNUSED(m)
	}

	/**
	 * @brief get the qint32 'Value'
	 * @return the value
	 */
	inline qint32 getMyValue(S_Model m) { return convert(m)->myRange.value(); }

	/**
	 * @brief set the qint32 'Value'
	 * @param value
	 */
	inline void setMyValue(S_Model m, qint32 value) { convert(m)->myRange = value; }

	/**
	 * @brief check if the qint32 is valid (as 'Value')
	 * @param value
	 * @return
	 */
	inline CheckState checkMyValue(S_Model m, qint32 value)
	{
		if(convert(m)->myRange.isValueInRange(value)) return CheckState::Valid;
		return CheckState::Error;
		Q_UNUSED(m)
	}

	/**
	 * @brief get the qint32 'Value'
	 * @return the value
	 */
	inline RangedInt getMyRange(S_Model m) { return convert(m)->myRange; }

	/**
	 * @brief set the qint32 'Value'
	 * @param value
	 */
	inline void setMyRange(S_Model m, RangedInt value) { convert(m)->myRange = value; }

public:
	ExampleControler();


	AccessorAction<bool> myBool1{"myBool1", this, &ExampleControler::getMyBool1, &ExampleControler::setMyBool1, &ExampleControler::checkMyBool1};
	AccessorAction<bool> myBool2{"myBool2", this, &ExampleControler::getMyBool2, &ExampleControler::setMyBool2, &ExampleControler::checkMyBool2};
	AccessorAction<bool> myBool3{"myBool3", this, &ExampleControler::getMyBool3, &ExampleControler::setMyBool3, &ExampleControler::checkMyBool3};

	AccessorAction<QString> myString{"myString", this, &ExampleControler::getMyString, &ExampleControler::setMyString, &ExampleControler::checkMyString};
	AccessorAction<qint8> myChar{"myChar", this, &ExampleControler::getMyChar, &ExampleControler::setMyChar};
	AccessorAction<quint8> myUChar{"myUChar", this, &ExampleControler::getMyUChar, &ExampleControler::setMyUChar};
	AccessorAction<qint16> myShort{"myShort", this, &ExampleControler::getMyShort, &ExampleControler::setMyShort};
	AccessorAction<quint16> myUShort{"myUShort", this, &ExampleControler::getMyUShort, &ExampleControler::setMyUShort};
	AccessorAction<qint32> myInt{"myInt", this, &ExampleControler::getMyInt, &ExampleControler::setMyInt, &ExampleControler::checkMyInt};
	AccessorAction<quint32> myUInt{"myUInt", this, &ExampleControler::getMyUInt, &ExampleControler::setMyUInt};
	AccessorAction<qint64> myLongLong{"myLongLong", this, &ExampleControler::getMyLongLong, &ExampleControler::setMyLongLong};
	AccessorAction<quint64> myULongLong{"myULongLong", this, &ExampleControler::getMyULongLong, &ExampleControler::setMyULongLong};
	AccessorAction<float> myFloat{"myFloat", this, &ExampleControler::getMyFloat, &ExampleControler::setMyFloat};
	AccessorAction<double> myDouble{"myDouble", this, &ExampleControler::getMyDouble, &ExampleControler::setMyDouble};

	AccessorAction<qint32> myMaxRange{"myMaxRange", this, &ExampleControler::getMyMaxRange, &ExampleControler::setMyMaxRange, &ExampleControler::checkMyMaxRange};
	AccessorAction<qint32> myMinRange{"myMinRange", this, &ExampleControler::getMyMinRange, &ExampleControler::setMyMinRange, &ExampleControler::checkMyMinRange};
	AccessorAction<qint32> myValue{"myValue", this, &ExampleControler::getMyValue, &ExampleControler::setMyValue, &ExampleControler::checkMyValue};
	AccessorAction<RangedInt> myRange{"myRange", this, &ExampleControler::getMyRange, &ExampleControler::setMyRange};
};

typedef std::shared_ptr<ExampleControler> S_ExampleControler ;

}

#endif // EXAMPLECONTROLER_H
