/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef EXAMPLEMODEL101_H
#define EXAMPLEMODEL101_H

#include "Core/Object/model.h"
#include "Core/Object/rangednumber.h"
#include "Core/Object/Interfaces/inamedobject.h"

namespace Imbrixamples::N101 {

/**
 * @brief a simple example class
 */
class ExampleModel: public Imbricable::Model, public Imbricable::INamedObject
{
public:
	bool myBool1 = true;   //!> 'MyBool1' property
	bool myBool2 = false;  //!> 'MyBool2' property
	bool myBool3 = false;  //!> 'MyBool3' property

	qint8 myChar = 0;                  //!> "MyChar" property
	qint16 myShort = 0;                //!> "MyShort" property
	quint16 myUShort = 0;              //!> "MyUShort" property
	QString myString = "Default text"; //!> 'MyString' property
	qint32 myInt = 0;                  //!> "MyInt" property
	quint32 myUInt = 0;                //!> "MyUInt" property
	qint64 myLongLong = 0;             //!> "MyLongLong" property
	quint64 myULongLong = 0;           //!> "MyULongLong" property
	double myDouble = 0;               //!> "MyDouble" property
	float myFloat = 0;                 //!> "MyFloat" property
	Imbricable::RangedInt myRange{0,100,25};                 //!> "MyRange" property
	quint8 myUChar = 0;                //!> "MyUChar" property

	/**
	 * @brief return name of the properties container
	 *
	 * This aims to display a understandable name in undo/redo list
	 *
	 * @return name
	 */
	QString getName() const override { return "Example01"; }

public:
	/**
	 * @brief Main constructor
	 */
	ExampleModel();
};

} //namespace

#endif // EXAMPLEMODEL001_H
