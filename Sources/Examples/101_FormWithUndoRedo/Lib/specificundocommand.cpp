/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "specificundocommand.h"
#include "examplemodel.h"

using namespace Imbricable;
using namespace Imbrixamples::N101;

SpecificUndoCommand::SpecificUndoCommand(S_Model model, AccessorAction<bool>* accessor, bool newValue, QUndoCommand *parent):
		AccessorActionUndoCommand<bool>(model,accessor,newValue,parent)
{
	_mybool1_oldvalue = std::dynamic_pointer_cast<ExampleModel>(model)->myBool1;
	_mybool2_oldvalue = std::dynamic_pointer_cast<ExampleModel>(model)->myBool2;
}

void SpecificUndoCommand::undo()
{
	std::dynamic_pointer_cast<ExampleModel>(this->model())->myBool1 = _mybool1_oldvalue;
	std::dynamic_pointer_cast<ExampleModel>(this->model())->myBool2 = _mybool2_oldvalue;
	std::dynamic_pointer_cast<ExampleModel>(this->model())->myBool3 = _oldvalue;

}

