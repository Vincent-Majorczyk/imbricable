#include "exampleform.h"
#include <QFormLayout>
#include <QLabel>
#include "Gui/Fields/fields.h"

using namespace Imbrixamples::N101;
using namespace Imbrigui;

ExampleForm::ExampleForm(QWidget *parent):
	Imbrigui::FieldFormWidget("N101ExampleForm", parent)
{
	Field_Bool *b1, *b2, *b3;

	this->addSubTitle("Booleans", "tooltip");
	this->addField("My first boolean", "value != myBool3", b1 = new Field_Bool(&_controler->myBool1));
	this->addField("My second boolean", "value != myBool3", b2 = new Field_Bool(&_controler->myBool2));
	this->addField("My third boolean", "true", b3 = new Field_Bool(&_controler->myBool3));

	b3->setMultiEditionEnabled(true);

	auto lamba = [b1,b2,b3](void)
	{
		b1->updateField();
		b2->updateField();
		b3->updateField();
	};

	QObject::connect(b1, &Field::modelChanged, this, lamba);
	QObject::connect(b2, &Field::modelChanged, this, lamba);
	QObject::connect(b3, &Field::modelChanged, this, lamba);

	Field_Double *p;

	this->addSubTitle("Basic LineEdit group", "tooltip");
	this->addField("My string", "length < 20", new Field_QString(&_controler->myString));
	this->addField("My char", "tooltip", new Field_Int8(&_controler->myChar));
	this->addField("My unsigned char", "tooltip", new Field_UInt8(&_controler->myUChar));
	this->addField("My short", "tooltip", new Field_Int16(&_controler->myShort));
	this->addField("My unsigned short", "tooltip", new Field_UInt16(&_controler->myUShort));
	this->addField("My int", "value != 3", new Field_Int32(&_controler->myInt));
	this->addField("My unsigned int", "value != 3", new Field_UInt32(&_controler->myUInt));
	this->addField("My long long", "tooltip", new Field_Int64(&_controler->myLongLong));
	this->addField("My unsigned long long", "tooltip", new Field_UInt64(&_controler->myULongLong));
	this->addField("My float", "tooltip", new Field_Float(&_controler->myFloat));
	this->addField("My double", "tooltip", p = new Field_Double(&_controler->myDouble));

	p->setMultiEditionEnabled(true);

	Field_Int32 *i1,*i2,*i3;
	SliderField_RangedInt32 *i4;
	ProgressBarField_RangedInt32 *i5;

	this->addSubTitle("Ranged group", "tooltip");
	this->addField("Max", "tooltip", i1 = new Field_Int32(&_controler->myMaxRange));
	this->addField("Min", "tooltip", i2 = new Field_Int32(&_controler->myMinRange));
	this->addField("Value", "tooltip", i3 = new Field_Int32(&_controler->myValue));
	this->addField("Val", "tooltip", i4 = new SliderField_RangedInt32(&_controler->myRange));
	this->addField("Progress", "tooltip", i5 = new ProgressBarField_RangedInt32(&_controler->myRange));

	auto lamba2 = [i1,i2,i3,i4,i5](void)
	{
		i1->updateField();
		i2->updateField();
		i3->updateField();
		i4->updateField();
		i5->updateField();
	};

	QObject::connect(i1, &Field::modelChanged, this, lamba2);
	QObject::connect(i2, &Field::modelChanged, this, lamba2);
	QObject::connect(i3, &Field::modelChanged, this, lamba2);
	QObject::connect(i4, &Field::modelChanged, this, lamba2);
}

Imbricable::S_Controler ExampleForm::controler()
{
	return _controler;
}

bool ExampleForm::isCompatible(Imbricable::S_Model model)
{
	return std::dynamic_pointer_cast<ExampleModel>(model) != nullptr;
}
