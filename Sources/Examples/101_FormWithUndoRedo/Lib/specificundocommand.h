/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef SPECIFICUNDOCOMMAND101_H
#define SPECIFICUNDOCOMMAND101_H

#include "Core/Actions/accessoractionundocommand.h"
#include "Core/Object/model.h"

namespace Imbrixamples::N101 {

class SpecificUndoCommand: public Imbricable::AccessorActionUndoCommand<bool>
{
	bool _mybool1_oldvalue;
	bool _mybool2_oldvalue;

public:
	SpecificUndoCommand(Imbricable::S_Model model, Imbricable::AccessorAction<bool>* accessor, bool newValue, QUndoCommand *parent = nullptr);

	void undo() override;
};

} //namespace

#endif // SPECIFICUNDOCOMMAND_H
