#ifndef EXAMPLEFORM101_H
#define EXAMPLEFORM101_H

#include <QWidget>
#include <Gui/FieldContainers/fieldformwidget.h>
#include "examplecontroler.h"

namespace Imbrixamples::N101 {

class ExampleForm: public Imbrigui::FieldFormWidget
{
Q_OBJECT
	S_ExampleControler _controler = std::make_shared<ExampleControler>();

public:
	ExampleForm(QWidget *parent = nullptr);

	Imbricable::S_Controler controler() override;

	bool isCompatible(Imbricable::S_Model model) override;
};

} // namespace

#endif // EXAMPLEFORM_H
