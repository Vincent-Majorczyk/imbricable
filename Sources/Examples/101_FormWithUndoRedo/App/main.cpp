/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include <QApplication>
#include <QUndoView>
#include <QUndoStack>
#include <QDebug>

#include "Core/Properties/propertyinfo.h"
#include "mainwindow.h"
#include "../Lib/examplemodel.h"

#include "Core/Actions/accessoraction.h"

//using namespace Imbrigui;

namespace Imbrixamples
{

/**
 * @brief this example aims to test the user interface which basic properties.
 */
namespace N101 {

int main(int argc, char *argv[])
{

	qDebug() << "1) Start application";
	QApplication a(argc, argv);
	QCoreApplication::setOrganizationName("Imbricable");
	QCoreApplication::setApplicationName("Imbrixamples");

	std::shared_ptr<ExampleModel> model( new ExampleModel() );

	MainWindow window = new MainWindow();

	ExampleForm* v = window.getForm();
	v->controler()->setModel(model);
	v->updateFields();


	window.show();


	qApp->setStyleSheet("QWidget[checkState=valid]{} QWidget[checkState=warning]{background-color:	khaki;} QWidget[checkState=error]{background-color:lightcoral;}  QWidget[checkState=conflict]{background-color:paleturquoise;}");

	qDebug() << "2) Show widget";
	v->loadSettings("Imbrixample");
	a.exec();
	v->saveSettings("Imbrixample");
	qDebug() << "3) Close widget and display result:";
	qDebug();
	qDebug() << "Boolean group";
	qDebug() << "  My bool 1:" << model->myBool1;
	qDebug() << "  My bool 2:" << model->myBool2;
	qDebug() << "  My bool 3:" << model->myBool3;
	qDebug() << "Basic LineEdit group";
	qDebug() << "  My string:" << model->myString;
	qDebug() << "  My Int8" << model->myChar;
	qDebug() << "  My unsigned Int8 :" << model->myUChar;
	qDebug() << "  My Int16" << model->myShort;
	qDebug() << "  My unsigned Int16 :" << model->myUShort;
	qDebug() << "  My Int32:" << model->myInt;
	qDebug() << "  My unsigned Int32:" << model->myUInt;
	qDebug() << "  My Int64:" << model->myLongLong;
	qDebug() << "  My unsigned Int64:" << model->myULongLong;
	qDebug() << "  My float:" << model->myFloat;
	qDebug() << "  My double:" << model->myDouble;
	qDebug();
	qDebug() << "4) Application end";

	return 1;
}

}} // namespaces

int main(int argc, char *argv[])
{
	return Imbrixamples::N101::main(argc,argv);
}
