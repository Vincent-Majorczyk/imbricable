/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */


#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#pragma once

#include <QMainWindow>
#include <QUndoStack>
#include <QUndoView>
#include <QToolBar>

#include "../Lib/exampleform.h"

namespace Imbrixamples::N101 {

class MainWindow: public QMainWindow
{
	QUndoStack* _undostack;
	QUndoView* _undoview;
	ExampleForm* _dataviewer;
	QToolBar* _toolbox;

public:
	MainWindow(QWidget *parent = nullptr);

	inline ExampleForm* getForm() { return _dataviewer; }

	/**
	 * @brief create an icon for Undo QAction. This method avoids the ressources inclusion in the program.
	 * @return a QIcon with an arrow and a text "Undo"
	 */
	static QIcon createUndoIcon();

	/**
	 * @brief create an icon for Redo QAction. This method avoids the ressources inclusion in the program.
	 * @return a QIcon with an arrow and a text "Redo"
	 */
	static QIcon createRedoIcon();
};

} // namespace

#endif
