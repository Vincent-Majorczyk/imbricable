/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "mainwindow.h"

#include <QHBoxLayout>
#include <QPainter>
#include <QPixmap>
#include <QIcon>

using namespace Imbrixamples::N101;

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent)
{
	_undostack = new QUndoStack(this);

	_dataviewer = new ExampleForm();
	_dataviewer->controler()->setUndoStack(_undostack);

	_undoview = new QUndoView();
	_undoview->setStack(_undostack);

	auto *b = new QHBoxLayout();
	b->addWidget(_dataviewer);
	b->addWidget(_undoview);
	auto* w = new QWidget(this);
	w->setLayout(b);
	this->setCentralWidget(w);

	auto *t = new QToolBar(this);

	QAction* undo = _undostack->createUndoAction(this,"&Undo");
	undo->setShortcuts(QKeySequence::Undo);
	undo->setIcon(createUndoIcon());

	QAction* redo = _undostack->createRedoAction(this,"&Redo");
	redo->setShortcuts(QKeySequence::Redo);
	redo->setIcon(createRedoIcon());

	connect(_undostack, SIGNAL(indexChanged(int)), _dataviewer, SLOT(updateFields()));

	t->addAction(undo);
	t->addAction(redo);
	this->addToolBar(t);
}

QIcon MainWindow::createUndoIcon()
{
	QPixmap pix(32,32);
	pix.fill(Qt::transparent);
	QPainter painter(&pix);
	painter.setPen(Qt::black);
	painter.drawText(QPoint(0,31),"Undo",14,0);
	QPen pen(Qt::darkGreen);
	pen.setWidth(2);
	painter.setPen(pen);
	painter.drawLine(5,8,27,8);
	painter.drawLine(5,8,12,2);
	painter.drawLine(5,8,12,14);
	return QIcon(pix);
}

QIcon MainWindow::createRedoIcon()
{
	QPixmap pix(32,32);
	pix.fill(Qt::transparent);
	QPainter painter(&pix);
	painter.setPen(Qt::black);
	painter.drawText(QPoint(0,31),"Redo",14,0);
	QPen pen(Qt::darkGreen);
	pen.setWidth(2);
	painter.setPen(pen);
	painter.drawLine(5,8,27,8);
	painter.drawLine(27,8,20,2);
	painter.drawLine(27,8,20,14);
	return QIcon(pix);
}

