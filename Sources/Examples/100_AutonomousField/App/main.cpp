/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include <QApplication>
#include <QUndoView>
#include <QUndoStack>
#include <QDebug>
#include <QDialog>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>

#include "Core/Actions/accessoraction.h"
#include "Gui/Fields/Numbers/field_int32.h"


//using namespace Imbrigui;
using namespace Imbricable;

namespace Imbrixample {

/**
 * @brief this example aims to test the user interface which one field.
 */
namespace N100 {

/**
 * @brief simple model class with an interger
 */
class FooModel: public Model
{
public:
	/// @brief an integer
	qint32 i = 666;
};

/**
 * @brief controler of FooModel
 */
class FooControler: public Controler
{
private:
	/// @brief convert model to FooModel
	inline std::shared_ptr<FooModel> convert(S_Model m)
	{ return std::dynamic_pointer_cast<FooModel>(m); }

	/// @brief get access to FooModel::i
	int getI(S_Model m) { return  convert(m)->i; }

	/// @brief set access to FooModel::i
	void setI(S_Model m, qint32 i) { convert(m)->i = i; }

public:
	/// @brief constructor
	FooControler():Controler("Foo") {}

	/// @brief accessor to FooModel:i
	AccessorAction<qint32> i{ "text", this, &FooControler::getI, &FooControler::setI, };
};

/**
 * @brief A dialog with a field
 */
class FooDialog: public QDialog
{
	Imbrigui::Field_Int32* field;
	std::shared_ptr<FooControler> controler = std::make_shared<FooControler>();

public:
	FooDialog():QDialog()
	{
		controler = std::make_shared<FooControler>();
		field = new Imbrigui::Field_Int32(&controler->i);

		auto l = new QHBoxLayout();
		l->addWidget(new QLabel("FooModel::i"));
		l->addWidget(field);
		this->setLayout(l);
	}

	void setModel(std::shared_ptr<FooModel> model)
	{
		controler->setModel(model);
		field->updateField();
	}
};

int main(int argc, char *argv[])
{
	qDebug() << "1) Start application";
	QApplication a(argc, argv);

	auto model = std::make_shared<FooModel>();
	auto controler = std::make_shared<FooControler>();
	controler->setModel(model);

	FooDialog* dialog= new FooDialog();
	dialog->setModel(model);

	dialog->show();

	qApp->setStyleSheet("QWidget[checkState=valid]{} QWidget[checkState=warning]{background-color:	khaki;} QWidget[checkState=error]{background-color:lightcoral;}  QWidget[checkState=conflict]{background-color:paleturquoise;}");

	qDebug() << "2) display initial value 'i':" << model->i;
	qDebug() << "3) Show widget";
	a.exec();
	qDebug() << "4) Close widget and display value 'i':" << model->i;
	qDebug() << "5) Application end";

	return 1;
}

}} // namespaces

int main(int argc, char *argv[])
{
	return Imbrixample::N100::main(argc,argv);
}
