/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef FACTORYBUILDER_H
#define FACTORYBUILDER_H

#include "Core/Factory/TypeFactory/typefactory.h"

namespace Imbrixamples::N200 {

class FactoryBuilder
{
public:
	static void fillTypesFactory(Imbricable::S_TypeFactory factory);

private:
	FactoryBuilder() = delete;
};

} // namespace

#endif // FACTORYBUILDER_H
