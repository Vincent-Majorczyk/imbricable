/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "grouptreeitem.h"
#include <QPixmap>
#include <QColor>
#include <QMenu>
#include <QAction>
#include "../ItemModel/itemtreeitem.h"

using namespace Imbrixamples::N200;

std::shared_ptr<QIcon> GroupTreeItem::_icon = nullptr;

QIcon GroupTreeItem::createIcon()
{
	if(_icon != nullptr) return *_icon;
	QPixmap pixmap(16,16);
	pixmap.fill( Qt::green );
	_icon = std::make_shared<QIcon>(pixmap);
	return *_icon;
}

GroupTreeItem::GroupTreeItem(S_GroupModel groupmodel):
	TreeNode(nullptr, groupmodel, groupmodel->name, createIcon())
{
}

GroupTreeItem::~GroupTreeItem()=default;

void GroupTreeItem::buildNode()
{
	auto group = std::dynamic_pointer_cast<GroupModel>(_model);
	if(group == nullptr) return;

	List_SItemModel& items = group->items();
	for(auto& g: items)
	{
		bool ok;
		addNode(g,ok);
	}
}

void GroupTreeItem::contextMenu()
{
	QMenu menu;
	menu.addAction(new Action_CreateItemA(this));
	menu.addAction(new Action_CreateItemB(this));

	// execute menu & action
	menu.exec(QCursor::pos());
}

void GroupTreeItem::createItemA()
{
	auto group = std::dynamic_pointer_cast<GroupModel>(this->_model);
	if(group == nullptr) return;
	group->items().append(SItemModel(new ItemAModel()));
	buildNode();
}

void GroupTreeItem::createItemB()
{
	auto group = std::dynamic_pointer_cast<GroupModel>(this->_model);
	if(group == nullptr) return;
	group->items().append(SItemModel(new ItemBModel()));
	buildNode();
}

Action_CreateItemA::Action_CreateItemA(GroupTreeItem* treeitem):
	_treeitem(treeitem)
{
	this->setIcon(ItemATreeItem::icon());
	this->setText("Add ItemA");
	connect(this,SIGNAL(triggered()), this, SLOT(execute()));
}

void Action_CreateItemA::execute()
{
	_treeitem->createItemA();
}

Action_CreateItemB::Action_CreateItemB(GroupTreeItem* treeitem):
	_treeitem(treeitem)
{
	this->setIcon(ItemBTreeItem::icon());
	this->setText("Add ItemB");
	connect(this,SIGNAL(triggered()), this, SLOT(execute()));
}

void Action_CreateItemB::execute()
{
	_treeitem->createItemB();
}
