/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef GROUPTREEITEM_H
#define GROUPTREEITEM_H

#include <QIcon>
#include <memory>
#include "Core/Object/model.h"
#include "Gui/SceneViewer/treenode.h"
#include "groupmodel.h"
#include "../GroupModel/groupmodel.h"
#include <QAction>

namespace Imbrixamples::N200 {

class GroupTreeItem: public Imbrigui::TreeNode
{
public:
	explicit GroupTreeItem(S_GroupModel groupmodel);
	~GroupTreeItem() override;

private:
	static std::shared_ptr<QIcon> _icon;
	static QIcon createIcon();

protected:
	void buildNode() override;
	void contextMenu() override;

public:// slots:
	void createItemA();
	void createItemB();
};

class Action_CreateItemA: public QAction
{
Q_OBJECT
	GroupTreeItem* _treeitem;
public:
	explicit Action_CreateItemA(GroupTreeItem* treeitem);

public slots:
	void execute();
};

class Action_CreateItemB: public QAction
{
Q_OBJECT
	GroupTreeItem* _treeitem;
public:
	explicit Action_CreateItemB(GroupTreeItem* treeitem);

public slots:
	void execute();
};

} //namespace

#endif // GROUPTREEITEM_H
