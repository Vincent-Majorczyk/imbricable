/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef GROUPMODEL_H
#define GROUPMODEL_H

#include <memory>
#include "Core/Object/model.h"
#include "Core/Object/Interfaces/cnamedobject.h"
#include "../ItemModel/itemmodel.h"
#include <QList>

namespace Imbrixamples::N200 {

/**
 * @brief a simple example class child of RootModel
 */
class GroupModel: public Imbricable::Model, public Imbricable::CNamedObject
{
private:
	static int _count;

	List_SItemModel _items;

public:

	/**
	 * @brief constructor
	 */
	GroupModel();

	/**
	 * @brief Main destructor
	 */
	~GroupModel() override;

	/**
	 * @brief get the items
	 * @return list of items
	 */
	inline List_SItemModel& items() { return _items; }

	QString toString(int indent) const;
};

using S_GroupModel = std::shared_ptr<GroupModel>;
using W_GroupModel = std::weak_ptr<GroupModel>;


} //namespace

#endif // GROUPMODEL_H
