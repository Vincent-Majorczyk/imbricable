/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "groupmodel.h"

#include "Core/Properties/propertyinfo.h"

using namespace Imbrixamples::N200;
using namespace Imbricable::Properties;

int GroupModel::_count = 0;

GroupModel::GroupModel():
	CNamedObject(QString("Group %1").arg(_count++))
{
}

GroupModel::~GroupModel()=default;

QString GroupModel::toString(int indent) const
{
	QString result = QString(indent,' ') + QString("-GroupModel:%1\n").arg(this->name);

	for(auto& g:_items)
	{
		result += g->toString(indent+1);
	}
	return result;
}

