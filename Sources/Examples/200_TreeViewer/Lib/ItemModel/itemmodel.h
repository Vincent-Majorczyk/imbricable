/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef ITEMMODEL_H
#define ITEMMODEL_H

#include <memory>
#include "Core/Object/model.h"
#include "Core/Object/Interfaces/cnamedobject.h"
#include "../SubItemModel/subitemmodel.h"
#include <QList>

namespace Imbrixamples::N200 {

/**
 * @brief a simple example class
 */
class ItemModel: public Imbricable::Model, public Imbricable::CNamedObject
{
private:
	static int _count;

private:
	List_SSubItemModel _subitems;

public:
	/**
	  * @brief Main constructor
	  */
	ItemModel(QString name);

	/**
	  * @brief Main destructor
	  */
	~ItemModel() override;

	/**
	  * @brief get the groups
	  * @return list of items
	  */
	inline List_SSubItemModel& items() { return _subitems; }

	/**
	  * @brief set the items
	  * @param items list of items
	  */
	inline void setItems(List_SSubItemModel items) { _subitems = items; }

	virtual QString toString(int indent) const;
};

class ItemAModel: public ItemModel
{
	int _myint;
public:
	/**
	  * @brief Main constructor
	  */
	ItemAModel(int i = 0);

	/**
	  * @brief Main destructor
	  */
	~ItemAModel() override;

	/**
	 * @brief myint
	 * @return
	 */
	inline int myInt() { return _myint; }

	/**
	 * @brief setMyint
	 * @param i
	 */
	inline void setMyInt(int i) { _myint = i; }
};

class ItemBModel: public ItemModel
{
	QString _mystring;
public:
	/**
	 * @brief Main constructor
	 */
	ItemBModel(QString = "none");

	/**
	 * @brief Main destructor
	 */
	~ItemBModel() override;

	/**
	 * @brief myint
	 * @return
	 */
	inline QString myString() { return _mystring; }

	/**
	 * @brief setMyint
	 * @param s
	 */
	inline void setMyString(QString s) { _mystring = s; }
};

using SItemModel = std::shared_ptr<ItemModel>;
using WItemModel = std::weak_ptr<ItemModel>;
using List_SItemModel = QList<std::shared_ptr<ItemModel>>;

} //namespace

#endif // EXAMPLEMODEL_H
