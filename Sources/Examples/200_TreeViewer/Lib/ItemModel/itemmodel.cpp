/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "itemmodel.h"

using namespace Imbrixamples::N200;

int ItemModel::_count = 0;

ItemModel::ItemModel(QString name):
	CNamedObject(QString("%1 %2").arg(name).arg(_count++))
{
}

ItemModel::~ItemModel()=default;

QString ItemModel::toString(int indent) const
{
	QString result = QString(indent,' ') + QString("-ItemModel:%1\n").arg(this->name);

	for(auto& g: _subitems)
	{
		result += g->toString(indent+1);
	}
	return result;
}

ItemAModel::ItemAModel(int i):
	ItemModel("ItemA")
{
	_myint = i;
}

ItemAModel::~ItemAModel()
{}

ItemBModel::ItemBModel(QString s):
	ItemModel("ItemB")
{
	_mystring = s;
}

ItemBModel::~ItemBModel()
{}
