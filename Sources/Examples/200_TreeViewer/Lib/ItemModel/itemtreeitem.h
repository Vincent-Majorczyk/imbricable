/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef ITEMTREEITEM_H
#define ITEMTREEITEM_H


#include <QIcon>
#include <memory>
#include "Core/Object/model.h"
#include "Gui/SceneViewer/treenode.h"
#include "itemmodel.h"
#include "../GroupModel/groupmodel.h"

namespace Imbrixamples::N200 {

class ItemTreeItem: public Imbrigui::TreeNode
{
public:
	ItemTreeItem(SItemModel itemmodel, QIcon icon);

	void buildNode() override;

protected:
	Imbrigui::TreeNode::DropType acceptTreeNode(TreeNode *destNode, TreeNode *object) override;
	bool removeTreeNode(TreeNode *destNode, TreeNode *object) override;
	bool insertTreeNode(TreeNode *destNode, TreeNode *object, int index) override;
};

class ItemATreeItem: public ItemTreeItem
{
public:
	ItemATreeItem(SItemModel itemmodel);

private:
	static std::shared_ptr<QIcon> _icon;
public:
	static QIcon icon();
};

class ItemBTreeItem: public ItemTreeItem
{
public:
	ItemBTreeItem(SItemModel itemmodel);

private:
	static std::shared_ptr<QIcon> _icon;
public:
	static QIcon icon();
};

} //namespace

#endif // ITEMTREEITEM_H
