/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "itemtreeitem.h"
#include <QPixmap>
#include <QColor>

using namespace Imbrixamples::N200;

ItemTreeItem::ItemTreeItem(SItemModel itemmodel, QIcon icon):
	TreeNode(nullptr, itemmodel, itemmodel->name, icon)
{
}

void ItemTreeItem::buildNode()
{
	auto group = std::dynamic_pointer_cast<ItemModel>(_model);
	if(group == nullptr) return;

	bool ok;
	auto even = addNode("even", ok);
	auto odd = addNode("odd", ok);

	List_SSubItemModel& items = group->items();
	for(auto g: items)
	{
		if(g->IsEven())
			even->addNode(g,ok);
		else
			odd->addNode(g,ok);
	}
}

Imbrigui::TreeNode::DropType ItemTreeItem::acceptTreeNode(TreeNode *destNode, TreeNode * object)
{
	auto s = std::dynamic_pointer_cast<SubItemModel>(object->model());
	if (s != nullptr)
		return MOVE;
	return NONE;
}

bool ItemTreeItem::removeTreeNode(TreeNode *, TreeNode *object)
{
	auto s = std::dynamic_pointer_cast<SubItemModel>(object->model());
	if(s == nullptr) return false;

	auto group = std::dynamic_pointer_cast<ItemModel>(_model);
	if(group == nullptr) return false;

	group->items().removeAll(s);
	return true;
}

bool ItemTreeItem::insertTreeNode(TreeNode *, TreeNode *object, int index)
{
	auto s = std::dynamic_pointer_cast<SubItemModel>(object->model());
	if(s == nullptr) return false;

	auto group = std::dynamic_pointer_cast<ItemModel>(_model);
	if(group == nullptr) return false;

	group->items().append(s);
	return true;
}

std::shared_ptr<QIcon> ItemATreeItem::_icon = nullptr;

ItemATreeItem::ItemATreeItem(SItemModel itemmodel):
	ItemTreeItem(itemmodel, icon())
{
}

QIcon ItemATreeItem::icon()
{
	if(_icon != nullptr) return *_icon;
	QPixmap pixmap(16,16);
	pixmap.fill( Qt::blue );
	_icon = std::make_shared<QIcon>(pixmap);
	return *_icon;
}


std::shared_ptr<QIcon> ItemBTreeItem::_icon = nullptr;

ItemBTreeItem::ItemBTreeItem(SItemModel itemmodel):
	ItemTreeItem(itemmodel, icon())
{
}

QIcon ItemBTreeItem::icon()
{
	if(_icon != nullptr) return *_icon;
	QPixmap pixmap(16,16);
	pixmap.fill( Qt::cyan );
	_icon = std::make_shared<QIcon>(pixmap);
	return *_icon;
}
