/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "examplemodelconstructor.h"
#include "RootModel/roottreeitem.h"
#include "GroupModel/grouptreeitem.h"
#include "ItemModel/itemtreeitem.h"
#include "SubItemModel/subitemmodel.h"
#include <math.h>

using namespace Imbrixamples::N200;
using namespace Imbricable;
using namespace Imbrigui;

std::shared_ptr<RootModel> ExampleModelConstructor::createModel()
{
	std::shared_ptr<RootModel> rootmodel = std::make_shared<RootModel>();
	auto g1 = std::make_shared<GroupModel>();
	auto g2 = std::make_shared<GroupModel>();
	auto g3 = std::make_shared<GroupModel>();

	rootmodel->groups().append(g1);
	rootmodel->groups().append(g2);
	rootmodel->groups().append(g3);

	auto i1( std::make_shared<ItemAModel>() );
	auto i2( std::make_shared<ItemAModel>() );
	auto i3( std::make_shared<ItemBModel>() );

	g1->items().append(i1);
	g1->items().append(i2);
	g1->items().append(i3);

	i1->items().append( std::make_shared<SubItemModel>() );
	i1->items().append( std::make_shared<SubItemModel>() );
	i1->items().append( std::make_shared<SubItemModel>() );

	i2->items().append( std::make_shared<SubItemModel>() );
	i2->items().append( std::make_shared<SubItemModel>() );

	i3->items().append( std::make_shared<SubItemModel>() );
	i3->items().append( std::make_shared<SubItemModel>() );
	i3->items().append( std::make_shared<SubItemModel>() );

	return  rootmodel;
}

std::shared_ptr<TreeFactory> ExampleModelConstructor::createFactory()
{
	std::shared_ptr<TreeFactory> factory(new TreeFactory());
	factory->add<RootModel, RootTreeItem>();
	factory->add<GroupModel, GroupTreeItem>();
	factory->add<ItemAModel, ItemATreeItem>();
	factory->add<ItemBModel, ItemBTreeItem>();

	return factory;
}
