/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "factorybuilder.h"

#include "Examples/200_TreeViewer/Lib/GroupModel/groupmodel.h"
#include "Examples/200_TreeViewer/Lib/SubItemModel/subitemmodel.h"
#include "Examples/200_TreeViewer/Lib/RootModel/rootmodel.h"
#include "Examples/200_TreeViewer/Lib/ItemModel/itemmodel.h"

using namespace Imbrixamples::N200;

void FactoryBuilder::fillTypesFactory(Imbricable::S_TypeFactory factory)
{
	factory->append<N200::RootModel>("RootModel");
	factory->append<N200::GroupModel>("GroupModel");
	factory->append<N200::ItemAModel>("ItemAModel");
	factory->append<N200::ItemBModel>("ItemBModel");
	factory->append<N200::SubItemModel>("SubItemModel");
}
