/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef ROOTMODEL_H
#define ROOTMODEL_H

#include "Core/Object/model.h"
#include "Examples/200_TreeViewer/Lib/GroupModel/groupmodel.h"
#include <QList>

namespace Imbrixamples::N200 {

/**
 * @brief a simple example class
 */
class RootModel: public Imbricable::Model
{
private:
	QList<S_GroupModel> _groups;

public:
	/**
	 * @brief get the groups
	 * @return list of groups
	 */
	inline QList<S_GroupModel>& groups() { return _groups; }

public:
	/**
	 * @brief Main constructor
	 */
	RootModel();

	~RootModel() override;

	/**
	 * @brief toString
	 * @return a string which represent the object and it's content.
	 */
	QString toString(int indent=0) const;
};

using S_RootModel = std::shared_ptr<RootModel>;

} //namespace

#endif // ROOTMODEL_H
