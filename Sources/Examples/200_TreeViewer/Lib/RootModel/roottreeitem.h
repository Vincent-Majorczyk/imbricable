/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef ROOTTREEITEM_H
#define ROOTTREEITEM_H

#include <QIcon>
#include <memory>
#include "Core/Object/model.h"
#include "Gui/SceneViewer/treenode.h"
#include "rootmodel.h"
#include "../GroupModel/groupmodel.h"

namespace Imbrixamples::N200 {

class RootTreeItem: public Imbrigui::TreeNode
{
public:
	RootTreeItem(S_RootModel rootmodel);
	void buildNode() override;


	Imbrigui::TreeNode::DropType acceptTreeNode(TreeNode *destNode, TreeNode * object) override;
	bool removeTreeNode(TreeNode *, TreeNode *object) override;
	bool insertTreeNode(TreeNode *, TreeNode *object, int index) override;

private:
	static std::shared_ptr<QIcon> _icon;
	static QIcon createIcon();
};

} //namespace

#endif // ROOTTREEITEM_H
