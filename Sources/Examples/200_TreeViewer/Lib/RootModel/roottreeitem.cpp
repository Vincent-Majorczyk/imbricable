/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "roottreeitem.h"
#include <QPixmap>
#include <QColor>

using namespace Imbrixamples::N200;

std::shared_ptr<QIcon> RootTreeItem::_icon = nullptr;

QIcon RootTreeItem::createIcon()
{
	if(_icon != nullptr) return *_icon;
	QPixmap pixmap(16,16);
	pixmap.fill( Qt::black );
	_icon = std::make_shared<QIcon>(pixmap);
	return *_icon;
}

RootTreeItem::RootTreeItem(S_RootModel rootmodel):
	TreeNode(nullptr, rootmodel, "Root", createIcon())
{
}

void RootTreeItem::buildNode()
{
	auto root = std::dynamic_pointer_cast<RootModel>(_model);
	if(root == nullptr) return;

	QList<S_GroupModel> groups = root->groups();
	for(auto& g: groups)
	{
		bool ok;
		addNode(g,ok);
	}
}

Imbrigui::TreeNode::DropType RootTreeItem::acceptTreeNode(TreeNode *destNode, TreeNode * object)
{
	auto s = std::dynamic_pointer_cast<SubItemModel>(object->model());
	if (s != nullptr)
		return MOVE;
	return NONE;
}

bool RootTreeItem::removeTreeNode(TreeNode *, TreeNode *object)
{
	auto s = std::dynamic_pointer_cast<GroupModel>(object->model());
	if(s == nullptr) return false;

	auto root = std::dynamic_pointer_cast<RootModel>(_model);
	if(root == nullptr) return false;

	root->groups().removeAll(s);
	return true;
}

bool RootTreeItem::insertTreeNode(TreeNode *, TreeNode *object, int index)
{
	auto s = std::dynamic_pointer_cast<GroupModel>(object->model());
	if(s == nullptr) return false;

	auto root = std::dynamic_pointer_cast<RootModel>(_model);
	if(root == nullptr) return false;

	root->groups().insert(index,s);
	return true;
}
