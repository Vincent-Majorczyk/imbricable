/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "rootmodel.h"
#include "Core/Properties/propertyinfo.h"
#include <iostream>

using namespace Imbrixamples::N200;
using namespace Imbricable::Properties;

RootModel::RootModel()=default;

RootModel::~RootModel()=default;

QString RootModel::toString(int indent) const
{
	QString result = QString(indent,' ') + "-Root\n";

	for(auto& g:_groups)
	{
		result += g->toString(indent+1);
	}
	return result;
}
