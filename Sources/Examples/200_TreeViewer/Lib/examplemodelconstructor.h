/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef EXAMPLEMODELCONSTRUCTOR_H
#define EXAMPLEMODELCONSTRUCTOR_H

#include "RootModel/rootmodel.h"
#include "Gui/SceneViewer/treefactory.h"


namespace Imbrixamples::N200 {

class ExampleModelConstructor
{
public:
	static std::shared_ptr<RootModel> createModel();
	static std::shared_ptr<Imbrigui::TreeFactory> createFactory();

private:
	ExampleModelConstructor() = delete;
};

} // namespace

#endif // EXAMPLEMODELCONSTRUCTOR_H
