/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef SUBITEMMODEL_H
#define SUBITEMMODEL_H

#include <memory>
#include "Core/Object/model.h"
#include "Core/Object/Interfaces/cnamedobject.h"

namespace Imbrixamples::N200 {

/**
 * @brief a simple example class
 */
class SubItemModel: public Imbricable::Model, public Imbricable::CNamedObject
{
private:
	static int _count;

	int _value;

public:
	/**
	 * @brief Main constructor
	 */
	SubItemModel();

	/**
	 * @brief Main destructor
	 */
	~SubItemModel() override;

	inline bool IsEven(){ return _value%2 == 0; }

	virtual QString toString(int indent) const;
};

using S_SubItemModel = std::shared_ptr<SubItemModel>;
using W_SubItemModel = std::weak_ptr<SubItemModel>;
using List_SSubItemModel = QList< std::shared_ptr<SubItemModel> >;

} //namespace

#endif // SUBITEMMODEL_H
