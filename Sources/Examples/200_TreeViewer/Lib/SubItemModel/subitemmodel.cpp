/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "subitemmodel.h"
#include <QDebug>

using namespace Imbrixamples::N200;

int SubItemModel::_count = 0;

SubItemModel::SubItemModel():
	CNamedObject(QString("SubItem %1").arg(_count)),
	_value(_count++)
{
}

SubItemModel::~SubItemModel()=default;

QString SubItemModel::toString(int indent) const
{
	QString result = QString(indent,' ') + QString("-SubItemModel1:%1\n").arg(this->name);
	return result;
}
