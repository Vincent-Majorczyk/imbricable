/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "Examples/200_TreeViewer/Lib/RootModel/rootmodel.h"
#include "Examples/200_TreeViewer/Lib/RootModel/roottreeitem.h"
#include "Examples/200_TreeViewer/Lib/GroupModel/groupmodel.h"
#include "Examples/200_TreeViewer/Lib/GroupModel/grouptreeitem.h"
#include "Examples/200_TreeViewer/Lib/ItemModel/itemtreeitem.h"
#include "Examples/200_TreeViewer/Lib/examplemodelconstructor.h"

#include "Gui/SceneViewer/treewidget.h"

#include <QApplication>
#include <QMainWindow>
#include <iostream>
#include <QDebug>
#include <QTableWidget>
#include <QJsonDocument>

using namespace Imbricable;
using namespace Imbrigui;
using namespace Imbrixamples::N200;

namespace Imbrixamples {

/**
 * @brief this example aims to test the user interface about scene tree.
 */
namespace N200 {

/**
 * @brief the main function of the example
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char *argv[])
{
	qDebug() << "begin";
	QApplication a(argc, argv);

	std::shared_ptr<RootModel> rootmodel = ExampleModelConstructor::createModel();
	std::shared_ptr<Imbrigui::TreeFactory> factory = ExampleModelConstructor::createFactory();

	std::cout << "initial:" << std::endl << rootmodel->toString().toStdString() << std::endl;

	TreeWidget v;
	v.setFactory(factory);
	v.setImbricableModel(rootmodel);
	v.buildView();
	v.show();
	a.exec();

	std::cout << "final:" << std::endl << rootmodel->toString().toStdString() << std::endl;

	qDebug() << "end";
	return 1;
}

}} // namespaces

int main(int argc, char *argv[])
{
	return Imbrixamples::N200::main(argc,argv);
}

