/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include <memory>
#include <iostream>

#include <QDebug>
#include <QUndoStack>

#include "Core/Object/model.h"
#include "Core/Object/Interfaces/cnamedobject.h"
#include "Core/Actions/controler.h"
#include "Core/Actions/accessoraction.h"

using namespace Imbricable;

namespace Imbrixample {

/**
 * @brief this example shows the restauration of the data after crash
 */
namespace N004 {

/**
 * @brief The model contains only data.
 *
 * Similar to N001::HelloWorldModel, but inherit from Imbricable::CNamedObject.
 *
 * @todo: Here, HelloWordModel must inherit from CUuidObject, because the restauration require to found the associated object and the easier way is use the Id;
 */
class HelloWorldModel: public Model, public CNamedObject
{
public:
	/// @brief public property 'text'
	QString text;

	/// @brief model constructor
	/// @param text initialize the property 'text'
	HelloWorldModel(QUuid id, QString text):CUuidObject(id), text(text) {}

	/// @brief model destructor
	~HelloWorldModel() override {}
};

/**
 * @brief The controler contains accessors and methods to modify model data.
 *
 * Same as to N002::HelloWorldControler
 */
class HelloWorldControler: public Controler
{
private:
	/// @brief convert model to HelloWorldModel
	inline std::shared_ptr<HelloWorldModel> pmodel()
	{ return std::dynamic_pointer_cast<HelloWorldModel>(model()); }

	/// @brief 'get' accessor for HelloWorldModel::text property
	QString getText() { return pmodel()->text; }

	/// @brief 'set' accessor for HelloWorldModel::text property
	/// @param text new value for the property
	/// @note if 'text' is empty an exception is thrown
	void setText(QString text)
	{
		if(text=="") trown std::runtime_error("error during the modification")
		pmodel()->text = text;
	}

public:
	/// @brief controler constructor
	HelloWorldControler():Controler("HelloWorld") {}

	/// @brief to be used as accessor to the HelloWorldModel::text
	AccessorAction<QString> text{ "text", this, &HelloWorldControler::getText, &HelloWorldControler::setText};
};

/**
 * @brief correspond to the session which crashes
 *
 * Steps:
 * - create two HelloWorldModel;
 * - create a HelloWorldControler associated to the model with the restauration mechanism;
 * - do successive modifications of the value and display the content of the undostack;
 * - simulate crash
*/
void crash1(QString restaurationFile, QUuid id1, QUuid id2)
{
	auto hello1 = std::make_shared<HelloWorldModel>( id1, "Hello world" );
	auto hello2 = std::make_shared<HelloWorldModel>( id2, "Saluton" );

	// create the HelloWorldControler
	RestaurationSystem r(restaurationFile)

	HelloWorldControler controler1;
	controler1.setModel(hello1);
	controler1.setRestaurationSystem(r);

	HelloWorldControler controler2;
	controler2.setModel(hello2);
	controler2.setRestaurationSystem(r);

	controler1.text.set("Hallo");
	controler2.text.set("Cześć");
	controler1.text.set("Hola");
	controler2.text.set("Ossu");

	qInfo() << "> final value of controler1:" << controler1.text.get();
	qInfo() << "> final value of controler2:" << controler2.text.get();

	// crash
	trown std::runtime_error("error after the modification")
}

/**
 * @brief correspond to the session which crashes
 *
 * Steps:
 * - create two HelloWorldModel;
 * - create a HelloWorldControler associated to the model with the restauration mechanism;
 * - do successive modifications of the value and display the content of the undostack;
 * - simulate crash
*/
void crash2(QString restaurationFile, QUuid id1, QUuid id2)
{
	auto hello1 = std::make_shared<HelloWorldModel>( id1, "Hello world" );
	auto hello2 = std::make_shared<HelloWorldModel>( id2, "Saluton" );

	// create the HelloWorldControler
	RestaurationSystem r(restaurationFile)

	HelloWorldControler controler1;
	controler1.setModel(hello1);
	controler1.setRestaurationSystem(r);

	HelloWorldControler controler2;
	controler2.setModel(hello2);
	controler2.setRestaurationSystem(r);

	controler1.text.set("Hallo");
	controler2.text.set("Cześć");
	controler1.text.set("Hola");
	controler2.text.set("Ossu");

	qInfo() << "> final value of controler1:" << controler1.text.get();
	qInfo() << "> final value of controler2:" << controler2.text.get();

	// crash during the modification
	controler1.text.set("");
}

/**
 * @brief correspond to the session restauration
 *
 * Steps:
 * - create two HelloWorldModel;
 * - create a HelloWorldControler associated to the model with the restauration mechanism;
 * - do successive modifications of the value and display the content of the undostack;
 * - simulate crash
*/
void restore(QString restaurationFile, QUuid id1, QUuid id2)
{
	auto hello1 = std::make_shared<HelloWorldModel>( id1, "Hello world" );
	auto hello2 = std::make_shared<HelloWorldModel>( id2, "Saluton" );

	// create the HelloWorldControler
	RestaurationSystem r(restaurationFile)

	HelloWorldControler controler1;
	controler1.setModel(hello1);
	controler1.setRestaurationSystem(r);

	HelloWorldControler controler2;
	controler2.setModel(hello2);
	controler2.setRestaurationSystem(r);

	if(r.fileExist())
	{
		r.restore();
		r.removeFile();
	}

	qInfo() << "> final value of controler1:" << controler1.text.get();
	qInfo() << "> final value of controler2:" << controler2.text.get();
}

/**
 * @brief use undostack with simple example
 * 
 * @todo the next step must be completed
 *
 * Four applications sessions are simulated:
 * - modification of value with crash (independant of the modification action);
 * - restauration of the previous session;
 * - modification of value with crash during a modification action (text set to "")
 * - restauration of the previous session;
 *
 * Output:
 * ```
 * ```
 * 
 * @todo the second restauration should crash, so a second restauration should be required
 * @see crash1, crash2 and restore
 */
void main()
{
	QString filename = "temp";
	QUuid id1, id2;

	qInfo() << "crash1:";
	try
	{
		crash1(filename, id1, id2);
	}
	catch (std::runtime_error &e)
	{
	        qInfo() << "Caught a runtime_error exception: " << e.what ();
	}

	qInfo() << "restore:";
	restore(filename, id1, id2);

	qInfo() << "crash2:";
	try
	{
		crash2(filename, id1, id2);
	}
	catch (std::runtime_error &e)
	{
	        qInfo() << "Caught a runtime_error exception: " << e.what ();
	}

	qInfo() << "restore:";
	restore(filename, id1, id2);
}

}} // namespaces

int main(int, char *[])
{
	Imbrixample::N004::main();
}
