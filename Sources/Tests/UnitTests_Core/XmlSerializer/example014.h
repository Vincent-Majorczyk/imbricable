/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef EXAMPLE014_H
#define EXAMPLE014_H

#include "example013.h"

namespace Imbritest {

class Example014: public Imbricable::Model
{
public:
	/// @brief public property (list) for type 'Bar1Model'
	QList<S_Example013A> bar1;
	/// @brief public property (list) for type generic model ('Bar1Model' or 'Bar2Model' or 'FooModel')
	QList<Imbricable::S_Model> generic1;
	/// @brief public property (list) for type 'Bar1Model' or 'Bar2Model'
	QList<S_Example013> interface1;

	QString barToString(S_Example013 m);

	QString genToString(Imbricable::S_Model m);

	template<class TList>
	QString listToString(QString name, TList& list)
	{
		QString slist = name + "[";
		for(auto& item : list)
		{
			slist += "<" + genToString(item) + "> ";
		}
		return  slist + "]";
	}

	QString toString();
};

typedef std::shared_ptr<Example014> S_Example014;

/**
 * @brief class to serialize HelloWorldModel
 */
class Example014Serializer:
		public Imbricable::XmlTypedModelSerializer<Example014,Example014Serializer,1>
{
protected:
	void buildTree(Imbricable::S_Model m) override
	{
		auto hw = std::dynamic_pointer_cast<Example014>(m);
		// FooModel::bar1
		auto b1 = addNew<Imbricable::XmlModelSerializer_List<Example013ASerializer, QList, Example013A>>(this, "Bar1List");
		b1->setAccess(&hw->bar1);
		// FooModel::generic1
		auto g1 = addNew<Imbricable::XmlFactorySerializer_List<QList>>(this, "Generic1List");
		g1->setAccess(&hw->generic1);
		// FooModel::interface1
		auto i1 = addNew<Imbricable::XmlFactorySerializer_List<QList, Example013>>(this, "Interface");
		i1->setAccess(&hw->interface1);
	}

public:
	/// @brief serializer constructor
	Example014Serializer(XmlSerializer* parent):XmlTypedModelSerializer("Foo", parent) {}
};

} // namespace

#endif // EXAMPLE014_H
