/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "case02.h"

using namespace Imbritest;

Case02A::Case02A():
	link(this)
{

}

Case02B::Case02B()
{

}

Case02::Case02(int i):
	i(i)
{
	if(i==1)
	{
		this->a = std::make_shared<Case02A>();
		this->b = std::make_shared<Case02B>();

		this->b->id = QUuid("{bb725196-914c-42e8-ad0d-8b4d60b155a5}");
		this->a->link = Imbricable::Link(this->b, nullptr);
	}
}

bool Case02::isEqual(Imbricable::S_Model &model)
{
	auto m = std::dynamic_pointer_cast<Case02>(model);
	if(m == nullptr) return false;

	if(m->a != nullptr && this->a != nullptr)
	{
		if(m->a->link.id() != this->a->link.id()) return false;
		if(! m->a->link.isValid()) return false;
		if(! this->a->link.isValid()) return false;
	}

	if(m->b != nullptr && this->b != nullptr)
	{
		if(m->b->id != this->b->id) return false;
	}

	if(m->a != nullptr && this->a != nullptr && m->b != nullptr && this->b != nullptr)
	{
		if(i == 1)
		{
			auto ma = m->a->link.pointer<Case02B>();
			auto ta = this->a->link.pointer<Case02B>();
			auto mb = m->b;
			auto tb = this->b;

			if( ma == nullptr || ta == nullptr) return false;
			if( ma != mb ) return false;
			if( ta != tb ) return false;
		}
	}

	return true;
}


void Case02AXml::buildTree(std::shared_ptr<Imbricable::Model> m)
{
	auto hw = std::dynamic_pointer_cast<Case02A>(m);
	addNew<Imbricable::XmlProperty_Link>(this,"Link", &hw->link);
}

Case02AXml::Case02AXml(XmlSerializer* parent):
	XmlTypedModelSerializer( "Case02A",parent)
{
}

void Case02BXml::buildTree(std::shared_ptr<Imbricable::Model> m)
{
	auto hw = std::dynamic_pointer_cast<Case02B>(m);
	addNew<Imbricable::XmlProperty_QUuid>(this,"Id", &hw->id);
}

Case02BXml::Case02BXml(XmlSerializer* parent):
	XmlTypedModelSerializer("Case03",parent)
{
}

void Case02Xml::buildTree(std::shared_ptr<Imbricable::Model> m)
{
	auto hw = std::dynamic_pointer_cast<Case02>(m);
	addNew<Imbricable::XmlProperty_Int32>(this,"I", &hw->i);
	auto ia = addNew<Imbricable::XmlFactorySerializer_Typed<Case02A>>(this,"A");
	ia->setAccess(&hw->a);
	auto ib = addNew<Imbricable::XmlFactorySerializer_Typed<Case02B>>(this,"B");
	ib->setAccess(&hw->b);
}

Case02Xml::Case02Xml(XmlSerializer* parent):
	XmlTypedModelSerializer( "Case02",parent)
{
}
