/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "case01.h"

using namespace Imbricable;
using namespace Imbritest;

Case01::Case01()
{}

Case01::Case01(qint8 v08, qint16 v16, qint32 v32, qint64 v64, quint8 vi08, quint16 vi16, quint32 vi32, quint64 vi64, QString vs, QFileInfo fileinfo, QUrl url, bool vb):
	b1(vb), i08(v08), u08(vi08), i16(v16), u16(vi16),
	i32(v32), u32(vi32), i64(v64), u64(vi64),
	string(vs), fileinfo(fileinfo), url(url)
{
}

bool Case01::isEqual(S_Model &model)
{
	auto c = std::dynamic_pointer_cast<Case01>(model);
	if (!c) return false;

	return this->i08 == c->i08 &&
			this->i16 == c->i16 &&
			this->i32 == c->i32 &&
			this->i64 == c->i64 &&
			this->string == c->string &&
			this->fileinfo == c->fileinfo &&
			this->b1 == c->b1 &&
			this->url == c->url;
}

void Case01Xml::buildTree(std::shared_ptr<Imbricable::Model> m)
{
	auto hw = std::dynamic_pointer_cast<Case01>(m);
	addNew<XmlProperty_Int8>(this, "I08", &hw->i08);
	addNew<XmlProperty_Int16>(this, "I16", &hw->i16);
	addNew<XmlProperty_Int32>(this, "I32", &hw->i32);
	addNew<XmlProperty_Int64>(this, "I64", &hw->i64);
	addNew<XmlProperty_UInt8>(this, "U08", &hw->u08);
	addNew<XmlProperty_UInt16>(this, "U16", &hw->u16);
	addNew<XmlProperty_UInt32>(this, "U32", &hw->u32);
	addNew<XmlProperty_UInt64>(this, "U64", &hw->u64);
	addNew<XmlProperty_QString>(this,"String", &hw->string);
	addNew<XmlProperty_QFileInfo>(this,"Fileinfo", &hw->fileinfo);
	addNew<XmlProperty_QUrl>(this,"Url", &hw->url);
	addNew<XmlProperty_Bool>(this,"Boolean", &hw->b1);
}

Case01Xml::Case01Xml(XmlSerializer* parent):
	XmlTypedModelSerializer( "Case01",parent)
{
}
