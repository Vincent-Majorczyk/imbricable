/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef EXAMPLE011_H
#define EXAMPLE011_H

#include "Core/Registration/Xml/xml.h"

namespace Imbritest {

/**
 * @brief The model contains a text.
 *
 * @see Same as Imbrixamples::N011::HelloWorldModel & Imbrixamples::N012::FooModel
 */
class Example011: public Imbricable::Model
{
public:
	/// @brief public property 'text'
	QString text = "empty";
};

/**
 * @brief class to serialize Example011
 *
 * @see  Same as Imbrixamples::N011::HelloWorldSerializer
 */
class Example011Serializer: public Imbricable::XmlModelSerializer
{
protected:
	void buildTree(std::shared_ptr<Imbricable::Model> m) override;

public:
	/// @brief serializer constructor
	Example011Serializer(XmlSerializer *parent = nullptr);

	QString typeName() override;
	uint version() override;
	Imbricable::S_Model createModel() override;
	bool isType(Imbricable::S_Model) override;
	Imbricable::S_XmlModelSerializer createSerializer(XmlSerializer*) override;
};

} // namespace

#endif // EXAMPLE011_H
