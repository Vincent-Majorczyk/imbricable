/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "example013.h"

using namespace Imbricable;
using namespace Imbritest;

Example013A::Example013A(qint64 value)
{ this->number = value; }

QString Example013A::toString()
{
	return QString("A %1").arg(number);
}

Example013B::Example013B(qint64 value)
{ this->number = value; }

QString Example013B::toString()
{
	return QString("B %1").arg(number);
}

QString Example013C::barToString(S_Example013 m)
{
	return m? m->toString() : "null";
}

QString Example013C::genToString(Imbricable::S_Model m)
{
	if(!m) return "null";
	auto bar = std::dynamic_pointer_cast<Example013>(m);
	if(bar) return barToString(bar);
	auto foo = std::dynamic_pointer_cast<Example013C>(m);
	if(foo) return foo->toString();
	return "invalid";
}

QString Example013C::toString()
{
	return QString("Foo: <%1> <%2> <%3> <%4> <%5>")
			.arg(barToString(this->bar1),
				 barToString(this->bar2),
				 genToString(this->generic1),
				 genToString(this->generic2),
				 barToString(this->interface1));
}

void Example013CSerializer::buildTree(std::shared_ptr<Imbricable::Model> m)
{
	auto hw = std::dynamic_pointer_cast<Example013C>(m);
	// FooModel::bar1
	auto b1 = addNew<Example013ASerializer>(this);
	b1->setName("A");
	b1->setAccess(&hw->bar1);
	// FooModel::bar2
	auto b2 = addNew<Example013BSerializer>(this);
	b2->setName("B");
	b2->setAccess(&hw->bar2);
	// FooModel::generic1
	auto g1 = addNew<Imbricable::XmlFactorySerializer>(this, "G1");
	g1->setAccess(&hw->generic1);
	// FooModel::generic2
	auto g2 = addNew<Imbricable::XmlFactorySerializer>(this, "G2");
	g2->setAccess(&hw->generic2);
	// FooModel::interface1
	auto i1 = addNew<Imbricable::XmlFactorySerializer_Typed<Example013>>(this, "I");
	i1->setAccess(&hw->interface1);
}

Example013CSerializer::Example013CSerializer(XmlSerializer* parent):XmlTypedModelSerializer("Foo", parent)
{ }
