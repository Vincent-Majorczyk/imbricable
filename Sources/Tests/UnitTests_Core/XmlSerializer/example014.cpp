/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "example014.h"

using namespace Imbritest;

QString Example014::barToString(S_Example013 m)
{
	return m? m->toString() : "null";
}

QString Example014::genToString(Imbricable::S_Model m)
{
	if(!m) return "null";
	auto bar = std::dynamic_pointer_cast<Example013>(m);
	if(bar) return barToString(bar);
	auto foo = std::dynamic_pointer_cast<Example014>(m);
	if(foo) return foo->toString();
	return "invalid";
}


QString Example014::toString()
{
	return QString("Example014: <%1> <%2> <%3>")
			.arg(listToString("bar1", this->bar1),
				 listToString("gen1", this->generic1),
				 listToString("int1", this->interface1));
}
