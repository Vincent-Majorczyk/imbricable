/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "example015_v2.h"

#include <QDebug>

#include "Core/Registration/Xml/xml.h"

using namespace Imbricable;

using namespace Imbritest::Example15_v2;

BarModel_V1::BarModel_V1(qint8 value)
{ this->number = value; }

QString BarModel_V1::toString()
{ return QString("BarV1 %1").arg(number); }


BarModel_V2::BarModel_V2(qint16 value)
{ this->number = value; }


BarModel_V2::BarModel_V2(S_BarModel_V1 oldversion)
{ this->number = oldversion->number; }


QString BarModel_V2::toString()
{ return QString("BarV2 %1").arg(number); }


typedef BarModel_V2 BarModel;
typedef std::shared_ptr<BarModel_V2> S_BarModel;

void BarSerializer_V1::buildTree(std::shared_ptr<Model> m)
{
	auto hw = std::dynamic_pointer_cast<BarModel_V1>(m);
	this->template addNew<XmlProperty_Int8>(this, QString("Number8bits"), &hw->number);
}

BarSerializer_V1::BarSerializer_V1(XmlSerializer* parent, QString name):
	XmlTypedModelSerializer<BarModel_V1,BarSerializer_V1,1>("BarModel",parent, name)
{}

BarSerializer_V1::BarSerializer_V1(XmlSerializer* parent):
	XmlTypedModelSerializer<BarModel_V1,BarSerializer_V1,1>("BarModel", parent)
{}

S_Model BarSerializer_V1::upgrade()
{
	S_BarModel_V1 model0 = std::dynamic_pointer_cast<BarModel_V1>(this->model());
	S_BarModel model1 = std::make_shared<BarModel_V2>(model0);
	return model1;
}

void BarSerializer_V2::buildTree(std::shared_ptr<Model> m)
{
	auto hw = std::dynamic_pointer_cast<BarModel_V2>(m);
	this->template addNew<XmlProperty_Int16>(this, QString("Number16bits"), &hw->number);
}

BarSerializer_V2::BarSerializer_V2(XmlSerializer* parent, QString name):
	XmlTypedModelSerializer<BarModel_V2,BarSerializer_V2,2>("BarModel",parent, name)
{ }

BarSerializer_V2::BarSerializer_V2(XmlSerializer* parent):
	XmlTypedModelSerializer<BarModel_V2,BarSerializer_V2,2>("BarModel", parent)
{ }


S_XmlModelSerializer BarSerializer_V2::previous()
{
	return std::make_shared<BarSerializer_V1>(this->parent());
}

QString FooModel_V1::toString()
{
	return QString("Foo: <%1>").arg(this->bar->toString());
}

void FooSerializer_V1::buildTree(std::shared_ptr<Model> m)
{
	auto hw = std::dynamic_pointer_cast<FooModel>(m);
	auto b1 = addNew<BarSerializer>(this, "Bar");
	b1->setAccess(&hw->bar);
}

FooSerializer_V1::FooSerializer_V1(XmlSerializer* parent):
	XmlTypedModelSerializer("FooModel", parent) {}

typedef FooSerializer_V1 FooSerializer;
typedef std::shared_ptr<FooSerializer_V1> S_FooSerializer;

S_Model Imbritest::Example15_v2::createModel()
{
	qint16 value = 16;
	auto bar = std::make_shared<BarModel>(value);
	auto foo = std::make_shared<FooModel>();
	foo->bar = bar;
	return foo;
}

S_XmlModelSerializerFactoryList Imbritest::Example15_v2::createFactory()
{
	auto factory = std::make_shared<XmlModelSerializerFactoryList>();
	factory->append<FooSerializer>();
	factory->append<BarSerializer>();
	return factory;
}
