/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "example011.h"

using namespace Imbritest;
using namespace Imbricable;

void Example011Serializer::buildTree(std::shared_ptr<Model> m)
{
	auto hw = std::dynamic_pointer_cast<Example011>(m);
	addNew<XmlProperty_QString>(this, "Text", &hw->text);
}

Example011Serializer::Example011Serializer(XmlSerializer *parent):XmlModelSerializer(parent, "Example011")
{}

QString Example011Serializer::typeName()
{ return  "Example011"; }

uint Example011Serializer::version()
{ return 1; }

S_Model Example011Serializer::createModel()
{ return std::make_shared<Example011>(); }

bool Example011Serializer::isType(S_Model model)
{
	Model &m = *(model.get());
	return typeid(m) == typeid(Example011);
}

S_XmlModelSerializer Example011Serializer::createSerializer(XmlSerializer*parent)
{ return std::make_shared<Example011Serializer>(parent); }
