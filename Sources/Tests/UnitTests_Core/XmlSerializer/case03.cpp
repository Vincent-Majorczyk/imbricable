/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "case03.h"

using namespace Imbricable;
using namespace Imbritest;

Case03::Case03(uint i)
{
	switch (i)
	{
	case 1:
		fileinfo.append( QList<QFileInfo>{QFileInfo("example1.txt"), QFileInfo("example2.txt")} );
		return;
	}
}

bool Case03::isEqual(S_Model &model)
{
	auto c = std::dynamic_pointer_cast<Case03>(model);
	if (!c) return false;

	if(fileinfo.size() != c->fileinfo.size()) return false;
	for(int i=0;i<fileinfo.size();i++)
	{
		auto a = fileinfo[i];
		auto b = c->fileinfo[i];
		if(a!=b) return false;
	}

	return true;
}

void Case03Xml::buildTree(std::shared_ptr<Imbricable::Model> m)
{
	auto hw = std::dynamic_pointer_cast<Case03>(m);
	addNew<XmlProperty_QListQFileInfo>(this,"Fileinfo", &hw->fileinfo);
}

Case03Xml::Case03Xml(XmlSerializer* parent):
	XmlTypedModelSerializer( "Case03",parent)
{
}
