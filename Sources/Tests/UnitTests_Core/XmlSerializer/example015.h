/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef EXAMPLE15_H
#define EXAMPLE15_H

#include <QDebug>

#include "Core/Registration/Xml/xml.h"
#include "example015_v1.h"
#include "example015_v2.h"

namespace Imbritest
{

class Example15
{
public:
	/// @brief represents the xml which contains information about version classe;
	QString version;
	/// @brief represents the xml which contains information about model;
	QString model;

	/**
	 * @brief create and save a model to string with xml
	 * @return
	 */
	static Example15 save(S_Model model, S_XmlModelSerializerFactoryList factory);

	/**
	 * @brief load and display a model from a string with xml
	 * @param xmlContent
	 */
	static S_Model load(Example15 xmlContent, S_XmlModelSerializerFactoryList factory);
};

} // namespace

#endif // EXAMPLE15_H
