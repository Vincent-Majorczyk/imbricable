/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef CASE03_H
#define CASE03_H

#include "Core/Object/model.h"
#include "Core/Object/Interfaces/iequal.h"
#include "Core/Registration/Xml/xml.h"
#include <QUrl>

namespace Imbritest {

class Case03:
		public Imbricable::Model,
		public Imbricable::IEqualSModel
{
public:
	QList<QFileInfo> fileinfo;

	Case03(uint i=0);

	bool isEqual(Imbricable::S_Model& model) override;
};

class Case03Xml:
		public Imbricable::XmlTypedModelSerializer<Case03,Case03Xml, 1>
{
protected:
	void buildTree(std::shared_ptr<Imbricable::Model> m) override;

public:
	/// @brief serializer constructor
	Case03Xml(XmlSerializer* parent);
};

} // namespace

#endif // CASE03_H
