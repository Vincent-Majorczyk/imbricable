/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "example015.h"

using namespace Imbritest;

Example15 Example15::save(S_Model model, S_XmlModelSerializerFactoryList factory)
{
	Example15 out;

	// serialize FooModel
	// - create document
	QDomDocument docW1("Version");
	// - serialize version
	XmlVersionManager v(factory);
	docW1.appendChild(v.serialize(&docW1));
	out.version = docW1.toString();

	// create serializer
	S_XmlModelSerializer serializer = factory->createSerializer("FooModel",nullptr);
	serializer->setName("Main");
	// - create document
	QDomDocument docW2("Model");
	serializer->setDocument(&docW2);
	// - serialize FooModel & display
	serializer->setModel(model);
	docW2.appendChild(serializer->serialize());
	out.model = docW2.toString();

	return out;
}

/**
 * @brief load and display a model from a string with xml
 * @param xmlContent
 */
S_Model Example15::load(Example15 xmlContent, S_XmlModelSerializerFactoryList factory)
{
	// "deserialize" Version
	// - load document
	QDomDocument docR1("Version");
	docR1.setContent(xmlContent.version);
	// - manage version
	XmlVersionManager v(factory);
	auto node = docR1.firstChild();
	v.deserialize(node);

	// create serializer
	S_XmlModelSerializer serializer = factory->createSerializer("FooModel",nullptr);
	serializer->initialize(factory);
	serializer->setName("Main");

	// "deserialize" Model
	// - load document
	QDomDocument docR2("Model");
	docR2.setContent(xmlContent.model);
	// - deserialize
	//node = node.nextSibling();
	serializer->setDocument(&docR2);
	node = docR2.firstChild();
	serializer->deserialize(node);

	return serializer->model();
}
