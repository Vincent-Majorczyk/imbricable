/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef UT_XMLSERIALIZER_H
#define UT_XMLSERIALIZER_H

#include <QObject>
#include "Core/Object/model.h"

namespace Imbritest
{

class UT_XmlSerializer: public QObject
{
	Q_OBJECT

private slots:
	void initTestCase();

	/// @brief relative to Imbrixamples::N011
	void example011();

	/// @brief relative to Imbrixamples::N012
	void example012();

	/// @brief relative to Imbrixamples::N013
	void example013();

	/// @brief relative to Imbrixamples::N014
	void example014();

	/// @brief relative to Imbrixamples::N015
	void example015();

	/// @brief relative to Imbrixamples::N015
	void cases_data();
	void cases();

	void cleanupTestCase();
};

} // namespace

#endif // UT_XMLSERIALIZER_H
