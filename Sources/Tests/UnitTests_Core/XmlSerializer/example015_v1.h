/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef EXAMPLE15_V1_H
#define EXAMPLE15_V1_H

#include <QDebug>

#include "Core/Registration/Xml/xml.h"

using namespace Imbricable;

namespace Imbritest
{

/**
 * @brief contains the code of the version 1: classes (BarModel & FooModel), serializer and functions (createModel, createFactory)
 */
namespace Example15_v1 {

/**
 * @brief current version of the BarModel (version 1).
 *
 * Here, the model contains one property `number` (qint8);
 *
 * @note this class use the alias `BarModel`: the alias aims to avoid deep modification in the code
 *
 * @see become Version2::BarModel_V1 in the next version of the code
 */
class BarModel_V1: public Model
{
public:
	/// @brief public property 'number'
	qint8 number = 0;

	/// @brief constructor
	BarModel_V1(qint8 value = 0);

	/// @brief return content as string
	QString toString();
};

typedef BarModel_V1 BarModel;
typedef std::shared_ptr<BarModel_V1> S_BarModel;

/**
 * @brief class to serialize BarModel_V1
 *
 * @note this class use the alias `BarSerializer`: the alias aims to avoid deep modification in the code
 */
class BarSerializer_V1: public XmlTypedModelSerializer<BarModel_V1, BarSerializer_V1, 1>
{
protected:
	/// @brief build serializer
	void buildTree(std::shared_ptr<Model> m) override;

public:
	/// @brief constructor
	BarSerializer_V1(XmlSerializer* parent, QString name);

	/// @brief constructor
	BarSerializer_V1(XmlSerializer* parent);
};

typedef BarSerializer_V1 BarSerializer;
typedef std::shared_ptr<BarSerializer_V1> S_BarSerializer;

/**
 * @brief current version of the FooModel (version 1).
 *
 * Here, this class contains a S_BarModel_V1 (version 1)
 *
 * @note this class use the alias `FooModel`: the alias aims to avoid deep modification in the code
 */
class FooModel_V1: public Model
{
public:
	/// @brief public property for type 'generic1'
	S_BarModel bar;

	/// @brief return content as string
	QString toString();
};

typedef FooModel_V1 FooModel;
typedef std::shared_ptr<FooModel_V1> S_FooModel;

/**
 * @brief version 1 of the FooSerializer
 *
 * @note this class use the alias `FooSerializer`: the alias aims to avoid deep modification in the code
 */
class FooSerializer_V1: public XmlTypedModelSerializer<FooModel_V1, FooSerializer_V1, 1>
{
protected:
	/// @brief build serializer
	void buildTree(std::shared_ptr<Model> m) override;

public:
	/// @brief serializer constructor
	FooSerializer_V1(XmlSerializer* parent);
};

typedef FooSerializer_V1 FooSerializer;
typedef std::shared_ptr<FooSerializer_V1> S_FooSerializer;

/**
 * @brief create a model
 * @return
 */
S_Model createModel();

/**
 * @brief create a factory
 * @return
 */
S_XmlModelSerializerFactoryList createFactory();



}} // namespace


#endif // EXAMPLE15_V1_H
