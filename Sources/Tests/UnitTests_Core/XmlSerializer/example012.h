/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef EXAMPLE012A_H
#define EXAMPLE012A_H

#include "Core/Registration/Xml/xml.h"

namespace Imbritest {

/**
 * @brief The model contains a text.
 *
 * @see Same as Imbrixamples::N012::BarModel
 */
class Example012: public Imbricable::Model
{
public:
	/// @brief public property 'number'
	int number = 0;
};

/**
 * @brief class to serialize Example012
 *
 * @see Same as Imbrixamples::N012::BarModelSerializer
 */
class Example012Serializer: public Imbricable::XmlTypedModelSerializer<Example012,Example012Serializer, 1>
{
protected:
	void buildTree(std::shared_ptr<Imbricable::Model> m) override;

public:
	/// @brief serializer constructor
	Example012Serializer(XmlSerializer* parent);
};

} // namespace

#endif // EXAMPLE012A_H
