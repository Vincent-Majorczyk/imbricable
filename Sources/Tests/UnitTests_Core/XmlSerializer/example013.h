/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef EXAMPLE013_H
#define EXAMPLE013_H

#include "Core/Registration/Xml/xml.h"

namespace Imbritest {

/**
 * @brief The model contains a text.
 *
 * @see Same as Imbrixamples::N013::BarModel
 */
class Example013: public Imbricable::Model
{
public:
	/// @brief public property 'text'
	qint64 number = 0;

	/// @brief
	virtual QString toString() = 0;
};

/**
 * @brief simple inherited class with the overrided method 'toString'
 *
 * @see Same as Imbrixamples::N013::Bar1Model
 */
class Example013A: public Example013
{
public:
	Example013A(qint64 value = 0);

	QString toString() override;
};

/**
 * @brief simple inherited class with the overrided method 'toString'
 *
 * @see Same as Imbrixamples::N013::Bar2Model
 */
class Example013B: public Example013
{
public:
	Example013B(qint64 value = 0);

	QString toString() override;
};

/**
 * @brief class to serialize Example013, Example013A & Example013B
 *
 * @see Same as Imbrixamples::N013::BarSerializer
 */
template<class TClass, char TBarId>
class Example013Serializer: public Imbricable::XmlTypedModelSerializer<TClass,Example013Serializer<TClass,TBarId>,1>
{
protected:
	void buildTree(std::shared_ptr<Imbricable::Model> m) override
	{
		auto hw = std::dynamic_pointer_cast<Example013>(m);
		this->template addNew<Imbricable::XmlProperty_Int64>(this, QString("Number%1").arg(TBarId), &hw->number);
	}

public:
	/// @brief serializer constructor
	Example013Serializer(Imbricable::XmlSerializer* parent):
		Imbricable::XmlTypedModelSerializer<TClass,Example013Serializer<TClass,TBarId>,1>(QString("Bar%1").arg(TBarId),parent)
	{}
};

typedef Example013Serializer<Example013A,'A'> Example013ASerializer;
typedef Example013Serializer<Example013B,'B'> Example013BSerializer;
typedef std::shared_ptr<Example013A> S_Example013A;
typedef std::shared_ptr<Example013B> S_Example013B;
typedef std::shared_ptr<Example013> S_Example013;

/**
 * @brief The model contains only sub objects.
 *
 * @see Same as Imbrixamples::N013::FooModel
 */
class Example013C: public Imbricable::Model
{
public:
	/// @brief public property for type 'Bar1Model'
	S_Example013A bar1 = nullptr;
	/// @brief public property for type 'Bar1Model'
	S_Example013B bar2 = nullptr;
	/// @brief public property for type generic model ('Bar1Model' or 'Bar2Model' or 'FooModel')
	Imbricable::S_Model generic1 = nullptr;
	/// @brief public property for type generic model ('Bar1Model' or 'Bar2Model' or 'FooModel')
	Imbricable::S_Model generic2 = nullptr;
	/// @brief public property for type 'Bar1Model' or 'Bar2Model'
	S_Example013 interface1 = nullptr;

	QString barToString(S_Example013 m);
	QString genToString(Imbricable::S_Model m);
	QString toString();
};

/**
 * @brief class to serialize Example013C
 *
 * @see Same as Imbrixamples::N013::FooSerializer
 */
class Example013CSerializer: public Imbricable::XmlTypedModelSerializer<Example013C,Example013CSerializer,1>
{
protected:
	void buildTree(std::shared_ptr<Imbricable::Model> m) override;

public:
	/// @brief serializer constructor
	Example013CSerializer(XmlSerializer* parent);
};

typedef std::shared_ptr<Example013C> S_Example013C;

} // namespace

#endif // EXAMPLE013_H
