/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef CASE02_H
#define CASE02_H

#include "Core/Object/model.h"
#include "Core/Object/Interfaces/iuuidobject.h"
#include "Core/Object/Interfaces/iequal.h"
#include "Core/Object/Link/link.h"
#include "Core/Registration/Xml/xml.h"

namespace Imbritest {

class Case02A: public Imbricable::Model
{
public:
	Imbricable::Link link;

	Case02A();
};

typedef std::shared_ptr<Case02A> S_Case02A;

class Case02B: public Imbricable::Model, public Imbricable::IUuidObject
{
public:
	Case02B();
};

typedef  std::shared_ptr<Case02B> S_Case02B;

class Case02: public Imbricable::Model, public Imbricable::IEqualSModel
{
public:
	int i = 0;
	S_Case02A a = nullptr;
	S_Case02B b = nullptr;

	Case02(int i = 0);

	bool isEqual(Imbricable::S_Model& model) override;
};

class Case02AXml:
		public Imbricable::XmlTypedModelSerializer<Case02A,Case02AXml, 1>
{
protected:
	void buildTree(std::shared_ptr<Imbricable::Model> m) override;

public:
	/// @brief serializer constructor
	Case02AXml(XmlSerializer* parent);
};

class Case02BXml:
		public Imbricable::XmlTypedModelSerializer<Case02B,Case02BXml, 1>
{
protected:
	void buildTree(std::shared_ptr<Imbricable::Model> m) override;

public:
	/// @brief serializer constructor
	Case02BXml(XmlSerializer* parent);
};

class Case02Xml:
		public Imbricable::XmlTypedModelSerializer<Case02,Case02Xml, 1>
{
protected:
	void buildTree(std::shared_ptr<Imbricable::Model> m) override;

public:
	/// @brief serializer constructor
	Case02Xml(XmlSerializer* parent);
};

} // namespace

#endif // CASE02_H
