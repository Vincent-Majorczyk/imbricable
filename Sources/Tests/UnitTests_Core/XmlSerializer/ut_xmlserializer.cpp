/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "ut_xmlserializer.h"
#include "example011.h"
#include "example012.h"
#include "example013.h"
#include "example014.h"
#include "example015.h"
#include "example015_v1.h"
#include "example015_v2.h"
#include "case01.h"
#include "case02.h"
#include "case03.h"

#include "Core/Object/Interfaces/iequal.h"
#include "Core/Object/Link/linksmanager.h"

#include <QtTest>
#include <QHash>

using namespace Imbritest;
using namespace Imbricable;

void UT_XmlSerializer::initTestCase()
{
	qSetGlobalQHashSeed( 0 );
}


void UT_XmlSerializer::cleanupTestCase()
{
}

void UT_XmlSerializer::example011()
{
	// "serialize" operation
	// - create model
	auto hello = std::make_shared<Example011>();
	hello->text = "hello world";

	// - create document
	QDomDocument docW("ExampleDoc");

	// - create the HelloWorldSerializer
	Example011Serializer serializerW;
	serializerW.setDocument(&docW);
	serializerW.setModel(hello);

	// - serialize & display
	docW.appendChild(serializerW.serialize());
	QString output = docW.toString();
	QCOMPARE(output, "<!DOCTYPE ExampleDoc>\n<Example011>\n <Text>hello world</Text>\n</Example011>\n");

	// "deserialize" operation
	// - load document
	QDomDocument docR("ExampleDoc");
	docR.setContent(output);
	QCOMPARE(docR.toString(), "<!DOCTYPE ExampleDoc>\n<Example011>\n <Text>hello world</Text>\n</Example011>\n");

	// - create the zerializer
	Example011Serializer serializerR;
	serializerR.setDocument(&docR);

	// - deserialize & display
	serializerR.deserialize(docR.firstChild());
	auto hello2 = std::dynamic_pointer_cast<Example011>(serializerR.model());
	QCOMPARE(hello2->text, "hello world");
}

void UT_XmlSerializer::example012()
{
	// initialization
	// - create models
	auto foo = std::make_shared<Example011>();
	foo->text = "I'm foo";
	auto bar = std::make_shared<Example012>();
	bar->number = 42;

	// - createFactory
	auto factory = std::make_shared<XmlModelSerializerFactoryList>();
	factory->append<Example011Serializer>();
	factory->append<Example012Serializer>();

	// - create the serializer
	XmlFactorySerializer serializer(nullptr,"Example_011Or012");
	serializer.initialize(factory);

	// serialize FooModel
	// - create document
	QDomDocument docW1("Example_012");
	serializer.setDocument(&docW1);

	// - serialize FooModel & display
	serializer.setModel(foo);
	docW1.appendChild(serializer.serialize());
	QString output1 = docW1.toString();
	QCOMPARE(output1,"<!DOCTYPE Example_012>\n<Example_011Or012 type=\"Example011\">\n <Text>I'm foo</Text>\n</Example_011Or012>\n");

	// serialize BarModel
	// - create document
	QDomDocument docW2("Example_012");
	serializer.setDocument(&docW2);

	// - serialize BarModel & display
	serializer.setModel(bar);
	docW2.appendChild(serializer.serialize());
	QString output2 = docW2.toString();
	QCOMPARE(output2,"<!DOCTYPE Example_012>\n<Example_011Or012 type=\"Example012\">\n <Number>42</Number>\n</Example_011Or012>\n");

	// "deserialize" FooModel
	// - load document
	QDomDocument docR1("Example_012");
	docR1.setContent(output1);
	serializer.setDocument(&docR1);
	serializer.deserialize(docR1.firstChild());

	S_Model sm = serializer.model();
	auto hello2 = std::dynamic_pointer_cast<Example011>(serializer.model());
	QCOMPARE(hello2->text, "I'm foo");

	// "deserialize" BarModel
	// - load document
	QDomDocument docR2("Example_011Or012");
	docR2.setContent(output2);
	serializer.setDocument(&docR2);
	serializer.deserialize(docR2.firstChild());
	auto hello3 = std::dynamic_pointer_cast<Example012>(serializer.model());
	QCOMPARE(hello3->number, 42);
}

void UT_XmlSerializer::example013()
{
	// initialization
	// - create models
	auto foo = std::make_shared<Example013C>();
	auto b1 = std::make_shared<Example013A>(1024);
	auto b2 = std::make_shared<Example013B>(144);
	auto g1 = std::make_shared<Example013A>(42);
	auto g2 = std::make_shared<Example013C>();
	auto g2_b2 = std::make_shared<Example013B>(56);
	auto g2_i1 = std::make_shared<Example013A>(666);
	auto i1 = std::make_shared<Example013A>(912);


	foo->bar1 = b1; // Bar1 1024
	foo->bar2 = b2; // Bar2 144

	foo->generic1 = g1; // Bar1 42
	foo->generic2 = g2; // Foo
	g2->bar2 = g2_b2; // Bar2 56
	g2->interface1 = g2_i1; // Bar1 666

	foo->interface1 = i1; // Bar1 912

	// - createFactory
	auto factory = std::make_shared<XmlModelSerializerFactoryList>();
	factory->append<Example013CSerializer>();
	factory->append<Example013ASerializer>();
	factory->append<Example013BSerializer>();

	// - create the serializer
	auto serializer = std::make_shared<Example013CSerializer>(nullptr);
	serializer->initialize(factory);
	serializer->setName("Main");


	// serialize FooModel
	// - create document
	QDomDocument docW1("Example_013");
	serializer->setDocument(&docW1);

	// - serialize FooModel & display
	serializer->setModel(foo);
	docW1.appendChild(serializer->serialize());
	QString output = docW1.toString();
	QCOMPARE(output, "<!DOCTYPE Example_013>\n<Main>\n <A>\n  <NumberA>1024</NumberA>\n </A>\n <B>\n  <NumberB>144</NumberB>\n </B>\n <G1 type=\"BarA\">\n  <NumberA>42</NumberA>\n </G1>\n <G2 type=\"Foo\">\n  <A type=\"null\"/>\n  <B>\n   <NumberB>56</NumberB>\n  </B>\n  <G1 type=\"null\"/>\n  <G2 type=\"null\"/>\n  <I type=\"BarA\">\n   <NumberA>666</NumberA>\n  </I>\n </G2>\n <I type=\"BarA\">\n  <NumberA>912</NumberA>\n </I>\n</Main>\n");

	// "deserialize" FooModel
	// - load document
	QDomDocument docR1("Example_013");
	docR1.setContent(output);

	serializer->setDocument(&docR1);
	serializer->deserialize(docR1.firstChild());

	S_Model sm = serializer->model();
	auto hello = std::dynamic_pointer_cast<Example013C>(serializer->model());
	QCOMPARE(hello->toString(),"Foo: <A 1024> <B 144> <A 42> <Foo: <null> <B 56> <null> <null> <A 666>> <A 912>");
}

void UT_XmlSerializer::example014()
{
	// initialization
	// - create models
	auto foo = std::make_shared<Example014>();
	auto b1 = std::make_shared<Example013A>(1024);
	auto b2 = std::make_shared<Example013A>(144);
	auto g1 = std::make_shared<Example013B>(42);
	auto g2 = std::make_shared<Example014>();
	auto g2_b2 = std::make_shared<Example013A>(56);
	auto g2_i1 = std::make_shared<Example013A>(666);
	auto i1 = std::make_shared<Example013B>(912);
	auto i2 = std::make_shared<Example013A>(1793);

	foo->bar1.append(b1); // Bar1 1024
	foo->bar1.append(b2); // Bar1 144

	foo->generic1.append(g1); // Bar2 42
	foo->generic1.append(g2); // Foo
	g2->bar1.append(g2_b2); // Bar1 56
	g2->interface1.append(g2_i1); // Bar1 666

	foo->interface1.append(i1); // Bar2 912
	foo->interface1.append(i2); // Bar1 1793

	// - createFactory
	auto factory = std::make_shared<XmlModelSerializerFactoryList>();
	factory->append<Example014Serializer>();
	factory->append<Example013ASerializer>();
	factory->append<Example013BSerializer>();

	// - create the serializer
	Example014Serializer serializer(nullptr);
	serializer.setName("Main");
	serializer.initialize(factory);

	// serialize FooModel
	// - create document
	QDomDocument docW1("ListModelExample");
	serializer.setDocument(&docW1);

	// - serialize FooModel & display
	serializer.setModel(foo);
	docW1.appendChild(serializer.serialize());
	QString output = docW1.toString();

	QString excepted = "<!DOCTYPE ListModelExample>\n<Main>\n <Bar1List>\n  <Item index=\"0\">\n   <NumberA>1024</NumberA>\n  </Item>\n  <Item index=\"1\">\n   <NumberA>144</NumberA>\n  </Item>\n </Bar1List>\n <Generic1List>\n  <Item type=\"BarB\" index=\"0\">\n   <NumberB>42</NumberB>\n  </Item>\n  <Item type=\"Foo\" index=\"1\">\n   <Bar1List>\n    <Item index=\"0\">\n     <NumberA>56</NumberA>\n    </Item>\n   </Bar1List>\n   <Generic1List/>\n   <Interface>\n    <Item type=\"BarA\" index=\"0\">\n     <NumberA>666</NumberA>\n    </Item>\n   </Interface>\n  </Item>\n </Generic1List>\n <Interface>\n  <Item type=\"BarB\" index=\"0\">\n   <NumberB>912</NumberB>\n  </Item>\n  <Item type=\"BarA\" index=\"1\">\n   <NumberA>1793</NumberA>\n  </Item>\n </Interface>\n</Main>\n";

	QCOMPARE(output, excepted);

	// "deserialize" FooModel
	// - load document
	QDomDocument docR1("ListModelExample");
	docR1.setContent(output);

	serializer.setDocument(&docR1);
	serializer.deserialize(docR1.firstChild());

	S_Model sm = serializer.model();
	auto hello = std::dynamic_pointer_cast<Example014>(serializer.model());
	QCOMPARE(hello->toString(), "Example014: <bar1[<A 1024> <A 144> ]> <gen1[<B 42> <Example014: <bar1[<A 56> ]> <gen1[]> <int1[<A 666> ]>> ]> <int1[<B 912> <A 1793> ]>");
}

void UT_XmlSerializer::example015()
{
	// simulate version 1 of program: create, save and load a model
	// - create and save
	Example15 outV1 = Example15::save(Example15_v1::createModel(), Example15_v1::createFactory());
	QCOMPARE(outV1.version, "<!DOCTYPE Version>\n<RequiredVersion>\n <Item type=\"FooModel\" version=\"1\"/>\n <Item type=\"BarModel\" version=\"1\"/>\n</RequiredVersion>\n");
	QCOMPARE(outV1.model,"<!DOCTYPE Model>\n<Main>\n <Bar>\n  <Number8bits>14</Number8bits>\n </Bar>\n</Main>\n");

	// - load
	S_Model sm = Example15::load(outV1, Example15_v1::createFactory());
	auto hello = std::dynamic_pointer_cast<Example15_v1::FooModel>(sm);
	QCOMPARE(hello->toString(),"Foo: <BarV1 14>");

	// simulate version 2 of program: create, save and load a model
	// - create and save
	Example15 outV2 = Example15::save(Example15_v2::createModel(), Example15_v2::createFactory());
	QCOMPARE(outV2.version, "<!DOCTYPE Version>\n<RequiredVersion>\n <Item type=\"FooModel\" version=\"1\"/>\n <Item type=\"BarModel\" version=\"2\"/>\n</RequiredVersion>\n");
	QCOMPARE(outV2.model, "<!DOCTYPE Model>\n<Main>\n <Bar>\n  <Number16bits>16</Number16bits>\n </Bar>\n</Main>\n");

	// load
	sm = Example15::load(outV2, Example15_v2::createFactory());
	auto hello2 = std::dynamic_pointer_cast<Example15_v2::FooModel>(sm);
	QCOMPARE(hello2->toString(), "Foo: <BarV2 16>");

	// simulate version 2 of program:load a model from version 1
	sm = Example15::load(outV1, Example15_v2::createFactory());
	auto hello3 = std::dynamic_pointer_cast<Example15_v2::FooModel>(sm);
	QCOMPARE(hello3->toString(),"Foo: <BarV2 14>");
	Example15 outV3 = Example15::save(sm, Example15_v2::createFactory());
	QCOMPARE(outV3.version,"<!DOCTYPE Version>\n<RequiredVersion>\n <Item type=\"FooModel\" version=\"1\"/>\n <Item type=\"BarModel\" version=\"2\"/>\n</RequiredVersion>\n");
	QCOMPARE(outV3.model,"<!DOCTYPE Model>\n<Main>\n <Bar>\n  <Number16bits>14</Number16bits>\n </Bar>\n</Main>\n");
}

void UT_XmlSerializer::cases_data()
{
	QTest::addColumn<int>("nmodel");
	QTest::newRow("case01 a") << 1;
	QTest::newRow("case01 b") << 2;
	QTest::newRow("case02") << 4;
	QTest::newRow("case03") << 3;
}

void getData(int i, Imbricable::S_Model& model, S_XmlModelSerializerFactoryList& factory, QString& result)
{
	factory = std::make_shared<XmlModelSerializerFactoryList>();
	factory->append<Case01Xml>();
	factory->append<Case02Xml>();
	factory->append<Case02AXml>();
	factory->append<Case02BXml>();
	factory->append<Case03Xml>();

	switch (i)
	{
	case 1:
		model = std::make_shared<Case01>(50,98,6555,555,65,789,87435,7778,"fooString", QFileInfo("./readme.txt"), QUrl("https://gitlab.com/Vincent-Majorczyk/imbricable"), true);
		result = "<!DOCTYPE Model>\n<Main>\n <I08>50</I08>\n <I16>98</I16>\n <I32>6555</I32>\n <I64>555</I64>\n <U08>65</U08>\n <U16>789</U16>\n <U32>87435</U32>\n <U64>7778</U64>\n <String>fooString</String>\n <Fileinfo>./readme.txt</Fileinfo>\n <Url>https://gitlab.com/Vincent-Majorczyk/imbricable</Url>\n <Boolean>True</Boolean>\n</Main>\n";
		return;
	case 2:
		model = std::make_shared<Case01>(-50,-98,-655,-99,255,19000,4545455,7777,"barString",QFileInfo("./test.txt"), QUrl("https://example.com"), false);
		result = "<!DOCTYPE Model>\n<Main>\n <I08>-50</I08>\n <I16>-98</I16>\n <I32>-655</I32>\n <I64>-99</I64>\n <U08>255</U08>\n <U16>19000</U16>\n <U32>4545455</U32>\n <U64>7777</U64>\n <String>barString</String>\n <Fileinfo>./test.txt</Fileinfo>\n <Url>https://example.com</Url>\n <Boolean>False</Boolean>\n</Main>\n";
		return;
	case 3:
		model = std::make_shared<Case03>(1);
		result = "<!DOCTYPE Model>\n<Main>\n <Fileinfo>\n  <Item>example1.txt</Item>\n  <Item>example2.txt</Item>\n </Fileinfo>\n</Main>\n";
		return;
	case 4:
		model = std::make_shared<Case02>(1);
		result = "<!DOCTYPE Model>\n<Main>\n <I>1</I>\n <A type=\"Case02A\">\n  <Link>{bb725196-914c-42e8-ad0d-8b4d60b155a5}</Link>\n </A>\n <B type=\"Case03\">\n  <Id>{bb725196-914c-42e8-ad0d-8b4d60b155a5}</Id>\n </B>\n</Main>\n";
		return;
	}
}

void UT_XmlSerializer::cases()
{
	QFETCH(int, nmodel);

	auto & i = Imbricable::LinksManager::getInstance();
	i.removeAll();

	S_Model model;
	S_XmlModelSerializerFactoryList factory;
	QString result;

	getData(nmodel,model,factory,result);

	// create serializer
	S_XmlModelSerializer serializer = factory->createSerializer(model,nullptr);
	serializer->setName("Main");
	// - create document
	QDomDocument docW2("Model");
	serializer->setDocument(&docW2);
	// - serialize FooModel & display
	serializer->setModel(model);
	docW2.appendChild(serializer->serialize());
	QString xml = docW2.toString();

	qInfo() << "xml result:" << xml;
	QCOMPARE(xml, result);

	i.removeAll();

	// "deserialize" Model
	// - load document
	QDomDocument docR2("Model");
	docR2.setContent(xml);
	// - deserialize
	//node = node.nextSibling();
	serializer->setDocument(&docR2);
	auto node = docR2.firstChild();
	serializer->deserialize(node);

	auto mtc = std::dynamic_pointer_cast< IEqualSModel >(serializer->model());

	QVERIFY(mtc != nullptr);
	QVERIFY(mtc->isEqual(model));

	i.removeAll();
}


