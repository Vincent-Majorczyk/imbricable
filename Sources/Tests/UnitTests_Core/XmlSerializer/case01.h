/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef CASE01_H
#define CASE01_H

#include "Core/Object/model.h"
#include "Core/Object/Interfaces/iequal.h"
#include "Core/Registration/Xml/xml.h"
#include <QUrl>

namespace Imbritest {

class Case01:
		public Imbricable::Model,
		public Imbricable::IEqualSModel
{
public:
	bool b1 = false;
	bool b2 = false;
	qint8 i08 = 0;
	quint8 u08 = 0;
	qint16 i16 = 0;
	quint16 u16 = 0;
	qint32 i32 = 0;
	quint32 u32 = 0;
	qint64 i64 = 0;
	quint64 u64 = 0;
	QString string = "";
	QFileInfo fileinfo = QFileInfo();
	QUrl url = QUrl();

	Case01();

	Case01(qint8 v08, qint16 v16, qint32 v32, qint64 v64, quint8 vi08, quint16 vi16, quint32 vi32, quint64 vi64,QString vs, QFileInfo fileinfo, QUrl url, bool vb);

	bool isEqual(Imbricable::S_Model& model) override;
};

class Case01Xml:
		public Imbricable::XmlTypedModelSerializer<Case01,Case01Xml, 1>
{
protected:
	void buildTree(std::shared_ptr<Imbricable::Model> m) override;

public:
	/// @brief serializer constructor
	Case01Xml(XmlSerializer* parent);
};

} // namespace

#endif // CASE01_H
