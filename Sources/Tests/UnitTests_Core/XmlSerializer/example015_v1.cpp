/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "example015_v1.h"

#include <QDebug>

#include "Core/Registration/Xml/xml.h"

using namespace Imbricable;
using namespace Imbritest::Example15_v1;

BarModel_V1::BarModel_V1(qint8 value)
{ this->number = value; }

QString BarModel_V1::toString()
{ return QString("BarV1 %1").arg(number); }


void BarSerializer_V1::buildTree(std::shared_ptr<Model> m)
{
	auto hw = std::dynamic_pointer_cast<BarModel_V1>(m);
	this->template addNew<XmlProperty_Int8>(this, QString("Number8bits"), &hw->number);
}

BarSerializer_V1::BarSerializer_V1(XmlSerializer* parent, QString name):
	XmlTypedModelSerializer<BarModel_V1,BarSerializer_V1,1>("BarModel",parent, name)
{ }

	/// @brief constructor
BarSerializer_V1::BarSerializer_V1(XmlSerializer* parent):
	XmlTypedModelSerializer<BarModel_V1,BarSerializer_V1,1>("BarModel", parent)
{ }

QString FooModel_V1::toString()
{
	return QString("Foo: <%1>").arg(this->bar->toString());
}

void FooSerializer_V1::buildTree(std::shared_ptr<Model> m)
{
	auto hw = std::dynamic_pointer_cast<FooModel>(m);
	auto b1 = addNew<BarSerializer>(this, "Bar");
	b1->setAccess(&hw->bar);
}

FooSerializer_V1::FooSerializer_V1(XmlSerializer* parent):
	XmlTypedModelSerializer("FooModel", parent)
{}

S_Model Imbritest::Example15_v1::createModel()
{
	qint8 value = 14;
	auto bar = std::make_shared<BarModel>(value);
	auto foo = std::make_shared<FooModel>();
	foo->bar = bar;
	return foo;
}

S_XmlModelSerializerFactoryList Imbritest::Example15_v1::createFactory()
{
	S_XmlModelSerializerFactoryList factory = std::make_shared<XmlModelSerializerFactoryList>();
	factory->append<FooSerializer>();
	factory->append<BarSerializer>();
	return factory;
}
