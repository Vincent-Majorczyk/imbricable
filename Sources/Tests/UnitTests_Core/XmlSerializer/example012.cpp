/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "example012.h"

using namespace Imbricable;
using namespace Imbritest;

void Example012Serializer::buildTree(std::shared_ptr<Imbricable::Model> m)
{
	auto hw = std::dynamic_pointer_cast<Example012>(m);
	addNew<XmlProperty_Int32>(this, "Number", &hw->number);
}


Example012Serializer::Example012Serializer(XmlSerializer* parent):
	XmlTypedModelSerializer( "Example012",parent) {}
