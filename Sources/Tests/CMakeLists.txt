#
# @copyright Licence CeCILL-C
# @date 2019
# @author Vincent Majorczyk
# @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
# @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#

add_subdirectory(TestTools)

if(IMBRICABLE_EXAMPLES)
	add_subdirectory(UnitTests_Math)
	add_subdirectory(UnitTests_Core)
endif()
