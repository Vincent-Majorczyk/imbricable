#
# @copyright Licence CeCILL-C
# @date 2019
# @author Vincent Majorczyk
# @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
# @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#

file(
		GLOB_RECURSE
		source_files
		*.cpp *.h
)

set(EXECUTABLE_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/bin/${CMAKE_BUILD_TYPE})
include_directories(${CMAKE_SOURCE_DIR}
	"${CMAKE_SOURCE_DIR}/Sources")

# Tell CMake to create the helloworld executable
add_executable(UnitTest_Math ${source_files})

enable_testing()
add_test(NAME UnitTest_Math COMMAND UnitTest_Math)

# Use the Widgets module from Qt 5
target_link_libraries(UnitTest_Math Qt5::Test Imbrimath Imbritest)
