/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include <QtTest/QtTest>
#include <QCoreApplication>
#include "Tests/TestTools/testmanager.h"
#include <iostream>

#include "test_discretefunction.h"

namespace Imbricable {

/**
 * @brief Unit testing
 */
namespace UnitTesting {

/**
 * @brief Unit testing relative to the imbrimath library
 */
namespace Math {

int main(int argc, char *argv[])
{
	using namespace Imbricable::UnitTest;

	//Log::initialize();
	QCoreApplication app(argc, argv);

	TestManager manager(argc, argv);

	QElapsedTimer timer;
	timer.start();

	manager.test<Test_DiscreteFunction>();

	auto elapsed = timer.elapsed();
	std::cout << "Total failed: " << manager.failled() << std::endl;
	std::cout << "Elapsed: " << elapsed << " ms" <<std::endl;
	std::cout.flush();

	return manager.failled();
}

}}} // namespaces

int main(int argc, char *argv[])
{
	return Imbricable::UnitTesting::Math::main(argc,argv);
}
