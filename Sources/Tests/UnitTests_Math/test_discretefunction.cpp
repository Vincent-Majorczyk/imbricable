#include "test_discretefunction.h"
#include <QtTest>

using namespace Imbricable::UnitTesting::Math;

void Test_DiscreteFunction::initTestCases()
{
	// 5 samples
	fmi = new Imbricable::Functions::DiscreteFunction();
	QMap<double,double> mapmi;
	mapmi.insert(0,0);
	mapmi.insert(2,1);
	mapmi.insert(6,2);
	mapmi.insert(8,6);
	mapmi.insert(10,10);
	fmi->setSamples(mapmi);

	omi = new Imbricable::Functions::OptimizedDiscreteFunction(*fmi);
	omi_ms = Imbricable::Functions::OptimizedDiscreteFunction::createForConsecutiveMemoryAllocation(*fmi);

	fmd = new Imbricable::Functions::DiscreteFunction();
	QMap<double,double> mapmd;
	mapmd.insert(0,10);
	mapmd.insert(2,6);
	mapmd.insert(4,2);
	mapmd.insert(8,1);
	mapmd.insert(10,0);
	fmd->setSamples(mapmd);

	omd = new Imbricable::Functions::OptimizedDiscreteFunction(*fmd);
	omd_ms = Imbricable::Functions::OptimizedDiscreteFunction::createForConsecutiveMemoryAllocation(*fmd);

	// 50 samples
	fmi2 = new Imbricable::Functions::DiscreteFunction();
	QMap<double,double> mapmi2;
	for(double i=0;i<50;i++)mapmi2.insert(i*0.2,i*0.2);
	fmi2->setSamples(mapmi2);
	omi2 = new Imbricable::Functions::OptimizedDiscreteFunction(*fmi2);
	omi2_ms = Imbricable::Functions::OptimizedDiscreteFunction::createForConsecutiveMemoryAllocation(*fmi2);

	// 500 samples
	fmi3 = new Imbricable::Functions::DiscreteFunction();
	QMap<double,double> mapmi3;
	for(double i=0;i<500;i++)mapmi3.insert(i*0.02,i*0.02);
	fmi3->setSamples(mapmi3);
	omi3 = new Imbricable::Functions::OptimizedDiscreteFunction(*fmi3);
}


void Test_DiscreteFunction::cleanupTestCase()
{
	delete fmi;
	delete omi;
	Imbricable::Functions::OptimizedDiscreteFunction::deleteConsecutiveMemoryAllocation(omi_ms);

	delete fmd;
	delete omd;
	Imbricable::Functions::OptimizedDiscreteFunction::deleteConsecutiveMemoryAllocation(omd_ms);

	delete fmi2;
	delete omi2;
	Imbricable::Functions::OptimizedDiscreteFunction::deleteConsecutiveMemoryAllocation(omi2_ms);
}

void Test_DiscreteFunction::createDataForMonotonic()
{
	QTest::addColumn<double>("x");
	QTest::addColumn<double>("y");

	QTest::addRow(" 0") << 0.0 << 0.0;
	QTest::addRow(" 1") << 1.0 << 0.5;
	QTest::addRow(" 2") << 2.0 << 1.0;
	QTest::addRow(" 3") << 3.0 << 1.25;
	QTest::addRow(" 4") << 4.0 << 1.5;
	QTest::addRow(" 5") << 5.0 << 1.75;
	QTest::addRow(" 6") << 6.0 << 2.0;
	QTest::addRow(" 7") << 7.0 << 4.0;
	QTest::addRow(" 8") << 8.0 << 6.0;
	QTest::addRow(" 9") << 9.0 << 8.0;
	QTest::addRow("10") << 10.0 << 10.0;
}

void Test_DiscreteFunction::testIncreasingMonotonic_data()
{
	createDataForMonotonic();
}

void Test_DiscreteFunction::testIncreasingMonotonic()
{
	QFETCH(double, x);
	QFETCH(double, y);

	QCOMPARE(fmi->y(x), y);
	QCOMPARE(fmi->x(y), x);
}

void Test_DiscreteFunction::testIncreasingMonotonic_optimized_data()
{
	createDataForMonotonic();
}

void Test_DiscreteFunction::testIncreasingMonotonic_optimized()
{
	QFETCH(double, x);
	QFETCH(double, y);

	QCOMPARE(omi->y(x), y);
	QCOMPARE(omi->x(y), x);
}

void Test_DiscreteFunction::testIncreasingMonotonic_optimized_memorystrategie_data()
{
	createDataForMonotonic();
}

void Test_DiscreteFunction::testIncreasingMonotonic_optimized_memorystrategie()
{
	QFETCH(double, x);
	QFETCH(double, y);

	QCOMPARE(omi_ms->y(x), y);
	QCOMPARE(omi_ms->x(y), x);
}

void Test_DiscreteFunction::testDecreasingMonotonic_data()
{
	createDataForMonotonic();
}

void Test_DiscreteFunction::testDecreasingMonotonic()
{
	QFETCH(double, x);
	QFETCH(double, y);

	QCOMPARE(fmd->y(10-x), y);
	QCOMPARE(fmd->x(y), 10-x);
}

void Test_DiscreteFunction::testDecreasingMonotonic_optimized_data()
{
	createDataForMonotonic();
}

void Test_DiscreteFunction::testDecreasingMonotonic_optimized()
{
	QFETCH(double, x);
	QFETCH(double, y);

	QCOMPARE(omd->y(10-x), y);
	QCOMPARE(omd->x(y), 10-x);
}

void Test_DiscreteFunction::testDecreasingMonotonic_optimized_memorystrategy_data()
{
	createDataForMonotonic();
}

void Test_DiscreteFunction::testDecreasingMonotonic_optimized_memorystrategy()
{
	QFETCH(double, x);
	QFETCH(double, y);

	QCOMPARE(omd_ms->y(10-x), y);
	QCOMPARE(omd_ms->x(y), 10-x);
}

void Test_DiscreteFunction::testMonotonicLimits()
{
	QCOMPARE(fmi->y(-1), 0);
	QCOMPARE(fmi->y(11), 10);
	QCOMPARE(fmi->x(-1), 0);
	QCOMPARE(fmi->x(11), 10);

	QCOMPARE(omi->y(-1), 0);
	QCOMPARE(omi->y(11), 10);
	QCOMPARE(omi->x(-1), 0);
	QCOMPARE(omi->x(11), 10);

	QCOMPARE(fmd->y(11), 0);
	QCOMPARE(fmd->y(-1), 10);
	QCOMPARE(fmd->x(-1), 10);
	QCOMPARE(fmd->x(11), 0);

	QCOMPARE(omd->y(11), 0);
	QCOMPARE(omd->y(-1), 10);
	QCOMPARE(omd->x(-1), 10);
	QCOMPARE(omd->x(11), 0);
}

void Test_DiscreteFunction::testBenchmark_data()
{
	QTest::addColumn<int>("testcase");

	QTest::newRow("5) increasing monotonic y(x)") << 0;
	QTest::newRow("5) increasing monotonic optimized y(x)") << 1;
	QTest::newRow("5) decreasing monotonic y(x)") << 2;
	QTest::newRow("5) decreasing monotonic optimized y(x)") << 3;
	QTest::newRow("5) increasing monotonic optimized ms y(x)") << 4;
	QTest::newRow("5) decreasing monotonic optimized ms y(x)") << 5;
	QTest::newRow("50) increasing monotonic y(x)") << 6;
	QTest::newRow("50) increasing monotonic optimized y(x)") << 7;
	QTest::newRow("50) increasing monotonic optimized ms y(x)") << 8;
	QTest::newRow("500) increasing monotonic optimized y(x)") << 9;
	QTest::newRow("500) increasing monotonic optimized ms y(x)") << 10;
}

void Test_DiscreteFunction::testBenchmark()
{
	QFETCH(int, testcase);
	double result;

	switch (testcase)
	{
	case 0:
		QBENCHMARK
		{
			result = fmi->y(10);
		}
		break;
	case 1:
		QBENCHMARK
		{
			result = omi->y(10);
		}
		break;
	case 2:
		QBENCHMARK
		{
			result = fmd->y(10);
		}
		break;
	case 3:
		QBENCHMARK
		{
			result = omd->y(10);
		}
		break;
	case 4:
		QBENCHMARK
		{
			result = omi_ms->y(10);
		}
		break;
	case 5:
		QBENCHMARK
		{
			result = omd_ms->y(10);
		}
		break;
	case 6:
		QBENCHMARK
		{
			result = fmi2->y(10);
		}
		break;
	case 7:
		QBENCHMARK
		{
			result = omi2->y(10);
		}
		break;
	case 8:
		QBENCHMARK
		{
			result = omi2_ms->y(10);
		}
		break;
	case 9:
		QBENCHMARK
		{
			result = fmi3->y(10);
		}
		break;
	case 10:
		QBENCHMARK
		{
			result = omi3->y(10);
		}
		break;
	}

	Q_UNUSED(result)
}
