#ifndef TEST_DISCRETEFUNCTION_H
#define TEST_DISCRETEFUNCTION_H

#include <QObject>
#include "Math/Functions/discretefunction.h"
#include "Math/Functions/optimizeddiscretefunction.h"

namespace Imbricable::UnitTesting::Math
{

class Test_DiscreteFunction: public QObject
{
	Q_OBJECT

	Imbricable::Functions::DiscreteFunction* fmi;
	Imbricable::Functions::OptimizedDiscreteFunction* omi;
	Imbricable::Functions::OptimizedDiscreteFunction* omi_ms;
	Imbricable::Functions::DiscreteFunction* fmd;
	Imbricable::Functions::OptimizedDiscreteFunction* omd;
	Imbricable::Functions::OptimizedDiscreteFunction* omd_ms;
	Imbricable::Functions::DiscreteFunction* fmi2;
	Imbricable::Functions::OptimizedDiscreteFunction* omi2;
	Imbricable::Functions::OptimizedDiscreteFunction* omi2_ms;
	Imbricable::Functions::DiscreteFunction* fmi3;
	Imbricable::Functions::OptimizedDiscreteFunction* omi3;

	void createDataForMonotonic();

private slots:
	void initTestCases();
	void testIncreasingMonotonic_data();
	void testIncreasingMonotonic();
	void testIncreasingMonotonic_optimized_data();
	void testIncreasingMonotonic_optimized();
	void testIncreasingMonotonic_optimized_memorystrategie_data();
	void testIncreasingMonotonic_optimized_memorystrategie();
	void testDecreasingMonotonic_data();
	void testDecreasingMonotonic();
	void testDecreasingMonotonic_optimized_data();
	void testDecreasingMonotonic_optimized();
	void testDecreasingMonotonic_optimized_memorystrategy_data();
	void testDecreasingMonotonic_optimized_memorystrategy();
	void testMonotonicLimits();
	void testBenchmark_data();
	void testBenchmark();

	void cleanupTestCase();
};

} // namespace

#endif // TEST_DISCRETEFUNCTION_H
