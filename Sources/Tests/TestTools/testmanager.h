/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef TESTHYDRAULICCOMPONENTS_H
#define TESTHYDRAULICCOMPONENTS_H

#include <QtTest/QtTest>
#include <QObject>

namespace Imbricable {

/**
 * @brief
 */
namespace UnitTest {

class TestManager
{
	int _failled = 0;
	int _argc;
	char** _argv;
public:
	TestManager(int argc, char** argv);

	template<class M, typename... Args>
	inline int test(Args... args)
	{
		M test(args...);
		_failled += QTest::qExec(&test, _argc, _argv);

		return _failled;
	}

	inline int failled()
	{
		return  _failled;
	}
};

}} // namespace

#endif // TESTHYDRAULICCOMPONENTS_H
