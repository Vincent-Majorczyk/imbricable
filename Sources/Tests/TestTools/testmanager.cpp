/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "testmanager.h"
#include <iostream>

using namespace Imbricable::UnitTest;

TestManager::TestManager(int argc, char** argv):
	_argc(argc),
	_argv(argv)
{

}
