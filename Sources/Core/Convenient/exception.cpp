#include "exception.h"

using namespace Imbricable;

Exception::Exception(const QString& message, const char* function)
{
	_msg = function;
	_msg += " : ";
	_msg += message.toStdString();
}

Exception::~Exception() noexcept = default;

const char* Exception::what() const noexcept
{ return _msg.c_str(); }
