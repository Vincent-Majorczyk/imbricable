#ifndef LOG_H
#define LOG_H

#include <memory>

#include <QLoggingCategory>
#include <QFile>
#include <QFileInfo>

namespace Imbricable {

class LogCatcher
{
	bool _active;
public:
	virtual ~LogCatcher();
	virtual void sendMessage(QtMsgType type, const QMessageLogContext &context, const QString &msg) = 0;

	void setActive(bool active) { _active = active; }
	bool isActive(){ return _active; }
};

/**
 * @brief The Log class
 *
 * @see https://evileg.com/en/post/154/
 */
class Log
{
public:
	enum LogMode
	{
		Simple,
		Complete
	};

private:
	static Log _log;
	static LogMode _mode;

	// Smart pointer to LogInterface
	static QMap<QString, QList<std::weak_ptr<LogCatcher>> > _logCatcher;

	// handler
	static void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

	static bool _forceConsole;

public:
	Log() = delete;

	static void initialize();

	inline static void setFilterRules(const QString& rules){ QLoggingCategory::setFilterRules(rules); }

	static QMap<QString, QList<std::weak_ptr<LogCatcher>> >& logInterface(){ return _logCatcher; }
	static void addLogInterface(const QLoggingCategory&cat, std::weak_ptr<LogCatcher> catcher);

	inline static void setForceConsole(bool fc){_forceConsole=fc;}

	inline static LogMode mode() { return _mode; }
	inline static void setMode(LogMode mode) { _mode = mode; }
};

class FileLogCatcher: public LogCatcher
{
	QString _suffix = "";
	std::shared_ptr<QFile> _file;
	bool _removeIfExist = false;

public:
	~FileLogCatcher() override;
	void sendMessage(QtMsgType type, const QMessageLogContext &context, const QString &msg) override;

	void newFile(QString suffix, bool removeIfExist = false);
	void closeFile();
	QFileInfo file();
};

class ObjectLogCatcher: public QObject, public FileLogCatcher
{
	Q_OBJECT

public:
	void sendMessage(QtMsgType type, const QMessageLogContext &context, const QString &msg) override;

signals:
	void messageSended(QtMsgType type, const QMessageLogContext &context, const QString &msg);
};

} // namespace

#endif
