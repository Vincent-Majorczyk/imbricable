#include "log.h"
#include <QTextStream>
#include <QDateTime>
#include <QFile>
#include <QDir>
#include <iostream>

using namespace Imbricable;

QMap<QString, QList<std::weak_ptr<LogCatcher>> > Log::_logCatcher;
bool Log::_forceConsole = true;
Log::LogMode Log::_mode = Log::Complete;

void Log::messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
	QString category = context.category;
	QString function = context.function;
	QString file = context.file;
	int line = context.line;
	//int version = context.version;

	// search the name of log file
	if(category.isEmpty()) return;
	auto items = category.split("."); // split
	QString filename = items.first();

	// get the
	auto i = _logCatcher.find(filename);

	// if exists logCatcher
	if(i != _logCatcher.end())
	{
		QList<std::weak_ptr<LogCatcher>> &list = *i;
		for(std::weak_ptr<LogCatcher>& c : list)
		{
			std::shared_ptr<LogCatcher> lc = c.lock();
			if(lc!=nullptr) lc->sendMessage(type,context,msg);
		}
	}

	if(i == _logCatcher.end() || _forceConsole)
	{
		// open stream
		QTextStream out(stdout);

		if(_mode == Log::Complete)
		{
			// write the date of the recording
			out << "[" << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz ");
			if(!function.isEmpty()) out << file << ":" << function << "l" << line;
			out << "] ";
		}

		// determine the message level
		switch (type)
		{
		case QtInfoMsg:     out << "INFO  "; break;
		case QtDebugMsg:    out << "DEBUG "; break;
		case QtWarningMsg:  out << "WARN  "; break;
		case QtCriticalMsg: out << "CRIT  "; break;
		case QtFatalMsg:    out << "FATAL "; break;
		}

		out << context.category << ": " << msg << Qt::endl;
	}
}

void Log::addLogInterface(const QLoggingCategory& cat, std::weak_ptr<LogCatcher> catcher)
{
	QString name = cat.categoryName();
	if(name.isEmpty()) return;
	auto items = name.split("."); // split
	_logCatcher[items.first()].append(catcher);
}

void Log::initialize()
{
	qInstallMessageHandler(Log::messageHandler);
}

LogCatcher::~LogCatcher()
{
}

FileLogCatcher::~FileLogCatcher()
{
	this->closeFile();
}

void FileLogCatcher::newFile(QString suffix, bool removeIfExist)
{
	_removeIfExist = removeIfExist;
	_suffix = suffix;
	if(_file != nullptr && _file->isOpen()) _file->close();
}

void FileLogCatcher::closeFile()
{
	if(_file != nullptr && _file->isOpen())
	{
		if(_file->isOpen()) _file->close();
		_file.reset();
	}
}

QFileInfo FileLogCatcher::file()
{
	if(_file == nullptr) return  QFileInfo();
	return QFileInfo(_file->fileName());
}

void FileLogCatcher::sendMessage(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
	bool isNew = false;

	QString category = context.category;
	QString function = context.function;
	QString file = context.file;
	int line = context.line;

	if(_file != nullptr && !_file->isOpen()) _file.reset();

	if(_file == nullptr)
	{
		if(category.isEmpty()) return;
		auto items = category.split("."); // split
		QString filename = items.first();

		_file = std::make_shared<QFile>(filename+_suffix+".log");

		if(_removeIfExist) _file->open(QFile::Append | QFile::Truncate);
		else _file->open(QFile::Append | QFile::Text);
		isNew = true;
	}

	if(!_file->isOpen()) return;

	// open stream
	QTextStream out(_file.get());

	if(Log::mode() == Log::Complete)
	{
		if(isNew)
		{
			out << "date,time,file,function,line,type,category,message" << Qt::endl;
		}

		// write the date of the recording
		out << QDateTime::currentDateTime().toString("yyyy-MM-dd,hh:mm:ss.zzz,");
		if(!function.isEmpty()) out << file << "," << function << "," << line << ",";
		else out << ",,,";
	}

	// determine the message level
	switch (type)
	{
	case QtInfoMsg:     out << "INFO  ,"; break;
	case QtDebugMsg:    out << "DEBUG ,"; break;
	case QtWarningMsg:  out << "WARN  ,"; break;
	case QtCriticalMsg: out << "CRIT  ,"; break;
	case QtFatalMsg:    out << "FATAL ,"; break;
	}

	out << context.category << "," << msg << Qt::endl;
}

void ObjectLogCatcher::sendMessage(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
	emit messageSended(type,context,msg);
}
