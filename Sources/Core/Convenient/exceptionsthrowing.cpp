/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "exceptionsthrowing.h"

using namespace Imbricable;


void ExceptionsThrowing::throwJsonRegistrationNodeException(const QString& alias)
{
	QString exception = QString("Exception: JsonRegistrationFactory have not RegistrationNode associated to %1").arg(alias);
	throw std::runtime_error(exception.toStdString());
}

void ExceptionsThrowing::throwConverterException(const QString& name)
{
	QString exception = QString("Exception: RegistrationFactory have not converter associated to %1").arg(name);
	throw std::runtime_error(exception.toStdString());
}

void ExceptionsThrowing::throwAliasException(const QString& name)
{
	QString exception = QString("Exception: TypeFactory have not alias associated to %1").arg(name);
	throw std::runtime_error(exception.toStdString());
}

void ExceptionsThrowing::throwNullUuidException(const QString& name)
{
	QString exception = QString("Exception: Link which points to object with null id (%1)").arg(name);
	throw std::runtime_error(exception.toStdString());
}
