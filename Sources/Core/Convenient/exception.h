#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <QString>
#include <stdexcept>

namespace Imbricable {

class Exception : public std::exception
{
private:
	std::string _msg;

	Exception(const QString& message, const char* function);

public:
	inline Exception():
		Exception(QString(), __FUNCTION__)
	{
	}

	inline Exception(const QString& message):
		Exception(message, __FUNCTION__)
	{
	}

	~Exception() noexcept override;

	const char* what() const noexcept override;
};

}

#endif // EXCEPTION_H
