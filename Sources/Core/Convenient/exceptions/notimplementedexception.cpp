/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "notimplementedexception.h"

NotImplementedException::NotImplementedException(const char* message, const char* function):
	std::logic_error("Not Implemented")
{
	_text = message;
	_text += " : ";
	_text += function;
}

const char *NotImplementedException::what() const noexcept
{
	return _text.c_str();
}
