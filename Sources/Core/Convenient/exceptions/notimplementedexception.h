/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef NOTIMPLEMENTEDEXCEPTION_H
#define NOTIMPLEMENTEDEXCEPTION_H
#pragma once

#include <stdexcept>

class NotImplementedException : public std::logic_error
{
private:
	std::string _text;

	NotImplementedException(const char* message, const char* function);
public:

	NotImplementedException():
		NotImplementedException("Not Implememented", __FUNCTION__)
	{
	}

	NotImplementedException(const char* message):
		NotImplementedException(message, __FUNCTION__)
	{
	}

	const char *what() const noexcept override;
};

#endif // NOTIMPLEMENTEDEXCEPTION_H
