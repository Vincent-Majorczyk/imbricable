/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef EXCEPTIONSTHROWING_H
#define EXCEPTIONSTHROWING_H

#include <QString>
#include <stdexcept>

namespace Imbricable {



class ExceptionsThrowing
{
public:
	/// @defgroup Factories Exceptions relative to factories
	/// @{

	/**
	  * @brief exception when JsonRegistrationFactory have not RegistrationNode associated to a specific type
	  */
	[[noreturn]] static void throwJsonRegistrationNodeException(const QString& alias);

	/**
	  * @brief exception when RegistrationFactory have not converter associated to a specific type
	  */
	[[noreturn]] static void throwConverterException(const QString&name);

	/**
	  * @brief  exception when TypeFactory have not alias associated to a specific type
	  */
	[[noreturn]] static void throwAliasException(const QString& name);

	/// @}

	/**
	  * @brief  exception when TypeFactory have not alias associated to a specific type
	  */
	[[noreturn]] static void throwNullUuidException(const QString&name);

	ExceptionsThrowing() = delete;
};

} // namespace

#endif // EXCEPTIONSTHROWING_H
