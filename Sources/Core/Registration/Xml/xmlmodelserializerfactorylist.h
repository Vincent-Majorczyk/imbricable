/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef XMLMODELSERIALIZERFACTORY_H
#define XMLMODELSERIALIZERFACTORY_H

#include <memory>
#include <QList>

#include "xmlserializer.h"
#include "Core/Object/model.h"

namespace Imbricable
{

class XmlModelSerializer;

class XmlModelSerializerFactoryList: public std::enable_shared_from_this<XmlModelSerializerFactoryList>
{
	QList<std::shared_ptr<XmlModelSerializer>> _serializers;

public:
	XmlModelSerializerFactoryList();

	inline QList<std::shared_ptr<XmlModelSerializer>>& list() { return _serializers; }

	std::shared_ptr<XmlModelSerializer> createSerializer(S_Model model, XmlSerializer* parent);
	std::shared_ptr<XmlModelSerializer> createSerializer(QString typeName, XmlSerializer* parent);
	std::shared_ptr<XmlModelSerializer> createSerializerWithFactory(S_Model model, XmlSerializer* parent);
	std::shared_ptr<XmlModelSerializer> createSerializerWithFactory(QString typeName, XmlSerializer* parent);

	void append(std::shared_ptr<XmlModelSerializer> serializer);

	void setVersion(QString typeName, uint version);
	uint getVersion(QString typeName);
	void resetVersion();
	S_Model upgrade(S_Model model);

	std::shared_ptr<XmlModelSerializer> getOriginal(QString typeName);

	template<class C>
	inline std::shared_ptr<XmlModelSerializer> append()
	{
		auto c = std::make_shared<C>(nullptr);
		this->append(c);
		return c;
	}
};

typedef std::shared_ptr<XmlModelSerializerFactoryList> S_XmlModelSerializerFactoryList;

} // namespace

#endif // XMLMODELSERIALIZERFACTORY_H
