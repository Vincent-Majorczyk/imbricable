#include "xmlhelper.h"

#include "xmlfactoryserializer.h"

#include <QTextStream>

using namespace Imbricable;

void XmlHelper::save(S_XmlModelSerializerFactoryList factory, QFileInfo file, S_Model model, QString docName, QString rootName)
{
	XmlFactorySerializer serializer(nullptr, rootName);
	serializer.initialize(factory);

	QDomDocument doc(docName);
	serializer.setDocument(&doc);

	serializer.setModel(model);
	doc.appendChild(serializer.serialize());

	QFile ffile(file.absoluteFilePath());
	if( !ffile.open( QIODevice::WriteOnly | QIODevice::Text ) )
	{
		throw std::runtime_error("failled to save file");
	}
	QTextStream stream( &ffile );
	doc.save(stream, 4);

	ffile.close();
}

S_Model XmlHelper::load(S_XmlModelSerializerFactoryList factory, QFileInfo file, QString docName, QString rootName)
{
	QDomDocument doc(docName);
	QFile ffile(file.absoluteFilePath());
	if (!ffile.open(QIODevice::ReadOnly))
		return nullptr;
	if (!doc.setContent(&ffile)) {
		ffile.close();
		return nullptr;
	}
	ffile.close();

	XmlFactorySerializer serializer(nullptr,rootName);
	serializer.initialize(factory);

	serializer.setDocument(&doc);
	serializer.deserialize(doc.firstChild());
	return serializer.model();
}
