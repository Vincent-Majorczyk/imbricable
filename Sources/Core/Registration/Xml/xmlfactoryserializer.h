/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef XMLFACTORYSERIALIZER_H
#define XMLFACTORYSERIALIZER_H

#include "xmlmodelserializer.h"

namespace Imbricable {

class XmlFactorySerializer_Base: public XmlNodeSerializer
{
public:
	XmlFactorySerializer_Base(XmlSerializer* parent, QString name);

	QDomNode serialize() override;
	void deserialize(QDomNode node) override;
};

class XmlFactorySerializer: public XmlFactorySerializer_Base
{
	std::shared_ptr<Model>* _access = nullptr;
public:
	XmlFactorySerializer(XmlSerializer* parent, QString name);
	XmlFactorySerializer(XmlSerializer* parent, QString name, std::shared_ptr<Model>* access);

	void setAccess(std::shared_ptr<Model>* access);

	void setModelToAccess() override;

	void getModelFromAccess() override;
};

template<class TInterface>
class XmlFactorySerializer_Typed: public XmlFactorySerializer_Base
{
	std::shared_ptr<TInterface>* _access = nullptr;
public:
	XmlFactorySerializer_Typed(XmlSerializer* parent, QString name):
		XmlFactorySerializer_Base(parent, name)
	{ }

	XmlFactorySerializer_Typed(XmlSerializer* parent, QString name, std::shared_ptr<TInterface>* access):
		XmlFactorySerializer_Base(parent, name),
		_access(access)
	{ }

	void setAccess(std::shared_ptr<TInterface>* access)
	{
		_access = access;
	}

	void setModelToAccess() override
	{
		if(_access != nullptr)
			(*_access) = std::dynamic_pointer_cast<TInterface>(this->model());
	}

	void getModelFromAccess() override
	{
		if(_access != nullptr)
			this->setModel(*_access);
	}
};

} // namespace

#endif // XMLFACTORYSERIALIZER_H
