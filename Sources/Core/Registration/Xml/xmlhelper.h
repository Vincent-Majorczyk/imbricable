#ifndef XMLHELPER_H
#define XMLHELPER_H

#include "xmlmodelserializerfactorylist.h"
#include <QFileInfo>

namespace Imbricable {

class XmlHelper
{
private:
	XmlHelper() = delete;
	XmlHelper(const XmlHelper&) = delete;
	XmlHelper& operator=(const XmlHelper&) = delete;


public:
	static void save(S_XmlModelSerializerFactoryList factory, QFileInfo file, S_Model model, QString docName , QString rootName);

	inline static void save(S_XmlModelSerializerFactoryList factory, QFileInfo file, S_Model model, QString docName)
	{ save(factory,file,model,docName,docName); }

	static S_Model load(S_XmlModelSerializerFactoryList factory, QFileInfo file, QString docName, QString rootName);

	inline static S_Model load(S_XmlModelSerializerFactoryList factory, QFileInfo file, QString docName)
	{ return load(factory,file,docName,docName); }

};

} // namespace

#endif // XMLHELPER_H
