/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef XMLNODESERIALIZER_H
#define XMLNODESERIALIZER_H

#include <memory>
#include <QString>
#include <QtXml/QDomDocument>
#include <QList>

#include <Core/Object/model.h>
#include "xmlserializer.h"
#include "xmlmodelserializerfactorylist.h"

namespace Imbricable
{

class XmlNodeSerializer: public XmlSerializer
{

	S_Model _model;

public:
	XmlNodeSerializer(XmlSerializer* parent, QString name);
	XmlNodeSerializer(XmlSerializer* parent);

	inline S_Model model() { return _model; }
	virtual void setModel(S_Model model);

	virtual void setModelToAccess();
	virtual void getModelFromAccess();

	void addToLLinkManager();
};

} // namespace

#endif // XMLNODESERIALIZER_H
