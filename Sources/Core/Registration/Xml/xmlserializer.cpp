/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "xmlserializer.h"
#include "xmlmodelserializerfactorylist.h"

using namespace Imbricable;

XmlSerializer::XmlSerializer(XmlSerializer* parent, QString name):
	_name(name),
	_parent(parent)
{ }

XmlSerializer::XmlSerializer(XmlSerializer* parent):
	_parent(parent)
{ }

XmlSerializer::~XmlSerializer() = default;

void XmlSerializer::initialize(std::shared_ptr<XmlModelSerializerFactoryList> factory)
{
	_factory = factory;
}

QString XmlSerializer::completeName()
{
	QString name = "";
	XmlSerializer * current = this;
	while(current != nullptr)
	{
		name = current->name() + "::" + name;
		current = current->parent();
	}

	return name;
}
