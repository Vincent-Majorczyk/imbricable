#ifndef XMLVERSIONMANAGER_H
#define XMLVERSIONMANAGER_H

#include <QtXml/QDomDocument>
#include <QList>

#include "xmlmodelserializerfactorylist.h"

namespace Imbricable {

class XmlVersionManager
{
	S_XmlModelSerializerFactoryList _factory;

public:
	XmlVersionManager(S_XmlModelSerializerFactoryList factory);

	QDomNode serialize(QDomDocument* _document);
	void deserialize(QDomNode node);
};

} // namespace

#endif // XMLVERSIONMANAGER_H
