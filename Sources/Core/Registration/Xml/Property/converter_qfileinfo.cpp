#include "converter_qfileinfo.h"

using namespace Imbricable;

QString Converter_QFileInfo::convert(QFileInfo*input)
{
	return input->filePath();
}

void Converter_QFileInfo::convert(QString data, bool *ok, QFileInfo *output)
{
	*output = QFileInfo(data);
	*ok = true;
}
