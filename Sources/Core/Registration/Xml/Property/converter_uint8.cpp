/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "converter_uint8.h"

using namespace Imbricable;

QString Converter_UInt8::convert(quint8 *input)
{
	return QString::number(*input);
}

void Converter_UInt8::convert(QString data, bool*ok, quint8 *output)
{
	*output = static_cast<quint8>(data.toUShort(ok));
}

