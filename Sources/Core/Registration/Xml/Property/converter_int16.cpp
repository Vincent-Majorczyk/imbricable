/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "converter_int16.h"

using namespace Imbricable;

QString Converter_Int16::convert(qint16 *input)
{
	return QString::number(*input);
}

void Converter_Int16::convert(QString data, bool*ok, qint16 *output)
{
	*output = data.toShort(ok);
}

