/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef CONVERTER_UUID_H
#define CONVERTER_UUID_H

#include <QString>
#include <QUuid>

namespace Imbricable
{

class Converter_QUuid
{
public:
	static QString convert(QUuid *input);
	static void convert(QString data, bool *ok, QUuid*output);
};

} // namespace

#endif // CONVERTER_UUID_H
