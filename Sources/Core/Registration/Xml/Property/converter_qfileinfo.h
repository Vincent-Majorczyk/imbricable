/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef CONVERTER_QFILEINFO_H
#define CONVERTER_QFILEINFO_H

#include <QString>
#include <QFileInfo>

namespace Imbricable
{

class Converter_QFileInfo
{
public:
	static QString convert(QFileInfo*input);
	static void convert(QString data, bool *ok, QFileInfo*output);
};

} // namespace

#endif // CONVERTER_QFILEINFO_H
