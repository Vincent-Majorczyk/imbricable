/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef CONVERTER_INT64_H
#define CONVERTER_INT64_H

#include <QString>

namespace Imbricable
{

class Converter_Int64
{
public:
	static QString convert(qint64 *input);
	static void convert(QString data, bool *ok, qint64* output);
};

} // namespace

#endif // CONVERTER_INT64_H
