/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef CONVERTER_INT8_H
#define CONVERTER_INT8_H

#include <QString>

namespace Imbricable
{

class Converter_Int8
{
public:
	static QString convert(qint8* input);
	static void convert(QString data, bool *ok, qint8* output);
};

} // namespace

#endif // CONVERTER_INT8_H
