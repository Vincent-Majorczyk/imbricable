/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef CONVERTER_FLOAT_H
#define CONVERTER_FLOAT_H

#include <QString>

namespace Imbricable
{

class Converter_Float
{
public:
	static QString convert(float *input);
	static void convert(QString data, bool *ok, float* output);
};

} // namespace

#endif // CONVERTER_FLOAT_H
