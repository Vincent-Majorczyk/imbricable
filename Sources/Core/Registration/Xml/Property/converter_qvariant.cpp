#include "converter_qvariant.h"

using namespace Imbricable;

QString Converter_QVariant::convert(QVariant* input)
{
	return input->toString();
}

void Converter_QVariant::convert(QString data, bool*ok, QVariant *output)
{
	*output = QVariant(data);
	*ok = output->isValid();
}
