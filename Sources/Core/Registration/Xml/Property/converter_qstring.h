/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef CONVERTER_QSTRING_H
#define CONVERTER_QSTRING_H

#include <QString>

namespace Imbricable
{

class Converter_QString
{
public:
	static QString convert(QString* input);
	static void convert(QString data, bool *ok, QString* output);
};

} // namespace

#endif // CONVERTER_QSTRING_H
