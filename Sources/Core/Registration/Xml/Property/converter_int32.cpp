/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "converter_int32.h"

using namespace Imbricable;

QString Converter_Int32::convert(qint32* input)
{
	return QString::number(*input);
}

void Converter_Int32::convert(QString data, bool*ok, qint32* output)
{
	*output = data.toInt(ok);
}
