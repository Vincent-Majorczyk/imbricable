#include "converter_bool.h"

using namespace Imbricable;

QString Converter_Bool::convert(bool *input)
{
	if(*input) return "True";
	else return "False";
}

void Converter_Bool::convert(QString data, bool *ok, bool* output)
{
	if(data == "True") *output = true;
	else if(data == "False") *output = false;
	else
	{
		*ok = false;
		return;
	}

	*ok = true;
}
