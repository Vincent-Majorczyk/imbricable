#include "converter_link.h"

using namespace Imbricable;


QString Converter_Link::convert(Imbricable::Link* input)
{
	return input->id().toString();
}

void Converter_Link::convert(QString data, bool *ok, Imbricable::Link* output)
{
	QUuid id(data);
	*output = Link(id, nullptr);
	*ok = true;
}
