/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef CONVERTER_UINT16_H
#define CONVERTER_UINT16_H

#include <QString>

namespace Imbricable
{

class Converter_UInt16
{
public:
	static QString convert(quint16* input);
	static void convert(QString data, bool *ok, quint16* output);
};

} // namespace

#endif // CONVERTER_INT16_H
