/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "converter_int64.h"

using namespace Imbricable;

QString Converter_Int64::convert(qint64* input)
{
	return QString::number(*input);
}

void Converter_Int64::convert(QString data, bool*ok, qint64 *output)
{
	*output = data.toLongLong(ok);
}
