/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "converter_quuid.h"

using namespace Imbricable;

QString Converter_QUuid::convert(QUuid *input)
{
	return input->toString();
}

void Converter_QUuid::convert(QString data, bool*ok, QUuid *output)
{
	*output = QUuid(data);
	*ok = true;
}
