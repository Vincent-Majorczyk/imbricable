/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef CONVERTER_LINK_H
#define CONVERTER_LINK_H

#include "xmlpropertyserializer.h"
#include "Core/Object/Link/link.h"

namespace Imbricable
{

class Converter_Link:public XmlPropertySerializer_Base
{
public:
	static QString convert(Imbricable::Link* input);
	static void convert(QString data, bool *ok, Imbricable::Link* output);
};

} // namespace

#endif // CONVERTER_LINK_H
