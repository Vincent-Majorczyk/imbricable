/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "converter_uint64.h"

using namespace Imbricable;

QString Converter_UInt64::convert(quint64 *input)
{
	return QString::number(*input);
}

void Converter_UInt64::convert(QString data, bool*ok, quint64 *output)
{
	*output = data.toULongLong(ok);
}
