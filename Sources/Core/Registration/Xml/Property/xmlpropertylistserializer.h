/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef XMLPROPERTYLISTSERIALIZER_H
#define XMLPROPERTYLISTSERIALIZER_H

#include "../xmlmodelserializer.h"

#include <QFileInfo>

namespace Imbricable
{

template<class TConverter, template<class, class...> class TList, class TClass, class...TArg>
class XmlPropertyListSerializer:public XmlSerializer
{
	TList<TClass,TArg...>* _list;

public:
	XmlPropertyListSerializer(XmlModelSerializer* parent, QString name, TList<TClass,TArg...>* list):
		XmlSerializer(parent,name), _list(list)
	{ }

	QDomNode serialize() override
	{
		QDomElement n = this->document()->createElement(this->name());

		//QList<QFileInfo> l;

		for(TClass& t: *_list)
		{
			QDomElement i = this->document()->createElement("Item");
			QDomText text = this->document()->createTextNode(TConverter::convert(&t));
			i.appendChild(text);
			n.appendChild(i);
		}

		return n;
	}

	void deserialize(QDomNode node) override
	{
		auto t = node.firstChild();

		if(t.isNull()) return;

		do
		{
			auto i = t.firstChild();

			if(i.isNull()) continue;

			bool ok = false;
			TClass t;
			TConverter::convert(i.toText().data(), &ok, &t);
			if(!ok)
			{
				QString error = QString("parse error: %1::%2 isn't well formated (%3)").arg(parent()->name()).arg(this->name()).arg(i.toText().data());
				throw std::runtime_error(error.toStdString());
			}
			_list->push_back(t);
		}
		while( ! (t = t.nextSibling()).isNull() );
	}
};

} // namespace

#endif // XMLPROPERTYLISTSERIALIZER_H
