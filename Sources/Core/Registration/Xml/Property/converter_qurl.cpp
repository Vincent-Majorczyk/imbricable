#include "converter_qurl.h"

using namespace Imbricable;

QString Converter_QUrl::convert(QUrl* input)
{
	return input->toString();
}

void Converter_QUrl::convert(QString data, bool*ok, QUrl *output)
{
	*output = QUrl(data);
	if(output->isValid()) *ok = true;
	else *ok = false;
}
