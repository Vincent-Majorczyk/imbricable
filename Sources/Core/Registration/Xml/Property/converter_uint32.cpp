/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "converter_uint32.h"

using namespace Imbricable;

QString Converter_UInt32::convert(quint32 *input)
{
	return QString::number(*input);
}

void Converter_UInt32::convert(QString data, bool*ok, quint32* output)
{
	*output = data.toUInt(ok);
}
