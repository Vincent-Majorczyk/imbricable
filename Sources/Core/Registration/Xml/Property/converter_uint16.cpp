/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "converter_uint16.h"

using namespace Imbricable;

QString Converter_UInt16::convert(quint16 *input)
{
	return QString::number(*input);
}

void Converter_UInt16::convert(QString data, bool*ok, quint16 *output)
{
	*output = data.toUShort(ok);
}

