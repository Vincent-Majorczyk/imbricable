/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "xmlpropertyserializer.h"

#include <QDebug>

using namespace Imbricable;

XmlPropertySerializer_Base::XmlPropertySerializer_Base(XmlModelSerializer* parent, QString name):XmlSerializer(parent,name)
{
}

QDomNode XmlPropertySerializer_Base::serialize()
{
	QDomElement n = this->document()->createElement(this->name());
	QDomText text = this->document()->createTextNode(this->convert());

	n.appendChild(text);
	return n;
}

void XmlPropertySerializer_Base::deserialize(QDomNode node)
{
	auto t = node.firstChild().toText();
	bool ok = false;
	this->convert(t.data(),&ok);
	if(!ok)
	{
		QString error = QString("parse error: %1::%2 isn't well formated (%3)").arg(parent()->name(),this->name(),t.data());
		throw std::runtime_error(error.toStdString());
	}
}
