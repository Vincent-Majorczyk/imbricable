#ifndef CONVERTER_DOUBLE_H
#define CONVERTER_DOUBLE_H

#include <QString>

namespace Imbricable
{

class Converter_Double
{
public:
	static QString convert(double *input);
	static void convert(QString data, bool *ok, double* output);
};

} // namespace

#endif // CONVERTER_DOUBLE_H
