/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef CONVERTER_QURL_H
#define CONVERTER_QURL_H

#include <QString>
#include <QUrl>

namespace Imbricable
{

class Converter_QUrl
{
public:
	static QString convert(QUrl* input);
	static void convert(QString data, bool *ok, QUrl* output);
};

} // namespace

#endif // CONVERTER_QURL_H
