/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef CONVERTER_BOOL_H
#define CONVERTER_BOOL_H

#include <QString>

namespace Imbricable
{

class Converter_Bool
{
public:
	static QString convert(bool *input);
	static void convert(QString data, bool *ok, bool* output);
};

} // namespace

#endif // CONVERTER_BOOL_H
