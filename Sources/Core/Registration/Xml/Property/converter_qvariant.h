/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef CONVERTER_QVARIANT_H
#define CONVERTER_QVARIANT_H

#include <QString>
#include <QVariant>

namespace Imbricable {

class Converter_QVariant
{
public:
	static QString convert(QVariant *input);
	static void convert(QString data, bool *ok, QVariant* output);
};

}

#endif // CONVERTER_QVARIANT_H

