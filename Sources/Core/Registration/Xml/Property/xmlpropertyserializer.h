/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef XMLPROPERTYSERIALIZER_H
#define XMLPROPERTYSERIALIZER_H

#include "../xmlmodelserializer.h"

namespace Imbricable
{

class XmlPropertySerializer_Base:public XmlSerializer
{
public:
	XmlPropertySerializer_Base(XmlModelSerializer* parent, QString name);

	QDomNode serialize() override;
	void deserialize(QDomNode node) override;

	virtual QString convert() = 0;
	virtual void convert(QString data, bool *ok) = 0;
};

template<class Converter, typename Type>
class XmlPropertySerializer: public XmlPropertySerializer_Base
{
	Type *_item;
public:
	XmlPropertySerializer(XmlModelSerializer* parent, QString name, Type* ptr_item):
		XmlPropertySerializer_Base(parent, name), _item(ptr_item)
	{ }

	QString convert() override
	{
		return Converter::convert(_item);
	}

	void convert(QString data, bool *ok) override
	{
		Converter::convert(data, ok, _item);
	}
};

} // namespace

#endif // XMLPROPERTYSERIALIZER_H
