/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "converter_float.h"

using namespace Imbricable;

QString Converter_Float::convert(float *input)
{
	return QString::number(*input);
}

void Converter_Float::convert(QString data, bool*ok, float *output)
{
	*output = data.toFloat(ok);
}
