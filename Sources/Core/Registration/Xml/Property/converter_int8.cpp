/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "converter_int8.h"

using namespace Imbricable;


QString Converter_Int8::convert(qint8 *input)
{
	return QString::number(*input);
}

void Converter_Int8::convert(QString data, bool*ok, qint8 *output)
{
	*output = static_cast<qint8>(data.toShort(ok));
}

