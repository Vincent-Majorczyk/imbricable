/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef XMLSERIALIZER_H
#define XMLSERIALIZER_H

#include <QtXml/QDomDocument>
#include <memory>

namespace Imbricable
{

class XmlModelSerializer;
class XmlModelSerializerFactoryList;

class XmlSerializer
{
private:
	std::shared_ptr<XmlModelSerializerFactoryList> _factory;
	QString _name;
	XmlSerializer* _parent;
	QDomDocument* _document;

protected:
	inline void setFactory(std::shared_ptr<XmlModelSerializerFactoryList> factory) { _factory = factory; }
	std::shared_ptr<XmlModelSerializerFactoryList> factory() { return _factory; }

public:
	XmlSerializer(XmlSerializer* parent, QString name);
	XmlSerializer(XmlSerializer* parent);
	virtual ~XmlSerializer();

	inline QString name() { return _name; }
	inline void setName(QString name) { _name = name; }
	QString completeName();

	virtual void initialize(std::shared_ptr<XmlModelSerializerFactoryList> factory);


	inline XmlSerializer* parent() { return _parent; }
	inline void setParent(XmlSerializer* parent) { _parent = parent; }

	virtual QDomNode serialize() = 0;
	virtual void deserialize(QDomNode node) = 0;

	inline void setDocument(QDomDocument* doc) {_document = doc; }
	inline QDomDocument* document() { return _document; }

};

typedef std::shared_ptr<XmlSerializer> S_XmlSerializer;

} // namespace

#endif // XMLSERIALIZER_H
