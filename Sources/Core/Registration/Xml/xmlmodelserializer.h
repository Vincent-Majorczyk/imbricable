/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef XMLREGISTER_H
#define XMLREGISTER_H

#include <memory>
#include <QString>
#include <QtXml/QDomDocument>
#include <QList>

#include <Core/Object/model.h>
#include "xmlserializer.h"
#include "xmlmodelserializerfactorylist.h"
#include "xmlnodeserializer.h"

namespace Imbricable
{

class XmlModelSerializer: public XmlNodeSerializer
{
protected:
	QList<S_XmlSerializer> _children;
	quint32 _deserializationVersion = 0;
	bool _typedArgument = false;
	void initialize(XmlModelSerializer* copy);

public:
	XmlModelSerializer(XmlSerializer* parent, QString name);
	XmlModelSerializer(XmlSerializer* parent);
	~XmlModelSerializer() override;

	void initialize(S_XmlModelSerializerFactoryList factory) override;

	QDomNode serialize() override;
	void deserialize(QDomNode node) override;

	void setModel(S_Model mode) override;

	virtual QString typeName() = 0;
	virtual bool isType(S_Model model) = 0;

	bool isInheritedType(S_Model model);

	virtual uint version() = 0;
	virtual S_Model createModel() = 0;
	virtual std::shared_ptr<XmlModelSerializer> createSerializer(XmlSerializer* parent) = 0;
	S_Model createModel(uint version);
	std::shared_ptr<XmlModelSerializer> createSerializer(XmlSerializer* parent, uint version);

	inline void setTypedArgument(bool ta)
	{
		_typedArgument = ta;
	}

	virtual std::shared_ptr<XmlModelSerializer> previous();

	std::shared_ptr<XmlModelSerializer> getOldSerializer(S_Model model);

	inline quint32 deserializationVersion()
	{
		return _deserializationVersion;
	}

	inline void setDeserializationVersion(quint32 version)
	{
		_deserializationVersion = version;
	}

	inline void resetDeserializationVersion()
	{
		_deserializationVersion = 0;
	}

	virtual S_Model upgrade();
	S_Model upgradeRecursively(S_Model model);

protected:
	virtual void buildTree(S_Model m) = 0;

	template<typename T>
	inline std::shared_ptr<T> add(std::shared_ptr<T> s)
	{
		s->initialize(this->factory());
		_children.append(s);
		return s;
	}

	template<typename T,typename ...TArg>
	inline std::shared_ptr<T> addNew(TArg ...args)
	{
		auto t = std::make_shared<T>( args... );
		return t = add(t);
	}

	template<class C>
	inline bool isType(S_Model model)
	{
		return typeid(model) == typeid(C);
	}
};

typedef std::shared_ptr<XmlModelSerializer> S_XmlModelSerializer;

/**
 * @brief
 *
 * @todo I wanted to set string in template (`template<const char* TName> class Foo() { QString text { return TName; } };`) to use the typename directly, but isn't possible currently. apparently C++20 gives a solution:
 * - [stckedoverflow](https://stackoverflow.com/a/58841797/12512174);
 * - [C++ Standards Meeting in Jacksonville, march 2018](https://botondballo.wordpress.com/2018/03/28/trip-report-c-standards-meeting-in-jacksonville-march-2018/);
 * - [accepted proposal](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2018/p0732r0.pdf);
 */
template<class TModel, class TSerializer, int TVersion>
class XmlTypedModelSerializer: public XmlModelSerializer
{
	QString _typeName;
	std::shared_ptr<TModel>* _access = nullptr;
public:
	XmlTypedModelSerializer(QString typeName, XmlSerializer* parent, QString name):
		XmlModelSerializer(parent, name),
		_typeName(typeName)
	{ }

	XmlTypedModelSerializer(QString typeName, XmlSerializer* parent):
		XmlModelSerializer(parent),
		_typeName(typeName)
	{ }

	QString typeName() override
	{
		return _typeName;
	}

	bool isType(S_Model model) override
	{
		Model &m = *(model.get());
		return typeid(m) == typeid(TModel);
	}

	uint version() override
	{
		return TVersion;
	}

	S_Model createModel() override
	{
		return std::make_shared<TModel>();
	}

	S_XmlModelSerializer createSerializer(XmlSerializer* parent) override
	{
		return std::make_shared<TSerializer>(parent);
	}

	void setAccess(std::shared_ptr<TModel>* access)
	{
		_access = access;
	}

	void setModelToAccess() override
	{
		if(_access != nullptr)
			(*_access) = std::dynamic_pointer_cast<TModel>(this->model());
	}

	void getModelFromAccess() override
	{
		if(_access != nullptr)
			this->setModel(*_access);
	}
};

} // namespace

#endif // XMLREGISTER_H
