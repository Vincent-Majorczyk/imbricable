/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "xmlnodeserializer.h"
#include "Core/Object/Link/linksmanager.h"

using namespace Imbricable;

XmlNodeSerializer::XmlNodeSerializer(XmlSerializer *parent, QString name):
	XmlSerializer (parent, name)
{ }

XmlNodeSerializer::XmlNodeSerializer(XmlSerializer *parent):
	XmlSerializer (parent)
{ }

void XmlNodeSerializer::setModel(S_Model model)
{ _model = model; }

void XmlNodeSerializer::setModelToAccess() { }

void XmlNodeSerializer::getModelFromAccess() {}

void XmlNodeSerializer::addToLLinkManager()
{
	std::shared_ptr<IUuidObject> c = std::dynamic_pointer_cast<IUuidObject>(this->model());
	if(c!=nullptr)
	{
		LinksManager::getInstance().add(c);
	}
}
