#include "xmlversionmanager.h"
#include "xmlmodelserializer.h"

using namespace Imbricable;

XmlVersionManager::XmlVersionManager(S_XmlModelSerializerFactoryList factory):
	_factory(factory)
{

}

QDomNode XmlVersionManager::serialize(QDomDocument* _document)
{
	QDomElement m = _document->createElement("RequiredVersion");

	for(S_XmlModelSerializer& s: _factory->list())
	{
		QDomElement n = _document->createElement("Item");

		n.setAttribute("type", s->typeName());
		n.setAttribute("version", s->version());
		m.appendChild(n);
	}
	return m;
}

void XmlVersionManager::deserialize(QDomNode node)
{
	QDomNode n = node.firstChild();

	_factory->resetVersion();

	while(!n.isNull())
	{
		QString type = n.attributes().namedItem("type").toAttr().value();
		uint version = n.attributes().namedItem("version").toAttr().value().toUInt();

		_factory->setVersion(type, version);

		n = n.nextSibling();
	}
}
