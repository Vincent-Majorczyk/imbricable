/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "xmlmodelserializer.h"
#include <QDebug>

using namespace Imbricable;

XmlModelSerializer::XmlModelSerializer(XmlSerializer *parent, QString name):
	XmlNodeSerializer (parent, name)
{ }

XmlModelSerializer::XmlModelSerializer(XmlSerializer *parent):
	XmlNodeSerializer (parent)
{ }


XmlModelSerializer::~XmlModelSerializer()
{
}

QDomNode XmlModelSerializer::serialize()
{
	this->getModelFromAccess();
	QDomElement n = this->document()->createElement(this->name());

	if(model() == nullptr)
	{
		n.setAttribute("type", "null");
		return n;
	}

	if(_typedArgument) n.setAttribute("type", this->typeName());

	for(auto& item : _children)
	{
		item->initialize(this->factory());
		n.appendChild(item->serialize());
	}

	return n;
}

void XmlModelSerializer::deserialize(QDomNode node)
{
	if(_deserializationVersion != 0 && _deserializationVersion != this->version())
	{
		auto serializer = this->createSerializer(this->parent(), _deserializationVersion);
		this->initialize(this->factory());
		serializer->deserialize(node);
		this->setModel( this->factory()->upgrade(serializer->model()) );
		this->addToLLinkManager();
		this->setModelToAccess();
		return;
	}

	auto attrib = node.attributes();
	if(attrib.contains("type") && attrib.namedItem("type").toAttr().value() == "null")
	{
		this->setModel(nullptr);
		this->addToLLinkManager();
		this->setModelToAccess();
		return;
	}
	else
		this->setModel( this->createModel() );

	for(auto& item : _children)
	{
		QString name = item->name();
		QDomElement sub = node.firstChildElement(name);

		if(sub.isNull())
		{
			QString text = QString("%1 doesn't contains the property %2").arg(this->name(),item->name());
			qWarning() << text;
			//throw std::runtime_error( text.toStdString() );
		}
		else
		{
			item->initialize(this->factory());
			item->deserialize(sub);
		}
	}

	this->addToLLinkManager();
	this->setModelToAccess();
}

void XmlModelSerializer::setModel(S_Model model)
{
	XmlNodeSerializer::setModel(model);
	_children.clear();
	if(model == nullptr) return;

	this->buildTree(model);

	for(auto& item : _children)
		item->setDocument(this->document());
}

S_Model XmlModelSerializer::upgrade()
{
	return this->model();
}

std::shared_ptr<XmlModelSerializer> XmlModelSerializer::previous()
{
	return nullptr;
}

S_Model XmlModelSerializer::createModel(uint version)
{
	if(version==0 || version == _deserializationVersion) return createModel();
	else
	{
		auto i = this->previous();
		while(i != nullptr)
		{
			if(version == i->_deserializationVersion)
			{
				return i->createModel();
			}
			i = i->previous();
		}
	}
	return nullptr;
}

std::shared_ptr<XmlModelSerializer> XmlModelSerializer::createSerializer(XmlSerializer* parent, uint version)
{
	if(version==0 || version == this->version())
	{
		auto z = createSerializer(parent);
		z->initialize(this);
		z->setDeserializationVersion(version);
		return z;
	}
	else
	{
		auto i = this->previous();
		while(i != nullptr)
		{
			if(version == i->version())
			{
				auto z = i->createSerializer(parent);
				z->initialize(i.get());
				z->setDeserializationVersion(version);
				return z;
			}
			i = i->previous();
		}
	}

	QString error("the version %1 of the serializer of %2 (last version %3)");
	throw std::runtime_error(error.arg(this->typeName()).arg(version).arg(this->version()).toStdString());
}

void XmlModelSerializer::initialize(XmlModelSerializer *copy)
{
	this->_deserializationVersion = copy->_deserializationVersion;
	this->setFactory(copy->factory());
}

void XmlModelSerializer::initialize(S_XmlModelSerializerFactoryList factory)
{
	auto l = factory->list();

	this->setFactory(factory);
	if(factory == nullptr) return;

	auto original = factory->getOriginal(this->typeName());
	if(original == nullptr)
	{
		QString error = "XmlModelSerializer::initialize(factory) => factory does not contain %1";
		throw std::runtime_error(error.arg(this->typeName()).toStdString());
	}
	this->_deserializationVersion = original->_deserializationVersion;
}

bool XmlModelSerializer::isInheritedType(S_Model model)
{
	if(this->isType(model)) return true;

	auto i = this->previous();
	while(i != nullptr)
	{
		if(i->isType(model)) return true;
		i = i->previous();
	}
	return false;
}

std::shared_ptr<XmlModelSerializer> XmlModelSerializer::getOldSerializer(S_Model model)
{
	auto i = this->previous();
	while(i != nullptr)
	{
		if(i->isType(model)) return i;
		i = i->previous();
	}
	return nullptr;
}

S_Model XmlModelSerializer::upgradeRecursively(S_Model model)
{
	QVector<S_XmlModelSerializer> list;

	if(this->isType(model)) return model;

	auto s = this->previous();
	while(s!=nullptr && !s->isType(model))
	{
		list.append(s);
	}

	if( s == nullptr )
	{
		throw std::runtime_error("error upgrade");
	}

	list.push_back(s);

	for (auto i = list.rbegin(); i != list.rend(); ++i)
	{
		S_XmlModelSerializer ser = *i;
		ser->setModel(model);
		model = ser->upgrade();
	}

	return model;
}

