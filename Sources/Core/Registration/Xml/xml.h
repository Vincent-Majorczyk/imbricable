/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef XML_H
#define XML_H

#include "xmlserializer.h"
#include "xmlmodelserializer.h"
#include "xmlmodelserializerfactorylist.h"
#include "xmlfactoryserializer.h"
#include "xmlversionmanager.h"
#include "xmlhelper.h"

#include "Property/xmlpropertyserializer.h"
#include "Property/xmlpropertylistserializer.h"

#include "Property/converter_bool.h"
#include "Property/converter_int8.h"
#include "Property/converter_int16.h"
#include "Property/converter_int32.h"
#include "Property/converter_int64.h"
#include "Property/converter_uint8.h"
#include "Property/converter_uint16.h"
#include "Property/converter_uint32.h"
#include "Property/converter_uint64.h"
#include "Property/converter_double.h"
#include "Property/converter_float.h"
#include "Property/converter_qstring.h"
#include "Property/converter_qfileinfo.h"
#include "Property/converter_qurl.h"
#include "Property/converter_quuid.h"
#include "Property/converter_qvariant.h"

#include "Property/converter_link.h"

#include "List/xmlmodelserializer_list.h"
#include "List/xmlfactoryserializer_list.h"

namespace Imbricable
{

// single (XmlPropertySerializer)
typedef XmlPropertySerializer<Converter_Bool, bool> XmlProperty_Bool;
typedef XmlPropertySerializer<Converter_Int8, qint8> XmlProperty_Int8;
typedef XmlPropertySerializer<Converter_Int16, qint16> XmlProperty_Int16;
typedef XmlPropertySerializer<Converter_Int32, qint32> XmlProperty_Int32;
typedef XmlPropertySerializer<Converter_Int64, qint64> XmlProperty_Int64;
typedef XmlPropertySerializer<Converter_UInt8, quint8> XmlProperty_UInt8;
typedef XmlPropertySerializer<Converter_UInt16, quint16> XmlProperty_UInt16;
typedef XmlPropertySerializer<Converter_UInt32, quint32> XmlProperty_UInt32;
typedef XmlPropertySerializer<Converter_UInt64, quint64> XmlProperty_UInt64;
typedef XmlPropertySerializer<Converter_Double, double> XmlProperty_Double;
typedef XmlPropertySerializer<Converter_Float, float> XmlProperty_Float;
typedef XmlPropertySerializer<Converter_QString, QString> XmlProperty_QString;
typedef XmlPropertySerializer<Converter_QFileInfo, QFileInfo> XmlProperty_QFileInfo;
typedef XmlPropertySerializer<Converter_QUrl, QUrl> XmlProperty_QUrl;
typedef XmlPropertySerializer<Converter_QUuid, QUuid> XmlProperty_QUuid;
typedef XmlPropertySerializer<Converter_Link, Link> XmlProperty_Link;
typedef XmlPropertySerializer<Converter_QVariant, QVariant> XmlProperty_QVariant;

// list (XmlPropertyListSerializer)
typedef XmlPropertyListSerializer<Converter_QFileInfo, QList, QFileInfo> XmlProperty_QListQFileInfo;
typedef XmlPropertyListSerializer<Converter_Link, QList, Imbricable::Link> XmlProperty_QListLink;
}

#endif // XML_H
