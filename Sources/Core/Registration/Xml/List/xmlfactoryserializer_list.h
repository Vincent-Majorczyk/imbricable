#ifndef XMLFACTORYSERIALIZER_LIST_H
#define XMLFACTORYSERIALIZER_LIST_H

#include "../xmlmodelserializer.h"

namespace Imbricable {

/*class XmlFactorySerializer_BaseList: public XmlNodeSerializer
{
public:
	XmlFactorySerializer_BaseList(XmlSerializer* parent, QString name);

	QDomNode serializeItem(S_Model model, XmlModelSerializer serializer, int index);
	void deserializeItem(QDomNode node);
};

class XmlFactorySerializer_List: public XmlFactorySerializer_BaseList
{
	std::shared_ptr<Model>* _access = nullptr;
public:
	XmlFactorySerializer_List(XmlSerializer* parent, QString name);
	XmlFactorySerializer_List(XmlSerializer* parent, QString name, std::shared_ptr<Model>* access);

	QDomNode serialize() override;
	void deserialize(QDomNode node) override;

	void setAccess(std::shared_ptr<Model>* access);

	void setModelToAccess() override;

	void getModelFromAccess() override;
};*/


template<template<class, class...> class TListModel, class TModel = Imbricable::Model, class...TArg>
class XmlFactorySerializer_List: public XmlNodeSerializer
{
	TListModel<std::shared_ptr<TModel>,TArg...>* _access = nullptr;

public:
	XmlFactorySerializer_List(XmlSerializer* parent, QString name)
		:XmlNodeSerializer(parent, name)
	{ }

	XmlFactorySerializer_List(XmlSerializer* parent, QString name, TListModel<std::shared_ptr<TModel>,TArg...>* access)
		:XmlNodeSerializer (parent, name),
		  _access(access)
	{ }

	void setAccess(TListModel<std::shared_ptr<TModel>,TArg...>* access)
	{
		_access = access;
	}

	QDomNode serialize() override
	{
		QDomElement n = this->document()->createElement(this->name());
		S_XmlModelSerializerFactoryList factory = this->factory();

		int id=0;
		for(S_Model item: *_access)
		{
			S_XmlModelSerializer serializer = factory->createSerializer(item, this);
			serializer->setTypedArgument(true);
			serializer->setDocument(this->document());

			serializer->setName("Item");
			serializer->setModel(item);
			QDomElement i = serializer->serialize().toElement();
			i.setAttribute("index", id);
			n.appendChild(i);
			id++;
		}

		return n;
	}

	void deserialize(QDomNode node) override
	{
		if(this->factory() == nullptr)
		{
			QString error("no factory set in %1");
			throw std::logic_error(error.arg(this->completeName()).toStdString());
		}

		S_XmlModelSerializerFactoryList factory = this->factory();

		QDomNode n = node.firstChild();

		while(!n.isNull())
		{
			QString type = n.attributes().namedItem("type").toAttr().value();

			S_XmlModelSerializer serializer = factory->createSerializer(type, this);

			serializer->setName(this->name());
			serializer->setDocument(this->document());
			serializer->initialize(this->factory());
			serializer->deserialize(n);
			(*_access).push_back( std::dynamic_pointer_cast<TModel>(serializer->model()));

			n = n.nextSibling();
		}
	}
};

} // namespace

#endif // XMLFACTORYSERIALIZER_LIST_H
