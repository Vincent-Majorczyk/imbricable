#ifndef XMLLISTMODELSERIALIZER_H
#define XMLLISTMODELSERIALIZER_H

#include "../xmlmodelserializer.h"

namespace Imbricable {

template<class TSerializer, template<class, class...> class TListModel, class TModel, class...TArg>
class XmlModelSerializer_List: public XmlNodeSerializer
{
	TListModel<std::shared_ptr<TModel>,TArg...>* _access = nullptr;

public:
	XmlModelSerializer_List(XmlSerializer* parent, QString name)
		:XmlNodeSerializer(parent, name)
	{ }

	XmlModelSerializer_List(XmlSerializer* parent, QString name, TListModel<std::shared_ptr<TModel>,TArg...>* access)
		:XmlNodeSerializer (parent, name),
		  _access(access)
	{ }

	void setAccess(TListModel<std::shared_ptr<TModel>,TArg...>* access)
	{
		_access = access;
	}

	QDomNode serialize() override
	{
		QDomElement n = this->document()->createElement(this->name());
		TSerializer s(this);
		XmlModelSerializer &serializer = s;
		serializer.initialize(this->factory());
		serializer.setDocument(this->document());

		int id=0;
		for(S_Model item: *_access)
		{
			serializer.setName("Item");
			serializer.setModel(item);
			QDomElement i = serializer.serialize().toElement();
			i.setAttribute("index", id);
			n.appendChild(i);
			id++;
		}

		return n;
	}

	void deserialize(QDomNode node) override
	{
		TSerializer s(this);
		XmlModelSerializer &serializer = s;
		serializer.initialize(this->factory());
		serializer.setDocument(this->document());

		QDomNode n = node.firstChild();

		while(!n.isNull())
		{
			serializer.deserialize(n);
			(*_access).push_back( std::dynamic_pointer_cast<TModel>(serializer.model()));

			n = n.nextSibling();
		}
	}
};

} // namespace

#endif // XMLLISTMODELSERIALIZER_H
