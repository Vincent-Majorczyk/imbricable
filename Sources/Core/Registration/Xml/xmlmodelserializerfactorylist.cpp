/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "xmlmodelserializerfactorylist.h"
#include "xmlmodelserializer.h"
#include "Core/Convenient/exception.h"

#include <QDebug>

using namespace Imbricable;

XmlModelSerializerFactoryList::XmlModelSerializerFactoryList()
{

}

std::shared_ptr<XmlModelSerializer> XmlModelSerializerFactoryList::createSerializer(S_Model model, XmlSerializer* parent)
{
	for(S_XmlModelSerializer& s : this->_serializers)
	{
		S_XmlModelSerializer g = nullptr;

		uint version = s->deserializationVersion();

		if(s->isType(model)) g = s;
		else g = s->getOldSerializer(model);

		if(g != nullptr)
		{
			auto c = g->createSerializer(parent, version);
			if(c == nullptr) return nullptr;

			c->initialize(this->shared_from_this());
			return c;
		}
	}

	return nullptr;
}



std::shared_ptr<XmlModelSerializer> XmlModelSerializerFactoryList::createSerializer(QString typeName, XmlSerializer* parent)
{

	for(S_XmlModelSerializer& s : this->_serializers)
	{
		if(s->typeName() == typeName)
		{
			uint version = s->deserializationVersion();
			S_XmlModelSerializer c = s->createSerializer(parent, version);
			if(c == nullptr) return nullptr;
			//S_XmlModelSerializerFactoryList factory = this->shared_from_this();
			//c->initialize(factory);
			return c;
		}
	}

	return nullptr;
}

S_Model XmlModelSerializerFactoryList::upgrade(S_Model model)
{
	for(S_XmlModelSerializer& s : this->_serializers)
	{
		if(s->isInheritedType(model))
		{
			return s->upgradeRecursively(model);
		}
	}

	return nullptr;
}

void XmlModelSerializerFactoryList::setVersion(QString typeName, uint version)
{
	for(S_XmlModelSerializer& s : this->_serializers)
	{
		if(s->typeName() == typeName)
		{
			s->setDeserializationVersion(version);
		}
	}
}

uint XmlModelSerializerFactoryList::getVersion(QString typeName)
{
	for(S_XmlModelSerializer& s : this->_serializers)
	{
		if(s->typeName() == typeName)
		{
			return s->deserializationVersion();
		}
	}
	return 0;
}

S_XmlModelSerializer XmlModelSerializerFactoryList::getOriginal(QString typeName)
{
	for(S_XmlModelSerializer& s : this->_serializers)
	{
		//if(s == nullptr) continue;
		if(s->typeName() == typeName)
		{
			return s;
		}
	}

	throw Imbricable::Exception(QString("typeName %1 doesn't exist").arg(typeName));
}

void XmlModelSerializerFactoryList::resetVersion()
{
	for(S_XmlModelSerializer& s : this->_serializers)
	{
		s->resetDeserializationVersion();
	}
}

void XmlModelSerializerFactoryList::append(std::shared_ptr<XmlModelSerializer> serializer)
{
	for(S_XmlModelSerializer& s : this->_serializers)
	{
		if(s->typeName() == serializer->typeName())
		{
			throw Imbricable::Exception(QString("typeName %1 already exists").arg(s->typeName()));
		}
	}

	_serializers.append(serializer);
}
