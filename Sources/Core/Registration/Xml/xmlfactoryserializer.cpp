/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "xmlfactoryserializer.h"

using namespace Imbricable;

XmlFactorySerializer_Base::XmlFactorySerializer_Base(XmlSerializer *parent, QString name): XmlNodeSerializer(parent, name)
{ }

QDomNode XmlFactorySerializer_Base::serialize()
{
	this->getModelFromAccess();

	S_XmlModelSerializerFactoryList factory = this->factory();

	if(this->model()!=nullptr)
	{
		S_XmlModelSerializer tmp = factory->createSerializer(this->model(), this);
		tmp->setName(this->name());
		tmp->setDocument(this->document());
		tmp->initialize(this->factory());
		tmp->setModel(this->model());
		tmp->setTypedArgument(true);

		return tmp->serialize();
	}
	else
	{
		QDomElement n = this->document()->createElement(this->name());
		n.setAttribute("type", "null");
		return n;
	}
}

void XmlFactorySerializer_Base::deserialize(QDomNode node)
{
	QString type = node.attributes().namedItem("type").toAttr().value();
	if(type == "null")
	{
		this->setModel(nullptr);
		return;
	}

	if(this->factory() == nullptr)
	{
		QString error("no factory set in %1");
		throw std::logic_error(error.arg(this->completeName()).toStdString());
	}

	S_XmlModelSerializerFactoryList factory = this->factory();
	S_XmlModelSerializer tmp = factory->createSerializer(type, this);

	tmp->setName(this->name());
	tmp->setDocument(this->document());
	tmp->initialize(this->factory());
	tmp->deserialize(node);
	this->setModel(tmp->model());
	this->addToLLinkManager();
	this->setModelToAccess();
	return;
}

XmlFactorySerializer::XmlFactorySerializer(XmlSerializer* parent, QString name)
	:XmlFactorySerializer_Base (parent, name)
{ }

XmlFactorySerializer::XmlFactorySerializer(XmlSerializer* parent, QString name, std::shared_ptr<Model>* access)
	:XmlFactorySerializer_Base (parent, name),
	  _access(access)
{ }

void XmlFactorySerializer::setAccess(std::shared_ptr<Model>* access)
{
	_access = access;
}

void XmlFactorySerializer::setModelToAccess()
{
	if(_access != nullptr)
		(*_access) = std::dynamic_pointer_cast<Model>(this->model());
}

void XmlFactorySerializer::getModelFromAccess()
{
	if(_access != nullptr)
		this->setModel(*_access);
}
