# Save and load files

The namespace Imbricable::Registration, contains tool to save and load files:

## Json

[Json](https://en.wikipedia.org/wiki/JSON) is an open-standard file format which may describes tree hierarchical data.

In Imbricable, Json representation of objects contains a property named `@Type` which allows to recognize the type of the data. It aims to use a factory which creates and fills the object from the data contained in the Json. The factory contains  a list of Imbricable::Registration::RegistrationNode which describe the properties to save on the Json representation of the object.

## Csv

[Csv](https://en.wikipedia.org/wiki/Comma-separated_values) is a file format which aims to describes list of item, matrix.

In Imbricable, each row is an object and each column is a property of the object. The first line contains the header of the column. The aim of Csv files, is to be used as databases which are stored at specific locations in the program.

So Imbricable::Registration::RegistrationNode can't be used in a factory because the type of the list of object is not saved in the Csv file. It doesn't make sense to register heterogeneous objects in the list because properties to save may be different: a type of object may have properties which doesn't exist in another class, two types may have the same property, but not the type associated to this property. In the case of homogeneous list of object, this is do in a particular context, so the object which load data, knows in advance the type of the model contained in the file.

## Dev notes:

### Csv

In Csv file, even if it doesn't seem to make sense to use heterogeneous data or define type in a Csv file, there are the next  solution propositions:

- if a list of heterogeneous objects is required, the solution is the creation of a column `@type`. This requires to list the properties of each type of class to create column before save objects.
- if a list of heterogeneous objects is required, the solution is the creation of a special row `@type`, or consider the list as an heterogeneous type. be careful to avoid conflict with heterogeneous list.