/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef CSVCONVERTER_H
#define CSVCONVERTER_H
#pragma once

#include "Core/Object/model.h"
#include "Core/Properties/typedpropertyinfo.h"
#include "Core/Properties/basepropertyinfo_list.h"

namespace Imbricable::Registration::Csv {

class CsvConverter
{
public:
	CsvConverter();

	virtual void load(S_Model model) const = 0;
	virtual void save(S_Model model) const = 0;
};

} // namespace

#endif // CSVCONVERTER_H
