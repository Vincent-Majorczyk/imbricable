#if false

#include "csvregistrationfactory.h"

#include "../registrationnode.h"
#include "csvconverter.h"

using namespace Imbricable::Registration::Csv;

CsvRegistrationFactory::CsvRegistrationFactory()
{

}

bool CsvRegistrationFactory::save(S_Model model, QFileInfo& file) const
{
	QString alias = this->typefactory()->getAlias(model);

	auto registration = this->getRegistrationNode(model);

	if(registration == nullptr) ExceptionsThrowing::throwJsonRegistrationNodeException(alias);

	for(auto converter : registration->properties())
	{
		std::shared_ptr<CsvConverter> csvconverter = std::dynamic_pointer_cast<CsvConverter>(converter);
		if(csvconverter==nullptr) continue;
//		csvconverter->save(file);
	}
}

QStringList CsvRegistrationFactory::authorizedSuffix() const
{
	return QStringList() << "cvs";
}

Imbricable::S_Model CsvRegistrationFactory::load(QFileInfo& file) const
{
	QFile loadFile(file.absoluteFilePath());
	QByteArray saveData = loadFile.readAll();

	//return csvconverter->load(file);
}

#endif
