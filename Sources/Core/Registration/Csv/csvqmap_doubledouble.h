/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef CSVQMAP_DOUBLEDOUBLE_H
#define CSVQMAP_DOUBLEDOUBLE_H
#pragma once

#include "csvconverter.h"
#include "Core/Properties/typedpropertyinfo.h"
#include "Core/Properties/basepropertyinfo_list.h"
#include <QFileInfo>
#include <QMap>

namespace Imbricable::Registration::Csv {

class CsvQMap_doubledouble: public CsvConverter
{
	const Properties::TypedPropertyInfo<QFileInfo>* _file = nullptr;
	const Properties::TypedPropertyInfo_List<QMap<double,double>>* _data = nullptr;
	QString _col1Name = "X";
	QString _col2Name = "Y";
	QFileInfo _fileinfo;

public:
	CsvQMap_doubledouble(const Properties::TypedPropertyInfo_List<QMap<double,double>>* data, const Properties::TypedPropertyInfo<QFileInfo>* file);

		CsvQMap_doubledouble(const Properties::TypedPropertyInfo_List<QMap<double,double>>* data, QFileInfo file);

	void load(S_Model model) const;
	void save(S_Model model) const;

	inline QString col1Name() { return _col1Name; }
	inline QString col2Name() { return _col2Name; }
	inline void setCol1Name(QString name) { _col1Name = name; }
	inline void setCol2Name(QString name) { _col2Name = name; }

private:
	inline const QFileInfo file(S_Model model) const { return (_file != nullptr)?_file->getValue(model):_fileinfo;}
};

} // namespace

#endif // CSVQMAP_DOUBLEDOUBLE_H
