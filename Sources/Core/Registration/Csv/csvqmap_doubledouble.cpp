/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "csvqmap_doubledouble.h"
#include "csvstream.h"
#include <QFile>
#include <QFileInfo>
#include <QMap>
#include <stdexcept>

using namespace Imbricable::Registration::Csv;

CsvQMap_doubledouble::CsvQMap_doubledouble(const Properties::TypedPropertyInfo_List<QMap<double,double>>* data, const Properties::TypedPropertyInfo<QFileInfo>* file):
	_file(file), _data(data)
{

}

CsvQMap_doubledouble::CsvQMap_doubledouble(const Properties::TypedPropertyInfo_List<QMap<double,double>>* data, QFileInfo file):
	_file(nullptr), _data(data), _fileinfo(file)
{

}

void CsvQMap_doubledouble::load(S_Model model) const
{
	QFileInfo f = file(model);
	bool ok = f.exists();
	QString fname = f.absoluteFilePath();

	if(ok)
	{
		QFile fr(fname);
		fr.open(QIODevice::ReadOnly);
		CsvStream stream(&fr);
		QMap<double,double>& data = _data->getList(model);
		data.clear();
		bool first = true;
		bool ok = true;
		while(!stream.atEnd())
		{
			QStringList list = stream.readCsvLine();
			if(first)
			{
				first = false;
				continue;
			}
			ok = list.count() == 2;
			if(!ok) break;
			double key = list[0].toDouble(&ok);
			if(!ok) break;
			double value = list[1].toDouble(&ok);
			if(!ok) break;

			data[key] = value;
		}

		fr.close();
	}

	if(!ok)
	{
		throw std::runtime_error( QString("CsvQMap_doubledouble: wrong file format for loading (%1)").arg(fname).toStdString() );
	}
}

void CsvQMap_doubledouble::save(S_Model model) const
{
	QFileInfo f = file(model);
	QString fname = f.absoluteFilePath();
	QFile fr(fname);
	fr.open(QIODevice::WriteOnly);
	CsvStream stream(&fr);
	QMap<double,double>& data = _data->getList(model);

	stream << _col1Name << "," << _col2Name << Qt::endl;

	QMapIterator<double, double> i(data);
	while (i.hasNext())
	{
		i.next();
		stream << i.key() << "," << i.value() << Qt::endl;
	}

	fr.close();
}
