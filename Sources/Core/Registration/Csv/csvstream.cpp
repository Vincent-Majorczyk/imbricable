/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "csvstream.h"
#include "QRegularExpression"

using namespace Imbricable::Registration::Csv;

CsvStream::CsvStream(const QByteArray &array, QIODevice::OpenMode openMode)
	: QTextStream(array, openMode)
{}

CsvStream::CsvStream(QByteArray *array, QIODevice::OpenMode openMode)
	: QTextStream (array,openMode)
{}

CsvStream::CsvStream(QString *string, QIODevice::OpenMode openMode)
	: QTextStream(string, openMode)
{}

CsvStream::CsvStream(FILE *fileHandle, QIODevice::OpenMode openMode)
	 :QTextStream (fileHandle, openMode)
{
}

CsvStream::CsvStream(QIODevice *device)
	: QTextStream(device)
{
}

/**
 * @brief CsvStream::readLine
 * @return
 * @note use regex expression: ```(?<=^|,)(?:"{2}|(?:)|[^,"\r\n]+|"(?:"{2}|[^"]+)+")(?=,|$))```
 */
QStringList CsvStream::readCsvLine()
{
	QString line = QTextStream::readLine();
	QRegularExpression regex("(?<=^|,)(?:\"{2}|(?:)|[^,\"]+|\"(?:\"{2}|[^\"]+)+\")(?=,|$)");

	//bool b = regex.isValid();
	//String info = regex.errorString();

	QRegularExpressionMatchIterator i = regex.globalMatch(line);

	QStringList items;
	while (i.hasNext())
	{
		QRegularExpressionMatch match = i.next();
		QString item = match.captured(0);
		items << item;
	}

	return items;
}
