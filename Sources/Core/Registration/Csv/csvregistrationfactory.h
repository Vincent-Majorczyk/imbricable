#if false

#ifndef CSVREGISTRATIONFACTORY_H
#define CSVREGISTRATIONFACTORY_H
#pragma once


#include "../registrationfactory.h"
#include <QFileInfo>

namespace Imbricable::Registration::Csv {

/**
 * @brief save and load a unsigned integer property to a json field
 *
 * @warning use a string format
 */
class CsvRegistrationFactory: public RegistrationFactory
{
public:
	/**
	 * @brief constructor
	 */
	CsvRegistrationFactory();

	bool save(S_Model model, QFileInfo& file) const override;
	QStringList authorizedSuffix() const override;

	S_Model load(QFileInfo& file) const override;
};

using S_CsvRegistrationFactory = std::shared_ptr<CsvRegistrationFactory>;
using W_CsvRegistrationFactory = std::weak_ptr<CsvRegistrationFactory>;

} // namespace
#endif // CSVREGISTRATIONFACTORY_H

#endif
