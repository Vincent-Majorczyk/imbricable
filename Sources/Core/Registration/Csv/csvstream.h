/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef CSVFILE_H
#define CSVFILE_H

#include <QTextStream>

namespace Imbricable::Registration::Csv {

class CsvStream: public QTextStream
{
public:
	CsvStream(const QByteArray &array, QIODevice::OpenMode openMode = QIODevice::ReadOnly);
	CsvStream(QByteArray *array, QIODevice::OpenMode openMode = QIODevice::ReadWrite);
	CsvStream(QString *string, QIODevice::OpenMode openMode = QIODevice::ReadWrite);
	CsvStream(FILE *fileHandle, QIODevice::OpenMode openMode = QIODevice::ReadWrite);
	CsvStream(QIODevice *device);

	QStringList readCsvLine();
};

} // namepsace

#endif // CSVFILE_H
