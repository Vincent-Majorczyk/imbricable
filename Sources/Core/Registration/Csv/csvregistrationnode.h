/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef CSVREGISTRATIONNODE_H
#define CSVREGISTRATIONNODE_H

class CsvRegistrationNode
{
public:
	CsvRegistrationNode();
};

#endif // CSVREGISTRATIONNODE_H
