/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef PROPERTYINFO_LINKLIST_H
#define PROPERTYINFO_LINKLIST_H

#include "propertyinfo_qlist.h"
#include "Core/Object/Link/link.h"

namespace Imbricable::Properties
{

template<class C>
using PropertyInfo_LinkList = PropertyInfo_qlist<C, Link>;

} // namespace

#endif // PROPERTYINFO_LINKLIST_H
