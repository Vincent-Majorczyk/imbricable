/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef BASEPROPERTYINFO_H
#define BASEPROPERTYINFO_H
#pragma once

#include <QString>
#include <memory>
#include "Core/Object/model.h"

namespace Imbricable {

/**
 * @brief contains class about properties management
 */
namespace Properties {

/**
 * @brief abstract class with basic informations about properties
 */
class BasePropertyInfo
{
protected:
	/**
	 * @brief name of the property
	 */
	QString _name;

protected:
	/**
	 * @brief basic constructor of the class
	 * @param name of the property
	 */
	BasePropertyInfo(QString name);

public:
	/**
	 * @brief class destructor
	 */
	virtual ~BasePropertyInfo();

	/**
	 * @brief returns the name of the property
	 * @return
	 */
	inline QString name() const { return _name; }

	/*
	virtual QString toString(S_Model object) const;
	virtual bool fromString(S_Model object, QString s) const;
	*/
};

}} //namespaces

#endif // BASEPROPERTYINFO_H
