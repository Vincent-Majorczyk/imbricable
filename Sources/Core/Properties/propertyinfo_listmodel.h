/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef PROPERTYINFO_QLISTMODEL_H
#define PROPERTYINFO_QLISTMODEL_H

#include <memory>
#include "propertyinfo.h"
#include "Core/Object/model.h"
#include "propertyinfo_qlist.h"

namespace Imbricable::Properties {

template <class C, class T>
class PropertyInfo_listmodel:
		public PropertyInfo_qlist<C,std::shared_ptr<T>>,
		public BasePropertyInfo_ModelList
{

public:
	PropertyInfo_listmodel(QString name, std::function<QList<std::shared_ptr<T>>&(C*)> get):
		PropertyInfo_qlist<C,std::shared_ptr<T>>(name,get)
	{
	}

	unsigned int count(S_Model obj) const override
	{
		auto soC = std::dynamic_pointer_cast<C>(obj);
		QList<std::shared_ptr<T>> &so = this->_get(soC.get());
		return so.count();
	}

	S_Model getModel(S_Model obj, unsigned int index) const override
	{
		auto soC = std::dynamic_pointer_cast<C>(obj);
		auto &so = this->_get(soC.get());
		auto &result = so.at(index);
		return std::dynamic_pointer_cast<Model>(result);
	}

	bool appendModel(S_Model obj, S_Model child) const override
	{
		auto soC = std::dynamic_pointer_cast<C>(obj);
		auto &so = this->_get(soC.get());
		auto soT = std::dynamic_pointer_cast<T>(child);
		so.append(soT);
		return true;
	}
};

} // namespace



#endif // PROPERTYINFO_QLISTMODEL_H
