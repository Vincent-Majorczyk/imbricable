/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef PROPERTYCOMMANDCONSTRUCTOR_H
#define PROPERTYCOMMANDCONSTRUCTOR_H
#pragma once

#include <QUndoStack>
#include "Core/Object/model.h"
#include "Core/Properties/typedpropertyinfo.h"

namespace Imbricable::Properties {

class BasePropertyCommandConstructor
{
protected:
	QUndoStack* _undostack = nullptr;

public:
	BasePropertyCommandConstructor();
	virtual ~BasePropertyCommandConstructor();
	inline void setUndoStack(QUndoStack* undostack) {_undostack=undostack;}
};

template <typename T>
class TypedPropertyCommandConstructor: public BasePropertyCommandConstructor
{
public:
	virtual void create(S_Model object, const TypedPropertyInfo<T>* propertyinfo, T newValue, QUndoCommand *parent = nullptr) = 0;
};

template <class UndoCommand, typename T>
class PropertyCommandConstructor: public TypedPropertyCommandConstructor<T>
{
public:
	void create(S_Model object, const TypedPropertyInfo<T>* propertyinfo, T newValue, QUndoCommand *parent = nullptr) override
	{
		if(this->_undostack != nullptr) this->_undostack->push(new UndoCommand(object,propertyinfo,newValue,parent));
	}
};

};

#endif // PROPERTYCOMMANDCONSTRUCTOR_H
