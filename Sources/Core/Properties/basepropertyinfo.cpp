/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "basepropertyinfo.h"

using namespace Imbricable::Properties;

BasePropertyInfo::BasePropertyInfo(QString name):_name(name)
{

}

BasePropertyInfo::~BasePropertyInfo() = default;

