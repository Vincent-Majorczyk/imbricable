/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef PROPERTYINFO_QLIST_H
#define PROPERTYINFO_QLIST_H

#include <memory>
#include "propertyinfo.h"
#include "Core/Object/model.h"
#include "basepropertyinfo_list.h"

namespace Imbricable::Properties {

/**
 * @brief PropertuInfo revalive to QList
 * @tparam C type of the class relative to the property
 * @tparam T type of the property: `QList<T>`
 */
template <class C, class T>
class PropertyInfo_qlist:
		public PropertyInfo_List<C,QList<T>>
{
public:
	/**
	 * @brief constructor
	 * @param name
	 * @param get function to access the property
	 */
	PropertyInfo_qlist(QString name, std::function<QList<T>&(C*)> get):
		PropertyInfo_List<C,QList<T>>(name, get)
	{
	}
};

} // namespace

#endif // PROPERTYINFO_QLIST_H
