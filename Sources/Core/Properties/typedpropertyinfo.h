﻿/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef TYPEDPROPERTYINFO_H
#define TYPEDPROPERTYINFO_H
#pragma once

#include <memory>
#include "basepropertyinfo.h"
#include "Core/Object/model.h"
#include <QList>
//#include <QUndoStack>

namespace Imbricable::Properties {

class BasePropertyCommandConstructor;

/**
 * @brief abstract class which contains typed assessors (getValue, setValue, checkValue)
 * @tparam T type of the property
 */
template <typename T>
class TypedPropertyInfo: public BasePropertyInfo
{
protected:
	/**
	 * @brief basic constructor of the class
	 * @param name of the property
	 */
	TypedPropertyInfo(QString name):BasePropertyInfo(name)
	{
	}

public:
	/**
	 * @brief get the value of the property from the object
	 * access to the assessor method (get) of the object associated to the property
	 * @param object whose the value is gotten
	 * @return the value
	 */
	virtual T getValue(S_Model object) const = 0;

	/**
	 * @brief get the value of the property from the object
	 * access to the assessor method (get) of the object associated to the property
	 * @param objects whose the value is gotten
	 * @return the value
	 */
	virtual T getValue(QList<S_Model> objects, bool &conflict) const
	{
		conflict = false;
		T value;
		bool first = true;
		for(auto& object: objects)
		{
			if(object == nullptr) continue;
			if(first)
			{
				value = getValue(object);
				first = false;
			}
			else
			{
				T value2 = getValue(object);
				if(value2 != value)
				{
					conflict = true;
					break;
				}
			}
		}
		return value;
	}

	/**
	 * @brief set the value of the property from the object
	 * access to the assessor method (set) of the object associated to the property
	 * @param object whose the value is setted
	 * @param value value to set
	 */
	virtual void setValue(S_Model object, T value) const = 0;

	virtual bool isReadOnly() const = 0;

	/**
	 * @brief set the value of the property from the object by UndoCommand
	 * access to the assessor method (set) of the object associated to the property
	 * @param object whose the value is setted
	 * @param value value to set
	 * @param command
	 */
	virtual void setValue(S_Model object, T value, std::shared_ptr<BasePropertyCommandConstructor> command) const = 0;

	/**
	 * @brief set the value of the property from the object by UndoCommand
	 * access to the assessor method (set) of the object associated to the property
	 * @param objects whose the value is setted
	 * @param value value to set
	 * @param command
	 */
	virtual void setValue(QList<S_Model> objects, T value, std::shared_ptr<BasePropertyCommandConstructor> command) const
	{
		for(auto& object : objects)
		{
			if(object==nullptr) continue;
			setValue(object,value,command);
		}
	}

	/**
	 * @brief checkValue check the validity of the value
	 * access to the checking method of the object associated to the property
	 * @param object whose the value is setted
	 * @param value value to test
	 * @return result of the checking
	 */
	virtual CheckState checkValue(S_Model object, T value) const = 0;

	/**
	 * @brief checkValue check the validity of the value
	 * access to the checking method of the object associated to the property
	 * @param object whose the value is setted
	 * @param value value to test
	 * @return result of the checking
	 */
	CheckState checkValue(QList<S_Model> objects, T value) const
	{
		CheckState state = CheckState::Valid;
		for(auto& object : objects)
		{
			if(object==nullptr) continue;
			CheckState s = checkValue(object, value);
			if(s>state) state = s;
			if(state == CheckState::Error) break;
		}
		return  state;
	}

	//virtual QString toString(S_Model object) const;
	//virtual bool fromString(S_Model object, QString s) const;
};
/*
template<typename T> bool TypedPropertyInfo<T>::fromString(S_Model object,QString s) const
{ return BasePropertyInfo::fromString(object,s); }

template<typename T> QString TypedPropertyInfo<T>::toString(S_Model object) const
{ return BasePropertyInfo::toString(object); }


template<> QString TypedPropertyInfo<QString>::toString(S_Model object) const;
template<> bool TypedPropertyInfo<QString>::fromString(S_Model object,QString s) const;
template<> QString TypedPropertyInfo<double>::toString(S_Model object) const;
template<> bool TypedPropertyInfo<double>::fromString(S_Model object,QString s) const;
template<> QString TypedPropertyInfo<float>::toString(S_Model object) const;
template<> bool TypedPropertyInfo<float>::fromString(S_Model object,QString s) const;
template<> QString TypedPropertyInfo<signed char>::toString(S_Model object) const;
template<> bool TypedPropertyInfo<signed char>::fromString(S_Model object,QString s) const;
template<> QString TypedPropertyInfo<short int>::toString(S_Model object) const;
template<> bool TypedPropertyInfo<short int>::fromString(S_Model object,QString s) const;
template<> QString TypedPropertyInfo<bool>::toString(S_Model object) const;
template<> bool TypedPropertyInfo<bool>::fromString(S_Model object,QString s) const;
template<> QString TypedPropertyInfo<int>::toString(S_Model object) const;
template<> bool TypedPropertyInfo<int>::fromString(S_Model object,QString s) const;
template<> QString TypedPropertyInfo<unsigned int>::toString(S_Model object) const;
template<> bool TypedPropertyInfo<unsigned int>::fromString(S_Model object,QString s) const;
template<> QString TypedPropertyInfo<long int>::toString(S_Model object) const;
template<> bool TypedPropertyInfo<long int>::fromString(S_Model object,QString s) const;
template<> QString TypedPropertyInfo<long long int>::toString(S_Model object) const;
template<> bool TypedPropertyInfo<long long int>::fromString(S_Model object,QString s) const;
template<> QString TypedPropertyInfo<unsigned char>::toString(S_Model object) const;
template<> bool TypedPropertyInfo<unsigned char>::fromString(S_Model object,QString s) const;
template<> QString TypedPropertyInfo<unsigned short int>::toString(S_Model object) const;
template<> bool TypedPropertyInfo<unsigned short int>::fromString(S_Model object,QString s) const;
template<> QString TypedPropertyInfo<unsigned long int>::toString(S_Model object) const;
template<> bool TypedPropertyInfo<unsigned long int>::fromString(S_Model object,QString s) const;
template<> QString TypedPropertyInfo<unsigned long long int>::toString(S_Model object) const;
template<> bool TypedPropertyInfo<unsigned long long int>::fromString(S_Model object,QString s) const;
*/
} // namespace


#endif // TYPEDPROPERTYINFO_H
