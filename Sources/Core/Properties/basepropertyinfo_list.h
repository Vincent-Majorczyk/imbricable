/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef BASEPROPERTYINFO_LIST_H
#define BASEPROPERTYINFO_LIST_H

#include <memory>
#include "Core/Object/model.h"
#include "propertyinfo.h"

namespace Imbricable::Properties {

class BasePropertyInfo_ModelList
{
public:
	BasePropertyInfo_ModelList();
	virtual ~BasePropertyInfo_ModelList();

	virtual unsigned int count(S_Model obj) const = 0;
	virtual S_Model getModel(S_Model obj, unsigned int index) const =0;
	virtual bool appendModel(S_Model obj, S_Model child) const =0;
};

template <typename List>
class TypedPropertyInfo_List: public BasePropertyInfo
{
public:
	TypedPropertyInfo_List(QString name): BasePropertyInfo(name)
	{
	}

	/**
	 * @brief get the value of the property from the object
	 * access to the assessor method (get) of the object associated to the property
	 * @param object where the value is gotten
	 * @return the value
	 */
	virtual List& getList(S_Model object) const = 0;
};

template <typename C,typename List>
class PropertyInfo_List: public TypedPropertyInfo_List<List>
{
protected:
	std::function<List&(C*)> _get; //!< access to the function associated to the property

public:
	PropertyInfo_List(QString name, std::function<List&(C*)> get): TypedPropertyInfo_List<List>(name), _get(get)
	{
	}

	/**
	 * @brief get the value of the property from the object
	 * access to the assessor method (get) of the object associated to the property
	 * @param object where the value is gotten
	 * @return the value
	 */
	virtual List& getList(S_Model object) const
	{
		auto o = std::dynamic_pointer_cast<C>(object);
		return this->_get(o.get());
	}
};

} // namespace

#endif // BASEPROPERTYINFO_LIST_H
