/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef BASEPROPERTYINFO_SUBMODEL_H
#define BASEPROPERTYINFO_SUBMODEL_H

#include <memory>
#include "Core/Object/model.h"

namespace Imbricable::Properties {

class BasePropertyInfo_SubModel
{
public:
	BasePropertyInfo_SubModel();
	virtual ~BasePropertyInfo_SubModel();

	virtual S_Model getModel(S_Model obj) const =0;
	virtual bool setModel(S_Model parent, S_Model child) const =0;
};

} //namespace

#endif // BASEPROPERTYINFO_SUBMODEL_H
