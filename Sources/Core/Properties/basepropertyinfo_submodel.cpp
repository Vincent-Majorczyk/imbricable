/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "basepropertyinfo_submodel.h"

using namespace Imbricable::Properties;

BasePropertyInfo_SubModel::BasePropertyInfo_SubModel()=default;

BasePropertyInfo_SubModel::~BasePropertyInfo_SubModel()=default;
