/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef PROPERTYINFO_H
#define PROPERTYINFO_H
#pragma once

#include "basepropertyinfo.h"
#include "typedpropertyinfo.h"
#include "propertycommandconstructor.h"
#include <functional>
#include <memory>

namespace Imbricable::Properties {


/**
 * @brief class which contains assessors (getValue, setValue, checkValue)
 * @tparam C type of the object
 * @tparam T type of the property
 */
template <class C, typename T>
class PropertyInfo: public TypedPropertyInfo<T>
{
protected:
	std::function<T(C*)> _get; //!> access to the 'get' method associated to property
	std::function<void(C*, T)> _set; //!> access to the 'set' method associated to property
	std::function<CheckState(C*, T)> _check; //!> access to the 'check' method associated to property

public:
	/**
	 * @brief constructor of property
	 * @param name of the property
	 * @param get access to the 'get' method
	 * @param set access to the 'set' method
	 * @param check access to the 'check' method
	 */
	PropertyInfo(QString name, std::function<T(C*)> get , std::function<void(C*, T)> set, std::function<CheckState(C*, T)> check):
		TypedPropertyInfo<T>(name), _get(get), _set(set), _check(check)
	{}

	/**
	 * @brief constructor of property
	 * @param name of the property
	 * @param get access to the 'get' method
	 * @param set access to the 'set' method
	 */
	PropertyInfo(QString name, std::function<T(C*)> get , std::function<void(C*, T)> set):
		TypedPropertyInfo<T>(name), _get(get), _set(set), _check(nullptr)
	{}

	T getValue(S_Model object) const override
	{
		auto obj = std::dynamic_pointer_cast<C>(object);
		return _get(obj.get());
	}

	void setValue(S_Model object, T value) const override
	{
		auto obj = std::dynamic_pointer_cast<C>(object);
		if(_set != nullptr)
			_set(obj.get(), value);
	}

	bool isReadOnly() const override
	{
		return _set == nullptr;
	}

	void setValue(S_Model object, T value, std::shared_ptr<BasePropertyCommandConstructor> command) const override
	{
		auto typedPCC = std::dynamic_pointer_cast<TypedPropertyCommandConstructor<T>>(command);
		if(typedPCC != nullptr)
		{
			if(this->getValue(object) == value) return;
			typedPCC->create(object,this,value);
		}
		this->setValue(object, value);
	}

	CheckState checkValue(S_Model object, T value) const override
	{
		if(_check == nullptr) return CheckState::Valid;
		std::shared_ptr<C> obj = std::dynamic_pointer_cast<C>(object);
		return _check(obj.get(), value);
	}
};


template<typename T>
class ChangePropertyCommand: public QUndoCommand
{
protected:
	S_Model _obj;
	const TypedPropertyInfo<T>* _propertyinfo;
	T _oldvalue;
	T _newvalue;

public:
	ChangePropertyCommand()
	= default;

	ChangePropertyCommand(S_Model model, const TypedPropertyInfo<T>* propertyinfo, T newValue, QUndoCommand *parent = nullptr):
		QUndoCommand(parent),
		_obj(model),
		_propertyinfo(propertyinfo),
		_oldvalue(_propertyinfo->getValue(model)),
		_newvalue(newValue)
	{
		this->setText(QString("Modify %1 %2").arg(Model::getName(model.get())).arg(_propertyinfo->name()));
	}

	~ChangePropertyCommand() override = default;

	void undo() override
	{
		_propertyinfo->setValue(_obj, _oldvalue);
	}

	void redo() override
	{
		_propertyinfo->setValue(_obj, _newvalue);
	}
};

} // namespace

#endif // PROPERTYINFO_H
