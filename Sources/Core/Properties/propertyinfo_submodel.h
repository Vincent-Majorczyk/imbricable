/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef PROPERTYINFO_SUBMODEL_H
#define PROPERTYINFO_SUBMODEL_H

#include "propertyinfo.h"
#include "basepropertyinfo_submodel.h"

namespace Imbricable::Properties {

template <class C, class T>
class PropertyInfo_SubModel: public PropertyInfo<C,std::shared_ptr<T>>, public BasePropertyInfo_SubModel
{
public:
	PropertyInfo_SubModel(QString name, std::function<std::shared_ptr<T>(C*)> get , std::function<void(C*, std::shared_ptr<T>)> set, std::function<CheckState(C*, std::shared_ptr<T>)> check):
		PropertyInfo<C,std::shared_ptr<T>>(name,get,set,check)
	{
	}

	PropertyInfo_SubModel(QString name, std::function<std::shared_ptr<T>(C*)> get , std::function<void(C*, std::shared_ptr<T>)> set):
		PropertyInfo<C,std::shared_ptr<T>>(name,get,set)
	{
	}

	S_Model getModel(S_Model obj) const override
	{
		auto so = this->getValue(obj);
		return std::dynamic_pointer_cast<Model>(so);
	}

	bool setModel(S_Model parent, S_Model child) const override
	{
		auto soC = std::dynamic_pointer_cast<C>(parent);
		auto soT = std::dynamic_pointer_cast<T>(child);
		this->setValue(soC,soT);
		return true;
	}

};

} //namespace

#endif // PROPERTYINFO_SUBMODEL_H
