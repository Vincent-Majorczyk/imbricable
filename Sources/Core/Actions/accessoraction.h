﻿#ifndef ACCESSORACTION_H
#define ACCESSORACTION_H

#include <memory>
#include <functional>
#include <stdexcept>

#include <QString>
#include <QList>

#include "Core/Object/model.h"
#include "Core/Actions/controler.h"
#include "Core/Actions/action.h"

namespace Imbricable
{

template<typename T>
class AccessorActionUndoCommand;

template<typename T>
class BaseAccessorActionUndoCommandBuilder;

template<class C, typename T>
class AccessorActionUndoCommandBuilder;

template<typename T>
class AccessorAction: public Action
{
protected:
	std::function<void(S_Model, T)> _set = nullptr;
	std::function<T(S_Model)> _get = nullptr;
	std::function<CheckState(S_Model, T)> _check = nullptr;

	BaseAccessorActionUndoCommandBuilder<T>* _undoBuilder = nullptr;

public:

	void defaultUndoBuilder();

	AccessorAction(QString name, Imbricable::Controler* controler):
		Action(name, controler)
	{ defaultUndoBuilder(); }

	template<class C, typename GET>
	AccessorAction(QString name, C* controler, GET get):
		Action(name, controler)
	{
		using std::placeholders::_1;
//		std::function<T(C&)> g = get;
		_get = std::bind(get,controler, _1);
		_set = nullptr;
		_check = nullptr;
		defaultUndoBuilder();
	}

	template<class C, typename GET, typename SET>
	AccessorAction(QString name, C* controler, GET get, SET set):
		Action(name, controler)
	{
		using std::placeholders::_1;
		using std::placeholders::_2;
//		std::function<T(C)> g = get;
//		std::function<void(C, T)> s = set;
		_get = std::bind(get,controler,_1);
		_set = std::bind(set,controler, _1, _2);
		_check = nullptr;
		defaultUndoBuilder();
	}

	template<class C, typename GET, typename SET, typename CHECK>
	AccessorAction(QString name, C* controler, GET get, SET set, CHECK check):
		Action(name, controler)
	{
		using std::placeholders::_1;
		using std::placeholders::_2;
//		std::function<T(C&)> g = get;
//		std::function<void(C, T)> s = set;
//		std::function<CheckState(C, T)> c = check;
		_get = std::bind(get, controler, _1);
		_set = std::bind(set, controler, _1, _2);
		_check = std::bind(check, controler, _1, _2);
		defaultUndoBuilder();
	}

	template<class>
	void changeUndoCommand();

/*	template<class C, class UndoCommand, typename GET>
	AccessorAction(QString name, C* controler, GET get):
		Action(name, controler)
	{
		using std::placeholders::_1;
		std::function<T(C&)> g = get;
		_get = std::bind(get,controler, _1);
		_set = nullptr;
		_check = nullptr;
		changeUndoCommand<UndoCommand>();
	}

	template<class C, class UndoCommand, typename GET, typename SET>
	AccessorAction(QString name, C* controler, GET get, SET set):
		Action(name, controler)
	{
		using std::placeholders::_1;
		using std::placeholders::_2;
		std::function<T(C&)> g = get;
		std::function<void(C, T)> s = set;
		_get = std::bind(get,controler,_1);
		_set = std::bind(set,controler, _1, _2);
		_check = nullptr;
		changeUndoCommand<UndoCommand>();
	}

	template<class C, class UndoCommand, typename GET, typename SET, typename CHECK>
	AccessorAction(QString name, C* controler, GET get, SET set, CHECK check):
		Action(name, controler)
	{
		using std::placeholders::_1;
		using std::placeholders::_2;
		std::function<T(C&)> g = get;
		std::function<void(C, T)> s = set;
		std::function<CheckState(C, T)> c = check;
		_get = std::bind(get, controler, _1);
		_set = std::bind(set, controler, _1, _2);
		_check = std::bind(check, controler, _1, _2);
		changeUndoCommand<UndoCommand>();
	}
*/
	~AccessorAction() override;

	template<class C>
	inline void setProperty(std::function<T(C&)> get, std::function<void(C&,T)> set = nullptr, std::function<CheckState(C&,T)> check = nullptr)
	{
		using std::placeholders::_1;
		using std::placeholders::_2;
		_set = std::bind(set, dynamic_cast<C>(this->controler()), _1, _2);
		_get = std::bind(get, dynamic_cast<C>(this->controler()), _1 );
		_check = std::bind(check, dynamic_cast<C>(this->controler()), _1, _2);
	}

	/**
	 * @brief return if the 'set' accessor is not accessible. that means the property is ReadOnly
	 * @return
	 */
	inline bool isReadOnly() override
	{
		return _set == nullptr;
	}

	/**
	 * @brief return if the 'check' method is accessible. that means the a candidate value may be checked
	 * @return
	 */
	inline bool isCheckable()
	{
		return _check != nullptr;
	}

	/**
	 * @brief modify value and register it in undostack
	 * @param value
	 */
	void set(T value);

	/**
	 * @brief modify value and register it in undostack
	 * @param model
	 * @param value
	 */
	void set(S_Model model, T value);

	/**
	 * @brief modify value of a list and register it in undostack
	 * @param model
	 * @param value
	 */
	void set(QList<S_Model>& models, T value);

	/**
	 * @brief modify value but not register it in undostack
	 * @param value
	 */
	void restore(T value);

	/**
	 * @brief modify value but not register it in undostack
	 * @param model
	 * @param value
	 */
	void restore(S_Model model, T value);

	/**
	 * @brief modify a value of a list but not register it in undostack
	 * @param model
	 * @param value
	 */
	void restore(QList<S_Model>& models, T value);

	/**
	 * @brief get the value of the model
	 * @return
	 */
	T get(bool &conflict);

	/**
	 * @brief get the value of the model
	 * @param model
	 * @return
	 */
	T get(S_Model model);

	T get(QList<S_Model> &models, bool &conflict);

	/**
	 * @brief check if the value is valid
	 * @param value
	 * @return
	 */
	CheckState check(T value);

	/**
	 * @brief check if the value is valid
	 * @param model
	 * @param value
	 * @return
	 */
	CheckState check(S_Model model, T value);

	CheckState check(QList<S_Model> &models, T value);

	QUndoCommand* createUndoCommand(S_Model model, AccessorAction<T>* accessor, T newValue, QUndoCommand *parent = nullptr);

};

} // namespace

#include "Core/Actions/accessoractionundocommand.h"

template<typename T>
template<class C>
void Imbricable::AccessorAction<T>::changeUndoCommand()
{
	if(_undoBuilder != nullptr) delete _undoBuilder;

	_undoBuilder = new AccessorActionUndoCommandBuilder<C, T>();
}

template<typename T>
void Imbricable::AccessorAction<T>::defaultUndoBuilder()
{
	if(_undoBuilder != nullptr) delete _undoBuilder;

	_undoBuilder = new AccessorActionUndoCommandBuilder<AccessorActionUndoCommand<T>, T>();
}

template<typename T>
Imbricable::AccessorAction<T>::~AccessorAction()
{
	if(_undoBuilder != nullptr) delete(_undoBuilder);
}

template<typename T>
QUndoCommand* Imbricable::AccessorAction<T>::createUndoCommand(S_Model model, AccessorAction<T>* accessor, T newValue, QUndoCommand *parent)
{
	return _undoBuilder->create(model, accessor, newValue, parent);
}

template<typename T>
void Imbricable::AccessorAction<T>::set(T value)
{
	Imbricable::Controler* c = this->controler();
	if(c == nullptr) throw std::runtime_error("accessor action without controler");
	set(c->models(), value);
}

template<typename T>
void Imbricable::AccessorAction<T>::set(S_Model model, T value)
{
	Imbricable::Controler* c = this->controler();
	if(c == nullptr) throw std::runtime_error("accessor action without controler");
	QUndoStack*u = c->undoStack();

	if(u != nullptr )
		u->push(createUndoCommand(model, this, value));

	this->_set(model, value);
}

template<typename T>
void Imbricable::AccessorAction<T>::set(QList<S_Model>& models, T value)
{
	int count = models.count();
	if(count == 0) return;
	if(count == 1)
	{
		set(models.first(), value);
		return;
	}
	Imbricable::Controler* c = this->controler();
	if(c == nullptr) throw std::runtime_error("accessor action without controler");
	QUndoStack*u = c->undoStack();

	QUndoCommand *commands = nullptr;
	if(u != nullptr )
	{
		commands = new QUndoCommand();
		u->push(commands);
	}

	bool first = true;
	for( auto& model : c->models() )
	{
		if(commands != nullptr)
		{
			auto auc = createUndoCommand(model, this, value, commands);
			if(first)
			{
				commands->setText( QString("%1 and %2 anothers").arg(auc->text()).arg(count-1) );
			}
		}

		this->_set(model, value);
	}

	this->_set(c->models().first(), value);
}

template<typename T>
void Imbricable::AccessorAction<T>::restore(T value)
{
	Imbricable::Controler* c = this->controler();
	if(c == nullptr) throw std::runtime_error("accessor action without controler");
	_set(c->models(), value);
}

template<typename T>
void Imbricable::AccessorAction<T>::restore(S_Model model, T value)
{
	_set(model, value);
}

template<typename T>
void Imbricable::AccessorAction<T>::restore(QList<S_Model> &models, T value)
{
	for( auto& model : models )
	{
		_set(model, value);
	}
}

template<typename T>
T Imbricable::AccessorAction<T>::get(bool &conflict)
{
	Imbricable::Controler* c = this->controler();
	if(c == nullptr) throw std::runtime_error("accessor action without controler");

	return get(c->models(), conflict);
}

template<typename T>
T Imbricable::AccessorAction<T>::get(S_Model model)
{
	return _get(model);
}

template<typename T>
T Imbricable::AccessorAction<T>::get(QList<S_Model> &models, bool &conflict)
{
	conflict = false;
	T value;
	bool first = true;
	for(auto& object: models)
	{
		if(object == nullptr) continue;
		if(first)
		{
			value = _get(object);
			first = false;
		}
		else
		{
			T value2 = _get(object);
			if(value2 != value)
			{
				conflict = true;
				break;
			}
		}
	}
	return value;
}

template<typename T>
Imbricable::CheckState Imbricable::AccessorAction<T>::check(T value)
{
	Imbricable::Controler* c = this->controler();
	if(c == nullptr) throw std::runtime_error("accessor action without controler");

	return check(c->models(), value);
}

template<typename T>
Imbricable::CheckState Imbricable::AccessorAction<T>::check(S_Model model, T value)
{
	if(_check == nullptr) return CheckState::Valid;

	return _check(model, value);
}

template<typename T>
Imbricable::CheckState Imbricable::AccessorAction<T>::check(QList<S_Model> &models, T value)
{
	CheckState worse = CheckState::Valid;
	for(auto& object: models)
	{
		if(object == nullptr) continue;

		CheckState current = check(object, value);
		if(current > worse) worse = current;
		if(worse == CheckState::Error) return CheckState::Error;
	}
	return worse;
}

#endif // ACCESSORACTION_H
