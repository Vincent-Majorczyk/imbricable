#ifndef ACCESSORACTIONUNDOCOMMAND_H
#define ACCESSORACTIONUNDOCOMMAND_H

#include <QUndoCommand>

#include "Core/Object/model.h"

namespace Imbricable
{

template<typename T>
class AccessorAction;


template<typename T>
class AccessorActionUndoCommand: public QUndoCommand
{
private:
	S_Model _model;
protected:
	AccessorAction<T>* _accessor;
	T _oldvalue;
	T _newvalue;

public:
	AccessorActionUndoCommand();

	AccessorActionUndoCommand(S_Model model, AccessorAction<T> *accessor, T newValue, QUndoCommand *parent = nullptr);

	~AccessorActionUndoCommand() override = default;

	inline S_Model model() { return _model; }

	void undo() override;
	void redo() override;

private:
	void initText();
};

template<typename T>
class BaseAccessorActionUndoCommandBuilder
{
public:
	BaseAccessorActionUndoCommandBuilder() = default;
	virtual ~BaseAccessorActionUndoCommandBuilder() = default;

	virtual QUndoCommand* create(S_Model model, AccessorAction<T>* accessor, T newValue, QUndoCommand *parent = nullptr) = 0;
};

template<class C, typename T>
class AccessorActionUndoCommandBuilder: public BaseAccessorActionUndoCommandBuilder<T>
{
	QUndoCommand* create(S_Model model, AccessorAction<T>* accessor, T newValue, QUndoCommand *parent = nullptr) override
	{
		return new C(model, accessor, newValue, parent);
	}
};


} // namespace

#include "accessoraction.h"

template<typename T>
Imbricable::AccessorActionUndoCommand<T>::AccessorActionUndoCommand()= default;

template<typename T>
Imbricable::AccessorActionUndoCommand<T>::AccessorActionUndoCommand(S_Model model, AccessorAction<T>* accessor, T newValue, QUndoCommand *parent):
	QUndoCommand(parent),
	_model(model),
	_accessor(accessor),
	_oldvalue(accessor->get(model)),
	_newvalue(newValue)
{
	initText();
}

template<typename T>
void Imbricable::AccessorActionUndoCommand<T>::initText()
{
	this->setText(QString("Modify %1::%2").arg(Model::getName(_model)).arg(_accessor->name()));
}

template<typename T>
void Imbricable::AccessorActionUndoCommand<T>::undo()
{
	this->_accessor->restore(_model,_oldvalue);
}

template<typename T>
void Imbricable::AccessorActionUndoCommand<T>::redo()
{
	this->_accessor->restore(_model,_newvalue);
}


#endif // ACCESSORACTIONUNDOCOMMAND_H
