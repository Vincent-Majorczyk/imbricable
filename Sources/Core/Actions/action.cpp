#include "action.h"

using namespace Imbricable;

Action::Action(QString name, Imbricable::Controler* controler):
	_name(name),
	_controler(controler)
{

}

Action::~Action() = default;
