#ifndef CONTROLER_H
#define CONTROLER_H

#include <QUndoStack>

#include "Core/Object/model.h"

namespace Imbricable
{

/**
 * @brief The Controler class
 *
 * @todo add undostack
 * @todo add list of BaseAccessorAction
 * @todo add method 'addAction()'
 */
class Controler
{
	QUndoStack *_undostack = nullptr;
	QString _name;
	QList<S_Model> _models;

public:
	/// @brief constructor
	Controler(QString name);
	virtual ~Controler();

	/// @brief set the current model;
	/// @note the model shouldn't be destructed when the controler is deleted
	void setModel(S_Model model)
	{
		_models.clear();
		_models.append(model);
	}

	void setModels(QList<S_Model> models) {_models = models;}

	QList<S_Model>& models() { return _models; }

	/// @brief set the undostack;
	/// @note the undostack is not destructed when the controler is deleted
	inline void setUndoStack(QUndoStack* undostack)
	{ _undostack = undostack; }

	inline QUndoStack* undoStack()
	{ return _undostack; }
};

typedef std::shared_ptr<Controler> S_Controler;

}

#endif // CONTROLER_H
