#include "accessoractionundocommand.h"

template<>
void Imbricable::AccessorActionUndoCommand<QString>::initText()
{
	this->setText(QString("Modify %1::%2 \'%3\'->\'%4\'").arg(Model::getName(_model),_accessor->name(),_oldvalue,_newvalue));
}

template<>
void Imbricable::AccessorActionUndoCommand<qint16>::initText()
{
	this->setText(QString("Modify %1::%2 \'%3\'->\'%4\'").arg(Model::getName(_model),_accessor->name()).arg(_oldvalue,_newvalue));
}

template<>
void Imbricable::AccessorActionUndoCommand<quint16>::initText()
{
	this->setText(QString("Modify %1::%2 \'%3\'->\'%4\'").arg(Model::getName(_model),_accessor->name()).arg(_oldvalue).arg(_newvalue));
}

template<>
void Imbricable::AccessorActionUndoCommand<qint32>::initText()
{
	this->setText(QString("Modify %1::%2 \'%3\'->\'%4\'").arg(Model::getName(_model),_accessor->name()).arg(_oldvalue).arg(_newvalue));
}

template<>
void Imbricable::AccessorActionUndoCommand<quint32>::initText()
{
	this->setText(QString("Modify %1::%2 \'%3\'->\'%4\'").arg(Model::getName(_model),_accessor->name()).arg(_oldvalue).arg(_newvalue));
}

template<>
void Imbricable::AccessorActionUndoCommand<qint64>::initText()
{
	this->setText(QString("Modify %1::%2 \'%3\'->\'%4\'").arg(Model::getName(_model),_accessor->name()).arg(_oldvalue).arg(_newvalue));
}

template<>
void Imbricable::AccessorActionUndoCommand<quint64>::initText()
{
	this->setText(QString("Modify %1::%2 \'%3\'->\'%4\'").arg(Model::getName(_model),_accessor->name()).arg(_oldvalue).arg(_newvalue));
}

template<>
void Imbricable::AccessorActionUndoCommand<float>::initText()
{
	this->setText(QString("Modify %1::%2 \'%3\'->\'%4\'").arg(Model::getName(_model),_accessor->name()).arg( (double)_oldvalue).arg( (double)_newvalue));
}

template<>
void Imbricable::AccessorActionUndoCommand<double>::initText()
{
	this->setText(QString("Modify %1::%2 \'%3\'->\'%4\'").arg(Model::getName(_model),_accessor->name()).arg(_oldvalue).arg(_newvalue));
}
