#ifndef ACTION_H
#define ACTION_H

#include <QString>

#include "Core/Actions/controler.h"

namespace Imbricable
{

class Action
{
	QString _name;
	Imbricable::Controler* _controler = nullptr;

public:

	Action(QString name, Imbricable::Controler* controler);
	virtual ~Action();

	inline QString name(){ return _name; }

	inline Imbricable::Controler* controler() { return _controler; }

	inline bool concernManyModels() { return this->controler()->models().count() > 1; }

	virtual bool isReadOnly() = 0;
};

} // namespace

#endif // ACTION_H
