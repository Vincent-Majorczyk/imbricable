/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef RANGEDNUMBER_H
#define RANGEDNUMBER_H

#include <stdexcept>
#include <QString>
#include "range1d.h"

namespace Imbricable {

/**
 * concerns a value in a range
 */
template<typename Type>
class RangedNumber: public Range1D<Type>
{
protected:
	Type _value; //!> value

public:
	RangedNumber(): Range1D<Type>()
	{ }

	/**
	 * @brief Constructor
	 * @param minimum set the minimum value
	 * @param maximum set the maximum value
	 * @param value set the value
	 */
	RangedNumber( Type minimum, Type maximum, Type value ):
		Range1D<Type>( minimum , maximum), _value( value )
	{
	}

	/**
	 * @brief Constructor
	 * @param range set the range (minimum and maximum)
	 * @param value set the value
	 */
	RangedNumber( Range1D<Type> range, Type value ):
		Range1D<Type>(range.minimum(), range.maximum()), _value( value )
	{
	}

	/**
	 * @brief Constructor to copy value of another RangeNumber
	 * @param range RangeNumber to copy
	 */
	RangedNumber( const RangedNumber& range ):
		Range1D<Type>( range.minimum(), range.maximum() ), _value( range.value() )
	{
	}

	/**
	 * @brief return the value
	 * @return
	 */
	inline int value() const { return _value; }

	/**
	 * @brief set the value
	 * @param value
	 */
	inline void setValue( const Type& value )
	{
		_value = value;
	}

	/**
	 * @brief verify if the value is in the range
	 * @return true if value in range
	 */
	inline bool isValueInRange() const
	{
		return _value >= this->_minimum && _value <= this->_maximum;
	}

	/**
	 * @brief verify if the value is in the range
	 * @return true if value in range
	 */
	inline bool isValueInRange( const Type& value ) const
	{
		return Range1D<Type>::isValueInRange(value);
	}

	/**
	 * @brief verify if the parameter 'minimum' is valid to replace the minimum
	 * @param minimum
	 * @return
	 */
	inline bool verifyMinimum( Type minimum )
	{
		return _value >= minimum && this->_minimum <= this->_maximum;
	}

	/**
	 * @brief verify if the parameter 'maximum' is valid to replace the maximum
	 * @param maximum
	 * @return
	 */
	inline bool verifyMaximum( Type maximum )
	{
		return _value <= maximum;
	}

	/**
	 * @brief set the value
	 * @param value
	 * @return
	 */
	inline RangedNumber& operator=( const Type& value )
	{
		this->setValue(value);
		return *this;
	}

	/**
	 * @brief erase value, maximum and minimum by data from another RangeNumber
	 * @param value
	 * @return
	 */
	inline RangedNumber& operator=( const RangedNumber& value )
	{
		this->setValue(value.value());
		this->setMaximum(value.maximum());
		this->setMinimum(value.minimum());
		return *this;
	}

	/**
	 * @brief comparison
	 * @param value
	 * @return
	 */
	inline bool operator==( const RangedNumber& value )
	{
		return (this->value() == value.value() && this->maximum() == value.maximum() && this->minimum() == value.minimum());
	}

	/**
	 * @brief comparison
	 * @param value
	 * @return
	 */
	inline bool operator!=( const RangedNumber& value )
	{
		return (this->value() != value.value() || this->maximum() != value.maximum() || this->minimum() != value.minimum());
	}

	/**
	 * @brief increment value
	 * @return
	 */
	inline RangedNumber& operator++()
	{
		_value++;
		if(_value>this->_maximum) _value=this->_maximum;
		return *this;
	}

	/**
	 * @brief decrement value
	 * @return
	 */
	inline RangedNumber& operator--()
	{
		_value--;
		if(_value<this->_minimum) _value=this->_minimum;
		return *this;
	}

	/**
	 * @brief add the parameter 'value' to the value
	 * @param value
	 * @return
	 */
	inline RangedNumber& operator+=( const Type& value )
	{
		_value+=value;
		if(value>this->_maximum) _value=this->_maximum;
		if(value<this->_minimum) _value=this->_minimum;
		return *this;
	}

	/**
	 * @brief substract the parameter 'value' to the value
	 * @param value
	 * @return
	 */
	inline RangedNumber& operator-=( const Type& value )
	{
		_value-=value;
		if(value>this->_maximum) _value=this->_maximum;
		if(value<this->_minimum) _value=this->_minimum;
		return *this;
	}
};

/**
 * @brief version of RangeNumber for int32
 */
using RangedInt = RangedNumber<int>;

/**
 * @brief version of RangeNumber for double
 */
using RangedDouble = RangedNumber<double>;

} // namespace

#endif // RANGEDNUMBER_H
