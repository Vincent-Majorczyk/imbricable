/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef RANGE1D_H
#define RANGE1D_H

namespace Imbricable {

template<typename Type>
class Range1D
{
protected:
	Type _minimum; //!> minimum value
	Type _maximum; //!> maximum value

public:
	Range1D() = default;

	/**
	 * @brief Constructor
	 * @param minimum set the minimum value
	 * @param maximum set the maximum value
	 */
	Range1D( Type minimum, Type maximum):
		_minimum( minimum ), _maximum( maximum )
	{
	}

	/**
	 * @brief Constructor to copy value of another RangeNumber
	 * @param range RangeNumber to copy
	 */
	Range1D( const Range1D& range ):
		_minimum( range.minimum() ), _maximum( range.maximum() )
	{
	}

	/**
	 * @brief return the minimum value
	 * @return
	 */
	inline int minimum() const { return _minimum; }

	/**
	 * @brief set the minimum value
	 * @param min
	 */
	inline void setMinimum( Type min )
	{
		_minimum = min;
	}

	/**
	 * @brief return the maximum value
	 * @return
	 */
	inline int maximum() const { return _maximum; }

	/**
	 * @brief set the maximum value
	 * @param max
	 */
	inline void setMaximum( Type max )
	{
		_maximum = max;
	}

	/**
	 * @brief set the range from another
	 * @param minimum set the minimum value
	 * @param maximum set the maximum value
	 */
	inline void set(const Type& minimum, const Type& maximum) { _minimum = minimum; _maximum = maximum; }

	/**
	 * @brief set the range from another
	 * @param range
	 */
	inline void set(const Range1D<Type>& range) { _minimum = range._minimum; _maximum = range._maximum; }

	/**
	 * @brief verify if the parameter 'value' is in the range
	 * @param value value to check
	 * @return true if the parameter 'value is in range
	 */
	inline bool isValueInRange( const Type& value ) const
	{
		return value >= _minimum && value <=_maximum;
	}

	/**
	 * @brief set the value
	 * @param value
	 * @return
	 */
	inline Range1D& operator=( const Type& value )
	{
		this->setValue(value);
		return *this;
	}

	/**
	 * @brief erase value, maximum and minimum by data from another RangeNumber
	 * @param value
	 * @return
	 */
	inline Range1D& operator=( const Range1D& value )
	{
		this->setMaximum(value.maximum());
		this->setMinimum(value.minimum());
		return *this;
	}

	inline bool operator==( const Range1D& value )
	{
		return (this->maximum() == value.maximum() && this->minimum() == value.minimum());
	}

	inline Range1D& intializeBoundaryBox(const Type& value)
	{
		_maximum = value;
		_minimum = value;
		return *this;
	}

	inline Range1D& extendBoundaryBox(const Type& value)
	{
		if(value>_maximum) _maximum = value;
		if(value<_minimum) _minimum = value;
		return *this;
	}
};

/**
 * @brief version of Range1D for int32
 */
using Range1DInt = Range1D<int>;

/**
 * @brief version of Range1D for double
 */
using Range1DDouble = Range1D<double>;

} // namespace

#endif // RANGE1D_H
