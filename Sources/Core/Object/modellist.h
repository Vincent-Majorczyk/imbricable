/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef MODELLIST_H
#define MODELLIST_H

#include <QList>
#include <memory>

namespace Imbricable {

/*
template<class C>
using ModelList = QList<std::shared_ptr<C>>;
*/

} // namespace

#endif // MODELLIST_H
