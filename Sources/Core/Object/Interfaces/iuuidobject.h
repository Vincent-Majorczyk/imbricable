/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef IUUIDPROPERTY_H
#define IUUIDPROPERTY_H

#include <QUuid>

namespace Imbricable {

class IUuidObject
{
public:
	IUuidObject();
	IUuidObject(QUuid id);

	virtual ~IUuidObject();

	QUuid id;

	inline void initializeId() { id = QUuid::createUuid(); }
};

} // namespace

#endif // IUUIDPROPERTY_H
