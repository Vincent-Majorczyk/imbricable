/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "iuuidobject.h"

using namespace Imbricable;

IUuidObject::IUuidObject()
{ }

IUuidObject::IUuidObject(QUuid id):
	id(id)
{ }

IUuidObject::~IUuidObject() = default;
