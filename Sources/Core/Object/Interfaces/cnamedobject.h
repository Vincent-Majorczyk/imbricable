/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef CNAMEDPROPERTY_H
#define CNAMEDPROPERTY_H

#include "inamedobject.h"
#include "Core/Properties/propertyinfo.h"

namespace Imbricable {

class CNamedObject : public INamedObject
{
public:
	QString name = "Default";				//!> Name property

public:
	/**
	 * @brief Constructor
	 * @param name default value
	 */
	CNamedObject(const QString& getName);

	/**
	 * @brief Constructor
	 */
	CNamedObject();

	/**
	  * @brief destructor
	  */
	~CNamedObject() override;

	/**
	 * @brief get the name
	 * @return the value
	 */
	inline QString getName() const override { return name; }

	/**
	 * @brief set the name
	 * @param name
	 */
	inline void setName(QString name) { name = name; }
};

} //namespace

#endif // CNAMEDPROPERTY_H
