#ifndef IEQUAL_H
#define IEQUAL_H

#include "Core/Object/model.h"

namespace Imbricable {

template<class M>
class IEqual
{
public:
	virtual ~IEqual()
	{}

	virtual bool isEqual(M& item) = 0;
};

typedef IEqual<Imbricable::Model> IEqualModel;
typedef IEqual<Imbricable::S_Model> IEqualSModel;

} // namespace

#endif // IEQUAL_H
