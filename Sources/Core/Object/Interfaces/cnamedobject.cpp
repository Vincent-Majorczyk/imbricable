/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "cnamedobject.h"

using namespace Imbricable;

CNamedObject::CNamedObject(const QString& name):
	name(name)
{ }

CNamedObject::CNamedObject()
{
	name = "Default";
}

CNamedObject::~CNamedObject() = default;
