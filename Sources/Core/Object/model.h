/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef MODEL_H
#define MODEL_H
#pragma once

#include <QString>
#include <memory>
#include "Interfaces/inamedobject.h"
#include "Interfaces/iuuidobject.h"

namespace Imbricable {

/**
 * @brief correspond to the return type of methods to check properties.
 * Class which uses PropertyInfo to declare their property may have methods to validate property.
 *
 * @code c++
 * class ExampleModel: public Model
 * {
 * public:
 *	static const Properties::PropertyInfo<ExampleModel,double> property;
 *
 * protected:
 *	double _value = 0;
 *
 * public:
 *	inline double myValue() const { return _value; }
 *
 *	inline void setMyValue(double value) { _value = value; }
 *
 * 	inline CheckState checkMyMaxRange(double value)
 *	{ return (value >= 0)? CheckState::Valid : CheckState::Error; }
 * }
 *
 * const PropertyInfo<ExampleModel,double>
 *   ExampleModel::property ("Value", ExampleModel::myValue, ExampleModel::setMyValue);
 * @endcode
 *
 * @see PropertyInfo
 */
enum CheckState
{
	Valid = 0,
	Warning = 1,
	Error = 2,
	Conflict = 3
};

/**
 * @brief base virtual class to manage classes
 */
class Model
{
public:
	Model();
	virtual ~Model();

	static QString getName(Model* model)
	{
		auto namedObject = dynamic_cast<INamedObject*>(model);
		if(namedObject != nullptr) return  namedObject->getName();

		auto indexedObject = dynamic_cast<IUuidObject*>(model);
		if(indexedObject != nullptr) return  indexedObject->id.toString();

		return "Unknow";
	}

	static QString getName(std::shared_ptr<Model> model)
	{
		return getName(model.get());
	}

	static QString getName(std::weak_ptr<Model> model)
	{
		auto m = model.lock();
		return getName(m.get());
	}
};

using S_Model = std::shared_ptr<Model>;
using W_Model = std::weak_ptr<Model>;

} // namespace

#endif // MODEL_H
