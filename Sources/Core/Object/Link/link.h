/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef LINK_H
#define LINK_H

#include "Core/Object/Interfaces/iuuidobject.h"
#include "Core/Object/model.h"
#include <memory>

namespace Imbricable {

/**
 * @brief link to an IUuidObject;
 *
 * When the program loads file to create items, links may point to an later item.
 * So link kwow an identifiant but are unable to point to the uncreated item.
 *
 * The loading must be doing in two step:
 * - create items and read identifiants of link;
 * - associate identifiant to items;
 *
 * **Principe:**
 * @code
 * // create an object
 * std::shared_ptr<Model1> object(new Model1);
 * // associate
 * object->setLinkToModel2(BaseLink(QUuid("67C8770B-44F1-410A-AB9A-F9B5446F13EE")));
 *
 * @endcode
 */
class Link
{
public:

	class CoreLink
	{
	public:
		CoreLink(Link*parent);
		CoreLink(QUuid id, std::weak_ptr<IUuidObject> pointer, Link* parent);

		QUuid id;
		std::weak_ptr<IUuidObject> pointer;
		Link* parent;
	};

private:
	std::shared_ptr<CoreLink> _corelink = nullptr;
	Model* _parent;

public:

	Link();

	/**
	 * @brief constructor
	 * @param object
	 * @param parent
	 */
	Link(std::weak_ptr<IUuidObject> object, Model* parent);

	/**
	 * @brief constructor
	 * @param id
	 * @param parent
	 */
	Link(QUuid id, Model* parent);

	/**
	 * @brief constructor
	 * @param l
	 * @param parent
	 */
	Link(const Link&l, Model* parent);

	/**
	 * @brief constructor
	 * @param l
	 * @param parent
	 */
	Link(const Link&l);

	/**
	 * @brief constructor
	 */
	Link(Model* parent);

	inline static Link create(std::shared_ptr<IUuidObject> object, std::shared_ptr<Model> parent)
	{
		return Link(std::weak_ptr<Imbricable::IUuidObject>(std::dynamic_pointer_cast<IUuidObject>(object)), parent.get());
	}

	inline static Link create(std::shared_ptr<IUuidObject> object, Model* parent)
	{
		return Link(std::weak_ptr<Imbricable::IUuidObject>(std::dynamic_pointer_cast<IUuidObject>(object)), parent);
	}

	/**
	 * @brief assignation
	 *
	 * assigne id, pointer but not parent
	 *
	 * @param link
	 */
	void operator=(const Link& link);

	/**
	 * @brief comparison
	 *
	 * assigne id, pointer but not parent
	 *
	 * @param link
	 */
	bool operator==(const Link& link);

	/**
	 * @brief comparison
	 *
	 * assigne id, pointer but not parent
	 *
	 * @param link
	 */
	bool operator!=(const Link& link);

	/**
	 * @brief comparison
	 *
	 * assigne id, pointer but not parent
	 *
	 * @param link
	 */
	bool operator==(const QUuid& id);
	/**
	 * @brief comparison
	 *
	 * assigne id, pointer but not parent
	 *
	 * @param link
	 */
	bool operator!=(const QUuid& id);

	/**
	 * @brief destructor
	 */
	virtual ~Link();

	/**
	 * @brief get the identifiant
	 * @return
	 */
	QUuid id() const;

	/**
	 * @brief set the identifiant
	 * @param id
	 */
	void setId(QUuid id);

	/**
	 * @brief get the object
	 * @return
	 */
	std::weak_ptr<IUuidObject> iuuidPointer() const;

	template<class C>
	inline std::shared_ptr<C> pointer() const
	{
		return std::dynamic_pointer_cast<C>(iuuidPointer().lock());
	}

	/**
	 * @brief set the object
	 * @param pointer
	 */
	void setIUuidObject(std::shared_ptr<IUuidObject> pointer);

	inline Model* parent() const { return _parent; }

	inline void setParent(Model* p) { _parent = p; }

	bool isValid();

	QString toString() const;

private:
	void removeInManager();
	void addInManager();
};

inline bool operator==(const Link& lhs, const Link& rhs){ return lhs.iuuidPointer().lock() == rhs.iuuidPointer().lock() && lhs.id() == lhs.id(); }

} // namespace

#endif // LINK_H
