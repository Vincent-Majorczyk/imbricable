/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "link.h"
#include "linksmanager.h"
#include "../Interfaces/inamedobject.h"

using namespace Imbricable;


Link::CoreLink::CoreLink(QUuid id, std::weak_ptr<IUuidObject> pointer, Link* parent):
	id(id),
	pointer(pointer),
	parent(parent)
{ }

Link::CoreLink::CoreLink(Link* parent):
	id(),
	pointer(  std::weak_ptr<IUuidObject>() ),
	parent(parent)
{ }

Link::Link():
	_corelink(new CoreLink(this)),
	_parent(nullptr)
{ }

Link::Link(std::weak_ptr<IUuidObject> object, Model* parent):
	_corelink(new CoreLink(QUuid(),object,this)),
	_parent(parent)
{ addInManager(); }

Link::Link(QUuid id, Model* parent):
	_corelink(new CoreLink(id,std::weak_ptr<IUuidObject>(),this)),
	_parent(parent)
{ addInManager(); }

Link::Link(Model*parent):
	_corelink(new CoreLink(QUuid(),std::weak_ptr<IUuidObject>(),this)),
	_parent(parent)
{ addInManager(); }

Link::Link(const Link& l, Model* parent):
	_corelink(new CoreLink(l.id(), l.iuuidPointer(), this)),
	_parent(parent)
{ addInManager(); }

Link::Link(const Link& l):
	_corelink(new CoreLink(l.id(), l.iuuidPointer(), this)),
	_parent(nullptr)
{ addInManager(); }

Link::~Link()
{
	removeInManager();
}

void Link::operator=(const Link& link)
{
	removeInManager();
	_corelink->id = link.id();
	_corelink->pointer = link.iuuidPointer();
	addInManager();
}

bool Link::operator==(const Link& link)
{ return this->id() == link.id(); }

bool Link::operator!=(const Link& link)
{ return this->id() != link.id(); }

bool Link::operator==(const QUuid& id)
{ return  this->id() == id; }

bool Link::operator!=(const QUuid& id)
{ return  this->id() != id; }

QUuid Link::id() const
{
	if(_corelink == nullptr) return _corelink->id;
	if(_corelink->pointer.expired()) return _corelink->id;
	else
	{
		return _corelink->pointer.lock()->id;
	}
}

void Link::setId(QUuid id)
{
	removeInManager();
	if(!_corelink->pointer.expired() && _corelink->pointer.lock()->id == id) return;

	_corelink->pointer.reset();
	_corelink->id = id;
	addInManager();
}

std::weak_ptr<IUuidObject> Link::iuuidPointer() const
{
	return _corelink->pointer;
}

void Link::setIUuidObject(std::shared_ptr<IUuidObject> pointer)
{
	removeInManager();
	_corelink->pointer = pointer;
	_corelink->id = QUuid();
	addInManager();
}

bool Link::isValid()
{
	if(!_corelink->pointer.expired()) return true;
	return false;
}

void Link::removeInManager()
{
	LinksManager::getInstance().remove(_corelink);
}

void Link::addInManager()
{
	LinksManager::getInstance().add(_corelink);
}

QString Link::toString() const
{
	auto ptr = _corelink->pointer.lock();
	auto ptr2 = std::dynamic_pointer_cast<INamedObject>(ptr);
	QUuid id = this->id();

	if(ptr2 != nullptr)
	{
		return QString("Link{%1}").arg(ptr2->getName());
	}

	if(!id.isNull())
	{
		return QString("Link%1").arg(id.toString());
	}

	return  QString("Link{null}");
}

