/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef LINKSMANAGER_H
#define LINKSMANAGER_H

#include "../Interfaces/iuuidobject.h"
#include <QMap>
#include "link.h"
#include "Core/Factory/TypeFactory/typefactory.h"

namespace Imbricable {

/**
 * @brief Singleton class to manage retolink;
 *
 * Initialy created to manage loading files, this may be used to check if objects are linked to a specific object and avoid breaked link if this last is deleted
 *
 * @todo remove all
 */
class LinksManager
{
public:
	/**
	 * @brief a structure with a pointer to an object and the links which points to this
	 */
	struct RetroLink
	{
		std::shared_ptr<IUuidObject> pointer;
		QList<std::weak_ptr<Link::CoreLink>> links;

		RetroLink();
		RetroLink(std::shared_ptr<IUuidObject> p);
		RetroLink(std::weak_ptr<Link::CoreLink> link);
	};

	enum ObjectDiagnostic
	{
		ObjectOk, //!< no problem found
		ObjectObsolete, //!< the object is obsolete: the object
		ObjectNotExist, //!< the object doesn't exist
		ObjectRetroLinkProblem, //!< if a retrolink of an object return a diagnostic different from RetroLinkOk
	};

	enum RetroLinkDiagnostic
	{
		RetroLinkOk,
		RetroLinkObsolete,
		RetroLinkNotExist,
		RetroLinkNotAssociated,
	};

private:
	QMap<QUuid, RetroLink> _map;

public:
	LinksManager();
	static LinksManager& getInstance();

	/**
	 * @brief append a retrolink based on a QUuid
	 * @param quuid
	 */
	void add(QUuid quuid);

	/**
	 * @brief append a retrolink based on a linked object
	 * @param link
	 */
	void add(std::shared_ptr<Link::CoreLink> link);

	/**
	 * @brief append a retrolink directly based on an item
	 * @param item
	 */
	void add(std::shared_ptr<IUuidObject> item);

	/**
	 * @brief remove the retrolink associated to an uuid
	 * @param uuid
	 */
	void remove(QUuid uuid);

	/**
	 * @brief remove a link in a retrolink
	 * @param link
	 */
	void remove(std::shared_ptr<Link::CoreLink> link);

	/**
	 * @brief remove the retrolink associated to an item
	 * @param item
	 */
	void remove(std::shared_ptr<IUuidObject> item);

	/**
	 * @brief remove all retrolinks
	 */
	void removeAll();

	/**
	 * @brief remove expired links of the retrolink associated to a uuid
	 * @param uuid
	 */
	void clear(QUuid uuid);

	/**
	 * @brief remove expired links of the retrolink associated to an item
	 * @param item
	 */
	void clear(std::shared_ptr<IUuidObject> item);

	/**
	 * @brief remove expired links of all retrolinks
	 */
	void clearAll();

	/**
	 * @brief return if undefinded retrolink exists
	 * @return
	 * @see getUndefinedRetroLinks
	 */
	bool hasUndefinedRetrolink();

	/**
	 * @brief get a list of RetroLinks which have no pointer.
	 * this method aims to verify links. if a link exist but it isn't associated to an object then the link is unvailable
	 * @return
	 */
	const QMap<QUuid, RetroLink> getProblematicRetroLinks();

	/**
	 * @brief get a list of RetroLinks.
	 * @return
	 */
	const QMap<QUuid, RetroLink> getRetroLinks();


	static RetroLinkDiagnostic diagnose(std::weak_ptr<Link::CoreLink>& link);
	static QString diagnose(std::weak_ptr<Link::CoreLink>& link, QUuid& id, QString& name, QString& type, std::shared_ptr<TypeFactory> factory);

	static ObjectDiagnostic diagnose(const LinksManager::RetroLink& link);
	static QString diagnose(const LinksManager::RetroLink& link, QString& name, QString& type, std::shared_ptr<TypeFactory> factory = nullptr);

	LinksManager(const LinksManager&) = delete; //<! no constructor, singleton class
	void operator=(const LinksManager&) = delete; //<! no assignation, singleton class
private:
	/**
	 * @brief remove expired links of a specific retrolink
	 * @param retro
	 */
	void clear(RetroLink& retro);

	/**
	 * @brief make a diagnostic of the corelink
	 * @param link retrolink to diagnose
	 * @param name name of the object if available
	 * @return
	 */
	static bool isObsolete(RetroLink&link);
};

} // namespace

#endif // LINKSMANAGER_H
