/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "linksmanager.h"
#include "Core/Convenient/exceptionsthrowing.h"
#include <typeindex>
#include "Core/Object/Interfaces/inamedobject.h"
#include "Core/Factory/TypeFactory/typefactory.h"
#include <QObject>

using namespace Imbricable;

LinksManager::RetroLink::RetroLink() = default;

LinksManager::RetroLink::RetroLink(std::shared_ptr<IUuidObject> p)
{
	this->pointer = p;
}

LinksManager::RetroLink::RetroLink(std::weak_ptr<Link::CoreLink> link)
{
	this->links.append(link);
}

LinksManager::LinksManager() = default;

LinksManager& LinksManager::getInstance()
{
	static LinksManager instance;
	return instance;
}

void LinksManager::add(QUuid uuid)
{
	// if the map does not contain the key uuid then add it;
	if(! _map.contains(uuid))
	{
		RetroLink i;
		_map.insert(uuid, i);
	}
}

void LinksManager::add(std::shared_ptr<Link::CoreLink> link)
{
	std::shared_ptr<IUuidObject> linkedObject = link->pointer.lock();
	QUuid uuid;

	// get the uuid of the object linked if it exists
	if(linkedObject != nullptr)
	{
		uuid = linkedObject->id;
	}
	else // else use the registrated uuid
	{
		uuid = link->id;
	}

	// if uuid is null, that means an empty link
	if(uuid.isNull()) return;

	// if the map does not contain the key uuid, then add an Item based on it;
	if(! _map.contains(uuid))
	{
		RetroLink retro(link);
		_map.insert(uuid, retro);
	}
	else // if the key exists, then update Item
	{
		RetroLink &retro = _map[uuid];
		retro.links.append(link);

		// if the pointer of the retro is not completed and the link contains a valid pointer,
		// then uses this pointer to complete the retro and update other links
		if(retro.pointer == nullptr)
		{
			if(linkedObject != nullptr)
			{
				retro.pointer = linkedObject;

				for(auto& wlink:retro.links)
				{
					auto slink = wlink.lock();
					if(slink == nullptr) continue;
					slink->pointer = linkedObject;
				}
			}
		}
		// if the pointer of the retro exists but the link does not contain a valid parameter,
		// then update the link
		else if(linkedObject == nullptr)
		{
			link->pointer = retro.pointer;
		}
	}
}

void LinksManager::add(std::shared_ptr<IUuidObject> item)
{
	QUuid uuid = item->id;

	// if the uuid of the item is null, then thretro exception
	if(uuid.isNull())
	{
		IUuidObject& model = *(item.get());
		std::type_index t(typeid(model));
		ExceptionsThrowing::throwNullUuidException(t.name());
	}

	// if the map does not contain the key uuid, then add an Item based on it;
	if(! _map.contains(uuid))
	{
		RetroLink i(item);
		_map.insert(uuid, i);
	}
	else // if the key exists, then update Item
	{
		RetroLink &i = _map[uuid];
		i.pointer = item;
		for(auto& wlink:i.links)
		{
			auto slink = wlink.lock();
			if(slink == nullptr) continue;
			slink->pointer = item;
		}
	}
}

void LinksManager::remove(QUuid uuid)
{
	// if the uuid of the item is null, then nothing to remove
	if(uuid.isNull()) return;

	// if the object is not registered then nothing to remove
	if(! _map.contains(uuid)) return;

	_map.remove(uuid);
}

void LinksManager::remove(std::shared_ptr<IUuidObject> item)
{
	QUuid uuid = item->id;
	remove(uuid);
}

void LinksManager::remove(std::shared_ptr<Link::CoreLink> link)
{
	std::shared_ptr<IUuidObject> linkedObject = link->pointer.lock();
	QUuid uuid;

	// get the uuid of the object linked if it exists
	if(linkedObject != nullptr)
	{
		uuid = linkedObject->id;
	}
	else // else use the registrated uuid
	{
		uuid = link->id;
	}

	// if uuid is null, then nothing to remove
	if(uuid.isNull()) return;

	// if the map does not contain the key uuid, then nothing to remove
	if(! _map.contains(uuid)) return;

	// remove the link
	RetroLink &retro = _map[uuid];
	int i=0;
	while(i<retro.links.count())
	{
		auto &l = retro.links.at(i);
		auto l2 = l.lock();
		if(l2==link) retro.links.removeAt(i);
		else i++;
	}
}

void LinksManager::removeAll()
{
	_map.clear();
}

void LinksManager::clear(QUuid uuid)
{
	// if the uuid of the item is null, then nothing to clear
	if(uuid.isNull()) return;

	// if the object is not registered then nothing to clear
	if(! _map.contains(uuid)) return;

	// clear retro
	clear(_map[uuid]);

}

void LinksManager::clear(std::shared_ptr<IUuidObject> item)
{
	QUuid uuid = item->id;
	remove(uuid);
}

void LinksManager::clear(RetroLink& retro)
{
	int i=0;
	while(i<retro.links.count())
	{
		std::weak_ptr<Link::CoreLink> corelink = retro.links.at(i);
		if(corelink.expired()) retro.links.removeAt(i);
		else i++;
	}
}

void LinksManager::clearAll()
{
	for(auto& retro: this->_map)
	{
		clear(retro);
	}
}

bool LinksManager::hasUndefinedRetrolink()
{
	for(auto& retro: this->_map)
	{
		if(retro.pointer == nullptr) return true;
	}
	return false;
}

const QMap<QUuid, LinksManager::RetroLink> LinksManager::getProblematicRetroLinks()
{
	QMap<QUuid, LinksManager::RetroLink> list;

	QMapIterator<QUuid, LinksManager::RetroLink> i(_map);
	while (i.hasNext())
	{
		i.next();
		QUuid id = i.key();
		auto &retro = i.value();
		if(LinksManager::diagnose(retro) != ObjectDiagnostic::ObjectOk) list.insert(id,retro);
	}

	return list;
}

const QMap<QUuid, LinksManager::RetroLink> LinksManager::getRetroLinks()
{
	QMap<QUuid, LinksManager::RetroLink> list;

	QMapIterator<QUuid, LinksManager::RetroLink> i(_map);
	while (i.hasNext())
	{
		i.next();
		QUuid id = i.key();
		auto &retro = i.value();
		list.insert(id,retro);
	}

	return list;
}

LinksManager::ObjectDiagnostic LinksManager::diagnose(const LinksManager::RetroLink& link)
{
	if(link.pointer == nullptr)
	{
		bool obsolete = true;
		bool retrolinkError = false;

		for(auto l:link.links)
		{
			if(l.lock() != nullptr)
			{
				obsolete = false;
				retrolinkError |= diagnose(l) != RetroLinkOk;
				if(retrolinkError) break;
			}
		}

		if(obsolete) return ObjectDiagnostic::ObjectObsolete;
		else if(!retrolinkError) return ObjectDiagnostic::ObjectNotExist;
		else return ObjectDiagnostic::ObjectRetroLinkProblem;
	}

	return ObjectDiagnostic::ObjectOk;
}

QString LinksManager::diagnose(const LinksManager::RetroLink& link, QString& name, QString& type, std::shared_ptr<TypeFactory> factory)
{
	ObjectDiagnostic value = LinksManager::diagnose(link);
	QString diagnostic;
	name = "~";
	type = "~";

	switch (value)
	{
	case ObjectObsolete:
		return QObject::tr("the object is obsolete");
	case ObjectNotExist:
		return QObject::tr("the object doesn't exist");
	case ObjectRetroLinkProblem:
		diagnostic = QObject::tr("the object have problematic retrolink");
		break;
	case ObjectOk:
		break;
	}

	auto named = std::dynamic_pointer_cast<INamedObject>(link.pointer);
	if(named == nullptr)
	{
		name = QObject::tr("~unnamed~");
	}
	else name = named->getName();

	auto model = std::dynamic_pointer_cast<Model>(link.pointer);
	if(model != nullptr && factory != nullptr)
	{
		try
		{
			type = factory->getAlias(model);
		}
		catch(std::exception &){}
	}

	return diagnostic;
}

LinksManager::RetroLinkDiagnostic LinksManager::diagnose(std::weak_ptr<Link::CoreLink>& link)
{
	auto cl = link.lock();

	if(cl == nullptr) return RetroLinkDiagnostic::RetroLinkObsolete;

	Model* model;

	try
	{
		model = cl->parent->parent();
	}
	catch (std::exception &)
	{
		return RetroLinkDiagnostic::RetroLinkNotExist;
	}

	if(model == nullptr) return RetroLinkDiagnostic::RetroLinkNotAssociated;

	return RetroLinkDiagnostic::RetroLinkOk;
}

QString LinksManager::diagnose(std::weak_ptr<Link::CoreLink>& link, QUuid& id, QString& name, QString& type, std::shared_ptr<TypeFactory> factory)
{
	RetroLinkDiagnostic value = LinksManager::diagnose(link);
	name = "~";

	switch (value)
	{
	case RetroLinkObsolete:
		return QObject::tr("the retrolink is obsolete");
	case RetroLinkNotExist:
		return QObject::tr("Link reference of the retrolink deleted");
	case RetroLinkNotAssociated:
		return QObject::tr("Link reference of the retrolink not associated to an object");
	case RetroLinkOk:
		auto cl = link.lock();
		Model* model = cl->parent->parent();

		auto idobj = dynamic_cast<IUuidObject*>(model);
		if(idobj == nullptr) id = QUuid();
		else id = idobj->id;

		auto named = dynamic_cast<INamedObject*>(model);

		if(named == nullptr) name = QObject::tr("~unnamed~");
		else name = named->getName();

		if(model != nullptr && factory != nullptr)
		{
			try
			{
				type = factory->getAlias(model);
			}
			catch(std::exception &) { }
		}

		return QString();
	}
}

bool LinksManager::isObsolete(LinksManager::RetroLink& link)
{
	for(int i=0;i< link.links.count();)
	{
		auto l = link.links[i];
		if(l.lock() == nullptr)
		{
			link.links.removeAt(i);
		}
		else i++;
	}

	return false;
}
