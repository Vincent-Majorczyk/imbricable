/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "linklist.h"

using namespace Imbricable;

LinkList::LinkList(Model *parent):
	_parent(parent)
{ }


void LinkList::append(std::shared_ptr<Imbricable::IUuidObject> unit)
{
	this->QList<Link>::append(Imbricable::Link::create(unit,_parent));
}

void LinkList::append(Link unit)
{
	Link current(unit,_parent);
	this->QList<Link>::append(current);
}

void LinkList::append(QUuid unit)
{
	Link current(unit,_parent);
	this->QList<Link>::append(current);
}
