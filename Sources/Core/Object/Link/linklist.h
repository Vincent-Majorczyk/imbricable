/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef LINKLIST_H
#define LINKLIST_H

#include <QList>
#include "link.h"

namespace Imbricable {

class LinkList: public QList<Link>
{
	Model * _parent;
public:
	LinkList(Model *parent);

	void append(std::shared_ptr<Imbricable::IUuidObject> unit);
	void append(Link unit);
	void append(QUuid unit);
};

} // namespace

#endif // LINKLIST_H
