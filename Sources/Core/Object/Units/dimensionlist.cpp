#include "dimensionlist.h"
#include "units.h"

using namespace Imbricable::Units;

DimensionList::DimensionList()
{

}

void DimensionList::initializeDefault()
{
	_list.append( std::make_shared<Length>(this) );
	_list.append( std::make_shared<Area>(this) );
	_list.append( std::make_shared<Volume>(this) );
	_list.append( std::make_shared<Angle>(this) );

	for(auto & l : _list)
		l->initializeDefault();
}

void DimensionList::load()
{}

void DimensionList::save()
{}

S_Dimension DimensionList::get(QString str)
{
	for(auto& dim : _list)
	{
		if(dim->name() == str) return dim;
	}

	return nullptr;
}
