#ifndef DIMENSIONLIST_H
#define DIMENSIONLIST_H

#include <QList>
#include "dimension.h"


namespace Imbricable::Units {

class DimensionList
{
	QList<S_Dimension> _list;

public:
	DimensionList();

	inline QList<S_Dimension>& list() { return _list; }

	void initializeDefault();
	void load();
	void save();

	S_Dimension get(QString str);
};

typedef std::shared_ptr<DimensionList> S_DimensionList;

} //namespace

#endif // DIMENSIONLIST_H
