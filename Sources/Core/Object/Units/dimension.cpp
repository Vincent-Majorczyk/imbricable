/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "dimension.h"
#include "unitsgroup.h"

using namespace Imbricable::Units;

Dimension::Dimension(QString name, DimensionList *parent):
	_parent(parent),
	_name(name)
{ }

void Dimension::initializeDefault()
{ }

Dimension::~Dimension()=default;

bool Dimension::contains(UnitsGroup* group)
{
	QList<S_UnitsGroup> &groups = this->groups();

	for(S_UnitsGroup& gr : groups)
	{
		if(gr->name() == group->name())
		{
			return true;
		}
	}
	return false;
}

std::shared_ptr<Unit> Dimension::searchUnit(QString unitId)
{
	QList<S_UnitsGroup>& groups = this->groups();

	for(S_UnitsGroup group : groups)
	{
		std::shared_ptr<Unit> unit = group->searchUnit(unitId);
		if(unit) return unit;
	}
	return nullptr;
}
