/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef UNITSGROUP_H
#define UNITSGROUP_H

#include "unit.h"

#include <QList>

namespace Imbricable::Units{

class Dimension;

/**
 * @brief abstract class which corresponds to a group of units (example: for length: metric units and US units)
 */
class UnitsGroup
{
	Dimension *_parent;

	/// @brief list of units contained in the group
	QList<S_Unit> _units;

	/// @brief name of the group
	QString _name;

	/// @brief indicate if the units of this group are avaibable for change (in TextUnitField)
	bool _isAvailable = true;


public:
	/// @brief constructor
	UnitsGroup(QString name, Dimension *parent);

	/// @brief destructor
	virtual ~UnitsGroup();

	inline Dimension* parent() { return _parent; }

	/**
	 * @brief return if units of the group are availabe to be used for conversion
	 * @return if group is available
	 */
	inline bool isAvailable()
	{
		return _isAvailable;
	}

	/**
	 * @brief setAvailable allow units of the group to be used for conversion
	 * @param available
	 */
	inline void setAvailable(bool available)
	{
		_isAvailable = available;
	}

	/**
	 * @brief return the list of units contained in the group
	 * @return the list of units
	 */
	inline QList<S_Unit>& units()
	{
		return _units;
	}

	/**
	 * @brief return the readable name of the group
	 * @return the name
	 */
	const QString name() const
	{
		return _name;
	}

	/**
	 * @brief determine if the unit exists in units().
	 * @param unit
	 * @return
	 */
	bool contains(Unit *unit);

	/**
	 * @brief in the group, search a unit by the Unit::id() properties
	 * @param unitId string id to search
	 * @return the unit or empty pointer if not found
	 */
	S_Unit searchUnit(QString unitId);

	/**
	 * @brief add a unit in the group
	 */
	template<class CUnit, typename ...Args>
	inline void addUnit(Args... args)
	{
		std::shared_ptr<Unit> u = std::make_shared<CUnit>(args...);
		this->units().append(u);
	}

	inline S_Unit addUnit(QString id, QString name, QString symbol, double toDefaultUnit, double fromDefaultUnit)
	{
		S_Unit u = std::make_shared<Unit>(id, name, symbol, toDefaultUnit, fromDefaultUnit, this);
		this->units().append(u);
		return u;
	}

	inline S_Unit addUnit(QString id, QString name, QString symbol, double toDefaultUnit)
	{
		S_Unit u = std::make_shared<Unit>(id, name, symbol, toDefaultUnit, 1.0/toDefaultUnit, this);
		this->units().append(u);
		return u;
	}

	inline S_Unit addUnit(QString id, QString name, QString symbol)
	{
		S_Unit u = std::make_shared<Unit>(id, name, symbol, 1, 1, this);
		this->units().append(u);
		return u;
	}
};

typedef std::shared_ptr<UnitsGroup> S_UnitsGroup;

} // namespace

#endif // UNITSGROUP_H
