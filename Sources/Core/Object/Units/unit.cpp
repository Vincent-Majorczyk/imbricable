/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "unit.h"
#include "unitsgroup.h"
#include "dimension.h"
#include "dimensionlist.h"

using namespace Imbricable::Units;

Unit::Unit(UnitsGroup* parent):
	_parent(parent)
{ }

Unit::Unit(QString id, QString name, QString symbol, UnitsGroup* parent):
	_parent(parent),
	_isAvailable(true),
	_id(id),
	_name(name),
	_symbol(symbol)
{ }

Unit::Unit(QString id, QString name, QString symbol, double convertToDefaultUnit, double convertFromDefaultUnit, UnitsGroup* parent):
	_parent(parent),
	_isAvailable(true),
	_id(id),
	_name(name),
	_symbol(symbol),
	_coeffConvertToDefaultUnit(convertToDefaultUnit),
	_coeffConvertFromDefaultUnit(convertFromDefaultUnit)
{ }

Dimension* Unit::parentDimension()
{
	if(_parent != nullptr) return _parent->parent();
	return nullptr;
}

DimensionList* Unit::parentDimensionList()
{
	Dimension* dim = parentDimension();
	if(dim != nullptr) return dim->parent();
	return nullptr;
}

Unit::~Unit() = default;
