/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef DIMENSION_H
#define DIMENSION_H

#include <QList>
#include <memory>
#include <exception>
#include "unitsgroup.h"

namespace Imbricable {

/**
 * @brief namespace for units conversion management
 */
namespace Units {

class DimensionList;

/**
 * @brief abstract class which corresponds to a category of unit (example: length, angle, area, ...)
 *
 * associated informations (from groups() and baseUnit()) require to be stored in static variables.
 * the registering and deleting of these variables are managed by a class derived by DimensionInitializer.
 */
class Dimension
{
	DimensionList* _parent = nullptr;

	/// @brief list of groups which are contained in the dimension
	QList<S_UnitsGroup> _groups;

	/// @brief base unit which is used as reference to conversions
	S_Unit _baseUnit;

	/// @brief the selected unit as default unit
	S_Unit _selectedUnit;

	/// @brief name of the dimension
	QString _name;

protected:
	/// @brief constructor
	Dimension(QString name, DimensionList *parent);

public:
	/// @brief simple destructor
	virtual ~Dimension();

	/// @brief initialize default group and units
	virtual void initializeDefault();

	inline DimensionList* parent() { return _parent; }

	/**
	 * @brief return the readable name of the Dimension (example: "Length")
	 * @return the name of the dimension
	 */
	inline const QString name() const
	{
		return _name;
	}

	/**
	 * @brief return the list of UnitsGroup relative to the dimension.
	 * @return the list of groups
	 */
	inline QList<S_UnitsGroup>& groups()
	{
		return _groups;
	}

	/**
	 * @brief set the base unit which is used as reference to conversions.
	 * this must be used in DimensionInitializer constructor of the dimension.
	 * @param baseUnit reference unit
	 */
	inline void setBaseUnit(std::shared_ptr<Unit> baseUnit)
	{
		_baseUnit = baseUnit;
		_selectedUnit = baseUnit;
	}

	/**
	 * @brief return the base unit which is used as reference to conversions
	 * @return return the base unit
	 */
	inline std::shared_ptr<Unit> baseUnit()
	{
		return _baseUnit;
	}

	/**
	 * @brief determine if the group exists in groups().
	 * @param group
	 * @return
	 */
	bool contains(UnitsGroup *group);

	/**
	 * @brief add a group in the dimension
	 * @return a pointer from the created group
	 */
	inline std::shared_ptr<UnitsGroup> addGroup(QString groupName)
	{
		S_UnitsGroup u = std::make_shared<UnitsGroup>(groupName, this);
		this->groups().append(u);
		return u;
	}

	/**
	 * @brief change the customized default unit
	 * @param unit new default unit
	 */
	inline void setSelectedUnit(std::shared_ptr<Unit> unit)
	{
		_selectedUnit = unit;
	}

	/**
	 * @brief return the customized default unit
	 * @return default unit
	 */
	inline std::shared_ptr<Unit> selectedUnit()
	{
		if(_selectedUnit == nullptr)
		{
			_selectedUnit = _baseUnit;
		}
		return _selectedUnit;
	}

	/**
	 * @brief in the dimension, search a unit by the Unit::id() properties
	 * @param unitId string id to search
	 * @return the unit or empty pointer if not found
	 */
	std::shared_ptr<Unit> searchUnit(QString unitId);

protected:
	/**
	 * @brief set the name of the dimension.
	 * this must be used in the constructor of the dimension
	 * @param name
	 */
	inline void setName(QString name)
	{
		_name = name;
	}
};

typedef std::shared_ptr<Dimension> S_Dimension;

}} // namespace



#endif // DIMENSION_H
