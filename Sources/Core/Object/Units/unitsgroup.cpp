/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "unitsgroup.h"
#include "unit.h"

using namespace Imbricable::Units;

UnitsGroup::UnitsGroup(QString name, Dimension *parent):
	_parent(parent),
	_name(name)
{

}

UnitsGroup::~UnitsGroup() = default;

bool UnitsGroup::contains(Unit* unit)
{
	auto units = this->units();

	for(auto& u : units)
	{
		if(u->name() == unit->name())
		{
			return true;
		}
	}
	return false;
}

std::shared_ptr<Unit> UnitsGroup::searchUnit(QString unitId)
{
	auto u = this->units();

	for(auto& unit : u)
	{
		if(unit->id() == unitId)
		{
			return unit;
		}
	}
	return nullptr;
}
