/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef BASEUNIT_H
#define BASEUNIT_H

#include <QString>
#include <QList>
#include <QObject>
#include <memory>

namespace Imbricable::Units {

class UnitsGroup;
class Dimension;
class DimensionList;

/**
 * @brief abstract class which corresponds to a  units (example: meter, radian, ...)
 */
class Unit
{
	UnitsGroup* _parent;

	/// @brief indicate if the unit is avaibable for change (in TextUnitField)
	bool _isAvailable;

	/// @brief identifiant of the unit (used to save state)
	QString _id;

	/// @brief name of the unit (example: meter)
	QString _name;

	/// @brief symbol of the unit (example: [m] for meter)
	QString _symbol;

	double _coeffConvertToDefaultUnit = 1.0;
	double _coeffConvertFromDefaultUnit = 1.0;

public:
	/// @brief constructor
	Unit(UnitsGroup* parent);

	Unit(QString id, QString name, QString symbol, UnitsGroup* parent);

	Unit(QString id, QString name, QString symbol, double convertToDefaultUnit, double convertFromDefaultUnit, UnitsGroup* parent);

	inline UnitsGroup* parent() { return _parent; }
	Dimension* parentDimension();
	DimensionList* parentDimensionList();

	/**
	 * @brief return if the unit is availabe to be used for conversion
	 * @return if the unit is available
	 */
	inline bool isAvailable()
	{
		return _isAvailable;
	}

	/**
	 * @brief setAvailable allow the unit to be used for conversion
	 * @param available
	 */
	inline void setAvailable(bool available)
	{
		_isAvailable = available;
	}

	/// @brief destructor
	virtual ~Unit();

	/**
	 * @brief return the identifiant of the unit (used to save state)
	 * @return the identifiant
	 */
	const QString id() const {return _id;}

	/**
	 * @brief return the name of the unit (example: meter)
	 * @return the name
	 */
	const QString name() const {return _name;}

	/**
	 * @brief return the symbol of the unit (example: [m] for meter)
	 * @return the symbol
	 */
	const QString symbol() const {return _symbol;}

	/**
	 * @brief convert a value from this unit to the default unit
	 * @param value to convert to default unit
	 * @return value in default unit
	 */
	double toDefaultUnit(double value)
	{ return _coeffConvertToDefaultUnit* value;}

	/**
	 * @brief convert a value from the default unit to this unit
	 * @param value to convert to current unit
	 * @return value in current unit
	 */
	double fromDefaultUnit(double value)
	{ return _coeffConvertFromDefaultUnit* value;}

protected:
	/**
	 * @brief set identifiant of the unit (used to save state)
	 * @param id
	 */
	inline void setId(QString id){_id = id;}

	/**
	 * @brief set the name of the unit (example: meter)
	 * @param name
	 */
	inline void setName(QString name){_name = name;}

	/**
	 * @brief set the symbol of the unit (example: [m] for meter)
	 * @param symbol
	 */
	inline void setSymbol(QString symbol){_symbol = symbol;}
};

/// @brief smart pointer for units
typedef std::shared_ptr<Unit> S_Unit;

} // namespace

#endif // BASEUNIT_H
