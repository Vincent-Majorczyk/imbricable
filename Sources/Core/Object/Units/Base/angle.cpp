/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "angle.h"
#include "../unitsgroup.h"
#include <math.h>

using namespace Imbricable::Units;

Angle::Angle(DimensionList *parent):
	Dimension("Angle", parent)
{ }

Angle::~Angle() = default;

void Angle::initializeDefault()
{
	S_UnitsGroup mlg = this->addGroup("Angle");
	{
		auto r = mlg->addUnit("Radian", QObject::tr("Radian"), "%1 rad");
		mlg->addUnit("Degree",QObject::tr("Degree"),"%1 °", M_PI/360.0, M_1_PI*360.0);
		mlg->addUnit("Gradian",QObject::tr("Gradian"),"%1 gon", M_PI*0.002, M_1_PI*400.0);
		mlg->addUnit("Turn",QObject::tr("Turn"),QObject::tr("%1 turn"),M_PI, M_1_PI);

		this->setBaseUnit(r);
	}
}
