/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "area.h"
#include "../unitsgroup.h"
#include <QList>

using namespace Imbricable::Units;

Area::Area(DimensionList *parent):
	Dimension("Area", parent)
{ }

Area::~Area() = default;

void Area::initializeDefault()
{
	std::shared_ptr<UnitsGroup> mlg = this->addGroup("Metric Length");
	{
		mlg->addUnit("SquareMillimeter", QObject::tr("Square millimeter"), "%1 mm²", 0.000001, 1000000.0);
		mlg->addUnit("SquareCentimeter",QObject::tr("Square centimeter"),"%1 cm²", 0.0001, 10000.0);
		auto base = mlg->addUnit("SquareMeter", QObject::tr("Square meter"), "%1 m²");
		mlg->addUnit("Are",QObject::tr("Are"),"%1 a", 100.0, 0.01);
		mlg->addUnit("Hectare", QObject::tr("Hectare"), "%1 ha", 10000.0, 0.0001);
		mlg->addUnit("SquareKilometer",QObject::tr("Square kilometer"),"%1 km²", 1000000.0, 0.000001);

		this->setBaseUnit(base);
	}

	std::shared_ptr<UnitsGroup> uslg = this->addGroup("US Area");
	{
		uslg->addUnit("SquareInch",QObject::tr("Square inch"),"%1 in²", 0.00064516, 1550.003);
		uslg->addUnit("SquareFoot",QObject::tr("Square foot"),"%1 ft²", 0.092903, 10.764);
		uslg->addUnit("SquareYard",QObject::tr("Square yard"),"%1 yd²", 0.836127, 1.196);
		uslg->addUnit("SquareMile",QObject::tr("Square mile"),"%1 mi²", 2.59e+6, 3.861e-7);
		uslg->addUnit("Acre",QObject::tr("Acre"),"%1 ac", 4046.856, 0.000247105);
	}
}
