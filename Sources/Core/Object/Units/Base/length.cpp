/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "length.h"
#include "../unitsgroup.h"
#include <QList>

using namespace Imbricable::Units;

Length::Length(DimensionList* parent):
	Dimension("Length", parent)
{ }

Length::~Length() = default;

void Length::initializeDefault()
{
	S_UnitsGroup mlg = this->addGroup("Metric Length");
	{
		mlg->addUnit("Millimetre",QObject::tr("Millimetre"),"%1 mm", 0.001,1000.0);
		mlg->addUnit("Centimetre",QObject::tr("Centimetre"),"%1 cm", 0.01,100.0);
		auto base = mlg->addUnit("Metre",QObject::tr("Metre"),"%1 m");
		mlg->addUnit("Kilometre",QObject::tr("Kilometre"),"%1 km",1000,0.001);

		this->setBaseUnit(base);
	}

	std::shared_ptr<UnitsGroup> uslg = this->addGroup("US Length");
	{
		uslg->addUnit("Inch",QObject::tr("Inch"),"%1 in",0.0254,39.3701);
		uslg->addUnit("Foot",QObject::tr("Foot"),"%1 ft",0.3048,3.28084);
		uslg->addUnit("Yard",QObject::tr("Yard"),"%1 yd",0.9144,1.09361);
		uslg->addUnit("Mile",QObject::tr("Mile"),"%1 mi",1609.34,0.000621371);
		uslg->addUnit("Nautical mile",QObject::tr("Nautical mile"),"%1 NM",1852.0,0.000539957);
	}
}
