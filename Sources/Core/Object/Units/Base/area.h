/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef AREA_H
#define AREA_H

#include "../dimension.h"

namespace Imbricable::Units{

/**
 * @brief concerns the area diemension (square meter, square feet, square yard, ...)
 */
class Area: public Dimension
{
public:
	/// @brief constructor
	Area(DimensionList* parent);

	/// @brief destructor
	~Area() override;

	void initializeDefault() override;
};

} //namespace

#endif // AREA_H
