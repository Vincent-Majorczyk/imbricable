/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef LENGTH_H
#define LENGTH_H

#include "../dimension.h"

namespace Imbricable::Units{

/**
 * @brief concerns the length diemension (meter, feet, yard, ...)
 */
class Length: public Dimension
{
public:
	/// @brief constructor
	Length(DimensionList *parent);

	/// @brief destructor
	~Length() override;

	void initializeDefault() override;
};

} //namespace

#endif // LENGTH_H
