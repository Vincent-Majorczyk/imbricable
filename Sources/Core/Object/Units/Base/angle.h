/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef ANGLE_H
#define ANGLE_H

#include "../dimension.h"

namespace Imbricable::Units {

/**
 * @brief concerns the angular diemension (radian, gradian, degree, turn, ...)
 */
class Angle: public Dimension
{
public:
	/// @brief constructor
	Angle(DimensionList *parent);

	/// @brief destructor
	~Angle() override;

	void initializeDefault() override;
};

}

#endif //ANGLE_H
