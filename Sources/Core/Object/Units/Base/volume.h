/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef VOLUME_H
#define VOLUME_H

#include "../dimension.h"

namespace Imbricable::Units {

/**
 * @brief concerns the volume diemension (cubic metre, cubic feet, ...)
 */
class Volume: public Dimension
{
public:
	/// @brief constructor
	Volume(DimensionList *parent);

	/// @brief destructor
	~Volume() override;

	void initializeDefault() override;
};

} //namespace

#endif // VOLUME_H
