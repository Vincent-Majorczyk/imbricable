/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "volume.h"
#include "../unitsgroup.h"
#include <QList>

using namespace Imbricable::Units;

Volume::Volume(DimensionList* parent):
	Dimension("Volume", parent)
{ }

Volume::~Volume() = default;

void Volume::initializeDefault()
{
	S_UnitsGroup mlg = this->addGroup("Metric volume");
	{
		auto base = mlg->addUnit("CubicMetre",QObject::tr("Cubic metre"),"%1 m³");
		mlg->addUnit("Litre",QObject::tr("Litre"),"%1 l",0.001,1000);

		this->setBaseUnit(base);
	}

	std::shared_ptr<UnitsGroup> uslg = this->addGroup("US volume");
	{
		uslg->addUnit("CubicFoot",QObject::tr("Cubic foot"),"%1 ft³",0.0283168,35.315);
		uslg->addUnit("CubicYard",QObject::tr("Cubic yard"),"%1 yd³",0.764555,1.308);
		uslg->addUnit("OilBarrels",QObject::tr("Oil barrels"),"%1 barrel",0.158987,6.29);
		uslg->addUnit("ImperialGallons",QObject::tr("Imperial gallons"),"%1 gal imp",0.00454609,219.969);
		uslg->addUnit("UsFluidGallons",QObject::tr("US gallons"),"%1 gal US",0.00378541,264.172);
	}
}
