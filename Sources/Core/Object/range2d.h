/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef RANGE2D_H
#define RANGE2D_H

#include "range1d.h"

namespace Imbricable {

template<typename Type>
class Range2D
{
protected:
	Range1D<Type> _x;
	Range1D<Type> _y;

public:
	Range2D()=default;

	Range2D(const Range1D<Type>& x, const Range1D<Type>& y):
		_x(x), _y(y)
	{
	}

	Range2D(const Type& xmin, const Type& xmax, const Type& ymin, const Type& ymax):
		_x(xmin, xmax), _y(ymin, ymax)
	{
	}

	inline bool isValueInRange( const Type& x, const Type& y) const
	{
		return _x->isValueInRange(x) && _y.isValueInRange(y);
	}

	inline Range2D& intializeBoundaryBox( const Type& x, const Type& y )
	{
		_x.intializeBoundaryBox(x);
		_y.intializeBoundaryBox(y);
		return  *this;
	}

	inline Range2D& extendBoundaryBox( const Type& x, const Type& y )
	{
		_x.extendBoundaryBox(x);
		_y.extendBoundaryBox(y);
		return *this;
	}

	inline Range1D<Type>& x() { return _x; }

	inline Range1D<Type>& y() { return _y; }
};

/**
 * @brief version of Range1D for int32
 */
using Range2DInt = Range2D<int>;

/**
 * @brief version of Range1D for double
 */
using Range2DDouble = Range2D<double>;

} // namespace

#endif // RANGE2D_H
