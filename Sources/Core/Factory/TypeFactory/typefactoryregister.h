/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef TYPEFACTORYREGISTER_H
#define TYPEFACTORYREGISTER_H

#include <memory>
#include <typeindex>
#include <QString>
#include "Core/Object/model.h"

namespace Imbricable {

class TypeFactoryCreator
{
protected:
	QString _alias;
	std::type_index _index;
public:
	TypeFactoryCreator(QString alias, std::type_index index);
	virtual ~TypeFactoryCreator();
	virtual S_Model create() const = 0;

	inline QString alias() const { return _alias; }

	inline std::type_index index() const { return _index; }
};


template<class MModel>
class TTypeFactoryCreator: public TypeFactoryCreator
{
public:
	TTypeFactoryCreator(QString alias):TypeFactoryCreator(alias, typeid(MModel)){}

	S_Model create() const override { return S_Model(new MModel()); }
};

} // namespaces

#endif // TYPEFACTORYREGISTER_H
