/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef TYPEFOACTORY_H
#define TYPEFOACTORY_H

#include <QString>
#include <typeindex>
#include <typeinfo>
#include "Core/Convenient/exceptionsthrowing.h"
#include "typefactoryregister.h"
#include <unordered_map>
#include <map>

namespace Imbricable {

class TypeFactory
{
	std::unordered_map<std::type_index, std::shared_ptr<TypeFactoryCreator>> _map_typeindex;
	std::map<QString, std::shared_ptr<TypeFactoryCreator>> _map_string;

public:
	TypeFactory();

	template<class T>
	inline void append(const QString& alias)
	{
		std::shared_ptr<TypeFactoryCreator> c(new TTypeFactoryCreator<T>(alias));
		_map_typeindex[std::type_index(typeid(T))] = c;
		_map_string[alias] = c;
	}

	inline S_Model create(const std::type_info& t) const
	{
		auto tt = std::type_index(t);
		return create(tt);
	}

	inline S_Model create(const std::type_index& t) const
	{
		if(_map_typeindex.find(t) == _map_typeindex.end()) ExceptionsThrowing::throwAliasException(t.name());
		return _map_typeindex.at(t)->create();
	}

	inline S_Model create(const QString& alias) const
	{
		if(_map_string.find(alias) == _map_string.end()) ExceptionsThrowing::throwAliasException(alias);
		return _map_string.at(alias)->create();
	}

	inline QString getAlias(const std::type_index& t) const
	{
		if(_map_typeindex.find(t) == _map_typeindex.end()) ExceptionsThrowing::throwAliasException(t.name());
		return _map_typeindex.at(t)->alias();
	}

	inline QString getAlias(const std::type_info& t) const
	{
		auto type = std::type_index(t);
		return getAlias(type);
	}

	inline QString getAlias(const S_Model& m) const
	{
		Model& mm = *(m.get());
		const std::type_info& t = typeid(mm);
		return getAlias(t);
	}

	inline QString getAlias(Model* m) const
	{
		const std::type_info& t = typeid(*m);
		return getAlias(t);
	}

private:
};

using S_TypeFactory = std::shared_ptr<TypeFactory>;

} // namespace

#endif // MODEL_H
