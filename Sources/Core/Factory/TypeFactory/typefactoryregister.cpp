/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "typefactoryregister.h"

using namespace Imbricable;

TypeFactoryCreator::TypeFactoryCreator(QString alias, std::type_index index):_alias(alias),_index(index) {}
TypeFactoryCreator::~TypeFactoryCreator() = default;
