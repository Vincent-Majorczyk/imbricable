#include "invalidmodelwidget.h"
#include <QVBoxLayout>

using namespace Imbrigui;

AbstractInvalidModelWidget::AbstractInvalidModelWidget(IControlerViewer* parent):
	QWidget(),
	_parent(parent)
{ }


InvalidModelWidget::InvalidModelWidget(IControlerViewer* parent):
	AbstractInvalidModelWidget(parent)
{
	_emptylabel = new QLabel();
	QVBoxLayout *emptylayout = new QVBoxLayout();

	emptylayout->addWidget(_emptylabel, 0, Qt::AlignHCenter | Qt::AlignVCenter);
	this->setLayout(emptylayout);
}

void InvalidModelWidget::setError(Error error)
{
	switch (error)
	{
	case Error::None:
		_emptylabel->setText(QObject::tr("No problem"));
		return;
	case Error::Empty:
		_emptylabel->setText(QObject::tr("Empty selection"));
		return;
	case Error::Invalid:
		_emptylabel->setText(QObject::tr("Invalid selection"));
		return;
	}
}
