#include "stackedwidgetformodel.h"

#include "Gui/FieldContainers/imodelviewer.h"
#include <QLabel>
#include <QVBoxLayout>
#include <QSettings>

using namespace Imbrigui;
using namespace Imbricable;

StackedWidgetForModel::StackedWidgetForModel(QString name, QWidget *parent, AbstractInvalidModelWidget *defaultwidget):
	IControlerViewer(parent)
{
	this->setAccessibleName(name);

	if(defaultwidget == nullptr)
		_defaultWidget = new InvalidModelWidget(this);
	else
		_defaultWidget = defaultwidget;

	_stacked = new QStackedWidget();
	QVBoxLayout *mainlayout = new QVBoxLayout();

	_stacked->addWidget(_defaultWidget);
	mainlayout->addWidget(_stacked);
	this->setLayout(mainlayout);

}

bool StackedWidgetForModel::setModel(S_Model model)
{
	QList<Imbricable::S_Model> models;
	if(model != nullptr) models.append(model);
	return this->setModels(models);
}

bool StackedWidgetForModel::setModels(QList<S_Model>& models)
{
	int count = _stacked->count();
	if(count == 0)
	{
		_defaultWidget->setError(AbstractInvalidModelWidget::Error::Empty);
		_stacked->setCurrentIndex(0);
		this->updateFields();
		return false;
	}

	for(int i=1;i<count;i++)
	{
		QWidget *w = _stacked->widget(i);
		auto cv = dynamic_cast<IControlerViewer*>(w);

		if(cv != nullptr && cv->areCompatible(models))
		{
			bool ok = cv->setModels(models);
			if(ok)
			{
				_stacked->setCurrentIndex(i);
				this->updateFields();
				return true;
			}
		}
	}

	_defaultWidget->setError(AbstractInvalidModelWidget::Error::Invalid);
	_stacked->setCurrentIndex(0);
	this->updateFields();
	return false;
}

void StackedWidgetForModel::setDimensions(Imbricable::Units::S_DimensionList dimension)
{
	IControlerViewer::setDimensions(dimension);

	int count = _stacked->count();

	for(int i=1;i<count;i++)
	{
		QWidget *w = _stacked->widget(i);
		auto cv = dynamic_cast<IControlerViewer*>(w);
		cv->setDimensions(dimension);
	}
}

Imbricable::S_Controler StackedWidgetForModel::controler()
{
	QWidget *w = _stacked->currentWidget();
	auto cv = dynamic_cast<IControlerViewer*>(w);

	if(cv == nullptr) return nullptr;
	return cv->controler();
}

void StackedWidgetForModel::updateFields()
{
	QWidget *w = _stacked->currentWidget();
	auto cv = dynamic_cast<IControlerViewer*>(w);

	if(cv != nullptr) cv->updateFields();
}

void StackedWidgetForModel::loadSettings(QString parent)
{
	QSettings settings;

	//QString baseName = parent + "/" + this->accessibleName();

	for(auto child : _stacked->children())
	{
		Imbrigui::IControlerViewer * f = dynamic_cast<Imbrigui::IControlerViewer*>(child);
		if(f==nullptr) continue;
		f->loadSettings(parent);
	}
}

void StackedWidgetForModel::saveSettings(QString parent)
{
	QSettings settings;

	//QString baseName = parent + "/" + this->accessibleName();

	for(auto child : _stacked->children())
	{
		Imbrigui::IControlerViewer * f = dynamic_cast<Imbrigui::IControlerViewer*>(child);
		if(f==nullptr) continue;
		f->saveSettings(parent);
	}
}

void StackedWidgetForModel::addWidget(QWidget* widget)
{
	this->stackedWidget()->addWidget(widget);

	auto w = dynamic_cast<IControlerViewer*>(widget);
	if(w!=nullptr)
	{
		connect(w,&IControlerViewer::dataUpdated, this,
				[=] { emit this->dataUpdated(); });
	}
}
