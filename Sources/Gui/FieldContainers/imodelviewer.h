#ifndef IMODELVIEWER_H
#define IMODELVIEWER_H

#include <Core/Actions/controler.h>
#include <Core/Object/Units/dimensionlist.h>

#include <QWidget>

namespace Imbrigui {

class IControlerViewer: public QWidget
{
	Q_OBJECT

	Imbricable::Units::S_DimensionList _dimensions;

public:
	IControlerViewer( QWidget *parent = nullptr);

	virtual ~IControlerViewer();

	virtual Imbricable::S_Controler controler() = 0;

	virtual bool isCompatible(Imbricable::S_Model model);

	bool areCompatible(QList<Imbricable::S_Model> &models);

	virtual void setDimensions(Imbricable::Units::S_DimensionList dimension);

	inline Imbricable::Units::S_DimensionList dimensions()
	{ return _dimensions; }

	inline Imbricable::Units::S_Dimension dimension(QString name)
	{ return _dimensions->get(name); }

	virtual bool setModel(Imbricable::S_Model model);
	virtual bool setModels(QList<Imbricable::S_Model>& models);

	virtual void saveSettings(QString parent);
	virtual void loadSettings(QString parent);

public slots:
	virtual void updateFields()=0;

signals:
	void dataUpdated();
};

} // namespace

#endif // IMODELVIEWER_H
