#include "fieldformwidget.h"
#include "../Fields/field.h"
#include <QLabel>
#include <QSettings>

using namespace Imbrigui;

FieldFormWidget::FieldFormWidget(QString name, QWidget *parent):
	IControlerViewer(parent)
{
	this->setAccessibleName(name);
	_layout = new QFormLayout();
	this->setLayout(_layout);
}

QWidget* FieldFormWidget::addField(QString name, QString tooltip, QWidget* widget, bool setDataUpdated)
{
	auto label = new QLabel(name);
	label->setToolTip(tooltip);
	widget->setToolTip(tooltip);

	_layout->addRow(label, widget);

	if(setDataUpdated)
	{
		auto w = dynamic_cast<Field*>(widget);
		if(w != nullptr) connect(w, &Field::modelChanged, this,
								 [=] { emit this->dataUpdated(); });
	}
	return widget;
}

QLabel* FieldFormWidget::addSubTitle(QString name, QString tooltip)
{
	auto label = new QLabel("**"+ name + "**");
	label->setTextFormat(Qt::MarkdownText);
	label->setToolTip(tooltip);
	_layout->addRow(label);
	return label;
}

void FieldFormWidget::updateFields()
{
	for(QObject* l : this->children())
	{
		Field* f = dynamic_cast<Field*>(l);
		if( f==nullptr ) continue;
		f->updateField();
	}
}

void FieldFormWidget::loadSettings(QString parent)
{
	QSettings settings;

	QString baseName = parent + "/" + this->accessibleName();

	for(auto child : this->children())
	{
		Imbrigui::Field * f = dynamic_cast<Imbrigui::Field*>(child);
		if(f==nullptr) continue;
		f->loadSettings(baseName);
	}
}

void FieldFormWidget::saveSettings(QString parent)
{
	QSettings settings;

	QString baseName = parent + "/" + this->accessibleName();

	for(auto child : this->children())
	{
		Imbrigui::Field * f = dynamic_cast<Imbrigui::Field*>(child);
		if(f==nullptr) continue;
		f->saveSettings(baseName);
	}
}
