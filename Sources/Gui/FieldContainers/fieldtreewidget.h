#ifndef FIELDTREEWIDGET_H
#define FIELDTREEWIDGET_H

#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QLabel>
#include "imodelviewer.h"

namespace Imbrigui {

class FieldTreeWidget: public IControlerViewer
{
	Q_OBJECT

	QTreeWidget* _tree;
public:
	FieldTreeWidget(QString name, QWidget *parent = nullptr);

	inline QTreeWidget* tree() { return _tree; }

	QTreeWidgetItem* addField(QTreeWidgetItem *group, QString name, QString tooltip, QWidget* widget, bool setDataUpdated = false);

	inline QTreeWidgetItem* addField(QTreeWidgetItem *group, QString name, QWidget* widget, bool setDataUpdated = false)
	{ return this->addField(group, name, "", widget, setDataUpdated); }

	QTreeWidgetItem* addSubTitle(QString name, QString tooltip = "");

	QTreeWidgetItem* addSubTitle(QTreeWidgetItem *group, QString name, QString tooltip = "");

	Imbricable::S_Controler controler() override = 0;

	void saveSettings(QString parent) override;
	void loadSettings(QString parent) override;

public slots:
	void updateFields() override;

private:
	virtual void updateFields(QTreeWidgetItem *item);

	void saveSettings(QString parent, QTreeWidgetItem* item);
	void loadSettings(QString parent, QTreeWidgetItem* item);
};

}

#endif // FIELDTREEWIDGET_H
