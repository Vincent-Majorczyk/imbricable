#ifndef FIELDFORMWIDGET_H
#define FIELDFORMWIDGET_H

#include <QWidget>
#include <QFormLayout>
#include <QLabel>
#include "imodelviewer.h"

namespace Imbrigui {

class FieldFormWidget: public IControlerViewer
{
	Q_OBJECT

	QFormLayout* _layout;
public:
	FieldFormWidget(QString name, QWidget *parent = nullptr);

	inline const QFormLayout* form() { return _layout; }

	QWidget* addField(QString name, QString tooltip, QWidget* widget, bool setDataUpdated = false);

	inline QWidget* addField(QString name, QWidget* widget, bool setDataUpdated = false)
	{ return this->addField(name,"", widget, setDataUpdated); }

	QLabel* addSubTitle(QString name, QString tooltip = "");

	Imbricable::S_Controler controler() override = 0;

	void saveSettings(QString parent) override;
	void loadSettings(QString parent) override;

public slots:
	void updateFields() override;
};

} // namespace

#endif // FIELDFORMWIDGET_H
