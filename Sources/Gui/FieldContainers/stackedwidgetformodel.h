#ifndef STACKEDWIDGETFORMODEL_H
#define STACKEDWIDGETFORMODEL_H

#include <QWidget>
#include <QStackedWidget>
#include <QLabel>

#include "imodelviewer.h"
#include "invalidmodelwidget.h"

namespace Imbrigui {

class StackedWidgetForModel: public IControlerViewer
{
	Q_OBJECT

	QStackedWidget* _stacked;

	AbstractInvalidModelWidget *_defaultWidget;
public:
	StackedWidgetForModel(QString name, QWidget *parent = nullptr, AbstractInvalidModelWidget *defaultwidget = nullptr);

	inline QStackedWidget* stackedWidget() { return _stacked; }

	void addWidget(QWidget* widget);

	bool setModel(Imbricable::S_Model model) override;
	bool setModels(QList<Imbricable::S_Model>& models) override;

	void setDimensions(Imbricable::Units::S_DimensionList dimension) override;

	Imbricable::S_Controler controler() override;

	void saveSettings(QString parent) override;
	void loadSettings(QString parent) override;

public slots:
	void updateFields() override;
};

} //namespace

#endif // STACKEDWIDGETFORMODEL_H
