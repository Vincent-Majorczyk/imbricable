#ifndef INVALIDMODELWIDGET_H
#define INVALIDMODELWIDGET_H

#include "imodelviewer.h"
#include <QWidget>
#include <QLabel>

namespace Imbrigui {

class AbstractInvalidModelWidget: public QWidget
{
	Q_OBJECT
protected:
	IControlerViewer* _parent;

public:
	enum Error { None, Empty, Invalid };

	AbstractInvalidModelWidget(IControlerViewer* parent);

	virtual void setError(Error error) = 0;
};

class InvalidModelWidget: public AbstractInvalidModelWidget
{
	Q_OBJECT

	QLabel *_emptylabel;

public:
	InvalidModelWidget(IControlerViewer* parent);

	void setError(Error error) override;
};

} // namespace

#endif // INVALIDMODELWIDGET_H
