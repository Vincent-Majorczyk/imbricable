#include "imodelviewer.h"

using namespace Imbrigui;

IControlerViewer::IControlerViewer( QWidget *parent):
	QWidget(parent)
{}

IControlerViewer::~IControlerViewer() = default;

bool IControlerViewer::isCompatible(Imbricable::S_Model model)
{
	return model != nullptr;
}

bool IControlerViewer::areCompatible(QList<Imbricable::S_Model>& models)
{
	if(models.count() == 0) return false;

	for(auto& model : models)
	{
		if(model == nullptr || !this->isCompatible(model)) return false;
	}
	return true;
}

bool IControlerViewer::setModel(Imbricable::S_Model model)
{
	if(!this->isCompatible(model)) return false;
	this->controler()->setModel(model);
	return true;
}

bool IControlerViewer::setModels(QList<Imbricable::S_Model>& models)
{
	if(!this->areCompatible(models)) return false;
	this->controler()->setModels(models);
	return true;
}

void IControlerViewer::setDimensions(Imbricable::Units::S_DimensionList dimension)
{
	_dimensions = dimension;
}


void IControlerViewer::saveSettings(QString parent)
{ Q_UNUSED(parent) }

void IControlerViewer::loadSettings(QString parent)
{ Q_UNUSED(parent) }
