#include "fieldtreewidget.h"
#include "../Fields/field.h"
#include <QVBoxLayout>
#include <QSettings>

using namespace Imbrigui;

FieldTreeWidget::FieldTreeWidget(QString name, QWidget *parent):
	IControlerViewer(parent)
{
	this->setAccessibleName(name);

	auto layout = new QVBoxLayout();

	_tree = new QTreeWidget();
	_tree->setColumnCount(2);
	_tree->setHeaderLabels( QStringList() << "Property" << "Value" );

	layout->addWidget(_tree);
	this->setLayout(layout);
}

QTreeWidgetItem* FieldTreeWidget::addField(QTreeWidgetItem* group, QString name, QString tooltip, QWidget* widget, bool setDataUpdated)
{
	auto item = new QTreeWidgetItem();
	item->setText(0, name);
	item->setToolTip(0,tooltip);
	item->setToolTip(1,tooltip);

	group->addChild(item);
	_tree->setItemWidget(item,1,widget);
	if(setDataUpdated)
	{
		auto w = dynamic_cast<Field*>(widget);
		if(w != nullptr) connect(w, &Field::modelChanged, this,
								 [=] { emit this->dataUpdated(); });
	}
	return item;
}

QTreeWidgetItem* FieldTreeWidget::addSubTitle(QString name, QString tooltip)
{
	auto item = new QTreeWidgetItem();
	item->setText(0, name);
	item->setToolTip(0,tooltip);
	item->setToolTip(1,tooltip);
	item->setFirstColumnSpanned(true);
	item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

	_tree->addTopLevelItem(item);

	return item;
}

QTreeWidgetItem* addSubTitle(QTreeWidgetItem *group, QString name, QString tooltip = "")
{
	auto item = new QTreeWidgetItem();
	item->setText(0, name);
	item->setToolTip(0,tooltip);
	item->setToolTip(1,tooltip);
	item->setFirstColumnSpanned(true);
	item->setFlags(Qt::ItemIsEnabled);

	group->addChild(item);

	return item;
}

void FieldTreeWidget::updateFields(QTreeWidgetItem *item)
{
	for(int j=0;j<item->columnCount();j++)
	{
		auto field = dynamic_cast<Imbrigui::Field*>(_tree->itemWidget(item,j));
		if( field== nullptr) continue;
		field->updateField();
	}

	for(int i=0;i<item->childCount(); ++i)
	{
		updateFields(item->child(i));
	}
}

void FieldTreeWidget::updateFields()
{
	for(int i=0;i<_tree->topLevelItemCount();i++)
	{
		updateFields(_tree->topLevelItem(i));
	}
}

void FieldTreeWidget::saveSettings(QString parent)
{
	QSettings settings;

	QString baseName = parent + "/" + this->accessibleName();

	for(int i=0;i<_tree->topLevelItemCount();i++)
	{
		saveSettings(baseName, _tree->topLevelItem(i));
	}
}

void FieldTreeWidget::saveSettings(QString parent, QTreeWidgetItem *item)
{
	QSettings settings;

	QString baseName = parent + ":" + item->text(0);

	settings.setValue(baseName + ":expanded", item->isExpanded());

	for(int i=0;i<this->_tree->columnCount();i++)
	{
		QWidget * sub = _tree->itemWidget(item,i);
		Imbrigui::Field * f = dynamic_cast<Imbrigui::Field*>(sub);
		if(f==nullptr) continue;
		f->saveSettings(baseName);
	}

	for(int i=0;i<item->childCount(); ++i)
	{
		saveSettings(baseName, item->child(i));
	}
}

void FieldTreeWidget::loadSettings(QString parent)
{
	QSettings settings;

	QString baseName = parent + "/" + this->accessibleName();

	for(int i=0;i<_tree->topLevelItemCount();i++)
	{
		loadSettings(baseName, _tree->topLevelItem(i));
	}
}

void FieldTreeWidget::loadSettings(QString parent, QTreeWidgetItem *item)
{
	QSettings settings;

	QString baseName = parent + ":" + item->text(0);

	item->setExpanded(settings.value(baseName + ":expanded", false).toBool());

	for(int i=0;i<this->_tree->columnCount();i++)
	{
		QWidget * sub = _tree->itemWidget(item,i);
		Imbrigui::Field * f = dynamic_cast<Imbrigui::Field*>(sub);
		if(f==nullptr) continue;
		f->loadSettings(baseName);
	}

	for(int i=0;i<item->childCount(); ++i)
	{
		loadSettings(baseName, item->child(i));
	}
}
