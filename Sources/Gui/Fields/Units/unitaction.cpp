/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "unitaction.h"

using namespace Imbrigui;

UnitAction::UnitAction(QString text, std::shared_ptr<Imbricable::Units::Unit> unit):
	QAction(text), _unit(unit)
{

}
