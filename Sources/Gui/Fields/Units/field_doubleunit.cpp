/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "field_doubleunit.h"
#include "Core/Properties/typedpropertyinfo.h"
#include "Core/Actions/accessoraction.h"

#include <QHBoxLayout>
#include <QVariant>
#include <QStyle>
#include <QSettings>

using namespace Imbrigui;
using namespace Imbricable::Properties;
using namespace Imbricable;


Field_DoubleUnit::Field_DoubleUnit(Imbricable::AccessorAction<double>* property, Imbricable::Units::S_Dimension dimension, QWidget* parent):
	Field_DoubleUnit(property,dimension,'g',6,parent)
{}

Field_DoubleUnit::Field_DoubleUnit(Imbricable::AccessorAction<double>* property, Imbricable::Units::S_Dimension dimension , char format, int precision, QWidget* parent):
	Field(property->name(), parent),
	_dimension(dimension),
	_property(property)
{
	this->w_box = new UnitLineEdit(format,precision);
	this->w_box->setDimension(dimension);
	QHBoxLayout *layout = new QHBoxLayout();
	layout->setContentsMargins(0,0,0,0);

	this->setLayout(layout);
	layout->addWidget(w_box);

	QObject::connect(w_box,SIGNAL(editingFinished()),this,SLOT(validateChange()));
	QObject::connect(w_box,SIGNAL(textChanged(QString)),this,SLOT(checkProperty()));
	QObject::connect(w_box,SIGNAL(unitChanged()), this, SIGNAL(configChanged()));
}

Field_DoubleUnit::~Field_DoubleUnit()
{
	delete w_box;
}

void Field_DoubleUnit::freezeWidget(bool readonly)
{
	const QSignalBlocker blocker(w_box);
	this->w_box->setReadOnly(readonly);
	this->w_box->setEnabled(!readonly);
	if(readonly && this->w_box->property("checkState") == "conflict") this->w_box->setText("***");
}


void Field_DoubleUnit::updateField()
{
	auto property = this->_property;

	bool conflict;
	double value = property->get(conflict);

	if(conflict)
	{
		updateFieldToDefaultContent();
		this->changeStyle(CheckState::Conflict, w_box);
		this->freezeWidgetA(property);
		return;
	}

	QSignalBlocker blocker(w_box);
	w_box->setValue(value);
	w_box->updateText();
	blocker.unblock();

	this->freezeWidgetA(property);
	this->checkProperty();
}

void Field_DoubleUnit::updateFieldToDefaultContent()
{
	QSignalBlocker blocker(w_box);
	w_box->updateEmptyText();
}

void Field_DoubleUnit::saveProperty()
{
	auto property = this->_property;
	bool ok = true;
	double value = this->w_box->value(&ok);
	if(ok) property->set(value);
}

CheckState Field_DoubleUnit::checkProperty()
{
	bool ok = true;
	auto property = this->_property;
	double value = this->w_box->value(&ok);
	CheckState state;
	if(!ok) state = CheckState::Error;
	else state = property->check(value);
	this->changeStyle(state,w_box);
	return state;
}

void Field_DoubleUnit::loadSettings(QString parent)
{
	if(!w_box) return;

	QSettings settings;

	QString baseName = parent + ":field:" + this->name();

	QString unitId = settings.value(baseName + "-unit", "Default").toString();
	if(unitId == "Default")
	{
		w_box->setDefaultUnit(true);
	}
	else
	{
		w_box->setDefaultUnit(false);
		w_box->setAssociatedUnit(_dimension->searchUnit(unitId));
	}

	this->updateField();
}

void Field_DoubleUnit::saveSettings(QString parent)
{
	QSettings settings;

	QString baseName = parent + ":field:" + this->name();

	if(w_box)
	{
		if(!w_box->isDefaultUnit())
		{
			auto unit = w_box->associatedUnit();
			if(unit)
			{
				settings.setValue(baseName + "-unit", unit->id());
				return;
			}
		}
	}

	settings.setValue(baseName + "-unit", "Default");
	return;
}
