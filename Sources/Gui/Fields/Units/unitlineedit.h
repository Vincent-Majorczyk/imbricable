/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef UNITLINEEDIT_H
#define UNITLINEEDIT_H
#pragma once

#include <QLineEdit>
#include "Core/Object/Units/dimension.h"
#include "Core/Object/Units/unit.h"
#include "unitaction.h"

namespace Imbrigui {

class UnitLineEdit : public QLineEdit
{
	Q_OBJECT

	Imbricable::Units::S_Dimension _dimension;
	bool _useDefaultunit;
	Imbricable::Units::S_Unit _associatedUnit;
	bool _focus = false;
	QString _stringValue;
	double _value;
	int _precision;
	char _format;
	bool _isEmpty = false;

public:
	UnitLineEdit(char format = 'g', int precision = 6, QWidget *parent = nullptr);
	~UnitLineEdit() override;

	inline void setDimension(Imbricable::Units::S_Dimension dimension) {_dimension = dimension;}
	inline void setDefaultUnit(bool useDefaultUnit){_useDefaultunit = useDefaultUnit; }
	inline void setAssociatedUnit(Imbricable::Units::S_Unit associatedUnit){ _associatedUnit = associatedUnit; }
	inline bool isDefaultUnit(){ return _useDefaultunit;}
	inline Imbricable::Units::S_Unit associatedUnit(){ return _associatedUnit; }


	//inline void setUnit(QString unit){_unit = unit;}
	void setValue(double value);
	double value(bool *ok);
	void updateText();
	void updateEmptyText();

protected:
	void focusInEvent(QFocusEvent *e) override;
	void focusOutEvent(QFocusEvent *e) override;
	void contextMenuEvent(QContextMenuEvent *event) override;
	void createUnitMenu(std::shared_ptr<Imbricable::Units::UnitsGroup> group, QMenu* group_menu, Imbricable::Units::S_Unit selectedunit);
	void createDefaultUnitMenu(QMenu* group_menu);
	int countGroups();

private slots:
	void updateValue(QString value);
	void unitSelected();
	void defaultUnitSelected();

signals:
	void unitChanged();
};

};

#endif // UNITLINEEDIT_H
