/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef UNITACTION_H
#define UNITACTION_H
#pragma once

#include "Core/Object/Units/unit.h"
#include <memory>
#include <QAction>

namespace Imbrigui {

class UnitAction : public QAction
{
	Imbricable::Units::S_Unit _unit;
public:
	UnitAction(QString text, Imbricable::Units::S_Unit unit);

	inline Imbricable::Units::S_Unit unit(){return  _unit;}
};

} // namespaces

#endif // UNITACTION_H
