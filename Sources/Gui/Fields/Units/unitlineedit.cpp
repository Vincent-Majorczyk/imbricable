/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "unitlineedit.h"
#include "Core/Object/Units/unitsgroup.h"
#include "Core/Object/Units/unit.h"
#include "unitaction.h"
#include <QMenu>
#include <QContextMenuEvent>
#include <memory>

//#include <QDebug>

using namespace Imbrigui;
using namespace Imbricable;

UnitLineEdit::UnitLineEdit(char format, int precision, QWidget *parent)
	: QLineEdit(parent),
	_useDefaultunit(true),
	_precision(precision),
	_format(format)
{
	QObject::connect(this,SIGNAL(textChanged(QString)),SLOT(updateValue(QString)));
}

UnitLineEdit::~UnitLineEdit()
= default;

void UnitLineEdit::focusInEvent(QFocusEvent *e)
{
	QLineEdit::focusInEvent(e);
	_focus = true;
	QSignalBlocker blocker(this);
	if(_isEmpty) updateEmptyText();
	else updateText();
}

void UnitLineEdit::focusOutEvent(QFocusEvent *e)
{
	QLineEdit::focusOutEvent(e);
	_focus = false;
	QSignalBlocker blocker(this);
	bool ok;
	_stringValue.toDouble(&ok);
	if(_isEmpty && !ok) updateEmptyText();
	else updateText();
}

void UnitLineEdit::contextMenuEvent(QContextMenuEvent *event)
{
	auto dim = _dimension.get();
	Imbricable::Units::S_Unit selectedUnit;

	// if the default choice
	if(_useDefaultunit || _associatedUnit == nullptr)
	{
		selectedUnit = _dimension->selectedUnit();
	}
	else // if the custom choice
	{
		selectedUnit = _associatedUnit;
	}

	auto groups = dim->groups();

	QMenu *menu = createStandardContextMenu();

	int groupCount = countGroups();

	// if a group associated to dimension exists
	if(groupCount!=0)
	{
		// add a new item in the context menu
		QMenu *changeUnit = new QMenu(tr("Change unit"));
		menu->insertMenu(menu->actions().first(), changeUnit);
		menu->insertSeparator(menu->actions()[1]);

		createDefaultUnitMenu(changeUnit);

		// if only one group exists then display units of this group
		if(groupCount == 1)
		{
			for(auto group_it = groups.begin(); group_it != groups.end(); ++group_it)
			{
				if((*group_it)->isAvailable())
				{
					createUnitMenu(groups.first(), changeUnit, selectedUnit);
					break;
				}
			}
		}
		// if there are more than a group then display groups as submenu in the contexmenu
		else
		{
			for(auto group_it = groups.begin(); group_it != groups.end(); ++group_it)
			{
				// each group menu contains the units of the group
				QMenu *group_menu = new QMenu((*group_it)->name());
				changeUnit->addMenu(group_menu);
				createUnitMenu(*group_it, group_menu, selectedUnit);
			}
		}
	}
	menu->exec(event->globalPos());
	delete menu;
}

int UnitLineEdit::countGroups()
{
	auto dim = _dimension.get();
	auto groups = dim->groups();
	int i = 0;
	for(auto group_it = groups.begin(); group_it != groups.end(); ++group_it)
	{
		//qDebug() << "g" << (*group_it)->name() << (*group_it)->isAvailable();
		if((*group_it)->isAvailable())
		{
			auto units = (*group_it)->units();
			for(auto unit_it = units.begin(); unit_it != units.end(); ++unit_it)
			{
				//qDebug() << "u"<< (*unit_it)->name() << (*group_it)->isAvailable();
				if((*unit_it)->isAvailable())
				{
					i++;
					break;
				}
			}
		}
	}
	return i;
}

void UnitLineEdit::createUnitMenu(std::shared_ptr<Units::UnitsGroup> group, QMenu* group_menu, Units::S_Unit selectedunit)
{
	auto units = group->units();
	for(auto unit_it = units.begin(); unit_it != units.end(); ++unit_it)
	{
		auto unitptr = *unit_it;
		if(unitptr->isAvailable())
		{
			QString text = QString("[%2] %1").arg((*unit_it)->name(), (*unit_it)->symbol()
												  .arg("").simplified());
			QAction *action = new UnitAction(text,unitptr);
			action->setCheckable(true);
			if(!_useDefaultunit && selectedunit == unitptr) action->setChecked(true);
			else action->setChecked(false);

			QObject::connect(action,SIGNAL(triggered(bool)), this, SLOT(unitSelected()));
			group_menu->addAction(action);
		}
	}
}

void UnitLineEdit::createDefaultUnitMenu(QMenu* group_menu)
{
	auto unit = _dimension->selectedUnit();

	QString text = QString("%3: [%2] %1").arg(unit->name(), unit->symbol().arg("").simplified(),tr("Default"));
	QAction *action = new QAction(text);
	action->setCheckable(true);

	if(_useDefaultunit) action->setChecked(true);
	else action->setChecked(false);

	QObject::connect(action,SIGNAL(triggered(bool)), this, SLOT(defaultUnitSelected()));
	group_menu->addAction(action);
}

void UnitLineEdit::updateText()
{
	if(_useDefaultunit) _associatedUnit = _dimension->selectedUnit();

	double v = _associatedUnit->fromDefaultUnit(_value);
	QString unitText = _associatedUnit->symbol();

	_stringValue = QString::number(v,_format,_precision);
	if(_focus) this->setText(_stringValue);
	else
	{
		this->setText( unitText.arg(_stringValue) );
	}
	_isEmpty = false;
}

void UnitLineEdit::updateEmptyText()
{
	if(_useDefaultunit) _associatedUnit = _dimension->selectedUnit();
	QString unitText = _associatedUnit->symbol();
	_stringValue = "";
	_value = 0;
	if(_focus) this->setText(_stringValue);
	else
	{ this->setText(unitText.arg("").simplified()); }
	_isEmpty = true;
}

void UnitLineEdit::unitSelected()
{
	QObject* sender = this->sender();
	UnitAction* action = dynamic_cast<UnitAction*>(sender);

	_associatedUnit = action->unit();
	_useDefaultunit = false;
	QSignalBlocker blocker(this);
	updateText();
	blocker.unblock();
	emit this->unitChanged();
}

void UnitLineEdit::defaultUnitSelected()
{
	_associatedUnit = _dimension->selectedUnit();
	_useDefaultunit = true;
	QSignalBlocker blocker(this);
	updateText();
	blocker.unblock();
	emit this->unitChanged();
}

void UnitLineEdit::updateValue(QString value)
{
	_stringValue = value;
}

double UnitLineEdit::value(bool *ok)
{
	if(_useDefaultunit) _associatedUnit = _dimension->selectedUnit();

	double v = _stringValue.toDouble(ok);
	_value = _associatedUnit->toDefaultUnit(v);
	return _value;
}

void UnitLineEdit::setValue(double value)
{
	_value = value;
}
