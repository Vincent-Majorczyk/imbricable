/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef UNITFIELD_H
#define UNITFIELD_H
#pragma once

#include "../field.h"
#include "Core/Actions/accessoraction.h"
#include "unitlineedit.h"
#include "Core/Object/Units/dimension.h"
#include <QJsonObject>

namespace Imbrigui
{

/**
 * @brief field for double with unit
 */
class Field_DoubleUnit: public Field
{
protected:
	Imbrigui::UnitLineEdit * w_box;
	Imbricable::Units::S_Dimension _dimension;
	Imbricable::AccessorAction<double>* _property;

public:
	Field_DoubleUnit(Imbricable::AccessorAction<double>* property, Imbricable::Units::S_Dimension dimension, QWidget* parent=nullptr);

	Field_DoubleUnit(Imbricable::AccessorAction<double>* property, Imbricable::Units::S_Dimension dimension, char format, int precision, QWidget* parent=nullptr);

	~Field_DoubleUnit() override;
	void freezeWidget(bool readonly) override;

	void loadSettings(QString parent) override;
	void saveSettings(QString parent) override;

protected:
	void saveProperty() override;
	void updateFieldToDefaultContent() override;

public slots:
	void updateField() override;

protected slots:
	Imbricable::CheckState checkProperty() override;
};

} // namespace

#endif // UNITFIELD_H
