/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "textfield.h"

#include <QVariant>
#include <QStyle>

using namespace Imbrigui;
using namespace Imbricable;

TextField::TextField(QString name, QWidget* parent):
	Field(name, parent)
{
	this->w_box = new QLineEdit();
	this->w_layout = new QHBoxLayout();
	this->w_layout->setContentsMargins(0,0,0,0);

	this->setLayout(this->w_layout);
	this->w_layout->addWidget(w_box);

	QObject::connect(w_box,SIGNAL(editingFinished()),this,SLOT(validateOneTime()));
	QObject::connect(w_box,SIGNAL(textChanged(QString)),this,SLOT(checkOneTime()));
}

void TextField::validateOneTime()
{
	if(_oneTime)
	{
		validateChange();
		_oneTime = false;
	}
}

void TextField::checkOneTime()
{
	checkProperty();
	_oneTime = true;
}

void TextField::updateFieldToDefaultContent()
{
	this->setText("");
}

TextField::~TextField()
{
	delete w_box;
}

void TextField::freezeWidget(bool readonly)
{
	const QSignalBlocker blocker(w_box);
	this->w_box->setReadOnly(readonly);
	this->w_box->setEnabled(!readonly);
	if(readonly && this->w_box->property("checkState") == "conflict") this->w_box->setText("***");
}

void TextField::setText(QString str)
{
	const QSignalBlocker blocker(w_box);
	w_box->setText(str);
}
