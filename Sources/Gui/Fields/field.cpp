/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "field.h"
#include <exception>
#include <QVariant>
#include <QStyle>

using namespace Imbrigui;
using namespace Imbricable;

Field::Field(QString name, QWidget *parent):
	QWidget(parent), _name(name),_readonly(false), _multieditionenabled(false)
{
}

Field::~Field()
{
	//_property shouldn't be deleted;
}

void Field::validateChange()
{
	CheckState state;

	try
	{
		state = checkProperty();
	}
	catch( std::exception &exception)
	{
		throw std::runtime_error("Field '" + _name.toStdString() + "' catch an exception during property checking: " + exception.what());
	}

	switch(state)
	{
	case CheckState::Valid:
	case CheckState::Warning:
		try
		{
			saveProperty();
			emit modelChanged();
		}
		catch( std::exception &exception)
		{
			throw std::runtime_error("Field '" + _name.toStdString() + "' catch an exception during property setting: " + exception.what());
		}
		break;
	case CheckState::Error:
	case CheckState::Conflict:
		break;
	}
}

QString Field::name()
{
	return _name;
}

void Field::setName(QString name)
{
	_name = name;
}

void Field::updateValue()
{
	updateField();
}

void Field::changeStyle(CheckState state, QWidget* widget)
{
	switch(state)
	{
	case CheckState::Valid:
		widget->setProperty("checkState","valid");
		break;
	case CheckState::Warning:
		widget->setProperty("checkState","warning");
		break;
	case CheckState::Error:
		widget->setProperty("checkState","error");
		break;
	case CheckState::Conflict:
		widget->setProperty("checkState","conflict");
		break;
	}

	widget->style()->unpolish(widget);
	widget->style()->polish(widget);
}

void Field::freezeWidgetA(Imbricable::Action* action)
{
	bool multiactionIsImpossible = action->concernManyModels() && !this->multiEditionEnabled();
	freezeWidget(multiactionIsImpossible || action->isReadOnly());
}

void Field::loadSettings(QString parent)
{
	Q_UNUSED(parent)
}

void Field::saveSettings(QString parent)
{
	Q_UNUSED(parent)
}

