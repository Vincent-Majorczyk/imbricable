/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef INCLUDEFIELDS_H
#define INCLUDEFIELDS_H

// include numbers directory
#include "Numbers/field_double.h"
#include "Numbers/field_float.h"

#include "Numbers/field_int8.h"
#include "Numbers/field_int16.h"
#include "Numbers/field_int32.h"
#include "Numbers/field_int64.h"

#include "Numbers/field_uint8.h"
#include "Numbers/field_uint16.h"
#include "Numbers/field_uint32.h"
#include "Numbers/field_uint64.h"

#include "Numbers/progressbarfield_rangedint32.h"
#include "Numbers/sliderfield_rangedint32.h"

// include units directory
#include "Units/field_doubleunit.h"

// include qt
#include "QtTypes/field_qstring.h"
#include "QtTypes/field_qvariant.h"
#include "QtTypes/field_qurl.h"

// include other
#include "Others/booleanfield.h"
#include "Others/field_link.h"

#endif // INCLUDEFIELDS_H

