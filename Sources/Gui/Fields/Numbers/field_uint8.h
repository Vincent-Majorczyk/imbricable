/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef FIELD_UINT8_H
#define FIELD_UINT8_H
#pragma once

#include "../textfield.h"

namespace Imbrigui
{

/**
 * @brief field for unsigned interger (8bits, unsigned char)
 */
class Field_UInt8: public TextField
{
	Imbricable::AccessorAction<quint8> *_property;
public:
	Field_UInt8(Imbricable::AccessorAction<quint8>* property, QWidget* parent=nullptr);
	~Field_UInt8() override;

protected:
	void saveProperty() override;

public slots:
	void updateField() override;

protected slots:
	Imbricable::CheckState checkProperty() override;
};

} // namespaces

#endif // UINT8TEXTFIELD_H
