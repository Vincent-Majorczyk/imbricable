/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "sliderfield_rangedint32.h"
#include "Core/Object/rangednumber.h"

#include <QHBoxLayout>
#include <QVariant>
#include <QStyle>

using namespace Imbrigui;
using namespace Imbricable::Properties;
using namespace Imbricable;

SliderField_RangedInt32::SliderField_RangedInt32(Imbricable::AccessorAction<Imbricable::RangedInt> *property, QWidget* parent):
	Field(property->name(), parent),
	_property(property)
{
	this->w_slider = new QSlider(Qt::Horizontal);
	this->w_spinbox = new QSpinBox();
	QHBoxLayout *layout = new QHBoxLayout();
	layout->setContentsMargins(0,0,0,0);

	this->setLayout(layout);
	layout->addWidget(w_slider);
	layout->addWidget(w_spinbox);

	QObject::connect(w_slider,SIGNAL(valueChanged(int)), w_spinbox, SLOT(setValue(int)));
	QObject::connect(w_spinbox,SIGNAL(valueChanged(int)), w_slider, SLOT(setValue(int)));
	QObject::connect(w_slider,SIGNAL(sliderReleased()), this, SLOT(validateChange()));
	QObject::connect(w_slider,SIGNAL(sliderReleased()), this, SLOT(validateChange()));
	QObject::connect(w_spinbox,SIGNAL(valueChanged(int)), this, SLOT(checkProperty()));
	QObject::connect(w_spinbox,SIGNAL(editingFinished()), this, SLOT(validateChange()));
}

SliderField_RangedInt32::~SliderField_RangedInt32()
{
	delete this->w_slider;
	delete this->w_spinbox;
}

void SliderField_RangedInt32::freezeWidget(bool readonly)
{
	this->w_slider->setEnabled(!readonly);
	this->w_spinbox->setReadOnly(readonly);
}

RangedInt SliderField_RangedInt32::testConflict(Imbricable::AccessorAction<Imbricable::RangedInt> *property, bool& conflictValue, bool& conflictLimits)
{
	auto p = property;
	// detect conflict
	conflictValue = false;
	conflictLimits = false;
	RangedInt value(0,0,0);
	bool first = true;
	for(auto& model : property->controler()->models())
	{
		if(first)
		{
			value = p->get(model);
			first = false;
		}
		else
		{
			RangedInt v = p->get(model);
			if(v.value() != value.value()) conflictValue = true;
			if(v.maximum() != value.maximum() || v.minimum() != value.minimum()) conflictLimits = true;
		}
	}

	return value;
}

void SliderField_RangedInt32::updateField()
{
	auto property = this->_property;

	// detect conflict
	bool conflictValue = false;
	bool conflictLimit = false;

	RangedInt value = testConflict(_property,conflictValue, conflictLimit);

	QSignalBlocker blocker1(w_slider);
	QSignalBlocker blocker2(w_spinbox);

	this->w_slider->setRange(value.minimum(), value.maximum());
	this->w_slider->setValue(value.value());
	this->w_spinbox->setRange(value.minimum(), value.maximum());
	this->w_spinbox->setValue(value.value());

	blocker1.unblock();
	blocker2.unblock();

	if(conflictLimit)
	{
		this->freezeWidget(true);
		return;
	}
	else this->freezeWidgetA(property);

	if(!conflictValue)
	{
		this->checkProperty();
	}
	else
	{
		this->changeStyle(CheckState::Conflict, this->w_slider);
		this->changeStyle(CheckState::Conflict, this->w_spinbox);
	}
}

void SliderField_RangedInt32::updateFieldToDefaultContent()
{}

void SliderField_RangedInt32::saveProperty()
{
	auto property = this->_property;
	RangedInt oldvalue = property->get(property->controler()->models().first());
	RangedInt newvalue(oldvalue);
	newvalue.setValue( this->w_spinbox->value() );
	property->set(newvalue);
}

CheckState SliderField_RangedInt32::checkProperty()
{
	auto property = this->_property;
	RangedInt oldvalue = property->get(property->controler()->models().first());
	RangedInt newvalue(oldvalue);
	newvalue.setValue( this->w_spinbox->value() );
	CheckState state = property->check(newvalue);
	if(!newvalue.isValueInRange()) state = CheckState::Error;

	this->changeStyle(state, this->w_slider);
	this->changeStyle(state, this->w_spinbox);
	return state;
}

