/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef USHORTTEXTFIELD_H
#define USHORTTEXTFIELD_H
#pragma once

#include "../textfield.h"

namespace Imbrigui
{

/**
 * @brief field for unsigned interger (16bits, short)
 */
class Field_UInt16: public TextField
{
	Imbricable::AccessorAction<quint16>*_property;
public:
	Field_UInt16(Imbricable::AccessorAction<quint16>* property, QWidget* parent=nullptr);
	~Field_UInt16() override;

protected:
	void saveProperty() override;

public slots:
	void updateField() override;

protected slots:
	Imbricable::CheckState checkProperty() override;
};

} // namespaces

#endif // USHORTTEXTFIELD_H
