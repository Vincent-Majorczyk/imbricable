/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef UINTTEXTFIELD_H
#define UINTTEXTFIELD_H
#pragma once

#include "../textfield.h"

namespace Imbrigui
{

/**
 * @brief field for unsigned interger (32bits, unigned int)
 */
class Field_UInt32: public TextField
{
	Imbricable::AccessorAction<quint32> *_property;
public:
	Field_UInt32(Imbricable::AccessorAction<quint32> *property, QWidget* parent=nullptr);
	~Field_UInt32() override;

protected:
	void saveProperty() override;

public slots:
	void updateField() override;

protected slots:
	Imbricable::CheckState checkProperty() override;
};

} // namespace

#endif // UINTTEXTFIELD_H
