/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef LONGTEXTFIELD_H
#define LONGTEXTFIELD_H
#pragma once

#include "../textfield.h"

namespace Imbrigui
{

/**
 * @brief field for interger (16bits, long long)
 */
class Field_Int64: public TextField
{
	Imbricable::AccessorAction<qint64> *_property;
public:
	Field_Int64(Imbricable::AccessorAction<qint64> *property, QWidget* parent=nullptr);
	~Field_Int64() override;

protected:
	void saveProperty() override;

public slots:
	void updateField() override;

protected slots:
	Imbricable::CheckState checkProperty() override;
};

} // namespace

#endif // LONGTEXTFIELD_H
