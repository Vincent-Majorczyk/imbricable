/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef FIELD_DOUBLE_H
#define FIELD_DOUBLE_H
#pragma once

#include "../textfield.h"

namespace Imbrigui
{

/**
 * @brief field for double
 */
class Field_Double: public TextField
{
protected:
	Imbricable::AccessorAction<double> * _property;
	char _format;
	int _precision;

public:
	Field_Double(Imbricable::AccessorAction<double> * property, QWidget* parent=nullptr, char format = 'g', int precision = 6);
	~Field_Double() override;

protected:
	void saveProperty() override;

public slots:
	void updateField() override;

protected slots:
	Imbricable::CheckState checkProperty() override;
};

} // namespace

#endif // DOUBLETEXTFIELD_H
