/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef SHORTTEXTFIELD_H
#define SHORTTEXTFIELD_H
#pragma once

#include "../textfield.h"

namespace Imbrigui
{

/**
 * @brief field for interger (16bits, short)
 */
class Field_Int16: public TextField
{
	Imbricable::AccessorAction<qint16> *_property;
public:
	Field_Int16(Imbricable::AccessorAction<qint16> * property, QWidget* parent=nullptr);
	~Field_Int16() override;

protected:
	void saveProperty() override;

public slots:
	void updateField() override;

protected slots:
	Imbricable::CheckState checkProperty() override;
};

} // namespaces

#endif // SHORTTEXTFIELD_H
