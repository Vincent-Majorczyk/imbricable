/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "field_uint16.h"
#include "Core/Properties/typedpropertyinfo.h"

#include <QHBoxLayout>
#include <QVariant>
#include <QStyle>

using namespace Imbrigui;
using namespace Imbricable::Properties;
using namespace Imbricable;

Field_UInt16::Field_UInt16(Imbricable::AccessorAction<quint16> *property, QWidget* parent):
	TextField(property->name(), parent),
	_property(property)
{
}

Field_UInt16::~Field_UInt16() = default;

void Field_UInt16::updateField()
{
	auto property = this->_property;
	bool conflict;
	quint16 value = property->get(conflict);

	if(conflict)
	{
		this->updateFieldToDefaultContent();
		this->changeStyle(CheckState::Conflict,w_box);
		this->freezeWidgetA(property);
		return;
	}

	this->setText(QString::number(value));
	this->freezeWidgetA(property);
	this->checkProperty();
}

void Field_UInt16::saveProperty()
{
	auto property = this->_property;
	quint16 value = this->w_box->text().toUShort();
	property->set(value);
}

CheckState Field_UInt16::checkProperty()
{
	auto property = this->_property;
	bool ok;
	quint16 value = this->w_box->text().toUShort(&ok);

	CheckState state;
	if(!ok) state = CheckState::Error;
	else state = property->check(value);

	this->changeStyle(state,w_box);
	return state;
}



