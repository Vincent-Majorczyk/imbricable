/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef INT32SLIDER_H
#define INT32SLIDER_H
#pragma once

#include "../field.h"
#include "Core/Properties/typedpropertyinfo.h"
#include "Core/Object/rangednumber.h"
#include <QSlider>
#include <QSpinBox>

namespace Imbrigui
{

/**
 * @brief field for a ranged interger (32bits, int) as slider
 */
class SliderField_RangedInt32 : public Field
{
	Imbricable::AccessorAction<Imbricable::RangedInt> *_property;
protected:
	QSlider * w_slider;
	QSpinBox * w_spinbox;

public:
	SliderField_RangedInt32(Imbricable::AccessorAction<Imbricable::RangedInt> *property, QWidget* parent=nullptr);
	~SliderField_RangedInt32() override;
	void freezeWidget(bool readonly) override;

	static Imbricable::RangedInt testConflict(Imbricable::AccessorAction<Imbricable::RangedInt> *property, bool& conflictValue, bool& conflictLimits);

protected:
	void saveProperty() override;
	void updateFieldToDefaultContent() override;

public slots:
	void updateField() override;

protected slots:
	Imbricable::CheckState checkProperty() override;
};

} // namespaces

#endif // INT32SLIDER_H
