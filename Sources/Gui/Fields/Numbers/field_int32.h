/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef INTTEXTFIELD_H
#define INTTEXTFIELD_H
#pragma once

#include "../textfield.h"

namespace Imbrigui
{

/**
 * @brief field for interger (32bits, int)
 */
class Field_Int32: public TextField
{
	Imbricable::AccessorAction<qint32> *_property;
public:
	Field_Int32(Imbricable::AccessorAction<qint32> *property, QWidget* parent=nullptr);
	~Field_Int32() override;

protected:
	void saveProperty() override;

public slots:
	void updateField() override;

protected slots:
	Imbricable::CheckState checkProperty() override;
};

} // namespace

#endif // INTTEXTFIELD_H
