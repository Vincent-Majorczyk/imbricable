/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef ULONGTEXTFIELD_H
#define ULONGTEXTFIELD_H
#pragma once

#include "../textfield.h"

namespace Imbrigui
{

/**
 * @brief field for unsigned interger (64bits, unsigned long long)
 */
class Field_UInt64: public TextField
{
	Imbricable::AccessorAction<quint64> *_property;
public:
	Field_UInt64(Imbricable::AccessorAction<quint64>* property, QWidget* parent=nullptr);
	~Field_UInt64() override;

protected:
	void saveProperty() override;

public slots:
	void updateField() override;

protected slots:
	Imbricable::CheckState checkProperty() override;
};

} // namespaces

#endif // ULONGTEXTFIELD_H
