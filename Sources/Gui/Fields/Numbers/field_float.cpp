/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "field_float.h"
#include "Core/Properties/typedpropertyinfo.h"

#include <QHBoxLayout>
#include <QVariant>
#include <QStyle>

using namespace Imbrigui;
using namespace Imbricable::Properties;
using namespace Imbricable;

Field_Float::Field_Float(Imbricable::AccessorAction<float> *property,
							   QWidget* parent,
							   char format,
							   int precision):
	TextField(property->name(), parent),
	_property(property) //, _format(format), _precision(precision)
{
	_format = format;
	_precision = precision;
}

Field_Float::~Field_Float() = default;

void Field_Float::updateField()
{
	auto property = this->_property;
	bool conflict;
	float value = property->get(conflict);

	if(conflict)
	{
		this->updateFieldToDefaultContent();
		this->changeStyle(CheckState::Conflict,w_box);
		this->freezeWidgetA(property);
		return;
	}

	this->setText(QString::number(value,_format,_precision));
	this->freezeWidgetA(property);
	this->checkProperty();
}

void Field_Float::saveProperty()
{
	auto property = this->_property;
	float value = this->w_box->text().toFloat();
	property->set(value);
}

CheckState Field_Float::checkProperty()
{
	auto property = this->_property;
	bool ok;
	float value = this->w_box->text().toFloat(&ok);

	CheckState state;
	if(!ok) state = CheckState::Error;
	else state = property->check(value);

	this->changeStyle(state,w_box);
	return state;
}

