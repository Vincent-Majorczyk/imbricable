/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "field_uint8.h"
#include "Core/Properties/typedpropertyinfo.h"

#include <QHBoxLayout>
#include <QVariant>
#include <QStyle>

using namespace Imbrigui;
using namespace Imbricable::Properties;
using namespace Imbricable;

Field_UInt8::Field_UInt8(Imbricable::AccessorAction<quint8> *property, QWidget* parent):
	TextField(property->name(), parent),
	_property(property)
{
}

Field_UInt8::~Field_UInt8() = default;

void Field_UInt8::updateField()
{
	auto property = this->_property;
	bool conflict;
	quint8 value = property->get(conflict);

	if(conflict)
	{
		this->updateFieldToDefaultContent();
		this->changeStyle(CheckState::Conflict,w_box);
		this->freezeWidgetA(property);
		return;
	}

	this->setText(QString::number((int)value));
	this->freezeWidgetA(property);
	this->checkProperty();
}

void Field_UInt8::saveProperty()
{
	auto property = this->_property;
	quint8 value = this->w_box->text().toInt();
	property->set(value);
}

CheckState Field_UInt8::checkProperty()
{
	auto property = this->_property;
	bool ok;
	int value = this->w_box->text().toInt(&ok);
	if(value < 0 || value > 255) ok = false;

	CheckState state;
	if(!ok) state = CheckState::Error;
	else state = property->check(value);

	this->changeStyle(state,w_box);
	return state;
}

