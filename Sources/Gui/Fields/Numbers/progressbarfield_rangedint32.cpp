/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "progressbarfield_rangedint32.h"
#include "sliderfield_rangedint32.h"
#include "Core/Properties/typedpropertyinfo.h"


#include <QHBoxLayout>
#include <QVariant>
#include <QStyle>

using namespace Imbrigui;
using namespace Imbricable::Properties;
using namespace Imbricable;

ProgressBarField_RangedInt32::ProgressBarField_RangedInt32(Imbricable::AccessorAction<RangedInt>* property, QWidget* parent):
	Field(property->name(), parent),
	_property(property)
{
	this->w_box = new QProgressBar();
	auto *layout = new QHBoxLayout();
	layout->setContentsMargins(0,0,0,0);

	this->setLayout(layout);
	layout->addWidget(w_box);
}

ProgressBarField_RangedInt32::~ProgressBarField_RangedInt32()
{
	delete w_box;
}

void ProgressBarField_RangedInt32::freezeWidget(bool /*readonly*/)
{
}

void ProgressBarField_RangedInt32::updateField()
{
	auto property = this->_property;
	bool conflictValue, conflictLimits;
	RangedInt value = SliderField_RangedInt32::testConflict(property, conflictValue, conflictLimits);

	w_box->setTextVisible(true);
	w_box->setRange(value.minimum(), value.maximum());
	w_box->setValue(value.value());

	if(conflictValue || conflictLimits)
		this->changeStyle(CheckState::Conflict, w_box);
	else
		this->checkProperty();
}

void ProgressBarField_RangedInt32::updateFieldToDefaultContent()
{
	w_box->setValue(0);
}

void ProgressBarField_RangedInt32::saveProperty()
{
}

CheckState ProgressBarField_RangedInt32::checkProperty()
{
	auto property = this->_property;
	RangedInt value = property->get(_property->controler()->models().first());
	CheckState state = property->check(value);
	if(!value.isValueInRange()) state = CheckState::Error;

	this->changeStyle(state,w_box);
	return state;
}

