/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "field_double.h"
#include "Core/Properties/typedpropertyinfo.h"

#include <QHBoxLayout>
#include <QVariant>
#include <QStyle>

using namespace Imbricable;
using namespace Imbrigui;
using namespace Imbricable::Properties;

Field_Double::Field_Double(Imbricable::AccessorAction<double> *property,
						   QWidget* parent, char format, int precision):
	TextField(property->name(), parent),
	_property(property)
{
	_format = format;
	_precision = precision;
}

Field_Double::~Field_Double() = default;

void Field_Double::updateField()
{
	auto property = this->_property;
	bool conflict;
	double value = property->get(conflict);

	if(conflict)
	{
		this->updateFieldToDefaultContent();
		this->changeStyle(CheckState::Conflict,w_box);
		this->freezeWidgetA(property);
		return;
	}

	this->setText(QString::number(value,_format,_precision));
	this->freezeWidgetA(property);
	this->checkProperty();
}

void Field_Double::saveProperty()
{
	auto property = this->_property;
	double value = this->w_box->text().toDouble();
	property->set(value);
}

CheckState Field_Double::checkProperty()
{
	auto property = this->_property;
	bool ok;
	double value = this->w_box->text().toDouble(&ok);

	CheckState state;
	if(!ok) state = CheckState::Error;
	else state = property->check(value);

	this->changeStyle(state,w_box);
	return state;
}

