/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "field_int64.h"
#include "Core/Properties/typedpropertyinfo.h"

#include <QHBoxLayout>
#include <QVariant>
#include <QStyle>

using namespace Imbrigui;
using namespace Imbricable::Properties;
using namespace Imbricable;

Field_Int64::Field_Int64(Imbricable::AccessorAction<qint64> *property, QWidget* parent):
	TextField(property->name(), parent),
	_property(property)
{
}

Field_Int64::~Field_Int64()
= default;


void Field_Int64::updateField()
{
	auto property = this->_property;

	bool conflict;
	qint64 value = property->get(conflict);

	if(conflict)
	{
		this->updateFieldToDefaultContent();
		this->changeStyle(CheckState::Conflict,w_box);
		this->freezeWidgetA(property);
		return;
	}

	this->setText(QString::number(value));
	this->freezeWidgetA(property);
	this->checkProperty();
}

void Field_Int64::saveProperty()
{
	auto property = this->_property;
	qint64 value = this->w_box->text().toLongLong();
	property->set(value);
}

CheckState Field_Int64::checkProperty()
{
	auto property = this->_property;
	bool ok;
	qint64 value = this->w_box->text().toLongLong(&ok);

	CheckState state;
	if(!ok) state = CheckState::Error;
	else state = property->check(value);

	this->changeStyle(state,w_box);
	return state;
}


