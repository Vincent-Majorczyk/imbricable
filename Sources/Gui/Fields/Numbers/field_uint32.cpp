/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "field_uint32.h"
#include "Core/Properties/typedpropertyinfo.h"

#include <QHBoxLayout>
#include <QVariant>
#include <QStyle>

using namespace Imbrigui;
using namespace Imbricable::Properties;
using namespace Imbricable;

Field_UInt32::Field_UInt32(Imbricable::AccessorAction<quint32> *property, QWidget* parent):
	TextField(property->name(), parent),
	_property(property)
{
}

Field_UInt32::~Field_UInt32()
= default;

void Field_UInt32::updateField()
{
	auto property = this->_property;

	bool conflict;
	quint32 value = property->get(conflict);

	if(conflict)
	{
		this->updateFieldToDefaultContent();
		this->changeStyle(CheckState::Conflict,w_box);
		this->freezeWidgetA(property);
		return;
	}

	this->setText(QString::number(value));
	this->freezeWidgetA(property);
	this->checkProperty();
}

void Field_UInt32::saveProperty()
{
	auto property = this->_property;
	quint32 value = this->w_box->text().toUInt();
	property->set(value);
}

CheckState Field_UInt32::checkProperty()
{
	auto property = this->_property;
	bool ok;
	quint32 value = this->w_box->text().toUInt(&ok);

	CheckState state;
	if(!ok) state = CheckState::Error;
	else state = property->check(value);

	this->changeStyle(state,w_box);
	return state;
}

