/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef FIELD_FLOAT_H
#define FIELD_FLOAT_H
#pragma once

#include "../textfield.h"

namespace Imbrigui
{

/**
 * @brief field for float
 */
class Field_Float: public TextField
{
protected:
	Imbricable::AccessorAction<float> * _property;
	char _format;
	int _precision;

public:
	Field_Float(Imbricable::AccessorAction<float> *  property, QWidget* parent=nullptr, char format = 'g', int precision = 6);
	~Field_Float() override;

protected:
	void saveProperty() override;

public slots:
	void updateField() override;

protected slots:
	Imbricable::CheckState checkProperty() override;
};

} // namespace

#endif // FLOATTEXTFIELD_H
