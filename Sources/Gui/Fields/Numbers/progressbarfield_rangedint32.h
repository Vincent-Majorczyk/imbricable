/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef INTPROGRESSBARFIELD_H
#define INTPROGRESSBARFIELD_H
#pragma once

#include "../field.h"
#include "Core/Object/rangednumber.h"
#include <QProgressBar>

namespace Imbrigui
{

/**
 * @brief field for ranged interger (32bits, int) as progress bar
 */
class ProgressBarField_RangedInt32 : public Field
{
	Imbricable::AccessorAction<Imbricable::RangedInt>* _property;
protected:
	QProgressBar * w_box;

public:
	ProgressBarField_RangedInt32(Imbricable::AccessorAction<Imbricable::RangedInt> *property, QWidget* parent=nullptr);
	~ProgressBarField_RangedInt32() override;
	void freezeWidget(bool readonly) override;

protected:
	void saveProperty() override;
	void updateFieldToDefaultContent() override;

public slots:
	void updateField() override;

protected slots:
	Imbricable::CheckState checkProperty() override;
};

} // namespaces

#endif // INTPROGRESSBARFIELD_H
