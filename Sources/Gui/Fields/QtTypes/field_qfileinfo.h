#ifndef FIELD_QFILEINFO_H
#define FIELD_QFILEINFO_H

#include "../textfield.h"
#include <QFileInfo>
#include <QPushButton>

namespace Imbrigui {


class Field_QFileInfo: public TextField
{
	Imbricable::AccessorAction<QFileInfo> * _property = nullptr;
	QPushButton* w_searchFile;

	QString _title = "open";
	QString _filter = "";

public:
	Field_QFileInfo(Imbricable::AccessorAction<QFileInfo> * property, QWidget* parent=nullptr);
	~Field_QFileInfo() override;

	inline void setTitle(QString title)
	{ _title = title; }

	inline void setFilter(QString filter)
	{ _filter = filter; }

protected:
	void saveProperty() override;

public slots:
	void updateField() override;
	void openSearchDialog();

protected slots:
	Imbricable::CheckState checkProperty() override;
};

} // namespace

#endif // FIELD_QFILEINFO_H
