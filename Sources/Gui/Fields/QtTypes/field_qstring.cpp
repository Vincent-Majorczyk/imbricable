/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "field_qstring.h"
#include "Core/Properties/typedpropertyinfo.h"

#include <QVariant>
#include <QStyle>

using namespace Imbrigui;
using namespace Imbricable::Properties;
using namespace Imbricable;

Field_QString::Field_QString(Imbricable::AccessorAction<QString> *property, QWidget* parent):
	TextField(property->name(), parent),
	_property(property)
{
}

void Field_QString::updateField()
{
	auto property = this->_property;
	bool conflict;
	QString value = property->get(conflict);
	if(conflict)
	{
		updateFieldToDefaultContent();
		this->changeStyle(CheckState::Conflict,w_box);
		this->freezeWidgetA(property);
		return;
	}

	this->setText(value);
	this->freezeWidgetA(property);
	this->checkProperty();
}

void Field_QString::setText(QString str)
{
	const QSignalBlocker blocker(w_box);
	w_box->setText(str);
}

void Field_QString::saveProperty()
{
	QString value = this->w_box->text();
	_property->set(value);
}

CheckState Field_QString::checkProperty()
{
	auto property = this->_property;
	QString value = this->w_box->text();
	CheckState state = property->check(value);
	this->changeStyle(state,w_box);
	return state;
}

