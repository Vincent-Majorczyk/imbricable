//
// Created by Arnaud on 6/15/20.
//

#ifndef SOCRATIC_QVARIANTFIELD_HPP
#define SOCRATIC_QVARIANTFIELD_HPP

#include "../field.h"
#include <QLineEdit>
#include <QHBoxLayout>

namespace Imbrigui
{

/**
	 * @brief field for QVariant
	 */
class Field_QVariant : public Field
{
protected:
	Imbricable::AccessorAction<QVariant> *_property;
	QLineEdit * w_box;
	QHBoxLayout *w_layout;

public:
	Field_QVariant(Imbricable::AccessorAction<QVariant> * property, QWidget* parent=nullptr);
	~Field_QVariant() override;
	void freezeWidget(bool readonly) override;

protected:
	void saveProperty() override;
	void setText(QString);

public slots:
	void updateField() override;
	void updateFieldToDefaultContent() override;

protected slots:
	Imbricable::CheckState checkProperty() override;
};

} // namespaces

#endif //SOCRATIC_QVARIANTFIELD_HPP
