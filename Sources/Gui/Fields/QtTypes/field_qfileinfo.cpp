#include "field_qfileinfo.h"
#include <QFileDialog>

using namespace Imbricable;
using namespace Imbrigui;

Field_QFileInfo::Field_QFileInfo(Imbricable::AccessorAction<QFileInfo> * property, QWidget* parent):
	TextField(property->name(), parent),
	_property(property)
{
	this->w_searchFile = new QPushButton(QIcon(":/imbricable/icons/file/file_search.svg"),"", this);
	this->w_layout->addWidget(this->w_searchFile);

	this->connect(this->w_searchFile, &QPushButton::clicked, this, &Field_QFileInfo::openSearchDialog);
}

void Field_QFileInfo::openSearchDialog()
{
	auto property = this->_property;
	bool conflict;
	QFileInfo value = property->get(conflict);

	QString filename = QFileDialog::getOpenFileName(this, _title, value.absoluteFilePath(), _filter);
	this->w_box->setText(filename);
	this->validateChange();
}

Field_QFileInfo::~Field_QFileInfo()
{ }

void Field_QFileInfo::updateField()
{
	auto property = this->_property;
	bool conflict;
	QFileInfo value = property->get(conflict);

	if(conflict)
	{
		this->updateFieldToDefaultContent();
		this->changeStyle(CheckState::Conflict,w_box);
		this->freezeWidgetA(property);
		return;
	}

	this->setText(value.filePath());
	this->freezeWidgetA(property);
	this->checkProperty();
}

void Field_QFileInfo::saveProperty()
{
	auto property = this->_property;
	QFileInfo value = QFileInfo(this->w_box->text());
	property->set(value);
}

CheckState Field_QFileInfo::checkProperty()
{
	auto property = this->_property;
	QFileInfo value = QFileInfo(this->w_box->text());

	CheckState state;
	//if(!value.isValid()) state = CheckState::Error; else
	state = property->check(value);

	this->changeStyle(state,w_box);
	return state;
}
