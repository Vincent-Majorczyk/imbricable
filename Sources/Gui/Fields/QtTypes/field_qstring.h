/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef STRINGFIELD_H
#define STRINGFIELD_H

#include "../textfield.h"
#include "Core/Actions/accessoraction.h"
#include <QLineEdit>
#include <QHBoxLayout>

namespace Imbrigui
{

/**
 * @brief field for string
 */
class Field_QString : public TextField
{
protected:
	Imbricable::AccessorAction<QString>* _property;

public:
	Field_QString(Imbricable::AccessorAction<QString> *property, QWidget* parent=nullptr);

protected:
	void saveProperty() override;
	void setText(QString);

public slots:
	void updateField() override;

protected slots:
	Imbricable::CheckState checkProperty() override;
};

} // namespaces

#endif // STRINGFIELD_H
