/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "field_qvariant.h"
#include "Core/Properties/typedpropertyinfo.h"

#include <QVariant>
#include <QStyle>

using namespace Imbrigui;
using namespace Imbricable::Properties;
using namespace Imbricable;

Field_QVariant::Field_QVariant(Imbricable::AccessorAction<QVariant> *property, QWidget* parent):
	Field(property->name(), parent),
	_property(property)
{
	this->w_box = new QLineEdit();
	this->w_layout = new QHBoxLayout();
	this->w_layout->setContentsMargins(0,0,0,0);

	this->setLayout(this->w_layout);
	this->w_layout->addWidget(w_box);

	QObject::connect(w_box,SIGNAL(editingFinished()),this,SLOT(validateChange()));
	QObject::connect(w_box,SIGNAL(textChanged(QString)),this,SLOT(checkProperty()));
}

Field_QVariant::~Field_QVariant()
{
	delete w_box;
}

void Field_QVariant::freezeWidget(bool readonly)
{
	const QSignalBlocker blocker(w_box);
	this->w_box->setReadOnly(readonly);
	this->w_box->setEnabled(!readonly);
	if(readonly && this->w_box->property("checkState") == "conflict") this->w_box->setText("***");
}

void Field_QVariant::updateField()
{
	auto property = this->_property;
	bool conflict;
	QVariant value = property->get(conflict);
	if(conflict)
	{
		updateFieldToDefaultContent();
		this->changeStyle(CheckState::Conflict, w_box);
		this->freezeWidgetA(property);
		return;
	}

	this->setText(value.toString());
	this->freezeWidgetA(property);
	this->checkProperty();
}

void Field_QVariant::updateFieldToDefaultContent()
{
	this->setText("");
}

void Field_QVariant::setText(QString str)
{
	const QSignalBlocker blocker(w_box);
	w_box->setText(str);
}

void Field_QVariant::saveProperty()
{
	auto property = this->_property;
	QVariant value = this->w_box->text();
	property->set(value);
}

CheckState Field_QVariant::checkProperty()
{
	auto property = this->_property;
	QVariant value = this->w_box->text();
	CheckState state = property->check(value);
	this->changeStyle(state,w_box);
	return state;
}
