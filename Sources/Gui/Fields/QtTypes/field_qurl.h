#ifndef FIELD_QURL_H
#define FIELD_QURL_H

#include "../textfield.h"
#include <QUrl>

namespace Imbrigui
{

/**
 * @brief field for double
 */
class Field_QUrl: public TextField
{
	Imbricable::AccessorAction<QUrl> * _property = nullptr;

public:
	Field_QUrl(Imbricable::AccessorAction<QUrl> * property, QWidget* parent=nullptr);
	~Field_QUrl() override;

protected:
	void saveProperty() override;

public slots:
	void updateField() override;

protected slots:
	Imbricable::CheckState checkProperty() override;
};

} // namespace

#endif // FIELD_QURL_H
