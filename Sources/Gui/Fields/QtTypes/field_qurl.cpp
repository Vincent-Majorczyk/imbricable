/**
  * @copyright Licence CeCILL-C
  * @date 2020
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "field_qurl.h"

#include <QHBoxLayout>
#include <QVariant>
#include <QStyle>
#include <QUrl>

using namespace Imbricable;
using namespace Imbrigui;

Field_QUrl::Field_QUrl(Imbricable::AccessorAction<QUrl> *property, QWidget* parent):
	TextField(property->name(), parent),
	_property(property)
{
}

Field_QUrl::~Field_QUrl() = default;

void Field_QUrl::updateField()
{
	auto property = this->_property;
	bool conflict;
	QUrl value = property->get(conflict);

	if(conflict)
	{
		this->updateFieldToDefaultContent();
		this->changeStyle(CheckState::Conflict,w_box);
		this->freezeWidgetA(property);
		return;
	}

	this->setText(value.url());
	this->freezeWidgetA(property);
	this->checkProperty();
}

void Field_QUrl::saveProperty()
{
	auto property = this->_property;
	QUrl value = QUrl::fromUserInput(this->w_box->text());
	property->set(value);
}

CheckState Field_QUrl::checkProperty()
{
	auto property = this->_property;
	QUrl value = QUrl::fromUserInput(this->w_box->text());

	CheckState state;
	if(!value.isValid()) state = CheckState::Error;
	else state = property->check(value);

	this->changeStyle(state,w_box);
	return state;
}

