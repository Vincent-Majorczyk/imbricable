/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef BOOLEANFIELD_H
#define BOOLEANFIELD_H

#include <QCheckBox>
#include "field.h"

namespace Imbrigui
{

/**
 * @brief field for boolean
 */
class BooleanField:public Field
{
protected:
	QCheckBox * w_box;

public:
	BooleanField(QString name,
				 QWidget* parent=nullptr);

	~BooleanField() override;

	void freezeWidget(bool readonly) override;

protected:
	void updateFieldToDefaultContent() override;

};

} // namespaces

#endif // BOOLEANFIELD_H
