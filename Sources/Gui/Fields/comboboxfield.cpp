/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "comboboxfield.h"
#include "Core/Properties/typedpropertyinfo.h"
#include "Core/Actions/accessoraction.h"

#include "QHBoxLayout"

using namespace Imbrigui;
using namespace Imbricable::Properties;
using namespace Imbricable;

ComboBoxField::ComboBoxField(QString name,QWidget* parent):
	Field(name, parent)
{
	this->w_box = new QComboBox();
	auto *layout = new QHBoxLayout();
	layout->setContentsMargins(0,0,0,0);

	this->setLayout(layout);
	layout->addWidget(w_box);

	QObject::connect(this->w_box, SIGNAL(currentIndexChanged(int)),this,SLOT(validateChange()));
}

ComboBoxField::~ComboBoxField()
{
	delete w_box;
}

void ComboBoxField::freezeWidget(bool readonly)
{
	this->setEnabled(!readonly);
}

void ComboBoxField::updateFieldToDefaultContent()
{
	QSignalBlocker blocker(this->w_box);
	if(w_box->count() > 0)
		w_box->setCurrentIndex(0);
}
