/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef FIELD_COMBOENUM_H
#define FIELD_COMBOENUM_H

#include <QCheckBox>
#include "../comboboxfield.h"

namespace Imbrigui
{

/**
 * @brief field for boolean
 */
template <typename Enum>
class Field_ComboEnum: public ComboBoxField
{
public:
	struct Item
	{
		Enum value;
		QString text;
	};

	Imbricable::AccessorAction<Enum> *  _property;
	QList<Item> _list;

public:
	Field_ComboEnum(Imbricable::AccessorAction<Enum> *  property, QWidget* parent=nullptr):
		ComboBoxField(property->name(), parent),
		_property(property)
	{ }

	~Field_ComboEnum() override
	{}

	inline const QList<Item>& list() { return _list; }

	void addItem(Enum e, QString text)
	{
		_list.append(Item{e,text});
	}

	Enum getEnum(QString text)
	{
		for(Item& i : _list)
		{
			if(i.text == text) return i.value;
		}
		QString str("Field_ComboEnum problem: %1 doesn't exist");
		throw std::runtime_error(str.arg(text).toStdString());
	}

	QString getText(Enum value)
	{
		for(Item& i : _list)
		{
			if(i.value == value) return i.text;
		}
		QString str("Field_ComboEnum problem: enum doesn't exist");
		throw std::runtime_error(str.toStdString());
	}

	void updateComboBox()
	{
		w_box->clear();
		for(Item& i : _list)
		{
			w_box->addItem(i.text);
		}
	}

protected:
	void saveProperty() override
	{
		auto property = this->_property;
		QString text = this->w_box->currentText();
		property->set(getEnum(text));
	}

public slots:
	void updateField() override
	{
		auto property = this->_property;
		bool conflict;
		Enum value = property->get(conflict);
		QString text = getText(value);

		QSignalBlocker blocker(this->w_box);

		if(conflict)
		{
			this->updateFieldToDefaultContent();
			this->changeStyle(Imbricable::CheckState::Conflict, w_box);
			this->freezeWidgetA(property);
		}
		else
		{
			this->w_box->setCurrentText(text);
			this->checkProperty();
		}

		blocker.unblock();

		this->freezeWidgetA(property);
	}

protected slots:
	Imbricable::CheckState checkProperty() override
	{
		auto property = this->_property;
		QString text = this->w_box->currentText();
		Enum value = getEnum(text);
		Imbricable::CheckState state = property->check(value);
		this->changeStyle(state,w_box);
		return state;
	}
};


} // namespaces

#endif // FIELD_COMBOENUM_H
