/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "field_bool.h"
#include "Core/Properties/typedpropertyinfo.h"

#include "QHBoxLayout"

using namespace Imbrigui;
using namespace Imbricable::Properties;
using namespace Imbricable;

Field_Bool::Field_Bool(Imbricable::AccessorAction<bool> *property, QWidget* parent):
	BooleanField(property->name(), parent),
	_property(property)
{ }

Field_Bool::~Field_Bool()
{ }

void Field_Bool::updateField()
{
	auto property = this->_property;
	bool conflict;
	bool value = property->get(conflict);

	QSignalBlocker blocker(this->w_box);

	if(conflict)
	{
		this->updateFieldToDefaultContent();
		this->changeStyle(CheckState::Conflict, w_box);
		this->freezeWidgetA(property);
	}
	else
	{
		this->w_box->setChecked(value);
		this->checkProperty();
	}

	blocker.unblock();

	this->freezeWidgetA(property);
}

void Field_Bool::saveProperty()
{
	auto property = this->_property;
	//bool value = this->w_box->isChecked();
	switch(this->w_box->checkState())
	{
	case Qt::CheckState::Unchecked:
		property->set(false);
		break;
	case Qt::CheckState::Checked:
		property->set(true);
		break;
	case Qt::CheckState::PartiallyChecked:
		break;
	}
}

CheckState Field_Bool::checkProperty()
{
	this->w_box->setTristate(false);
	auto property = this->_property;
	bool value = this->w_box->isChecked();
	CheckState state = property->check(value);
	this->changeStyle(state,w_box);
	return state;
}

