/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef FIELD_BOOL_H
#define FIELD_BOOL_H

#include <QCheckBox>
#include "../booleanfield.h"

namespace Imbrigui
{

/**
 * @brief field for boolean
 */
class Field_Bool: public BooleanField
{
	Imbricable::AccessorAction<bool> *  _property;
public:
	Field_Bool(Imbricable::AccessorAction<bool> *  property,
				 QWidget* parent=nullptr);

	~Field_Bool() override;

protected:
	void saveProperty() override;

public slots:
	void updateField() override;

protected slots:
	Imbricable::CheckState checkProperty() override;
};

} // namespaces

#endif // FIELD_BOOL_H
