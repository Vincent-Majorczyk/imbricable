/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "field_link.h"
#include "Core/Properties/typedpropertyinfo.h"

#include <QVariant>
#include <QStyle>
#include <Core/Object/Link/link.h>

using namespace Imbrigui;
using namespace Imbricable::Properties;
using namespace Imbricable;

Field_Link::Field_Link(Imbricable::AccessorAction<Link> *property, QWidget* parent):
	Field(property->name(), parent),
	_property(property)
{
	this->w_box = new QLabel();
	this->w_box->setSizePolicy(QSizePolicy::Policy::Expanding,QSizePolicy::Policy::Preferred);
	this->w_unlink = new QPushButton(QIcon(":/imbricable/icons/link/link_delete.svg"),"");
	this->w_unlink->setSizePolicy(QSizePolicy::Policy::Minimum,QSizePolicy::Policy::Preferred);

	this->w_layout = new QHBoxLayout();
	this->w_layout->setContentsMargins(0,0,0,0);

	this->setLayout(this->w_layout);
	this->w_layout->addWidget(w_box);
	this->w_layout->addWidget(w_unlink);

	//QObject::connect(w_box,SIGNAL(textChanged(QString)),this,SLOT(checkProperty()));
	//QObject::connect(w_box,SIGNAL(editingFinished()),this,SLOT(validateChange()));

	QObject::connect(w_unlink,SIGNAL(clicked()),this,SLOT(removeLink()));
}

Field_Link::~Field_Link()
{
	delete w_box;
}

void Field_Link::freezeWidget(bool readonly)
{
	this->w_unlink->setEnabled(!readonly);
}

void Field_Link::removeLink()
{
	auto property = this->_property;
	Imbricable::Link l;
	property->set(l);
	updateField();
	emit modelChanged();
}

void Field_Link::updateField()
{
	auto property = this->_property;
	bool conflict;
	Imbricable::Link value = property->get(conflict);
	_link = value;
	if(conflict)
	{
		updateFieldToDefaultContent();
		this->freezeWidgetA(property);
		return;
	}

	std::shared_ptr<Imbricable::INamedObject> ptr = value.pointer<Imbricable::INamedObject>();
	if (ptr != nullptr)
		this->setText(ptr->getName());
	else
		updateFieldToDefaultContent();
	this->freezeWidgetA(property);
	this->checkProperty();
}

void Field_Link::updateFieldToDefaultContent()
{
	this->setText("");
}

void Field_Link::setText(QString str)
{
	const QSignalBlocker blocker(w_box);
	w_box->setText(str);
}

void Field_Link::saveProperty()
{
	/*auto property = dynamic_cast<const TypedPropertyInfo<QString>*>(this->_property);
	QString value = this->w_box->text();
	property->setValue(this->_model, value, _command);*/
}

CheckState Field_Link::checkProperty()
{
	auto property = this->_property;
	CheckState state = property->check(_link);
	this->changeStyle(state,w_box);
	return state;
}
