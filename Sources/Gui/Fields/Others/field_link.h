//
// Created by Arnaud on 6/17/20.
//

#ifndef SOCRATIC_LINKFIELD_HPP
#define SOCRATIC_LINKFIELD_HPP

#include "../field.h"
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <Core/Object/Link/link.h>

namespace Imbrigui
{

/**
	 * @brief field for string
	 */
class Field_Link : public Field
{
	Q_OBJECT
protected:
	Imbricable::AccessorAction<Imbricable::Link>* _property;

	Imbricable::Link _link;

	QLabel * w_box;
	QPushButton * w_unlink;
	QHBoxLayout *w_layout;

public:
	Field_Link(Imbricable::AccessorAction<Imbricable::Link> * property, QWidget* parent=nullptr);
	~Field_Link() override;
	void freezeWidget(bool readonly) override;

protected:
	void saveProperty() override;
	void setText(QString);

public slots:
	void updateField() override;
	void updateFieldToDefaultContent() override;
	void removeLink();

protected slots:
	Imbricable::CheckState checkProperty() override;
};

} // namespaces

#endif //SOCRATIC_LINKFIELD_HPP
