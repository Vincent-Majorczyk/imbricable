/**
  * @copyright Licence CeCILL-C
  * @date 2021
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */


#ifndef COMBOBOXFIELD_H
#define COMBOBOXFIELD_H


#include <QComboBox>
#include "field.h"

namespace Imbrigui
{

/**
 * @brief field for boolean
 */
class ComboBoxField:public Field
{
protected:
	QComboBox * w_box;

public:
	ComboBoxField(QString name,
				 QWidget* parent=nullptr);

	~ComboBoxField() override;

	void freezeWidget(bool readonly) override;

protected:
	void updateFieldToDefaultContent() override;

};

} // namespaces

#endif // COMBOBOXFIELD_H
