/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef TEXTFIELD_H
#define TEXTFIELD_H

#include "field.h"
#include <QLineEdit>
#include <QHBoxLayout>

namespace Imbrigui
{

/**
 * @brief field for string
 */
class TextField : public Field
{
Q_OBJECT

protected:
	QLineEdit * w_box;
	QHBoxLayout *w_layout;
	bool _oneTime = false;

public:
	TextField(QString name, QWidget* parent=nullptr);
	~TextField() override;
	void freezeWidget(bool readonly) override;

public slots:
	void updateFieldToDefaultContent() override;

protected:
	void setText(QString);
	inline QLineEdit* lineEdit() { return w_box; }

private slots:
	void validateOneTime();
	void checkOneTime();
};

} // namespaces

#endif // STRINGFIELD_H
