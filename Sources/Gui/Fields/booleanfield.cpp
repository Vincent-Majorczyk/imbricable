/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "booleanfield.h"
#include "Core/Properties/typedpropertyinfo.h"
#include "Core/Actions/accessoraction.h"

#include "QHBoxLayout"

using namespace Imbrigui;
using namespace Imbricable::Properties;
using namespace Imbricable;

BooleanField::BooleanField(QString name,QWidget* parent):
	Field(name, parent)
{
	this->w_box = new QCheckBox();
	auto *layout = new QHBoxLayout();
	layout->setContentsMargins(0,0,0,0);

	this->setLayout(layout);
	layout->addWidget(w_box);

	QObject::connect(this->w_box,SIGNAL(stateChanged(int)),this,SLOT(validateChange()));
}

BooleanField::~BooleanField()
{
	delete w_box;
}

void BooleanField::freezeWidget(bool readonly)
{
	this->setEnabled(!readonly);
	//this->w_box->setCheckable(!readonly);
	//this->w_box->setAttribute(Qt::WA_TransparentForMouseEvents, readonly);
	//this->w_box->setFocusPolicy(readonly ? Qt::NoFocus : Qt::StrongFocus);
}

void BooleanField::updateFieldToDefaultContent()
{
	QSignalBlocker blocker(this->w_box);
	w_box->setCheckState(Qt::PartiallyChecked);
}
