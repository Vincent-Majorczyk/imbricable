/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef FIELD_H
#define FIELD_H

#include <QString>
#include <QWidget>
#include <QUndoStack>
#include <QList>
#include "Core/Actions/accessoraction.h"

namespace Imbrigui
{

/**
 * @brief abstract class for field connected to a property of a class
 */
class Field:public QWidget
{
	Q_OBJECT

public:

protected:
	QString _name; //!> Name of the field;
	bool _readonly;
	bool _multieditionenabled;

public:
	/**
	 * @brief Constructor of the field
	 * @param name Name of the field;
	 * @param parent Parent of the field;
	 */
	Field(QString name, QWidget* parent=nullptr);

	/**
	 * @brief destructor
	 */
	~Field() override;

	/**
	 * @brief return the name of the field
	 * @return the name of the field
	 */
	QString name();

	/**
	 * @brief modify the name of the field
	 * @param name
	 */
	void setName(QString name);

	inline void setReadOnly(bool ro)
	{
		_readonly = ro;
	}

	inline bool readOnly()
	{
		return  _readonly;
	}

	inline void setMultiEditionEnabled(bool e)
	{
		_multieditionenabled = e;
	}

	inline bool multiEditionEnabled()
	{
		return _multieditionenabled;
	}

	void freezeWidgetA(Imbricable::Action *action);

	virtual void freezeWidget(bool readonly)=0;

	virtual void loadSettings(QString parent);

	virtual void saveSettings(QString parent);

signals:
	void modelChanged();
	void configChanged();

protected:
	virtual void saveProperty()=0;
	virtual void changeStyle(Imbricable::CheckState state, QWidget *widget);


public slots:
	void updateValue();
	virtual void updateField()=0;
	virtual void updateFieldToDefaultContent()=0;

protected slots:
	void validateChange();
	virtual Imbricable::CheckState checkProperty()=0;
};

} // namespaces

#endif // FIELD_H
