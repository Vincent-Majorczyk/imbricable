/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef TREENODE_H
#define TREENODE_H

#include <QList>
#include <QTreeWidgetItem>
#include "Core/Object/model.h"

#include <memory>

namespace Imbrigui {

class TreeFactory;

/**
 * @brief The SceneViewerItem class
 */
class TreeNode: public QTreeWidgetItem
{
protected:
	QString _name;
	Imbricable::S_Model _model;
	std::shared_ptr<TreeFactory> _factory;
	bool _unusedNodeFlag = false;
	int _index;
	int _childindex;

	explicit TreeNode(TreeNode *parent = nullptr);
	explicit TreeNode(TreeNode *parent, QString name);
	explicit TreeNode(TreeNode *parent, QString name, QIcon icon);
	explicit TreeNode(TreeNode *parent, Imbricable::S_Model model, QString name);
	explicit TreeNode(TreeNode *parent, Imbricable::S_Model model);
	explicit TreeNode(TreeNode *parent, Imbricable::S_Model model, QString name, QIcon icon);
	~TreeNode() override;

public:
	enum DropType {
		MOVE,
		LINK,
		NONE
	};

	void setFactory(std::shared_ptr<TreeFactory> factory);
	virtual void updateName();

	TreeNode* addNode(Imbricable::S_Model model, bool &exist);
	TreeNode* addNode(QString name, bool &exist);
	TreeNode* addNode(QString name, QIcon icon, bool &exist);

	template<class TreeNodeType>
	TreeNode* addNode(Imbricable::S_Model model, QString name, bool &exist)
	{
		return addNode(model, name, &TreeNode::newNode<TreeNodeType>, exist);
	}

	template<class TreeNodeType>
	TreeNode* addNode(Imbricable::S_Model model, bool &exist)
	{
		return addNode(model, QString(), &TreeNode::newNode<TreeNodeType>, exist);
	}

	template<class TreeNodeType>
	TreeNode* addNode(QString name, bool &exist)
	{
		return addNode(nullptr, name, &TreeNode::newNode<TreeNodeType>, exist);
	}

	TreeNode* search(QString name);
	TreeNode* search(Imbricable::S_Model model);

	bool operator< (const QTreeWidgetItem &other) const override;

	void updateNode();
	virtual void contextMenu();

	virtual TreeNode::DropType acceptTreeNode(TreeNode *destNode, TreeNode * object);
	virtual void linkTreeNode(TreeNode *, TreeNode * );
	virtual bool insertTreeNode(TreeNode *destNode, TreeNode *object, int index);
	virtual bool removeTreeNode(TreeNode *destNode, TreeNode *object);

	inline Imbricable::S_Model model(){return _model;}
	inline int index(){return _index;}

	inline QString name() { return _name; }
	inline void setName(QString name) { _name = name; }

	virtual void keyPressEvent(QKeyEvent *event);

	inline TreeNode* parent_TreeNode()
	{
		return dynamic_cast<TreeNode*>(this->parent());
	}

protected:
	virtual void buildNode();


private:
	TreeNode* addNode(Imbricable::S_Model model, QString name, std::function<TreeNode*(TreeNode*, Imbricable::S_Model, QString)> funct, bool &exist);

	template<class TreeNodeType>
	TreeNode* newNode(Imbricable::S_Model model, QString name)
	{
		auto e = new TreeNodeType(model);
		if(!name.isEmpty()) e->setName(name);
		return e;
	}

	TreeNode* newNodeByFactory(Imbricable::S_Model model, QString name);

	void setUnusedNodeFlag();
	void deleteUnusedNode();
};

} // namespace

#endif // TREENODE_H
