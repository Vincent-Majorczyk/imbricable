/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef TREEFACTORY_H
#define TREEFACTORY_H

#include <QMap>
#include <typeinfo>
#include <memory>
#include "treenode.h"
#include "treefactorycreator.h"


namespace Imbrigui {


class TreeFactory
{
	QList<std::shared_ptr<TreeFactoryCreator>> _list;

public:
	TreeFactory();

	template<class Model, class SVItem>
	inline void add()
	{
		TreeFactoryCreator* r = new TTreeFactoryCreator<Model,SVItem>();
		std::shared_ptr<TreeFactoryCreator> sp(r);
		_list.append(sp);
	}

	inline TreeNode* create(Imbricable::S_Model model, QString name)
	{
		if(model == nullptr) return nullptr;
		Imbricable::Model & m = *model.get();
		const std::type_info& type = typeid(m);

		QList<std::shared_ptr<TreeFactoryCreator> >::iterator it;
		for(it = _list.begin(); it != _list.end(); ++it)
		{
			if((*it)->getType() == type)
			{
				std::shared_ptr<TreeFactoryCreator> factoryRegister = *it;
				return factoryRegister->create(model, name);
			}
		}
		return nullptr;
	}
};



} //namespace

#endif // TREEFACTORY_H
