/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "treenode.h"
#include "treefactory.h"
#include "Core/Object/Interfaces/cnamedobject.h"

using namespace Imbrigui;
using namespace Imbricable;

TreeNode::TreeNode(TreeNode* parent): QTreeWidgetItem (parent)
{
}

TreeNode::TreeNode(TreeNode* parent, QString name):
	TreeNode(parent)
{
	this->_name = name;
	this->setText(0,name);
}

TreeNode::TreeNode(TreeNode* parent, QString name, QIcon icon):
	TreeNode(parent, name)
{
	this->_name = name;
	this->setIcon(0,icon);
}

TreeNode::TreeNode(TreeNode *parent, S_Model model, QString name):
	TreeNode(parent, name)
{
	_model = model;
}

TreeNode::TreeNode(TreeNode *parent, S_Model model, QString name, QIcon icon):
	TreeNode(parent, name, icon)
{
	_model = model;
}

TreeNode::TreeNode(TreeNode *parent, S_Model model):
	TreeNode(parent,model,QString())
{
}

TreeNode::~TreeNode()
= default;


void TreeNode::buildNode()
{ }

void TreeNode::updateName()
{
	auto obj  = std::dynamic_pointer_cast<Imbricable::INamedObject>(_model);
	if (obj)
	{
		this->setText(0 , obj->getName());
	}
	else
	{
		this->setText(0 , "Unknown");
	}
}

TreeNode* TreeNode::addNode( S_Model model, bool &exist)
{
	return addNode(model, QString(), &TreeNode::newNodeByFactory, exist);
}

TreeNode* TreeNode::addNode(S_Model model, QString name, std::function<TreeNode *(TreeNode *, S_Model, QString)> funct, bool &exist)
{
	TreeNode* tn = nullptr;
	if(!name.isEmpty()) tn = this->search(name);
	else if(model != nullptr) tn = this->search(model);

	if(tn != nullptr)
	{
		exist = true;
	}
	else
	{
		exist = false;

		tn = funct(this, model, name);

		if(tn != nullptr)
		{
			this->addChild(tn);
			tn->_factory = this->_factory;
		}
		else
		{
			auto named = std::dynamic_pointer_cast<INamedObject>(model);
			if(named != nullptr)
			{
				name = QString();
			}
			else
			{
				name = QObject::tr("Unknow");
			}

			tn = new TreeNode(this, model, name);
			tn->_factory = this->_factory;
		}
	}
	tn->_index = this->_childindex++;
	tn->updateNode();
	tn->updateName();
	return tn;
}

TreeNode* TreeNode::addNode(QString name, bool& exist)
{
	auto tn = this->search(name);
	if(tn != nullptr)
	{
		tn->_unusedNodeFlag = false;
		exist = true;
	}
	else
	{
		exist = false;
		tn = new TreeNode(this, name);
		tn->_factory = this->_factory;
	}
	tn->_index = this->_childindex++;
	return tn;
}

TreeNode* TreeNode::addNode(QString name, QIcon icon, bool &exist)
{
	auto tn = this->search(name);
	if(tn != nullptr)
	{
		tn->_unusedNodeFlag = false;
		exist = true;
	}
	else
	{
		exist = false;
		tn = new TreeNode(this, name, icon);
		tn->_factory = this->_factory;
	}
	tn->_index = this->_childindex++;
	return tn;
}

TreeNode* TreeNode::search(QString name)
{
	int count = this->childCount();
	for(int i=0;i<count;i++)
	{
		auto qtwichild = this->child(i);
		auto tnchild = dynamic_cast<TreeNode*>(qtwichild);
		if(tnchild == nullptr) continue;
		if(tnchild->name() == name) return tnchild;
	}
	return nullptr;
}

TreeNode* TreeNode::search(S_Model model)
{
	int count = this->childCount();
	for(int i=0;i<count;i++)
	{
		QTreeWidgetItem* qtwichild = this->child(i);
		TreeNode* tnchild = dynamic_cast<TreeNode*>(qtwichild);
		if(tnchild != nullptr && tnchild->_model == model) return tnchild;
	}
	return nullptr;
}

void TreeNode::setFactory(std::shared_ptr<TreeFactory> factory)
{
	_factory = factory;
	int count = this->childCount();
	for(int i=0;i<count;i++)
	{
		auto qtwichild = this->child(i);
		auto tnchild = dynamic_cast<TreeNode*>(qtwichild);
		tnchild->setFactory(factory);
	}
}

TreeNode* TreeNode::newNodeByFactory(S_Model model, QString name)
{
	return this->_factory->create(model, name);
}

void TreeNode::contextMenu()
{ }

void TreeNode::updateNode()
{
	this->_unusedNodeFlag = false;
	this->_childindex = 0;
	this->setUnusedNodeFlag();
	this->buildNode();
	this->deleteUnusedNode();
	this->sortChildren(-1,Qt::SortOrder::DescendingOrder);
}

void TreeNode::setUnusedNodeFlag()
{
	int count = this->childCount();
	for(int i=0;i<count;i++)
	{
		auto qtwichild = this->child(i);
		auto tnchild = dynamic_cast<TreeNode*>(qtwichild);
		tnchild->_unusedNodeFlag = true;
		if(tnchild->model()==nullptr) tnchild->setUnusedNodeFlag();
	}
}

void TreeNode::deleteUnusedNode()
{
	int count = this->childCount();
	for(int i=0;i<count;i++)
	{
		auto qtwichild = this->child(i);
		auto tnchild = dynamic_cast<TreeNode*>(qtwichild);
		if(tnchild->_unusedNodeFlag)
		{
			this->removeChild(qtwichild);
			i--;
			count--;
		}
		else tnchild->deleteUnusedNode();
	}
}

bool TreeNode::operator< (const QTreeWidgetItem &other) const
{
	const QTreeWidgetItem* const ptr = &other;
	const TreeNode* const tnchild = dynamic_cast<const TreeNode* const>(ptr);

	if(tnchild==nullptr) return false;

	return this->_index > tnchild->_index;
}

TreeNode::DropType TreeNode::acceptTreeNode(TreeNode *, TreeNode * )
{
	return NONE;
}

void TreeNode::linkTreeNode(TreeNode *, TreeNode * )
{
	return;
}

bool TreeNode::insertTreeNode(TreeNode *, TreeNode *, int )
{
	return false;
}

bool TreeNode::removeTreeNode(TreeNode *, TreeNode *)
{
	return false;
}

void TreeNode::keyPressEvent(QKeyEvent *event)
{
	Q_UNUSED(event)
}

