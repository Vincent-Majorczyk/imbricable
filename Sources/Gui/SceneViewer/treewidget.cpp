/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "treewidget.h"
#include "treefactory.h"
#include <QVBoxLayout>
#include <QDropEvent>
#include <QAbstractItemView>
#include <stdexcept>
#include <QApplication>

using namespace Imbrigui;
using namespace Imbricable;

TreeWidget::TreeWidget(QWidget *parent): QTreeWidget (parent)
{
	this->setHeaderHidden(true);
	this->setColumnCount(1);
	this->setDragEnabled(true);
	this->viewport()->setAcceptDrops(true);
	this->setDropIndicatorShown(true);
	//this->setDragDropMode(QAbstractItemView::DragDrop);

	this->setContextMenuPolicy(Qt::CustomContextMenu);
	//this->setSelectionMode(QAbstractItemView::ExtendedSelection);
	this->setSelectionBehavior(QAbstractItemView::SelectItems);
	//this->setSortingEnabled(true);
	connect(this, &QTreeWidget::customContextMenuRequested, this, &TreeWidget::onCustomContextMenu);
}

void TreeWidget::setImbricableModel(S_Model model)
{
	_model = model;
	buildView();
}

void TreeWidget::setFactory(std::shared_ptr<TreeFactory> factory)
{
	_factory = factory;
}

void TreeWidget::updateName()
{
	for( int i = 0; i < this->topLevelItemCount(); ++i )
	{
		QTreeWidgetItem *item = this->topLevelItem(i);
		auto node = dynamic_cast<TreeNode*>(item);
		if (node)
		{
			this->updateNameOfNodes(node);
		}
	}
}

void TreeWidget:: updateNameOfNodes(TreeNode* treenode)
{
	treenode->updateName();

	for (int i = 0; i < treenode->childCount() ; ++i)
	{
		auto node = dynamic_cast<TreeNode *>(treenode->child(i));
		if (node)
		{
			this->updateNameOfNodes(node);
		}
	}
}

void TreeWidget::updateView()
{
	auto qtwiRoot = this->takeTopLevelItem(0);
	auto tnRoot = dynamic_cast<TreeNode*>(qtwiRoot);

	if(tnRoot != nullptr)
	{
		tnRoot->updateNode();
	}
	else
	{
		tnRoot = _factory->create(_model, QString());
		tnRoot->setFactory(_factory);
		tnRoot->updateNode();
		this->addTopLevelItem(tnRoot);
	}
}

void TreeWidget::buildView()
{
	this->clear();
	updateView();
}

void TreeWidget::onCustomContextMenu()
{
	auto selectedItems = this->selectedItems();
	if(selectedItems.count()==1)
	{
		auto qtwi = selectedItems.first();
		auto tn = dynamic_cast<TreeNode*>(qtwi);
		if(tn != nullptr) tn->contextMenu();
	}
}

TreeNode * TreeWidget::getDropPosition(QDropEvent *event,int &id )
{
	//verify than the destination item is not null or correspond to the invisible root item.
	TreeNode * dest = dynamic_cast<TreeNode*>(itemAt(event->pos()));
	if(dest==nullptr || dest==this->invisibleRootItem())
	{ return nullptr; }

	//manage the insert depending of the dropIndicatorPosition
	id = 0;
	switch(this->dropIndicatorPosition())
	{
	case QAbstractItemView::OnItem: //dest is the destination
		id = dest->childCount();
		break;
	case QAbstractItemView::AboveItem:
		id = dest->parent()->indexOfChild(dest);
		dest = dynamic_cast<TreeNode*>(dest->parent());
		break;
	case QAbstractItemView::BelowItem:
		if(dest->isExpanded())
		{
			id = 0;
		}
		else
		{
			id = dest->parent()->indexOfChild(dest)+1;
			dest = dynamic_cast<TreeNode*>(dest->parent());
		}
		break;
	case QAbstractItemView::OnViewport:
		return dest;
	}

	if(dest==this->invisibleRootItem())
	{ return nullptr; }

	return dest;
}

void TreeWidget::dropEvent(QDropEvent *event)
{
	QTreeWidget * source;

	//if the source widget is something other than
	source = dynamic_cast<TreeWidget*>(event->source());
	if(source==nullptr)
	{
		return;
	}

	int id;
	TreeNode * dest = getDropPosition(event, id);

	if(dest==nullptr) { return; }

	//as QTreeWidget,
	QList<QTreeWidgetItem *> selectList = source->selectedItems();
	QTreeWidgetItem *myDraggedItem = selectList.at(0);

	TreeWidget * scrOwidget = dynamic_cast<TreeWidget*>(source);
	bool internal = (scrOwidget==this);

	TreeNode * myItem = dynamic_cast<TreeNode*>(myDraggedItem);
	TreeNode * src = dynamic_cast<TreeNode*>(myDraggedItem->parent());

	TreeNode * realScr = getRealParent(src);
	TreeNode * realDest = getRealParent(dest);

	switch (realDest->acceptTreeNode(dest,myItem))
	{
	case (TreeNode::MOVE) :
		if(internal) {
			if (myItem && realScr && realDest) {
				if (src == dest) {
					if (src->index() < id && id > 0)
						id--;
				}

				bool ok = realScr->removeTreeNode(src, myItem);
				if (ok) {
					ok = realDest->insertTreeNode(dest, myItem, id);
					if (!ok) {
						ok = realScr->insertTreeNode(src, myItem, src->index());
						if (!ok)
							throw std::runtime_error("TreeNode deleted during move in TreeWidget");
					}
				}
				realDest->updateNode();
				if (realDest != realScr)
					realScr->updateNode();
			}
		}
		break;
	case (TreeNode::LINK) :
		realDest->linkTreeNode(dest, myItem);
		emit updateLinkModel();
		break;
	case (TreeNode::NONE) :
		return;
	}
	this->sortByColumn(0,Qt::SortOrder::DescendingOrder);
}

TreeNode *TreeWidget::getRealParent(TreeNode * parent)
{
	if (parent == nullptr) return nullptr;
	TreeNode * realParent = parent;
	while (true)
	{
		auto m = realParent->model();
		auto p = realParent->parent();

		if(m != nullptr) break;
		realParent = dynamic_cast<TreeNode*>(p);
		if(realParent == nullptr) break;
	}
	return realParent;
}

bool TreeWidget::dropMimeData(QTreeWidgetItem *parent, int index, const QMimeData *data, Qt::DropAction action) { return QTreeWidget::dropMimeData(parent, index, data, action); }

void TreeWidget::keyPressEvent(QKeyEvent *event)
{
	QTreeWidget::keyPressEvent(event);
	auto treenode = dynamic_cast<TreeNode*>(this->currentItem());
	if(treenode != nullptr) treenode->keyPressEvent(event);
};

QList<Imbricable::S_Model> TreeWidget::currentModels()
{
	QList<Imbricable::S_Model> list;
	auto treenode = dynamic_cast<TreeNode*>(this->currentItem());
	if(treenode != nullptr && treenode->model() != nullptr) list.append(treenode->model());
	return list;
}
