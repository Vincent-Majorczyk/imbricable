/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef TREEFACTORYCREATOR_H
#define TREEFACTORYCREATOR_H

#include <QMap>
#include <typeinfo>
#include <memory>
#include "treenode.h"
#include "Core/Object/model.h"


namespace Imbrigui {

class TreeFactoryCreator
{
public:
	virtual ~TreeFactoryCreator();

	virtual TreeNode* create(Imbricable::S_Model model, QString name) = 0;
	virtual const std::type_info& getType() = 0;
};

template<class CModel, class CTreeObject>
class TTreeFactoryCreator: public TreeFactoryCreator
{
public:
	TTreeFactoryCreator()
	= default;

	TreeNode* create(Imbricable::S_Model model, QString name) override
	{
		auto m = std::dynamic_pointer_cast<CModel>(model);
		if( m==nullptr ) return nullptr;
		auto t = new CTreeObject(m);
		if(!name.isEmpty()) t->setName(name);
		return t;
	}

	const std::type_info& getType() override
	{
		return typeid(CModel);
	}
};

} //namespace

#endif // TREEFACTORYREGISTER_H
