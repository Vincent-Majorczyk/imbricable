/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef TREEWIDGET_H
#define TREEWIDGET_H

#include <QWidget>
#include <QKeyEvent>
#include <memory>
#include <iostream>
#include "Core/Object/model.h"
#include <QTreeWidget>

namespace Imbrigui {

class TreeFactory;
class TreeNode;

/**
 * @brief The SceneViewer class
 */
class TreeWidget: public QTreeWidget
{
	Q_OBJECT

	Imbricable::S_Model _model;
	std::shared_ptr<TreeFactory> _factory;

public:
	explicit TreeWidget(QWidget *parent = nullptr);

	void setImbricableModel(Imbricable::S_Model model);

	inline Imbricable::S_Model model()
	{
		return _model;
	}

	void setFactory(std::shared_ptr<TreeFactory> factory);

	inline std::shared_ptr<TreeFactory> factory()
	{
		return _factory;
	}

	QList<Imbricable::S_Model> currentModels();

protected:
	void keyPressEvent(QKeyEvent *event) override;
	void clearView();
	TreeNode* getRealParent(TreeNode * node);
	TreeNode * getDropPosition(QDropEvent *event,int &id );

private:
	/**
	 * @brief recursive updateName
	 * @param treenode
	 */
	void updateNameOfNodes(TreeNode* treenode);

signals:
	void updateLinkModel();

public slots:
	void updateView();
	void updateName();
	void buildView();
	void dropEvent(QDropEvent *event) override;
	bool dropMimeData(QTreeWidgetItem *parent, int index, const QMimeData *data, Qt::DropAction action) override;

protected slots:
	void onCustomContextMenu();
};

} // namespace

#endif // SCENEVIEWER_H
