/**
  * @copyright Licence CeCILL-C
  * @date 2020
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef LINKDIAGNOSTICWIDGET_H
#define LINKDIAGNOSTICWIDGET_H

#include <QTreeWidget>
#include <QList>
#include "Core/Object/Link/linksmanager.h"
#include "Core/Factory/TypeFactory/typefactory.h"

namespace Imbricable {

class LinkDiagnosticWidget: public QTreeWidget
{
	std::shared_ptr<TypeFactory> _typefactory = nullptr;

public:
	LinkDiagnosticWidget(QWidget *parent = nullptr);

	void readAllLinks();
	void readProblematicLinks();
	void readLinks(const QMap<QUuid, LinksManager::RetroLink> links);

	inline void setTypeFactory(std::shared_ptr<TypeFactory> factory) {_typefactory = factory;}
	inline bool isValidTypeFactory() { return _typefactory != nullptr ;}
};

} // namespace

#endif // LINKDIAGNOSTICWIDGET_H
