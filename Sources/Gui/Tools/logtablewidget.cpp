/**
  * @copyright Licence CeCILL-C
  * @date 2020
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "logtablewidget.h"
#include "Core/Convenient/log.h"
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSplitter>
#include <QTime>
#include <QDate>

using namespace Imbricable;

LogTableWidget::LogTableWidget(QWidget* parent): QWidget(parent)
{
	auto hlayout = new QHBoxLayout();
	w_clearbutton = new QPushButton("clear",this);
	w_continuebutton = new QPushButton("continue",this);
	w_stopbutton = new QPushButton("stop",this);
	this->continueLog();

	hlayout->addWidget(w_clearbutton);
	hlayout->addWidget(w_continuebutton);
	hlayout->addWidget(w_stopbutton);
	hlayout->addStretch();

	auto vlayout = new QVBoxLayout();
	vlayout->addLayout(hlayout);

	w_table = new QTreeWidget();

	w_text = new QTextEdit();
	w_text->setReadOnly(true);

	auto splitter = new QSplitter();
	splitter->setOrientation(Qt::Vertical);
	splitter->addWidget(w_table);
	splitter->addWidget(w_text);

	vlayout->addWidget(splitter);

	this->setLayout(vlayout);

	w_table->setColumnCount(5);
	QStringList list = {"Type", "Date", "Function", "Category", "Message"};
	w_table->setHeaderLabels(list);

	this->connect(w_clearbutton,&QPushButton::clicked, this, &LogTableWidget::clearLog);
	this->connect(w_continuebutton,&QPushButton::clicked, this, &LogTableWidget::continueLog);
	this->connect(w_stopbutton,&QPushButton::clicked, this, &LogTableWidget::stopLog);
	this->connect(w_table, &QTreeWidget::itemSelectionChanged, this, &LogTableWidget::onSelectionChanged);
}

void LogTableWidget::clearLog()
{
	w_table->clear();
}

void LogTableWidget::continueLog()
{
	w_stopbutton->setEnabled(true);
	_acceptMessage = true;
	w_continuebutton->setEnabled(false);
}

void LogTableWidget::stopLog()
{
	w_stopbutton->setEnabled(false);
	_acceptMessage = false;
	w_continuebutton->setEnabled(true);
}

void LogTableWidget::onMessageSended(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
	if(!_acceptMessage) return;

	QString category = context.category;
	QString function = context.function;
	//QString file = context.file;
	QString date = QDate::currentDate().toString(Qt::ISODate) + " " + QTime::currentTime().toString(Qt::ISODate);
	// determine the message level
	QString stype;
	switch (type)
	{
	case QtInfoMsg:     stype = "INFO"; break;
	case QtDebugMsg:    stype = "DEBUG"; break;
	case QtWarningMsg:  stype = "WARN"; break;
	case QtCriticalMsg: stype = "CRIT"; break;
	case QtFatalMsg:    stype = "FATAL"; break;
	}

	QStringList items;
	items << stype << date << function << category << msg;
	w_table->addTopLevelItem( new QTreeWidgetItem(items) );
}

void LogTableWidget::onSelectionChanged()
{
	auto items = w_table->selectedItems();
	w_text->clear();

	for(auto item: items)
	{
		QString str = "<table><tr><td><b>Date:</b></td><td>" + item->text(0) + "</td></tr>"
					  + "<tr><td><b>Function:</b></td><td> " + item->text(1) + "</td></tr>"
					  + "<tr><td><b>Type:</b></td><td> " + item->text(2) + "</td></tr>"
					  + "<tr><td><b>Category:</b></td><td> " + item->text(3) + "</td></tr></table><br/><br/>"
					  + item->text(4) + "<br/><br/>";
		w_text->setHtml(str);
	}

}
