/**
  * @copyright Licence CeCILL-C
  * @date 2020
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef LOGTABLEWIDGET_H
#define LOGTABLEWIDGET_H

#include <QWidget>
#include <QTreeWidget>
#include <QTextEdit>
#include <QPushButton>
#include <memory>
#include "Core/Convenient/log.h"

namespace Imbricable {

class LogTableWidget: public QWidget
{
	Q_OBJECT

	QTreeWidget * w_table;
	QTextEdit * w_text;
	QPushButton* w_clearbutton;
	QPushButton* w_continuebutton;
	QPushButton* w_stopbutton;
	bool _acceptMessage;
public:
	LogTableWidget(QWidget*parent = nullptr);

public slots:
	void clearLog();
	void continueLog();
	void stopLog();

	void onMessageSended(QtMsgType type, const QMessageLogContext &context, const QString &msg);
	void onSelectionChanged();
};

} // namespace

#endif // LOGTABLEWIDGET_H
