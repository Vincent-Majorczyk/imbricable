/**
  * @copyright Licence CeCILL-C
  * @date 2020
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "linkdiagnosticwidget.h"
#include "Core/Object/Link/linksmanager.h"

using namespace Imbricable;

LinkDiagnosticWidget::LinkDiagnosticWidget(QWidget *parent): QTreeWidget(parent)
{
	setColumnCount(3);
	QStringList list = {"Link", "Type", "Name",  "Status"};
	this->setHeaderLabels(list);
}

void LinkDiagnosticWidget::readAllLinks()
{
	auto& manager = LinksManager::getInstance();
	readLinks(manager.getRetroLinks());
}

void LinkDiagnosticWidget::readProblematicLinks()
{
	auto& manager = LinksManager::getInstance();
	readLinks(manager.getProblematicRetroLinks());
}

void LinkDiagnosticWidget::readLinks(const QMap<QUuid, LinksManager::RetroLink> links)
{
	if( !this ->isValidTypeFactory() ) throw std::runtime_error("LinkDiagnosticWidget: Type factory null");

	this->clear();

	QMapIterator<QUuid,LinksManager::RetroLink> i(links);
	while (i.hasNext())
	{
		i.next();
		QUuid uuid = i.key();
		auto& retro = i.value();

		QString name;
		QString type;
		QString diagnostic = LinksManager::diagnose(retro, name,type,_typefactory);

		QStringList item;
		item << ((uuid.isNull())?"null":uuid.toString()) << type << name << diagnostic;
		auto it = new QTreeWidgetItem(item);

		for(std::weak_ptr<Link::CoreLink> link: retro.links)
		{
			diagnostic.clear();

			QString diagnostic = LinksManager::diagnose(link, uuid, name,type,_typefactory);

			QStringList item2;
			item2 << ((uuid.isNull())?"null":uuid.toString()) << type << name << diagnostic;
			auto it2 = new QTreeWidgetItem(item2);
			it->addChild(it2);
		}

		this->addTopLevelItem(it);
	}
}


