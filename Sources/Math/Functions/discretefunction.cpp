/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "discretefunction.h"
#include <exception>
#include <QDebug>

using namespace Imbricable::Functions;

DiscreteFunction::DiscreteFunction()
= default;

DiscreteFunction::DiscreteFunction(const DiscreteFunction& copy, bool invert):
	DiscreteFunction()
{
	QMap<double,double>::ConstIterator it = copy._samples.constBegin();
	if(invert)
	{
		for(; it != copy._samples.constEnd(); ++it)
		{
			this->_samples[it.value()] = it.key();
		}
	}
	else
	{
		for(; it != copy._samples.constEnd(); ++it)
		{
			this->_samples[it.key()] = it.value();
		}
	}
}

DiscreteFunction::~DiscreteFunction()
= default;

double DiscreteFunction::y(double x, bool safe) const
{
	if(this->_samples.count() == 0) throw new std::length_error("invalid size of samples");

	QMap<double,double>::ConstIterator it = this->_samples.constBegin();

	double lastX = it.key();
	double lastY = it.value();
	++it;

	if(x<lastX)
	{
		if(safe) return lastY;
		throw new std::range_error("x outside the range of samples");
	}

	for(; it != this->_samples.constEnd(); ++it)
	{
		double currentX = it.key();
		double currentY = it.value();

		if( x < currentX)
		{
			double ratio = (x-lastX)/(currentX-lastX);
			return currentY * ratio + lastY * (1 - ratio);
		}

		lastX = currentX;
		lastY = currentY;
	}

	if(safe) return lastY;
	throw new std::range_error("x outside the range of samples");
}

double DiscreteFunction::x(double y, bool safe) const
{
	if(this->_samples.count() == 0) throw std::length_error("invalid size of samples");
	if(! (_monotonicState & Monotonic)) throw std::logic_error("invalid type of function");

	QMap<double,double>::ConstIterator it = this->_samples.constBegin();

	const Range1DDouble& yrange = range().y();

	if(yrange.minimum() > y)
	{
		if(safe)
		{
			if(_monotonicState & Increasing) return this->_samples.firstKey();
			if(_monotonicState & Decreasing) return this->_samples.lastKey();
		}
		throw new std::range_error("y outside the range of samples");
	}

	if(yrange.maximum() < y)
	{
		if(safe)
		{
			if(_monotonicState & Increasing) return this->_samples.lastKey();
			if(_monotonicState & Decreasing) return this->_samples.firstKey();
		}
		throw new std::range_error("y outside the range of samples");
	}

	double lastX = it.key();
	double lastY = it.value();
	++it;

	for(; it != this->_samples.constEnd(); ++it)
	{
		double currentX = it.key();
		double currentY = it.value();

		if	( (_monotonicState & Increasing && y <= currentY)
			|| (_monotonicState & Decreasing && y >= currentY) )
		{
			double ratio = (y-lastY)/(currentY-lastY);
			return currentX * ratio + lastX * (1 - ratio);
		}

		lastX = currentX;
		lastY = currentY;
	}

	return lastX;
}

Function::MonotonicState DiscreteFunction::evaluateMonotonicState() const
{
	if(this->_samples.count() == 0) throw new std::length_error("invalid size of samples");

	bool increasing = false;
	bool decreasing = false;
	bool strickly = true;

	QMap<double,double>::ConstIterator it = this->_samples.constBegin();

	//double lastX = it.key();
	double lastY = it.value();
	++it;

	for(; it != this->_samples.constEnd(); ++it)
	{
		//double currentX = it.key();
		double currentY = it.value();

		if( lastY < currentY) increasing = true;
		else if( lastY > currentY) decreasing = true;
		else strickly = false;

		if(increasing && decreasing) return Function::NotMonotonic;

		//lastX = currentX;
		lastY = currentY;
	}

	if(strickly)
	{
		if(increasing) return Function::MonotonicallyStrictlyIncreasing;
		else return Function::MonotonicallyStrictlyDecreasing;
	}
	else
	{
		if(increasing) return Function::MonotonicallyIncreasing;
		else return Function::MonotonicallyDecreasing;
	}
}

Imbricable::Range2DDouble DiscreteFunction::evaluateRange() const
{
	Imbricable::Range2DDouble range;

	bool first = true;
	for(auto it = this->_samples.constBegin(); it != this->_samples.constEnd(); ++it)
	{
		double x = it.key();
		double y = it.value();
		if(first)
		{
			range.intializeBoundaryBox(x,y);
			first = false;
		}
		else
		{
			range.extendBoundaryBox(x,y);
		}
	}

	return range;
}

#if false
DiscreteFunction2::DiscreteFunction2()
{

}

DiscreteFunction2::DiscreteFunction2(const DiscreteFunction2& copy, bool invert):
	DiscreteFunction2()
{
	QVector<double>::ConstIterator it = copy._samples.constBegin();
	if(invert)
	{
		for(; it != copy._samples.constEnd(); ++it)
		{
			double x = *it;
			++it;
			double y = *it;

			this->_samples.append(y);
			this->_samples.append(x);
		}
	}
	else
	{
		for(; it != copy._samples.constEnd(); ++it)
		{
			double x = *it;
			++it;
			double y = *it;

			this->_samples.append(x);
			this->_samples.append(y);
		}
	}

}

DiscreteFunction2::~DiscreteFunction2()
{ }

double DiscreteFunction2::y(double x, bool safe) const
{
	if(this->_samples.count() == 0) throw new std::length_error("invalid size of samples");

	QVector<double>::ConstIterator it = this->_samples.constBegin();

	double lastX = *it;
	++it;
	double lastY = *it;
	++it;

	if(x<lastX)
	{
		if(safe) return lastY;
		throw new std::range_error("x outside the range of samples");
	}

	for(; it != this->_samples.constEnd(); ++it)
	{
		double currentX = *it;
		it++;
		double currentY = *it;

		if( x < currentX)
		{
			double ratio = (x-lastX)/(currentX-lastX);
			return currentY * ratio + lastY * (1 - ratio);
		}

		lastX = currentX;
		lastY = currentY;
	}

	if(safe) return lastY;
	throw new std::range_error("x outside the range of samples");
}


Function::MonotonicState DiscreteFunction2::monotonicState() const
{
	if(this->_samples.count() == 0) throw new std::length_error("invalid size of samples");

	bool increasing = false;
	bool decreasing = false;
	bool strickly = true;

	QVector<double>::ConstIterator it = this->_samples.constBegin();

	double lastX = *it;
	it++;
	double lastY = *it;
	++it;

	for(; it != this->_samples.constEnd(); ++it)
	{
		double currentX = *it;
		it++;
		double currentY = *it;

		if( lastX < currentX) increasing = true;
		else if( lastX > currentX) decreasing = true;
		else strickly = false;

		if(increasing && decreasing) return Function::NotMonotonic;

		lastX = currentX;
		lastY = currentY;
	}

	if(strickly)
	{
		if(increasing) return Function::MonotonicallyStrictlyIncreasing;
		else return Function::MonotonicallyStrictlyDecreasing;
	}
	else
	{
		if(increasing) return Function::MonotonicallyIncreasing;
		else return Function::MonotonicallyDecreasing;
	}
}
#endif
