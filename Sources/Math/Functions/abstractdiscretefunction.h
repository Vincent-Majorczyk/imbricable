/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef ABSTRACTDISCRETEFUNCTION_H
#define ABSTRACTDISCRETEFUNCTION_H

#include "function.h"
#include <QFlag>

namespace Imbricable::Functions {

class AbstractDiscreteFunction: public Function
{
public:
	AbstractDiscreteFunction();
	~AbstractDiscreteFunction() override;

	/**
	 * @brief Return y(x) by interpolation.
	 * @param x
	 * @return Y
	 */
	double y(double x) const override = 0;

	/**
	 * @brief return the number of samples contained in the discrete function
	 * @return
	 */
	virtual unsigned long count() const = 0;

	/**
	 * @brief get the minimum X of the discrete function, the function assume that samples exists
	 * @return
	 */
	virtual double minimum() const = 0;

	/**
	 * @brief get the maximum X of the discrete function, the function assume that samples exists
	 * @return
	 */
	virtual double maximum() const = 0;
};

} //namespace

#endif // ABSTRACTDISCRETEFUNCTION_H
