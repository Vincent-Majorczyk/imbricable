/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#include "optimizeddiscretefunction.h"
#include <QDebug>

using namespace Imbricable::Functions;

OptimizedDiscreteFunction::OptimizedDiscreteFunction(const DiscreteFunction& copy, void* memorylocation)
{
	copy.evaluateMonotonicState();
	copy.evaluateRange();

	_monotonicState = copy.monotonicState();

	if(! (_monotonicState & Monotonic)) throw std::logic_error("invalid type of function");
	_size = copy.count();
	if(_size <= 0) throw std::length_error("invalid size of samples");

	_range = copy.range();

	if(memorylocation == nullptr)
	{
		_samples = new double[copy.count() << 1];
		_isManualMemoryLocation = false;
	}
	else
	{
		_samples = new(memorylocation) double[copy.count() << 1];
		_isManualMemoryLocation = true;
	}
	auto sample = _samples;

	auto it = copy.samples().constBegin();

	for(; it != copy.samples().constEnd(); ++it)
	{
		double x = it.key();
		double y = it.value();

		*(sample++) = x;
		*(sample++) = y;
	}
}

OptimizedDiscreteFunction::~OptimizedDiscreteFunction()
{
	if(!_isManualMemoryLocation) delete[] _samples;
}

unsigned int OptimizedDiscreteFunction::calculateConsecutiveMemoryAllocationSize(const DiscreteFunction& item)
{
	return sizeof(OptimizedDiscreteFunction) + 2 * item.count() * sizeof (double);
}

OptimizedDiscreteFunction* OptimizedDiscreteFunction::createForConsecutiveMemoryAllocation(const DiscreteFunction& item, void* address)
{
	if(address == nullptr) address = malloc(OptimizedDiscreteFunction::calculateConsecutiveMemoryAllocationSize(item));
	char * cAddress = static_cast<char*>(address) + sizeof(OptimizedDiscreteFunction);
	return new(address) OptimizedDiscreteFunction(item, cAddress);
}

void OptimizedDiscreteFunction::deleteConsecutiveMemoryAllocation(OptimizedDiscreteFunction* item)
{
	item->~OptimizedDiscreteFunction();
	free(item);
}

double OptimizedDiscreteFunction::y(double x, bool safe) const
{
	auto size = _size;

	unsigned int i = 0;

	double *it = _samples;
	//void* _debug = _samples;

	double lastX = *(it++);
	//_debug = it;
	double lastY = *(it++);
	//_debug = it;

	if(x<lastX)
	{
		if(safe) return lastY;
		throw new std::range_error("x outside the range of samples");
	}

	for(i = 1; i<size;i++)
	{
		double currentX = *(it++);
		//_debug = it;
		double currentY = *(it++);
		//_debug = it;

		if( x < currentX)
		{
			double ratio = (x-lastX)/(currentX-lastX);
			return currentY * ratio + lastY * (1 - ratio);
		}

		lastX = currentX;
		lastY = currentY;
	}

	if(safe) return lastY;
	throw new std::range_error("x outside the range of samples");
}

double OptimizedDiscreteFunction::x(double y, bool safe) const
{
	auto samples = _samples;
	auto size = _size;

	const Range1DDouble& yrange = range().y();

	if(yrange.minimum() > y)
	{
		if(safe)
		{
			if(_monotonicState & Increasing) return samples[0];
			if(_monotonicState & Decreasing) return samples[(size<<1)-2];
		}
		throw new std::range_error("y outside the range of samples");
	}

	if(yrange.maximum() < y)
	{
		if(safe)
		{
			if(_monotonicState & Increasing) return samples[(size<<1)-2];
			if(_monotonicState & Decreasing) return samples[0];
		}
		throw new std::range_error("y outside the range of samples");
	}

	unsigned int i = 0;
	auto it = samples;

	double lastX = *(it++);
	double lastY = *(it++);

	for(i = 1; i<size;i++)
	{
		double currentX = *(it++);
		double currentY = *(it++);

		if( (_monotonicState & Increasing && y <= currentY) ||
				(_monotonicState & Decreasing && y >= currentY) )
		{
			double ratio = (y-lastY)/(currentY-lastY);
			return currentX * ratio + lastX * (1 - ratio);
		}

		lastX = currentX;
		lastY = currentY;
	}

	return lastX;
}
