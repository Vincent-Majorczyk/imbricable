/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef OPTIMIZEDDISCRETEFUNCTION_H
#define OPTIMIZEDDISCRETEFUNCTION_H

#include "discretefunction.h"
#include <vector>

namespace Imbricable::Functions {

/**
 * @brief an optimized DiscreteFunction
 *
 * this class aims to improve speed computation with a low level memory management.
 * this class avoids to update the cache memory during the search.
 *
 * # create an OptimizedDiscreteFunction
 *
 * Objects of this class may be created in different way:
 *
 * ```c++
 * // creation  with partial concecutive memory strategy
 * auto optimized = new OptimizedDiscreteFunction(discretefunction);
 * double result = optimized->x(5);
 * detele optimized;
 * ```
 * In this first case, the object contains a pointer to another memory location which contains the array of the discrete function.
 *
 * ```c++
 * // creation with full concecutive memory strategy
 * OptimizedDiscreteFunction *optimized = createForConsecutiveMemoryAllocation(discretefunction);
 * deleteConsecutiveMemoryAllocation(optimized);
 * ```
 *
 * ```c++
 * // creation with full concecutive memory strategy
 * uint size = calculateConsecutiveMemoryAllocationSize(discretefunction);
 * void *address = malloc(size);
 * OptimizedDiscreteFunction *optimized = createForConsecutiveMemoryAllocation(discretefunction, address);
 * deleteConsecutiveMemoryAllocation(optimized);
 * ```
 * In these other cases, the object and the array of the discrete function are adjacent.
 *
 * Nevertheless, there are no speed improvement between these different uses:
 * the aims of the last case is to be integrated in a greater memory strategy where there are lot of successive objects to treat;
 *
 * # Brenchmark: Discrete Function of 50 samples and loop of 1M to get the last item
 *
 * | method                      | samples | execution time (ms) | speed up
 * | :-------------------------- | ------: | ------------------: | -----: /
 * | DiscreteFunction            | 5       | 0.000092            | 1.00 |
 * | OptimizedDiscreteFunction   | 5       | 0.000024            | 4.16 |
 * | DiscreteFunction            | 50      | 0.00083             | 1.00 |
 * | OptimizedDiscreteFunction   | 50      | 0.00017             | 4.88 |
 * | DiscreteFunction            | 500      | 0.0084             | 1.00 |
 * | OptimizedDiscreteFunction   | 500      | 0.0016             | 5.25 |
 */
class OptimizedDiscreteFunction: public AbstractDiscreteFunction
{
	/**
	 * @brief (X,Y) couple values of the discrete function
	 */
	double* _samples = nullptr;
	unsigned int _size;
	MonotonicState _monotonicState;
	Range2DDouble _range;
	bool _isManualMemoryLocation;
public:
	/**
	 * @brief Constructor which copy values of another DiscreteFunction
	 *
	 * This suppose the memory space is already allocated by using of the method `calculateConsecutiveMemoryAllocationSize()`
	 *
	 * @param copy DiscreteFunction to copy
	 * @param memorylocation (optional) the locatation where the sample will be created
	 */
	OptimizedDiscreteFunction(const DiscreteFunction& copy, void* memorylocation = nullptr);

	~OptimizedDiscreteFunction() override;

	/**
	 * @brief calculate required memory size for consecutive location. That means the OptimizedDiscreteFunction is followed by the samples in memory.
	 * @param copy DiscreteFunction used for the creation of the OptimizedDiscreteFunction
	 * @return
	 */
	static unsigned int calculateConsecutiveMemoryAllocationSize(const DiscreteFunction& item);

	/**
	 * @brief create an OptimizedDiscreteFunction from a DiscreteFunction with an optimized memory allocation
	 * @param copy
	 * @param address
	 * @return
	 */
	static OptimizedDiscreteFunction* createForConsecutiveMemoryAllocation(const DiscreteFunction& item, void* address = nullptr);

	static void deleteConsecutiveMemoryAllocation(OptimizedDiscreteFunction*item);

	/**
	 * @brief Return y(x) by linear interpolation between discrete samples in safe mode.
	 * @param x
	 * @return Y
	 * @see y(double x, bool safe)
	 */
	inline double y(double x) const override
	{ return y(x,true); }

	/**
	 * @brief Return y(x) by linear interpolation between discrete samples.
	 * If safe is true, the method return the closer limit when x is outside the range;
	 * else return range exception;
	 * @param x
	 * @param safe avoid exception when x is outside the range;
	 * @return Y
	 * @throw std::length_error if samples is empty;
	 * @throw std::range_error if x is outside the range of function and safe is false;
	 */
	virtual double y(double x, bool safe) const;

	virtual double x(double x, bool safe) const;

	/**
	 * @brief Return the number of samples contained in the discrete function.
	 * @return
	 */
	inline unsigned long count() const override { return _size/2; }

	/**
	 * @brief Return the minimum X of the discrete function, this function assumes that samples exists.
	 * @return
	 */
	inline double minimum() const override { return this->range().x().minimum(); }

	/**
	 * @brief Return the maximum X of the discrete function, this function assumes that samples exists.
	 * @return
	 */
	inline double maximum() const override { return this->range().x().maximum(); }

	inline MonotonicState monotonicState() const override { return _monotonicState; }

	inline Range2DDouble range() const override { return _range; }

	inline double x(double y) const
	{ return x(y,true); }
};

} //namespace

#endif // OPTIMIZEDDISCRETEFUNCTION_H
