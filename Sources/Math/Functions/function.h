/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef FUNCTION_H
#define FUNCTION_H

#include "Core/Object/range2d.h"

namespace Imbricable {

/**
 * @brief relative to functions
 */
namespace Functions {

/**
 * @brief abstract class to manage relation between two variables
 */
class Function
{
public:
	/**
	 * @brief Define state relave to monotonic
	 */
	enum MonotonicState {
		Undefined = 0x0, //!> The function is not evaluable.
		Monotonic = 0x1, //!> The function is monotonic.
		Strictly = 0x2, //!> The function is monotonic strictly increasing or decreasing.
		Increasing = 0x4, //!> The function is increasing in a part of the domain.
		Decreasing = 0x8, //!> The function is decreasing in a part of the domain.
		MonotonicallyIncreasing = Monotonic | Increasing, //!> The function is monotonically increasing.
		MonotonicallyStrictly = Monotonic | Strictly, //!> The function is monotonically strictly increasing or decreasing.
		MonotonicallyStrictlyIncreasing = Monotonic | Strictly | Increasing, //!> The function is monotonically strictly increasing.
		MonotonicallyDecreasing = Monotonic | Decreasing, //!> The function is monotonically decreasing.
		MonotonicallyStrictlyDecreasing = Monotonic | Strictly | Decreasing, //!> The function is monotonically strictly decreasing.
		NotMonotonic = Increasing | Decreasing, //!> The function is not monotonic.
	};

public:
	Function();
	virtual ~Function();

	/**
	 * @brief Return y(x).
	 * @param x
	 * @return Y
	 */
	virtual double y(double x) const = 0;

	/**
	 * @brief return the monotonicState
	 * @return
	 */
	virtual MonotonicState monotonicState() const = 0;

	/**
	 * @brief return the range
	 * @return
	 */
	virtual Range2DDouble range() const = 0;
};

}} //namespace

#endif // ABSTRACTFUNCTION_H
