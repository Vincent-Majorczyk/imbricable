/**
  * @copyright Licence CeCILL-C
  * @date 2019
  * @author Vincent Majorczyk
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
  * @see http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
  */

#ifndef DISCRETEFUNCTION_H
#define DISCRETEFUNCTION_H

#include <QMap>
#include <QVector>
#include <vector>
#include "abstractdiscretefunction.h"

namespace Imbricable::Functions {

/**
 * @brief contains a list of (X,Y) values which may be interpolate.
 *
 * ```c++
 * // create a DiscreteFunction of 5 items
 * fmd = new DiscreteFunction();
 * QMap<double,double> mapmd;
 * mapmd.insert(0,10);
 * mapmd.insert(2,6);
 * mapmd.insert(4,2);
 * mapmd.insert(8,1);
 * mapmd.insert(10,0);
 * fmd->setSamples(mapmd);
 * ```
 */
class DiscreteFunction: public AbstractDiscreteFunction
{
	/**
	 * @brief (X,Y) couple values of the discrete function
	 */
	QMap<double,double> _samples;
	MonotonicState _monotonicState;
	Range2DDouble _range;

public:
	/**
	 * @brief Simple constructor
	 */
	DiscreteFunction();

	/**
	 * @brief Constructor which copy values of another DiscreteFunction
	 * @param copy DiscreteFunction to copy
	 * @param invert if true invert X and Y, the function assumes function is strickly monotonic (increasing or decreasing).
	 */
	DiscreteFunction(const DiscreteFunction& copy, bool invert = false);

	~DiscreteFunction() override;

	/**
	 * @brief access to the map values
	 * @return
	 */
	inline const QMap<double,double>& samples() const { return _samples; }

	/**
	 * @brief access to the map values
	 *
	 * if the samples are modified, use updateRange() and updateMonotonicState() to update properties of the samples
	 *
	 * @return
	 */
	inline QMap<double,double>& samples() { return _samples; }

	/**
	 * @brief set the map values
	 * @return
	 */
	inline void setSamples(const QMap<double,double>& sample)
	{
		_samples = sample;
		updateRange();
		updateMonotonicState();
	}

	/**
	 * @brief Return y(x) by linear interpolation between discrete samples in safe mode.
	 * @param x
	 * @return Y
	 * @see y(double x, bool safe)
	 */
	inline double y(double x) const override
	{ return y(x,true); }

	/**
	 * @brief Return y(x) by linear interpolation between discrete samples.
	 * If safe is true, the method return the closer limit when x is outside the range;
	 * else return range exception;
	 * @param x
	 * @param safe avoid exception when x is outside the range;
	 * @return y
	 * @throw std::length_error if samples is empty;
	 * @throw std::range_error if x is outside the range of function and safe is false;
	 */
	virtual double y(double x, bool safe) const;

	inline double x(double y) const
	{ return x(y,true); }

	/**
	 * @brief Return x(y) by linear interpolation between discrete samples.
	 * The method requires to be used with strictly monotonic function to avoid conflict between candidate values.
	 * In the cas of no strickly monotonic function, only the first candidate is returned
	 * If safe is true, the method return the closer limit when x is outside the range;
	 * else return range exception;
	 * @param y
	 * @param safe avoid exception when x is outside the range;
	 * @return x
	 * @throw std::length_error if samples is empty;
	 * @throw std::range_error if x is outside the range of function and safe is false;
	 */
	virtual double x(double y, bool safe) const;

	/**
	 * @brief Return the number of samples contained in the discrete function.
	 * @return
	 */
	inline unsigned long count() const override { return (unsigned long) _samples.count(); }

	/**
	 * @brief Return the minimum X of the discrete function, this function assumes that samples exists.
	 * @return
	 */
	inline double minimum() const override { return _samples.firstKey(); }

	/**
	 * @brief Return the maximum X of the discrete function, this function assumes that samples exists.
	 * @return
	 */
	inline double maximum() const override { return _samples.lastKey(); }

	/**
	 * @brief get the monotonic state of the discrete function
	 * @return
	 * @throw std::length_error if samples is empty;
	 */
	virtual MonotonicState evaluateMonotonicState() const;

	/**
	 * @brief evaluate the range of the function
	 * @return
	 * @throw std::length_error if samples is empty;
	 */
	virtual Range2DDouble evaluateRange() const;

	/**
	 * @brief update the internal variable MonotonicState
	 *
	 * @see monotonicState()
	 */
	inline void updateMonotonicState() { _monotonicState =  evaluateMonotonicState(); }

	/**
	 * @brief return the monotonicState
	 * @return
	 *
	 * @see updateMonotonicState()
	 */
	inline MonotonicState monotonicState() const override { return _monotonicState; }

	/**
	 * @brief update the internal variable MonotonicState
	 *
	 * @see monotonicState()
	 */
	inline void updateRange() { _range = evaluateRange(); }

	/**
	 * @brief return the range
	 * @return
	 *
	 * @see range()
	 */
	inline Range2DDouble range() const override { return _range; }
};



#if false

/**
 * @brief contains a list of (X,Y) values which may be interpolate.
 */
class DiscreteFunction2: public AbstractDiscreteFunction
{
	/**
	 * @brief (X,Y) couple values of the discrete function
	 */
	QVector<double> _samples;


public:
	/**
	 * @brief Simple constructor
	 */
	DiscreteFunction2();

	/**
	 * @brief Constructor which copy values of another DiscreteFunction
	 * @param copy DiscreteFunction to copy
	 * @param invert if true invert X and Y, the function assumes function is strickly monotonic (increasing or decreasing).
	 */
	DiscreteFunction2(const DiscreteFunction2& copy, bool invert = false);

	~DiscreteFunction2() override;

	/**
	 * @brief access to the map values
	 * @return
	 */
	inline QVector<double>& samples(){ return _samples; }

	/**
	 * @brief Return y(x) by linear interpolation between discrete samples in safe mode.
	 * @param x
	 * @return Y
	 * @see y(double x, bool safe)
	 */
	inline double y(double x) const override
	{ return y(x,true); }

	/**
	 * @brief Return y(x) by linear interpolation between discrete samples.
	 * If safe is true, the method return the closer limit when x is outside the range;
	 * else return range exception;
	 * @param x
	 * @param safe avoid exception when x is outside the range;
	 * @return Y
	 * @throw std::length_error if samples is empty;
	 * @throw std::range_error if x is outside the range of function and safe is false;
	 */
	virtual double y(double x, bool safe) const;

	/**
	 * @brief Return the number of samples contained in the discrete function.
	 * @return
	 */
	inline unsigned long count() const override { return (unsigned long) _samples.count(); }

	/**
	 * @brief Return the minimum X of the discrete function, this function assumes that samples exists.
	 * @return
	 */
	inline double minimum() const override { return _samples.first(); }

	/**
	 * @brief Return the maximum X of the discrete function, this function assumes that samples exists.
	 * @return
	 */
	inline double maximum() const override { return _samples[_samples.count()-2]; }

	/**
	 * @brief get the monotonic state of the discrete function
	 * @return
	 * @throw std::length_error if samples is empty;
	 */
	MonotonicState monotonicState() const override;
};

#endif

} //namespace

#endif // DISCRETEFUNCTION_H
