# Libraries {#Libraries}

# Copyright
- Copyright: Licence CeCILL-C ([fr](http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html)),([en](http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html))
- Date: 2019
- Author: Vincent Majorczyk
